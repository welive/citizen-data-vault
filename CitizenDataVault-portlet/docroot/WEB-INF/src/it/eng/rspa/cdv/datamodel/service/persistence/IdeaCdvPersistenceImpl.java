/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException;
import it.eng.rspa.cdv.datamodel.model.IdeaCdv;
import it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvImpl;
import it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the idea cdv service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see IdeaCdvPersistence
 * @see IdeaCdvUtil
 * @generated
 */
public class IdeaCdvPersistenceImpl extends BasePersistenceImpl<IdeaCdv>
	implements IdeaCdvPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link IdeaCdvUtil} to access the idea cdv persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = IdeaCdvImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvModelImpl.FINDER_CACHE_ENABLED, IdeaCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvModelImpl.FINDER_CACHE_ENABLED, IdeaCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NEED = new FinderPath(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvModelImpl.FINDER_CACHE_ENABLED, IdeaCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByneed",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEED = new FinderPath(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvModelImpl.FINDER_CACHE_ENABLED, IdeaCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByneed",
			new String[] { Boolean.class.getName() },
			IdeaCdvModelImpl.NEED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NEED = new FinderPath(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByneed",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the idea cdvs where need = &#63;.
	 *
	 * @param need the need
	 * @return the matching idea cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaCdv> findByneed(boolean need) throws SystemException {
		return findByneed(need, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the idea cdvs where need = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param need the need
	 * @param start the lower bound of the range of idea cdvs
	 * @param end the upper bound of the range of idea cdvs (not inclusive)
	 * @return the range of matching idea cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaCdv> findByneed(boolean need, int start, int end)
		throws SystemException {
		return findByneed(need, start, end, null);
	}

	/**
	 * Returns an ordered range of all the idea cdvs where need = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param need the need
	 * @param start the lower bound of the range of idea cdvs
	 * @param end the upper bound of the range of idea cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching idea cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaCdv> findByneed(boolean need, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEED;
			finderArgs = new Object[] { need };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NEED;
			finderArgs = new Object[] { need, start, end, orderByComparator };
		}

		List<IdeaCdv> list = (List<IdeaCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (IdeaCdv ideaCdv : list) {
				if ((need != ideaCdv.getNeed())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_IDEACDV_WHERE);

			query.append(_FINDER_COLUMN_NEED_NEED_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(IdeaCdvModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(need);

				if (!pagination) {
					list = (List<IdeaCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<IdeaCdv>(list);
				}
				else {
					list = (List<IdeaCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first idea cdv in the ordered set where need = &#63;.
	 *
	 * @param need the need
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching idea cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a matching idea cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv findByneed_First(boolean need,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaCdvException, SystemException {
		IdeaCdv ideaCdv = fetchByneed_First(need, orderByComparator);

		if (ideaCdv != null) {
			return ideaCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("need=");
		msg.append(need);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIdeaCdvException(msg.toString());
	}

	/**
	 * Returns the first idea cdv in the ordered set where need = &#63;.
	 *
	 * @param need the need
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching idea cdv, or <code>null</code> if a matching idea cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv fetchByneed_First(boolean need,
		OrderByComparator orderByComparator) throws SystemException {
		List<IdeaCdv> list = findByneed(need, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last idea cdv in the ordered set where need = &#63;.
	 *
	 * @param need the need
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching idea cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a matching idea cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv findByneed_Last(boolean need,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaCdvException, SystemException {
		IdeaCdv ideaCdv = fetchByneed_Last(need, orderByComparator);

		if (ideaCdv != null) {
			return ideaCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("need=");
		msg.append(need);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIdeaCdvException(msg.toString());
	}

	/**
	 * Returns the last idea cdv in the ordered set where need = &#63;.
	 *
	 * @param need the need
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching idea cdv, or <code>null</code> if a matching idea cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv fetchByneed_Last(boolean need,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByneed(need);

		if (count == 0) {
			return null;
		}

		List<IdeaCdv> list = findByneed(need, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the idea cdvs before and after the current idea cdv in the ordered set where need = &#63;.
	 *
	 * @param ideaId the primary key of the current idea cdv
	 * @param need the need
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next idea cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv[] findByneed_PrevAndNext(long ideaId, boolean need,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaCdvException, SystemException {
		IdeaCdv ideaCdv = findByPrimaryKey(ideaId);

		Session session = null;

		try {
			session = openSession();

			IdeaCdv[] array = new IdeaCdvImpl[3];

			array[0] = getByneed_PrevAndNext(session, ideaCdv, need,
					orderByComparator, true);

			array[1] = ideaCdv;

			array[2] = getByneed_PrevAndNext(session, ideaCdv, need,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected IdeaCdv getByneed_PrevAndNext(Session session, IdeaCdv ideaCdv,
		boolean need, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_IDEACDV_WHERE);

		query.append(_FINDER_COLUMN_NEED_NEED_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(IdeaCdvModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(need);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ideaCdv);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<IdeaCdv> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the idea cdvs where need = &#63; from the database.
	 *
	 * @param need the need
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByneed(boolean need) throws SystemException {
		for (IdeaCdv ideaCdv : findByneed(need, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(ideaCdv);
		}
	}

	/**
	 * Returns the number of idea cdvs where need = &#63;.
	 *
	 * @param need the need
	 * @return the number of matching idea cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByneed(boolean need) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NEED;

		Object[] finderArgs = new Object[] { need };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_IDEACDV_WHERE);

			query.append(_FINDER_COLUMN_NEED_NEED_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(need);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NEED_NEED_2 = "ideaCdv.need = ?";

	public IdeaCdvPersistenceImpl() {
		setModelClass(IdeaCdv.class);
	}

	/**
	 * Caches the idea cdv in the entity cache if it is enabled.
	 *
	 * @param ideaCdv the idea cdv
	 */
	@Override
	public void cacheResult(IdeaCdv ideaCdv) {
		EntityCacheUtil.putResult(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvImpl.class, ideaCdv.getPrimaryKey(), ideaCdv);

		ideaCdv.resetOriginalValues();
	}

	/**
	 * Caches the idea cdvs in the entity cache if it is enabled.
	 *
	 * @param ideaCdvs the idea cdvs
	 */
	@Override
	public void cacheResult(List<IdeaCdv> ideaCdvs) {
		for (IdeaCdv ideaCdv : ideaCdvs) {
			if (EntityCacheUtil.getResult(
						IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
						IdeaCdvImpl.class, ideaCdv.getPrimaryKey()) == null) {
				cacheResult(ideaCdv);
			}
			else {
				ideaCdv.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all idea cdvs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(IdeaCdvImpl.class.getName());
		}

		EntityCacheUtil.clearCache(IdeaCdvImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the idea cdv.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(IdeaCdv ideaCdv) {
		EntityCacheUtil.removeResult(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvImpl.class, ideaCdv.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<IdeaCdv> ideaCdvs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (IdeaCdv ideaCdv : ideaCdvs) {
			EntityCacheUtil.removeResult(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
				IdeaCdvImpl.class, ideaCdv.getPrimaryKey());
		}
	}

	/**
	 * Creates a new idea cdv with the primary key. Does not add the idea cdv to the database.
	 *
	 * @param ideaId the primary key for the new idea cdv
	 * @return the new idea cdv
	 */
	@Override
	public IdeaCdv create(long ideaId) {
		IdeaCdv ideaCdv = new IdeaCdvImpl();

		ideaCdv.setNew(true);
		ideaCdv.setPrimaryKey(ideaId);

		return ideaCdv;
	}

	/**
	 * Removes the idea cdv with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ideaId the primary key of the idea cdv
	 * @return the idea cdv that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv remove(long ideaId)
		throws NoSuchIdeaCdvException, SystemException {
		return remove((Serializable)ideaId);
	}

	/**
	 * Removes the idea cdv with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the idea cdv
	 * @return the idea cdv that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv remove(Serializable primaryKey)
		throws NoSuchIdeaCdvException, SystemException {
		Session session = null;

		try {
			session = openSession();

			IdeaCdv ideaCdv = (IdeaCdv)session.get(IdeaCdvImpl.class, primaryKey);

			if (ideaCdv == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchIdeaCdvException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ideaCdv);
		}
		catch (NoSuchIdeaCdvException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected IdeaCdv removeImpl(IdeaCdv ideaCdv) throws SystemException {
		ideaCdv = toUnwrappedModel(ideaCdv);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ideaCdv)) {
				ideaCdv = (IdeaCdv)session.get(IdeaCdvImpl.class,
						ideaCdv.getPrimaryKeyObj());
			}

			if (ideaCdv != null) {
				session.delete(ideaCdv);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ideaCdv != null) {
			clearCache(ideaCdv);
		}

		return ideaCdv;
	}

	@Override
	public IdeaCdv updateImpl(it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv)
		throws SystemException {
		ideaCdv = toUnwrappedModel(ideaCdv);

		boolean isNew = ideaCdv.isNew();

		IdeaCdvModelImpl ideaCdvModelImpl = (IdeaCdvModelImpl)ideaCdv;

		Session session = null;

		try {
			session = openSession();

			if (ideaCdv.isNew()) {
				session.save(ideaCdv);

				ideaCdv.setNew(false);
			}
			else {
				session.merge(ideaCdv);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !IdeaCdvModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((ideaCdvModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEED.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { ideaCdvModelImpl.getOriginalNeed() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NEED, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEED,
					args);

				args = new Object[] { ideaCdvModelImpl.getNeed() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NEED, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEED,
					args);
			}
		}

		EntityCacheUtil.putResult(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
			IdeaCdvImpl.class, ideaCdv.getPrimaryKey(), ideaCdv);

		return ideaCdv;
	}

	protected IdeaCdv toUnwrappedModel(IdeaCdv ideaCdv) {
		if (ideaCdv instanceof IdeaCdvImpl) {
			return ideaCdv;
		}

		IdeaCdvImpl ideaCdvImpl = new IdeaCdvImpl();

		ideaCdvImpl.setNew(ideaCdv.isNew());
		ideaCdvImpl.setPrimaryKey(ideaCdv.getPrimaryKey());

		ideaCdvImpl.setIdeaId(ideaCdv.getIdeaId());
		ideaCdvImpl.setIdeaName(ideaCdv.getIdeaName());
		ideaCdvImpl.setNeed(ideaCdv.isNeed());

		return ideaCdvImpl;
	}

	/**
	 * Returns the idea cdv with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the idea cdv
	 * @return the idea cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv findByPrimaryKey(Serializable primaryKey)
		throws NoSuchIdeaCdvException, SystemException {
		IdeaCdv ideaCdv = fetchByPrimaryKey(primaryKey);

		if (ideaCdv == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchIdeaCdvException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ideaCdv;
	}

	/**
	 * Returns the idea cdv with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException} if it could not be found.
	 *
	 * @param ideaId the primary key of the idea cdv
	 * @return the idea cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv findByPrimaryKey(long ideaId)
		throws NoSuchIdeaCdvException, SystemException {
		return findByPrimaryKey((Serializable)ideaId);
	}

	/**
	 * Returns the idea cdv with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the idea cdv
	 * @return the idea cdv, or <code>null</code> if a idea cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		IdeaCdv ideaCdv = (IdeaCdv)EntityCacheUtil.getResult(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
				IdeaCdvImpl.class, primaryKey);

		if (ideaCdv == _nullIdeaCdv) {
			return null;
		}

		if (ideaCdv == null) {
			Session session = null;

			try {
				session = openSession();

				ideaCdv = (IdeaCdv)session.get(IdeaCdvImpl.class, primaryKey);

				if (ideaCdv != null) {
					cacheResult(ideaCdv);
				}
				else {
					EntityCacheUtil.putResult(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
						IdeaCdvImpl.class, primaryKey, _nullIdeaCdv);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(IdeaCdvModelImpl.ENTITY_CACHE_ENABLED,
					IdeaCdvImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ideaCdv;
	}

	/**
	 * Returns the idea cdv with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ideaId the primary key of the idea cdv
	 * @return the idea cdv, or <code>null</code> if a idea cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaCdv fetchByPrimaryKey(long ideaId) throws SystemException {
		return fetchByPrimaryKey((Serializable)ideaId);
	}

	/**
	 * Returns all the idea cdvs.
	 *
	 * @return the idea cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaCdv> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the idea cdvs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of idea cdvs
	 * @param end the upper bound of the range of idea cdvs (not inclusive)
	 * @return the range of idea cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaCdv> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the idea cdvs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of idea cdvs
	 * @param end the upper bound of the range of idea cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of idea cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaCdv> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<IdeaCdv> list = (List<IdeaCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_IDEACDV);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_IDEACDV;

				if (pagination) {
					sql = sql.concat(IdeaCdvModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<IdeaCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<IdeaCdv>(list);
				}
				else {
					list = (List<IdeaCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the idea cdvs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (IdeaCdv ideaCdv : findAll()) {
			remove(ideaCdv);
		}
	}

	/**
	 * Returns the number of idea cdvs.
	 *
	 * @return the number of idea cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_IDEACDV);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the idea cdv persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.IdeaCdv")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<IdeaCdv>> listenersList = new ArrayList<ModelListener<IdeaCdv>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<IdeaCdv>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(IdeaCdvImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_IDEACDV = "SELECT ideaCdv FROM IdeaCdv ideaCdv";
	private static final String _SQL_SELECT_IDEACDV_WHERE = "SELECT ideaCdv FROM IdeaCdv ideaCdv WHERE ";
	private static final String _SQL_COUNT_IDEACDV = "SELECT COUNT(ideaCdv) FROM IdeaCdv ideaCdv";
	private static final String _SQL_COUNT_IDEACDV_WHERE = "SELECT COUNT(ideaCdv) FROM IdeaCdv ideaCdv WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ideaCdv.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No IdeaCdv exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No IdeaCdv exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(IdeaCdvPersistenceImpl.class);
	private static IdeaCdv _nullIdeaCdv = new IdeaCdvImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<IdeaCdv> toCacheModel() {
				return _nullIdeaCdvCacheModel;
			}
		};

	private static CacheModel<IdeaCdv> _nullIdeaCdvCacheModel = new CacheModel<IdeaCdv>() {
			@Override
			public IdeaCdv toEntityModel() {
				return _nullIdeaCdv;
			}
		};
}