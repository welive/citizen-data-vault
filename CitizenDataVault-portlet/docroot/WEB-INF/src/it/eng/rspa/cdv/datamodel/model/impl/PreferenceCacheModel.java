/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.Preference;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Preference in entity cache.
 *
 * @author Engineering
 * @see Preference
 * @generated
 */
public class PreferenceCacheModel implements CacheModel<Preference>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{preferenceId=");
		sb.append(preferenceId);
		sb.append(", name=");
		sb.append(name);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Preference toEntityModel() {
		PreferenceImpl preferenceImpl = new PreferenceImpl();

		preferenceImpl.setPreferenceId(preferenceId);

		if (name == null) {
			preferenceImpl.setName(StringPool.BLANK);
		}
		else {
			preferenceImpl.setName(name);
		}

		preferenceImpl.resetOriginalValues();

		return preferenceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		preferenceId = objectInput.readLong();
		name = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(preferenceId);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}
	}

	public long preferenceId;
	public String name;
}