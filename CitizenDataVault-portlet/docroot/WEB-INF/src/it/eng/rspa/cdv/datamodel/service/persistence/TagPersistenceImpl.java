/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchTagException;
import it.eng.rspa.cdv.datamodel.model.Tag;
import it.eng.rspa.cdv.datamodel.model.impl.TagImpl;
import it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the tag service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see TagPersistence
 * @see TagUtil
 * @generated
 */
public class TagPersistenceImpl extends BasePersistenceImpl<Tag>
	implements TagPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TagUtil} to access the tag persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TagImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagModelImpl.FINDER_CACHE_ENABLED, TagImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagModelImpl.FINDER_CACHE_ENABLED, TagImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TAGNAME = new FinderPath(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagModelImpl.FINDER_CACHE_ENABLED, TagImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBytagname",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TAGNAME =
		new FinderPath(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagModelImpl.FINDER_CACHE_ENABLED, TagImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBytagname",
			new String[] { String.class.getName() },
			TagModelImpl.TAGNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TAGNAME = new FinderPath(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBytagname",
			new String[] { String.class.getName() });

	/**
	 * Returns all the tags where tagname = &#63;.
	 *
	 * @param tagname the tagname
	 * @return the matching tags
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Tag> findBytagname(String tagname) throws SystemException {
		return findBytagname(tagname, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tags where tagname = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tagname the tagname
	 * @param start the lower bound of the range of tags
	 * @param end the upper bound of the range of tags (not inclusive)
	 * @return the range of matching tags
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Tag> findBytagname(String tagname, int start, int end)
		throws SystemException {
		return findBytagname(tagname, start, end, null);
	}

	/**
	 * Returns an ordered range of all the tags where tagname = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tagname the tagname
	 * @param start the lower bound of the range of tags
	 * @param end the upper bound of the range of tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tags
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Tag> findBytagname(String tagname, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TAGNAME;
			finderArgs = new Object[] { tagname };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TAGNAME;
			finderArgs = new Object[] { tagname, start, end, orderByComparator };
		}

		List<Tag> list = (List<Tag>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Tag tag : list) {
				if (!Validator.equals(tagname, tag.getTagname())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TAG_WHERE);

			boolean bindTagname = false;

			if (tagname == null) {
				query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_1);
			}
			else if (tagname.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_3);
			}
			else {
				bindTagname = true;

				query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TagModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTagname) {
					qPos.add(tagname);
				}

				if (!pagination) {
					list = (List<Tag>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Tag>(list);
				}
				else {
					list = (List<Tag>)QueryUtil.list(q, getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first tag in the ordered set where tagname = &#63;.
	 *
	 * @param tagname the tagname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tag
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a matching tag could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag findBytagname_First(String tagname,
		OrderByComparator orderByComparator)
		throws NoSuchTagException, SystemException {
		Tag tag = fetchBytagname_First(tagname, orderByComparator);

		if (tag != null) {
			return tag;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tagname=");
		msg.append(tagname);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTagException(msg.toString());
	}

	/**
	 * Returns the first tag in the ordered set where tagname = &#63;.
	 *
	 * @param tagname the tagname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tag, or <code>null</code> if a matching tag could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag fetchBytagname_First(String tagname,
		OrderByComparator orderByComparator) throws SystemException {
		List<Tag> list = findBytagname(tagname, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last tag in the ordered set where tagname = &#63;.
	 *
	 * @param tagname the tagname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tag
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a matching tag could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag findBytagname_Last(String tagname,
		OrderByComparator orderByComparator)
		throws NoSuchTagException, SystemException {
		Tag tag = fetchBytagname_Last(tagname, orderByComparator);

		if (tag != null) {
			return tag;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tagname=");
		msg.append(tagname);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTagException(msg.toString());
	}

	/**
	 * Returns the last tag in the ordered set where tagname = &#63;.
	 *
	 * @param tagname the tagname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tag, or <code>null</code> if a matching tag could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag fetchBytagname_Last(String tagname,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBytagname(tagname);

		if (count == 0) {
			return null;
		}

		List<Tag> list = findBytagname(tagname, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the tags before and after the current tag in the ordered set where tagname = &#63;.
	 *
	 * @param tagid the primary key of the current tag
	 * @param tagname the tagname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next tag
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a tag with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag[] findBytagname_PrevAndNext(long tagid, String tagname,
		OrderByComparator orderByComparator)
		throws NoSuchTagException, SystemException {
		Tag tag = findByPrimaryKey(tagid);

		Session session = null;

		try {
			session = openSession();

			Tag[] array = new TagImpl[3];

			array[0] = getBytagname_PrevAndNext(session, tag, tagname,
					orderByComparator, true);

			array[1] = tag;

			array[2] = getBytagname_PrevAndNext(session, tag, tagname,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Tag getBytagname_PrevAndNext(Session session, Tag tag,
		String tagname, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TAG_WHERE);

		boolean bindTagname = false;

		if (tagname == null) {
			query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_1);
		}
		else if (tagname.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_3);
		}
		else {
			bindTagname = true;

			query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TagModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindTagname) {
			qPos.add(tagname);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(tag);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Tag> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the tags where tagname = &#63; from the database.
	 *
	 * @param tagname the tagname
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBytagname(String tagname) throws SystemException {
		for (Tag tag : findBytagname(tagname, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(tag);
		}
	}

	/**
	 * Returns the number of tags where tagname = &#63;.
	 *
	 * @param tagname the tagname
	 * @return the number of matching tags
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBytagname(String tagname) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TAGNAME;

		Object[] finderArgs = new Object[] { tagname };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TAG_WHERE);

			boolean bindTagname = false;

			if (tagname == null) {
				query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_1);
			}
			else if (tagname.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_3);
			}
			else {
				bindTagname = true;

				query.append(_FINDER_COLUMN_TAGNAME_TAGNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTagname) {
					qPos.add(tagname);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TAGNAME_TAGNAME_1 = "tag.tagname IS NULL";
	private static final String _FINDER_COLUMN_TAGNAME_TAGNAME_2 = "tag.tagname = ?";
	private static final String _FINDER_COLUMN_TAGNAME_TAGNAME_3 = "(tag.tagname IS NULL OR tag.tagname = '')";

	public TagPersistenceImpl() {
		setModelClass(Tag.class);
	}

	/**
	 * Caches the tag in the entity cache if it is enabled.
	 *
	 * @param tag the tag
	 */
	@Override
	public void cacheResult(Tag tag) {
		EntityCacheUtil.putResult(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagImpl.class, tag.getPrimaryKey(), tag);

		tag.resetOriginalValues();
	}

	/**
	 * Caches the tags in the entity cache if it is enabled.
	 *
	 * @param tags the tags
	 */
	@Override
	public void cacheResult(List<Tag> tags) {
		for (Tag tag : tags) {
			if (EntityCacheUtil.getResult(TagModelImpl.ENTITY_CACHE_ENABLED,
						TagImpl.class, tag.getPrimaryKey()) == null) {
				cacheResult(tag);
			}
			else {
				tag.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all tags.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TagImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TagImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the tag.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Tag tag) {
		EntityCacheUtil.removeResult(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagImpl.class, tag.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Tag> tags) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Tag tag : tags) {
			EntityCacheUtil.removeResult(TagModelImpl.ENTITY_CACHE_ENABLED,
				TagImpl.class, tag.getPrimaryKey());
		}
	}

	/**
	 * Creates a new tag with the primary key. Does not add the tag to the database.
	 *
	 * @param tagid the primary key for the new tag
	 * @return the new tag
	 */
	@Override
	public Tag create(long tagid) {
		Tag tag = new TagImpl();

		tag.setNew(true);
		tag.setPrimaryKey(tagid);

		return tag;
	}

	/**
	 * Removes the tag with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param tagid the primary key of the tag
	 * @return the tag that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a tag with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag remove(long tagid) throws NoSuchTagException, SystemException {
		return remove((Serializable)tagid);
	}

	/**
	 * Removes the tag with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the tag
	 * @return the tag that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a tag with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag remove(Serializable primaryKey)
		throws NoSuchTagException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Tag tag = (Tag)session.get(TagImpl.class, primaryKey);

			if (tag == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTagException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(tag);
		}
		catch (NoSuchTagException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Tag removeImpl(Tag tag) throws SystemException {
		tag = toUnwrappedModel(tag);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(tag)) {
				tag = (Tag)session.get(TagImpl.class, tag.getPrimaryKeyObj());
			}

			if (tag != null) {
				session.delete(tag);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (tag != null) {
			clearCache(tag);
		}

		return tag;
	}

	@Override
	public Tag updateImpl(it.eng.rspa.cdv.datamodel.model.Tag tag)
		throws SystemException {
		tag = toUnwrappedModel(tag);

		boolean isNew = tag.isNew();

		TagModelImpl tagModelImpl = (TagModelImpl)tag;

		Session session = null;

		try {
			session = openSession();

			if (tag.isNew()) {
				session.save(tag);

				tag.setNew(false);
			}
			else {
				session.merge(tag);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TagModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((tagModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TAGNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { tagModelImpl.getOriginalTagname() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TAGNAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TAGNAME,
					args);

				args = new Object[] { tagModelImpl.getTagname() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TAGNAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TAGNAME,
					args);
			}
		}

		EntityCacheUtil.putResult(TagModelImpl.ENTITY_CACHE_ENABLED,
			TagImpl.class, tag.getPrimaryKey(), tag);

		return tag;
	}

	protected Tag toUnwrappedModel(Tag tag) {
		if (tag instanceof TagImpl) {
			return tag;
		}

		TagImpl tagImpl = new TagImpl();

		tagImpl.setNew(tag.isNew());
		tagImpl.setPrimaryKey(tag.getPrimaryKey());

		tagImpl.setTagid(tag.getTagid());
		tagImpl.setTagname(tag.getTagname());

		return tagImpl;
	}

	/**
	 * Returns the tag with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the tag
	 * @return the tag
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a tag with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTagException, SystemException {
		Tag tag = fetchByPrimaryKey(primaryKey);

		if (tag == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTagException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return tag;
	}

	/**
	 * Returns the tag with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchTagException} if it could not be found.
	 *
	 * @param tagid the primary key of the tag
	 * @return the tag
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a tag with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag findByPrimaryKey(long tagid)
		throws NoSuchTagException, SystemException {
		return findByPrimaryKey((Serializable)tagid);
	}

	/**
	 * Returns the tag with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the tag
	 * @return the tag, or <code>null</code> if a tag with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Tag tag = (Tag)EntityCacheUtil.getResult(TagModelImpl.ENTITY_CACHE_ENABLED,
				TagImpl.class, primaryKey);

		if (tag == _nullTag) {
			return null;
		}

		if (tag == null) {
			Session session = null;

			try {
				session = openSession();

				tag = (Tag)session.get(TagImpl.class, primaryKey);

				if (tag != null) {
					cacheResult(tag);
				}
				else {
					EntityCacheUtil.putResult(TagModelImpl.ENTITY_CACHE_ENABLED,
						TagImpl.class, primaryKey, _nullTag);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(TagModelImpl.ENTITY_CACHE_ENABLED,
					TagImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return tag;
	}

	/**
	 * Returns the tag with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param tagid the primary key of the tag
	 * @return the tag, or <code>null</code> if a tag with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tag fetchByPrimaryKey(long tagid) throws SystemException {
		return fetchByPrimaryKey((Serializable)tagid);
	}

	/**
	 * Returns all the tags.
	 *
	 * @return the tags
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Tag> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tags
	 * @param end the upper bound of the range of tags (not inclusive)
	 * @return the range of tags
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Tag> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tags
	 * @param end the upper bound of the range of tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tags
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Tag> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Tag> list = (List<Tag>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TAG);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TAG;

				if (pagination) {
					sql = sql.concat(TagModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Tag>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Tag>(list);
				}
				else {
					list = (List<Tag>)QueryUtil.list(q, getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the tags from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Tag tag : findAll()) {
			remove(tag);
		}
	}

	/**
	 * Returns the number of tags.
	 *
	 * @return the number of tags
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TAG);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the tag persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.Tag")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Tag>> listenersList = new ArrayList<ModelListener<Tag>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Tag>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TagImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_TAG = "SELECT tag FROM Tag tag";
	private static final String _SQL_SELECT_TAG_WHERE = "SELECT tag FROM Tag tag WHERE ";
	private static final String _SQL_COUNT_TAG = "SELECT COUNT(tag) FROM Tag tag";
	private static final String _SQL_COUNT_TAG_WHERE = "SELECT COUNT(tag) FROM Tag tag WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "tag.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Tag exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Tag exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TagPersistenceImpl.class);
	private static Tag _nullTag = new TagImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Tag> toCacheModel() {
				return _nullTagCacheModel;
			}
		};

	private static CacheModel<Tag> _nullTagCacheModel = new CacheModel<Tag>() {
			@Override
			public Tag toEntityModel() {
				return _nullTag;
			}
		};
}