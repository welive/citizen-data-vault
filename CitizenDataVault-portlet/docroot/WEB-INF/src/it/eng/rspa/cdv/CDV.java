package it.eng.rspa.cdv;

import it.eng.rspa.cdv.utils.RestApiUtils;
import it.eng.rspa.cdv.utils.model.PushModel;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class CDV
 */
public class CDV extends MVCPortlet {

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		super.serveResource(resourceRequest, resourceResponse);
		
		String  data=resourceRequest.getParameter("data");			
		
		PushModel pm = new Gson().fromJson(data, PushModel.class);
		try{
			RestApiUtils.pushEventHandler(pm);
		}
		catch(Exception e){
			JSONObject out = JSONFactoryUtil.createJSONObject();
			out.put("error", e.getMessage());
			resourceResponse.getWriter().write(out.toString());
			
			System.out.println(out.toString());
		}	
		

		
		JSONObject out = JSONFactoryUtil.createJSONObject();
		out.put("error", 0);
		resourceResponse.getWriter().write(out.toString());
		
		
	}
	
	

}
