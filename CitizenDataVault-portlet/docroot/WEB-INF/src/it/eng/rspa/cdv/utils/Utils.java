package it.eng.rspa.cdv.utils;

import it.eng.rspa.cdv.datamodel.model.Application;
import it.eng.rspa.cdv.datamodel.model.Endorsement;
import it.eng.rspa.cdv.datamodel.model.IdeaCdv;
import it.eng.rspa.cdv.datamodel.model.Language;
import it.eng.rspa.cdv.datamodel.model.Preference;
import it.eng.rspa.cdv.datamodel.model.SkillCdv;
import it.eng.rspa.cdv.datamodel.model.UsedApplication;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import it.eng.rspa.cdv.datamodel.model.UserIdea;
import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.model.UserPreference;
import it.eng.rspa.cdv.datamodel.model.UserSkill;
import it.eng.rspa.cdv.datamodel.service.ApplicationLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.IdeaCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.LanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.PreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.SkillCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UsedApplicationLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserIdeaLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserLanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserPreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserSkillLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK;
import it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK;
import it.eng.rspa.cdv.utils.exception.CustomException;
import it.eng.rspa.cdv.utils.exception.InternalErrorException;
import it.eng.rspa.cdv.utils.exception.WrongCcuseridException;
import it.eng.rspa.cdv.utils.model.Location;
import it.eng.rspa.cdv.utils.model.OutApp;
import it.eng.rspa.cdv.utils.model.OutSkill;
import it.eng.rspa.cdv.utils.model.ProfileData;
import it.eng.rspa.cdv.utils.model.UserIdeaRole;
import it.eng.rspa.cdv.utils.model.UserProfile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portal.util.comparator.UserLoginDateComparator;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;

public abstract class Utils {
	
	public static long getGenericPortalCompanyId() {
		long companyId = 0;

		//Ottengo utente default Liferay
		 String webId = new String("liferay.com");
		 Company company = null;
		try {
			company = CompanyLocalServiceUtil.getCompanyByWebId(webId);
			
			 companyId = company.getCompanyId();
			
		} catch (PortalException | SystemException e) {
            e.printStackTrace();
			 return companyId;
		}
		
		return companyId;
	}
	
	public static Long getUserCcUserId(User u){
		try{ return (Long)u.getExpandoBridge().getAttribute("CCUserID"); }
		catch(Exception e){ return -1L; }
	}

	
	/**
	 * @param skillId
	 * @param userId
	 * @param endorserUserId
	 * @return
	 */
	public static boolean isAlreadyEndorsed (long skillId, long userId ,long endorserUserId){
		
		if (userId==endorserUserId)
			return true;
		
		EndorsementPK ePK = new EndorsementPK();
		
		ePK.setSkillId(skillId);
		ePK.setUserId(userId);
		ePK.setEndorserId(endorserUserId);
		

		Endorsement endors = null;
		try {
			endors = EndorsementLocalServiceUtil.fetchEndorsement(ePK);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		
		if (Validator.isNotNull(endors))
			return true;
		
		return false;
		
		
	}
	
	/**
	 * @param skillId
	 * @param userId
	 * @param endorserUserId
	 * @return
	 */
	public static boolean isEndorsementRemovible (long skillId, long userId ,long endorserUserId){
		
		if (userId == endorserUserId )
			return true;
		
		boolean  isAlreadyEndorsed = isAlreadyEndorsed(skillId, userId, endorserUserId);
		
		if (isAlreadyEndorsed)
			return true;
		
		return false;
	}
	
	/**
	 * @param skillId
	 * @param userId
	 * @return
	 */
	public static boolean hasSelfEndorsement (long skillId, long userId){
		
		EndorsementPK ePK = new EndorsementPK();
		
		ePK.setSkillId(skillId);
		ePK.setUserId(userId);
		ePK.setEndorserId(userId);
		
		Endorsement endorsement = null;
		
		try {
			 endorsement = EndorsementLocalServiceUtil.fetchEndorsement(ePK);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		if (Validator.isNotNull(endorsement))
			return true;
		 
		 return false;
		 
		
	}
	
	
	
	
	public static Long getUserIdByCcUserId(String ccUserId) throws WrongCcuseridException{
		
		try{
			long companyId = getGenericPortalCompanyId();
			Long userId = null;
			long columnId = ExpandoColumnLocalServiceUtil.getColumn(companyId, User.class.getName(), "CUSTOM_FIELDS", "CCUserID").getColumnId();
			List<ExpandoValue> values = ExpandoValueLocalServiceUtil.getColumnValues(columnId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			for(ExpandoValue v:values){
				if(v.getData().equals(ccUserId)){
					userId = v.getClassPK();
					return userId;
				}
			}
		}
		catch(Exception e){
			throw new WrongCcuseridException("No user associated with the ccUserId "+ccUserId);
		}
			
		throw new WrongCcuseridException("No user associated with the ccUserId "+ccUserId);
	}
	
	public static User getUserByCcUserId(long ccUserId) throws WrongCcuseridException{
		
		try{
			long userid = getUserIdByCcUserId(String.valueOf(ccUserId));
			return UserLocalServiceUtil.getUser(userid);
		}
		catch(Exception e){
			throw new WrongCcuseridException("No user associated with the ccUserId "+ccUserId);
		}
	}
	
	public static UserCdv getUserCdvByCcUserId(long ccUserId) throws WrongCcuseridException{
		
		try{
			long userid = getUserIdByCcUserId(String.valueOf(ccUserId));
			UserCdvPK upk = new UserCdvPK();
			upk.setCcuid(ccUserId);
			upk.setUserId(userid);
			return UserCdvLocalServiceUtil.getUserCdv(upk);
		}
		catch(Exception e){
			throw new WrongCcuseridException("No user associated with the ccUserId "+ccUserId);
		}
	}
	
	public static boolean hasRole(User u, String rolename){
		try {
			if(Validator.isNotNull(u) && Validator.isNotNull(rolename)){
				Role dtoRole = RoleLocalServiceUtil.getRole(getGenericPortalCompanyId(), rolename);
				return u.getRoles().contains(dtoRole);
			}
		}
		catch(Exception e){ /* The specified role doesn't exists */ }
		
		return false;
		
	}
	
	public static List<UserIdea> getUserAuthorships(long userid){
		return UserIdeaLocalServiceUtil.getUserIdeaByUserIdAndRole(userid, UserIdeaRole.Author.toString());
	}
	
	public static List<UserIdea> getUserCollaborations(long userid){
		return UserIdeaLocalServiceUtil.getUserIdeaByUserIdAndRole(userid, UserIdeaRole.Collaborator.toString());
	}
	
	public static List<UsedApplication> getUsedApplications(long userid){
		return UsedApplicationLocalServiceUtil.getUsedApplicationByUserId(userid);
	}
	
	public static List<UserSkill> getUserSkills(long userid){
		return UserSkillLocalServiceUtil.getUserSkillByUserId(userid);
	}
	
	public static List<UserPreference> getUserPreferences(long userid){
		return UserPreferenceLocalServiceUtil.getUserPreferenceByUserid(userid);
	}
	
	public static Map<Long, String> getAuthorshipsMap(long userid){
		List<UserIdea> autorships = Utils.getUserAuthorships(userid);
		Map<Long, String> authorshipsMap = new HashMap<Long, String>();
		
		for(UserIdea ui : autorships){
			try{
				IdeaCdv i = IdeaCdvLocalServiceUtil.getIdeaCdv(ui.getIdeaId());
				authorshipsMap.put(i.getIdeaId(), i.getIdeaName());
			}
			catch(Exception e){ /* Skip this idea */ }
		}
		
		return authorshipsMap;
	}
	
	public static Map<Long, String> getCollaboratiosMap(long userid){
		List<UserIdea> collaborations = Utils.getUserCollaborations(userid);
		Map<Long, String> collaborationsMap = new HashMap<Long, String>();
		
		for(UserIdea ui : collaborations){
			try{
				IdeaCdv i = IdeaCdvLocalServiceUtil.getIdeaCdv(ui.getIdeaId());
				collaborationsMap.put(i.getIdeaId(), i.getIdeaName());
			}
			catch(Exception e){ /* Skip this idea */ }
		}
		
		return collaborationsMap;
	}

	public static Map<Long, String> getAppsMap(long userid) {
		List<UsedApplication> apps = Utils.getUsedApplications(userid);
		Map<Long, String> appsMap = new HashMap<Long, String>();
		
		for(UsedApplication app : apps){
			try{
				Application a = ApplicationLocalServiceUtil.getApplication(app.getAppId());
				appsMap.put(a.getAppId(), a.getAppName());
			}
			catch(Exception e){ /* Skip this app */ }
		}
		
		return appsMap;
	}

	public static Map<SkillCdv, Long> getSkillsMap(long userid) {
		List<UserSkill> skills = Utils.getUserSkills(userid);
		Map<SkillCdv, Long> skillsMap = new HashMap<SkillCdv, Long>();
		
		for(UserSkill skill : skills){
			try{
				SkillCdv s = SkillCdvLocalServiceUtil.getSkillCdv(skill.getSkillId());
				skillsMap.put(s, skill.getEndorsmentCounter());
			}
			catch(Exception e){ /* Skip this skill */ }
		}
		
		Map<SkillCdv, Long> skillsMapSorted = sortByValue(skillsMap);
		
		
		return skillsMapSorted;
	}

	
	//Order a Map by value
	public static <K, V extends Comparable<? super V>> Map<K, V>  sortByValue( Map<K, V> map ){
	    List<Map.Entry<K, V>> list =new LinkedList<Map.Entry<K, V>>( map.entrySet() );
	    Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
	        public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 ){
	            return (o2.getValue()).compareTo( o1.getValue() );
	        }
	    } );
	
	    Map<K, V> result = new LinkedHashMap<K, V>();
	    for (Map.Entry<K, V> entry : list)
	    {
	        result.put( entry.getKey(), entry.getValue() );
	    }
	    return result;
	}
	
	
	public static Map<Long, String> getPreferencesMap(long userid) {
		List<UserPreference> prefs = Utils.getUserPreferences(userid);
		Map<Long, String> prefsMap = new HashMap<Long, String>();
		
		for(UserPreference pref : prefs){
			try{
				Preference p = PreferenceLocalServiceUtil.getPreference(pref.getPreferenceId());
				prefsMap.put(p.getPreferenceId(), p.getName());
			}
			catch(Exception e){ /* Skip this pref */ }
		}
		
		return prefsMap;
	}
	
	public static Set<String> getLanguages(long userid){
		Set<String> out = new HashSet<String>();
		List<UserLanguage> ulangs = UserLanguageLocalServiceUtil.getUserLanguagesByUserId(userid);
		for(UserLanguage ul : ulangs){
			try{
				Language l = LanguageLocalServiceUtil.getLanguage(ul.getLanguageId());
				out.add(l.getName());
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		
		return out;
	}
	
	public static Set<String> getAvailableLanguages() {
		Set<String> out = new HashSet<String>();
		try{ 
			List<Language> langs = LanguageLocalServiceUtil.getLanguages(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			for(Language l:langs){
				try{ out.add(l.getName()); }
				catch(Exception e){ e.printStackTrace(); }
			}
		}
		catch(Exception e){
			/* Do nothing */
		}
		
		return out;
	}
	
	public static JSONObject getUserProfile(String uid){
		
		JSONObject response = JSONFactoryUtil.createJSONObject();
		try{
			UserCdv ucdv = null;
			long ccuid = Long.parseLong(uid);
			try{ ucdv = Utils.getUserCdvByCcUserId(ccuid); }
			catch(CustomException ce){
				response.put("message", ce.getMessageNumber());
				response.put("response", 1);
				response.put("text", ce.getMessage());
				
				return response;
			}
			
			UserProfile profile = new UserProfile();
			profile.setCcUserID(ccuid);
			profile.setName(ucdv.getName());
			profile.setSurname(ucdv.getSurname());
			profile.setGender(ucdv.getGender());
			if(ucdv.getBirthdate() != null){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
				profile.setBirthdate(sdf.format(ucdv.getBirthdate()));
			}
			profile.setAddress(ucdv.getAddress());
			profile.setCity(ucdv.getCity());
			profile.setCountry(ucdv.getCountry());
			profile.setZipCode(ucdv.getZipcode());
			profile.setIsDeveloper(ucdv.getIsDeveloper());

			long authorship = 0L;
			try{ 
				List<UserIdea>  uis = UserIdeaLocalServiceUtil.getUserIdeaByUserIdAndRole(ucdv.getUserId(), UserIdeaRole.Author.toString());
				if(Validator.isNotNull(uis))
					authorship = uis.size();
			}
			catch(Exception e){ authorship = 0L; };
			
			long collaborations = 0L;
			try{ 
				List<UserIdea>  uis = UserIdeaLocalServiceUtil.getUserIdeaByUserIdAndRole(ucdv.getUserId(), UserIdeaRole.Collaborator.toString()); 
				if(Validator.isNotNull(uis))
					collaborations = uis.size();
			}
			catch(Exception e){ collaborations = 0L; };
			
			ProfileData pd = new ProfileData();
			pd.setNumCreatedIdeas(authorship);
			pd.setNumCollaborationsInIdeas(collaborations);
			pd.setReputationScore(ucdv.getReputation());
			
			profile.setProfileData(pd);
			
			//Retrieve languages
			try{
				List<UserLanguage> uls = UserLanguageLocalServiceUtil.getUserLanguagesByUserId(ucdv.getUserId());
				for(UserLanguage ul : uls){
					try{
						String langname = LanguageLocalServiceUtil.fetchLanguage(ul.getLanguageId()).getName();
						profile.getLanguages().add(langname);
					}
					catch(Exception e){
						/* Skip this language */
					}
				}
			}
			catch(Exception e){
				/* Skip the skill section */
			}
			
			profile.setLastKnownLocation(new Location(String.valueOf(ucdv.getLastKnownLatitude()), String.valueOf(ucdv.getLastKnownLongitude())));
			profile.setReferredPilot(ucdv.getPilot());
			profile.setEmail(ucdv.getEmail());
			
			//Retrieve skills
			try{
				List<UserSkill> uss = UserSkillLocalServiceUtil.getUserSkillByUserId(ucdv.getUserId());
				for(UserSkill us : uss){
					try{
						OutSkill os = new OutSkill();
						os.setEndorsementCounter(us.getEndorsmentCounter());
						os.setSkillName(SkillCdvLocalServiceUtil.getSkillCdv(us.getSkillId()).getSkillname());
						
						profile.getSkills().add(os);
					}
					catch(Exception e){
						/* Skip this skill */
					}
				}
			}
			catch(Exception e){
				/* Skip the skill section */
			}
			
			//Retrieve preferences
			try{
				List<UserPreference> ups = UserPreferenceLocalServiceUtil.getUserPreferenceByUserid(ucdv.getUserId());
				for(UserPreference up : ups){
					try{
						String prefname = PreferenceLocalServiceUtil.getPreference(up.getPreferenceId()).getName();
						profile.getUserTags().add(prefname);
					}
					catch(Exception e){
						e.printStackTrace();
						/* Skip this preference */
					}
				}
			}
			catch(Exception e){
				/* Skip the usedApps section */
			}
			
			//Retrieve usedApps
			try{
				List<UsedApplication> uas = UsedApplicationLocalServiceUtil.getUsedApplicationByUserId(ucdv.getUserId());
				for(UsedApplication ua : uas){
					try{
						OutApp oa = new OutApp();
						oa.setAppID(ua.getAppId());
						oa.setAppName(ApplicationLocalServiceUtil.getApplication(ua.getAppId()).getAppName());
						
						profile.getUsedApps().add(oa);
					}
					catch(Exception e){
						/* Skip this app */
					}
				}
			}
			catch(Exception e){
				/* Skip the usedApps section */
			}
			
			Gson gson = new Gson();
			String sJson = gson.toJson(profile);
			try{ response = JSONFactoryUtil.createJSONObject(sJson); }
			catch(Exception e){
				throw new InternalErrorException(e.getMessage());
			}
		}
		catch(CustomException ce){
			response = JSONFactoryUtil.createJSONObject();
			response.put("message", ce.getMessageNumber());
			response.put("response", 1);
			response.put("text", ce.getMessage());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	/**
	 * @param currentUserId
	 * @param cdvUserId
	 * @return
	 */
	public static boolean isFriend (long currentUserId, long cdvUserId){
		
		boolean isFriend = false;

		try {
			int friendsCount = UserLocalServiceUtil.getSocialUsersCount(currentUserId);
			List<User> users = UserLocalServiceUtil.getSocialUsers(currentUserId, 0, friendsCount, new UserLoginDateComparator());
			
			for (User u : users){
				
				if (u.getUserId() == cdvUserId)
					return true;
			}
			
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		
		
		return isFriend;
		
	}
	
	/**
	 * @param currentUserId
	 * @param cdvUserId
	 * @return
	 */
	public static boolean isDifferentUser (long currentUserId, long cdvUserId){
		
		boolean isDifferent = false;

		if (currentUserId!=cdvUserId)
			isDifferent = true;
		
		
		return isDifferent;
		
	}
	
	
	

public static String getOrganization(long userId, Boolean isCompany) {
		
		String org = null;
		try{
			List<Organization> p_orgs = UserLocalServiceUtil.getUser(userId).getOrganizations();
			List<Organization> orgs = new ArrayList<Organization>();
			if(isCompany != null){
				for(Organization o : p_orgs){
					try{
						if(isCompany == Boolean.parseBoolean(o.getExpandoBridge().getAttribute("isCompany").toString())){
							orgs.add(o);
						}
					}
					catch(Exception e){ System.out.println(e.getMessage()); }
				}
			}
			else{ orgs.addAll(p_orgs); }
			
			if(orgs!=null && !orgs.isEmpty()){
				Organization dtoOrg = orgs.get(0);
				org = dtoOrg.getName();
			}
		}
		catch(Exception e){ e.printStackTrace(); }
		
		return org;
	}


public static String getProfileUrl(long userId){

	//http://localhost:8080/web/nome.utente/so/profile

	User user=null;
	try {
		user = UserLocalServiceUtil.getUser(userId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
	}
	String userURL = getRootUrl()+"/web/"+user.getScreenName()+"/so/profile";
	
	
	return userURL;
	
}

/**
 * @return
 */
public static String getRootUrl() {
	
	String rootUrl = ""; 

	//Ottengo utente default Liferay
	 String webId = new String("liferay.com");
	 Company company = null;
	try {
		company = CompanyLocalServiceUtil.getCompanyByWebId(webId);
		 rootUrl = PortalUtil.getPortalURL(company.getVirtualHostname(), PortalUtil.getPortalPort(), false);
		
	} catch (PortalException | SystemException e) {
        e.printStackTrace();
		 return rootUrl;
	}
	
	
	return rootUrl;


}

}
