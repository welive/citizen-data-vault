package it.eng.rspa.cdv.utils.model;

public class ProfileData {

	private Long numCreatedIdeas;
	private Long numCollaborationsInIdeas;
	private Long reputationScore;
	
	public ProfileData(){
		numCreatedIdeas = 0L;
		numCollaborationsInIdeas = 0L;
		reputationScore = 0L;
	}
	
	public Long getNumCreatedIdeas() {
		return numCreatedIdeas;
	}
	public void setNumCreatedIdeas(Long numCreatedIdeas) {
		this.numCreatedIdeas = numCreatedIdeas;
	}
	public Long getNumCollaborationsInIdeas() {
		return numCollaborationsInIdeas;
	}
	public void setNumCollaborationsInIdeas(Long numCollaborationsInIdeas) {
		this.numCollaborationsInIdeas = numCollaborationsInIdeas;
	}
	public Long getReputationScore() {
		return reputationScore;
	}
	public void setReputationScore(Long reputationScore) {
		this.reputationScore = reputationScore;
	}
	
	
	
}
