package it.eng.rspa.cdv.utils.model;

import it.eng.rspa.cdv.utils.exception.CustomException;
import it.eng.rspa.cdv.utils.exception.InvalidInputException;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;

public class AddUserBean extends GenericUserBean{
	
	private String referredPilot;
	private String firstName;
	private String lastName;
	private String email;
	private Boolean isDeveloper;
	private CustomRoleBean role;
	private CustomEmployementBean employement;
	private String liferayScreenName;
	private Long id;
	
	protected AddUserBean(){
		super();
	}
	
	public String getReferredPilot() {
		return referredPilot;
	}


	public String getFirstName() {
		return firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public String getEmail() {
		return email;
	}


	public Boolean getIsDeveloper() {
		return isDeveloper;
	}


	public CustomRoleBean getRole() {
		return role;
	}

	public CustomEmployementBean getEmployement() {
		return employement;
	}

	public String getLiferayScreenName() {
		return liferayScreenName;
	}
	
	public Long getId(){
		return id;
	}
	
	public String toString(){
		String out = this.getClass().getSimpleName();
		Field[] fields = this.getClass().getDeclaredFields();
		for(Field f:fields){
			try{
				out += "\n\t"+f.getName()+" = "+f.get(this);
			}
			catch(Exception e){ 
				out += "\n\t"+f.getName()+" = "+null;
				System.out.println("["+e.getClass().getSimpleName()+"] "+e.getMessage());
			}
		}
		return out;
	}

	@Override
	public void check(JSONObject jin) 
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CustomException {

		System.out.println("Checking "+jin);
		
		if(Validator.isNull(jin)){
			throw new InvalidInputException("The provided input is not valid");
		}
		
		try{
			if(jin.has("ccUserID"))
				jin.getLong("ccUserID");
			else
				throw new Exception("Missing mandatory field ccUserID");
			
			if(jin.has("id"))
				jin.getLong("id");
			else
				throw new Exception("Missing mandatory field id");
			
			if(jin.has("isMale")){
				String ismale = jin.getString("isMale");
				if(!ismale.equalsIgnoreCase("true") && !ismale.equalsIgnoreCase("false"))
					throw new Exception("Value for field 'isMale' is not valid.");
			}
			
			if(jin.has("referredPilot"))
				jin.getString("referredPilot");
			else
				throw new Exception("Missing mandatory field referredPilot");
			
			if(jin.has("firstName"))
				jin.getString("firstName");
			else
				throw new Exception("Missing mandatory field firstName");
			
			if(jin.has("lastName"))
				jin.getString("lastName");
			else
				throw new Exception("Missing mandatory field lastName");
			
			if(jin.has("email"))
				jin.getString("email");
			else
				throw new Exception("Missing mandatory field email");
			
			if(jin.has("isDeveloper")){
				String ismale = jin.getString("isDeveloper");
				if(!ismale.equalsIgnoreCase("true") && !ismale.equalsIgnoreCase("false"))
					throw new Exception("Value for field 'isDeveloper' is not valid.");
			}
			else
				throw new Exception("Missing mandatory field isDeveloper");
			
			CustomRoleBean.valueOf(jin.getString("role"));
			
			if(jin.has("employement") && !jin.getString("employement").equalsIgnoreCase(""))
				CustomEmployementBean.valueOf(jin.getString("employement"));
			
			if(jin.has("liferayScreenName"))
				jin.getString("liferayScreenName");
			else
				throw new Exception("Missing mandatory field liferayScreenName");
			
			if(jin.has("birthDate")){
				JSONObject date = jin.getJSONObject("birthDate");
				int day = date.getInt("day");
				int month = date.getInt("month")-1;
				int year = date.getInt("year");
				
				Calendar d = GregorianCalendar.getInstance();
				try{
					d.setLenient(false);
					d.clear();
					d.set(year, month, day);
					
					d.getTime();
				}
				catch(Exception e){
					throw new Exception("Invalid birthDate: "+e.getMessage());
				}
				
				Calendar now = GregorianCalendar.getInstance();
				if(now.compareTo(d) < 0)
					throw new Exception("Future birthdate are not allowed");
			}
			
			if(jin.has("address"))
				jin.getString("address");
			
			if(jin.has("city"))
				jin.getString("city");
			
			if(jin.has("country"))
				jin.getString("country");
			
			if(jin.has("zipCode"))
				jin.getString("zipCode");
			
			if(jin.has("languages")){
				JSONArray larr = jin.getJSONArray("languages");
				for(int i=0; i<larr.length(); i++){
					larr.getString(i);
				}
			}
			
			if(jin.has("userTags")){
				JSONArray tarr = jin.getJSONArray("userTags");
				for(int i=0; i<tarr.length(); i++){
					tarr.getString(i);
				}
			}
			
		}
		catch(Exception e){
			throw new InvalidInputException(e.getMessage());
		}
		
	}
	
}
