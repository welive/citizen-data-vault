package it.eng.rspa.cdv.utils.exception;

public abstract class CustomException extends Exception{

	private static final long serialVersionUID = -5398085213847195022L;
	
	private long messageNumber;
	
	public CustomException(String message){
		super(message);
	}
	
	public Long getMessageNumber(){
		return this.messageNumber;
	};
	
	protected void setMessageNumber(Long l){
		this.messageNumber = l;
	}
	
}
