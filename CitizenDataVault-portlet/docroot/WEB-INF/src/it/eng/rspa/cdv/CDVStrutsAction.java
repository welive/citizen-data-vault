package it.eng.rspa.cdv;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.eng.rspa.cdv.datamodel.model.Language;
import it.eng.rspa.cdv.datamodel.model.Preference;
import it.eng.rspa.cdv.datamodel.model.SkillCdv;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.model.UserPreference;
import it.eng.rspa.cdv.datamodel.model.UserSkill;
import it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserLanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserPreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserSkillLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK;
import it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK;
import it.eng.rspa.cdv.utils.PlatformUserSynchronizer;
import it.eng.rspa.cdv.utils.RestApiUtils;
import it.eng.rspa.cdv.utils.Utils;
import it.eng.rspa.cdv.utils.exception.InternalErrorException;
import it.eng.rspa.service.apilumLocalServiceUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.struts.BaseStrutsPortletAction;
import com.liferay.portal.kernel.struts.StrutsPortletAction;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.portlet.PortletProps;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class CDVStrutsAction extends BaseStrutsPortletAction {
	
	private static boolean platformSync = Boolean.parseBoolean(PortletProps.get("platform.synchronization.active"));
	
	@Override
	public void processAction(StrutsPortletAction originalStrutsPortletAction,
			PortletConfig portletConfig, ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		
		String cdv_custom_action = actionRequest.getParameter("cdv_custom_action");
		
		if("delete_account".equals(cdv_custom_action)){
			String deleteall = actionRequest.getParameter("deleteall_field");
			boolean deleteAllbinary = Boolean.parseBoolean(deleteall);
			
			System.out.println("Delete account Action!!");
			long userId = PortalUtil.getUserId(actionRequest);
			boolean itsOk = apilumLocalServiceUtil.doRightToBeForgotten(userId, deleteAllbinary, actionRequest, actionResponse);
			
			System.out.println("Esito: "+itsOk);
		}
		
		super.processAction(originalStrutsPortletAction, portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void serveResource(StrutsPortletAction originalStrutsPortletAction,
			PortletConfig portletConfig, ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws Exception {
		
		String action = resourceRequest.getParameter("action");
		
		if("downloadprofile".equalsIgnoreCase(action)){
			
			String uid = resourceRequest.getParameter("uid");
			JSONObject response = Utils.getUserProfile(uid);
			resourceResponse.getWriter().write(response.toString());
		}
		else if("revokeauth".equalsIgnoreCase(action)){
			
			String ccuid = resourceRequest.getParameter("ccuid");
			String clientid = resourceRequest.getParameter("clientid");
			
			Client client = Client.create();
			String user = PortletProps.get("remoteService.serviceUser");
			String pwd = PortletProps.get("remoteService.servicePass");
			
			client.addFilter(new HTTPBasicAuthFilter(user, pwd));
			WebResource webResource = client.resource(PortletProps.get("aac.url")+"/eauth/revoke/"+clientid+"/"+ccuid);
			ClientResponse resp = webResource.delete(ClientResponse.class);
			
			if(resp.getStatus()!=200){
				throw new Exception("Received response code "+resp.getStatus());
			}
			
		}
		else if("getauth".equalsIgnoreCase(action)){
			
			String ccuid = resourceRequest.getParameter("ccuid");
			
			Client client = Client.create();
			String user = PortletProps.get("remoteService.serviceUser");
			String pwd = PortletProps.get("remoteService.servicePass");
			
			client.addFilter(new HTTPBasicAuthFilter(user, pwd));
			WebResource webResource = client.resource(PortletProps.get("aac.url")+"/resources/clientinfo/oauth/"+ccuid);
			ClientResponse resp = webResource.get(ClientResponse.class);
			
			if(resp.getStatus()!=200){
				throw new Exception("Received response code "+resp.getStatus());
			}
			
			resourceResponse.getWriter().write(resp.getEntity(String.class));
		}
		else if("updateuser".equalsIgnoreCase(action)){
			
			try{
				
				String ccuid = resourceRequest.getParameter("ccuid");
				String sUid = resourceRequest.getParameter("uid");
				Long uid = Long.parseLong(sUid);
				UserCdv ucdv = UserCdvLocalServiceUtil.getUserCdvByPK(Long.parseLong(ccuid), uid);
				
				String name = resourceRequest.getParameter("name");
				if(name!=null){
					if(name.isEmpty()){
						throw new Exception("invalid-name");
					}
					
					if(platformSync){
						PlatformUserSynchronizer.setName(uid, name);
					}
					
					ucdv.setName(name);
				}
				
				String surname = resourceRequest.getParameter("surname");
				if(surname!=null){
					if(platformSync){
						PlatformUserSynchronizer.setSurname(uid, surname);
					}
					
					ucdv.setSurname(surname);
				}
				
				String birthdate = resourceRequest.getParameter("birthdate");
				if(birthdate!=null){
					Date dd = null;
					
					if(!birthdate.isEmpty()){
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						dd = sdf.parse(birthdate);
						Calendar d = GregorianCalendar.getInstance();
						d.clear();
						d.setTimeInMillis(dd.getTime());
						
						Calendar now = GregorianCalendar.getInstance();
						if(now.compareTo(d) < 0)
							throw new Exception("future-birthdate");
					}
					if(platformSync){
						PlatformUserSynchronizer.setBirthdate(uid, dd);
					}
					ucdv.setBirthdate(dd);
					
				}
				
				String gender = resourceRequest.getParameter("gender");
				if(gender!=null){
					ucdv.setGender(gender.toUpperCase());
				}
				
				String sIsdeveloper = resourceRequest.getParameter("isdeveloper");
				if(sIsdeveloper!=null){
					boolean isDeveloper = Boolean.parseBoolean(sIsdeveloper);
					
					PlatformUserSynchronizer.isDeveloper(uid, isDeveloper);
					
					ucdv.setIsDeveloper(isDeveloper);
				}
				
				
				String[] languages = resourceRequest.getParameterValues("languages[]");
				if(languages!=null){
					
					//Remove all previous languages
					List<UserLanguage> uls = UserLanguageLocalServiceUtil.getUserLanguagesByUserId(uid);
					if(uls!=null){
						for(UserLanguage ul : uls){
							UserLanguageLocalServiceUtil.deleteUserLanguage(ul);
						}
					}
					
					//Add all current languages
					for(String lang:languages){
						
						String currlang = lang.trim().toLowerCase();
						Language dtoLang = RestApiUtils.getLanguage(currlang);
						
						RestApiUtils.associateUserLanguage(ucdv, dtoLang);
					}
				}
				
				String[] userTags = resourceRequest.getParameterValues("userTags[]");
				if(userTags!=null){
					
					//Remove all previous preferences
					List<UserPreference> ups = UserPreferenceLocalServiceUtil.getUserPreferenceByUserid(uid);
					if(ups!=null){
						for(UserPreference up : ups){
							UserPreferenceLocalServiceUtil.deleteUserPreference(up);
						}
					}
					
					//Add all current preferences
					for(String pref:userTags){
						String currpref = pref.trim().toLowerCase();
						Preference dtoPref = RestApiUtils.getPreference(currpref);
						
						RestApiUtils.associateUserPreference(ucdv, dtoPref);
					}
					
				}
				
				String newskill = resourceRequest.getParameter("newskill");
				if(newskill!=null){
					String skillname = newskill.trim().toLowerCase();
					SkillCdv dtoSkill = RestApiUtils.getSkill(skillname);
					
					UserSkillPK uspk = new UserSkillPK();
					uspk.setSkillId(dtoSkill.getSkillId());
					uspk.setUserId(uid);
					UserSkill us = null;
					try{ us = UserSkillLocalServiceUtil.getUserSkill(uspk); }
					catch(Exception e){ /* The variable remains NULL */ }
					
					if(Validator.isNull(us)){
						RestApiUtils.associateUserSkill(ucdv, dtoSkill, ucdv.getUserId());
					}
					else{
						Long endrsmnt = 0L;
						us.setEndorsmentCounter(endrsmnt);
						try{ UserSkillLocalServiceUtil.updateUserSkill(us); }
						catch(Exception e){
							throw new InternalErrorException(e.getMessage());
						}
					}
				}
				
				String removedskill = resourceRequest.getParameter("removedskill");
				if(removedskill!=null){
					String skillname = removedskill.trim().toLowerCase();
					SkillCdv dtoSkill = RestApiUtils.getSkill(skillname);
					
					if(!Validator.isNull(dtoSkill)){
						UserSkillPK uspk = new UserSkillPK();
						uspk.setSkillId(dtoSkill.getSkillId());
						uspk.setUserId(uid);
						
						UserSkill us = null;
						try{ us = UserSkillLocalServiceUtil.getUserSkill(uspk); }
						catch(Exception e){
							//The user does not have that skill
						}
						
						if(!Validator.isNull(us)){
							try{ UserSkillLocalServiceUtil.deleteUserSkill(us); }
							catch(Exception e){
								throw new InternalErrorException("Unable to delete the association between User and Skill.");
							}
						}
						EndorsementLocalServiceUtil.deleteAllEndorsementsBySkillAndUser(dtoSkill.getSkillId(), uid);
					}
				}
				
				String address = resourceRequest.getParameter("address");
				if(address!=null){
					ucdv.setAddress(address);
				}
				
				String zipCode = resourceRequest.getParameter("zipCode");
				if(zipCode!=null){
					ucdv.setZipcode(zipCode);
				}
				
				String city = resourceRequest.getParameter("city");
				if(city!=null){
					ucdv.setCity(city);
				}
				
				String country = resourceRequest.getParameter("country");
				if(country!=null){
					ucdv.setCountry(country);
				}
				
				UserCdvLocalServiceUtil.updateUserCdv(ucdv);
				
				JSONObject out = JSONFactoryUtil.createJSONObject();
				out.put("error", 0);
				resourceResponse.getWriter().write(out.toString());
			}
			catch(Exception e){
				JSONObject out = JSONFactoryUtil.createJSONObject();
				out.put("error", e.getMessage());
				resourceResponse.getWriter().write(out.toString());
				
				System.out.println(out.toString());
			}
		}
		
		super.serveResource(originalStrutsPortletAction, portletConfig, resourceRequest, resourceResponse);
		
	}

	@Override
	public String render(StrutsPortletAction originalStrutsPortletAction, 
						PortletConfig portletConfig, 
						RenderRequest renderRequest, 
						RenderResponse renderResponse) 
			throws Exception {

		/*	Se � presente il parametro p_u_i_d l'utente Admin sta accedendo al dettaglio utente dal pannello di controllo
		 *  Se il parametro p_u_i_d non � presente nella richiesta allora l'utente sta accedendo alla propria pagina My Account
		 */
		
		//Logged User
		User currUser = PortalUtil.getUser(renderRequest);
		long currUserID = currUser.getUserId();
		long currCcUserID = Long.parseLong(currUser.getExpandoBridge().getAttribute("CCUserID").toString());
		
		//Selected User
		User selUser = currUser;
		long selUserID = currUserID;
		long selCcUserID = currCcUserID;
		
		if(renderRequest.getParameterMap().containsKey("p_u_i_d")){
			selUserID = Long.parseLong(renderRequest.getParameter("p_u_i_d"));
			selUser = UserLocalServiceUtil.getUser(selUserID);
			selCcUserID = Long.parseLong(selUser.getExpandoBridge().getAttribute("CCUserID").toString());
		}
		
		renderRequest.setAttribute("selUser", selUser);	
		renderRequest.setAttribute("authority", Utils.hasRole(selUser, PortletProps.get("role.authority")));
		renderRequest.setAttribute("citizen", Utils.hasRole(selUser, PortletProps.get("role.citizen")));
		
		if(Utils.hasRole(selUser, PortletProps.get("role.citizen")) 
				|| Utils.hasRole(selUser, PortletProps.get("role.academy")) 
				|| Utils.hasRole(selUser, PortletProps.get("role.business")) 
				|| Utils.hasRole(selUser, PortletProps.get("role.entrepreneur"))
				|| Utils.hasRole(selUser, PortletProps.get("role.pmi"))){
			
			UserCdvPK upk = new UserCdvPK();
					upk.setCcuid(selCcUserID);
					upk.setUserId(selUserID);
					
			UserCdv ucdv = UserCdvLocalServiceUtil.getUserCdv(upk);
			setAttributes(renderRequest, ucdv);
			
			//Idea Authorship
			Map<Long, String> authorshipsMap = Utils.getAuthorshipsMap(ucdv.getUserId());
			renderRequest.setAttribute("authorships", authorshipsMap);
			
			//Idea collaborations
			Map<Long, String> collaborationsMap = Utils.getCollaboratiosMap(ucdv.getUserId());
			renderRequest.setAttribute("collaborations", collaborationsMap);
			
			//Used Applications
			Map<Long, String> appsMap = Utils.getAppsMap(ucdv.getUserId());
			renderRequest.setAttribute("apps", appsMap);
			
			//Skills
			Map<SkillCdv, Long> skillsMap = Utils.getSkillsMap(ucdv.getUserId());
			renderRequest.setAttribute("skills", skillsMap);
			
			//Languages
			Set<String> langs = Utils.getLanguages(ucdv.getUserId());
			renderRequest.setAttribute("langs", langs);
			
			//Available languages
			Set<String> availableLanguages = Utils.getAvailableLanguages();
			renderRequest.setAttribute("availablelangs", availableLanguages);
			
			//Preferences
			Map<Long,String> preferencesMap = Utils.getPreferencesMap(ucdv.getUserId());
			renderRequest.setAttribute("preferences", preferencesMap);
			
			//Preferences
			String organization = Utils.getOrganization(ucdv.getUserId(), true);
			renderRequest.setAttribute("organization", organization);
			
			renderRequest.setAttribute("business", Utils.hasRole(selUser, PortletProps.get("role.business")));
			renderRequest.setAttribute("developer", Utils.hasRole(selUser, PortletProps.get("role.developer")));
			renderRequest.setAttribute("academy", Utils.hasRole(selUser, PortletProps.get("role.academy")));
			renderRequest.setAttribute("entrepreneur", Utils.hasRole(selUser, PortletProps.get("role.entrepreneur")));
			renderRequest.setAttribute("isLeader", Utils.hasRole(selUser, PortletProps.get("role.companyleader")));
		}
		
		return originalStrutsPortletAction.render(portletConfig, renderRequest, renderResponse);
	}

	private void setAttributes(RenderRequest renderRequest, UserCdv ucdv) {
		
		//User identifiers
		renderRequest.setAttribute("ccuid", ucdv.getCcuid());
		renderRequest.setAttribute("userid", ucdv.getUserId());
		
		//User main data
		renderRequest.setAttribute("username", ucdv.getUsername());
		renderRequest.setAttribute("name", ucdv.getName());
		renderRequest.setAttribute("surname", ucdv.getSurname());
		renderRequest.setAttribute("email", ucdv.getEmail());
		
		//Address
		renderRequest.setAttribute("address", ucdv.getAddress());
		renderRequest.setAttribute("zipcode", ucdv.getZipcode());
		renderRequest.setAttribute("city", ucdv.getCity());
		renderRequest.setAttribute("country", ucdv.getCountry());
		
		//Other metadata
		renderRequest.setAttribute("birthdate", ucdv.getBirthdate());
		renderRequest.setAttribute("gender", ucdv.getGender().toLowerCase());
		
		renderRequest.setAttribute("isDeveloper", ucdv.getIsDeveloper());
		renderRequest.setAttribute("pilot", ucdv.getPilot());
		renderRequest.setAttribute("reputation", ucdv.getReputation());
		
		renderRequest.setAttribute("lklat", ucdv.getLastKnownLatitude());
		renderRequest.setAttribute("lklng", ucdv.getLastKnownLongitude());
		
		return;
	}
}
