package it.eng.rspa.cdv.utils.exception;

public class UserExistsException extends CustomException{

	private static final long serialVersionUID = 2L;
	
	public UserExistsException(String message){
		super(message);
		setMessageNumber(1L);
	}
	
}
