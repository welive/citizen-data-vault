/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import it.eng.rspa.cdv.datamodel.model.Endorsement;
import it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.base.EndorsementLocalServiceBaseImpl;
import it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK;
import it.eng.rspa.cdv.datamodel.service.persistence.EndorsementUtil;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;

/**
 * The implementation of the endorsement local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.cdv.datamodel.service.EndorsementLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author SIRCHIA ANTONINO
 * @see it.eng.rspa.cdv.datamodel.service.base.EndorsementLocalServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil
 */
public class EndorsementLocalServiceImpl extends EndorsementLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil} to access the endorsement local service.
	 */
	
	
	
	/**
	 * @param skillId
	 * @param userId
	 * @return
	 */
	public List<Endorsement> getEndorsementsBySkillIdAndUserId (long skillId, long userId){
		
		List<Endorsement> allendors = new ArrayList<Endorsement>();
		
		try {
			allendors = EndorsementUtil.findByskillIDAndUserID(skillId, userId);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return allendors;
		
	}
	
	
	/**
	 * @param skillId
	 * @param userId
	 * @return
	 */
	public List<Endorsement> getEndorsementsBySkillIdAndUserIdNoSelf (long skillId, long userId){
		
		List<Endorsement> allendors = new ArrayList<Endorsement>();
		List<Endorsement> allendorsNoSelf = new ArrayList<Endorsement>();
		
		try {
			allendors = EndorsementUtil.findByskillIDAndUserID(skillId, userId);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		for (Endorsement endorse:allendors){
			
			if (endorse.getUserId() != endorse.getEndorserId())
				allendorsNoSelf.add(endorse);
		}
		
		return allendorsNoSelf;
		
	}
	
	
	
	
	/**
	 * @param userId
	 * @return
	 */
	public List<Endorsement> getEndorsementsByUserId (long userId){
		
		List<Endorsement> allendors = new ArrayList<Endorsement>();
		
		try {
			allendors = EndorsementUtil.findByuserId(userId);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return allendors;
		
	}
	
/**
 * @param skillId
 * @param userId
*/
public void deleteAllEndorsementsBySkillAndUser(long skillId, long userId){
		
		List<Endorsement> endorses = EndorsementLocalServiceUtil.getEndorsementsBySkillIdAndUserId(skillId, userId);	
		
		for (Endorsement endorse:endorses){
			try {
				EndorsementLocalServiceUtil.deleteEndorsement(endorse);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
}
	
/**
 * @param skillId
 * @param userId
 * @param endorserId
 */
public void deleteEndorsementByPKs(long skillId, long userId, long endorserId){
	
	EndorsementPK ePK= new EndorsementPK();
	ePK.setEndorserId(endorserId);
	ePK.setSkillId(skillId);
	ePK.setUserId(userId);
	
	try {
		EndorsementLocalServiceUtil.deleteEndorsement(ePK);
	} catch (PortalException | SystemException e) {
		e.printStackTrace();
	}

	
}
	
}