/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException;
import it.eng.rspa.cdv.datamodel.model.CustomDataEntry;
import it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryImpl;
import it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the custom data entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see CustomDataEntryPersistence
 * @see CustomDataEntryUtil
 * @generated
 */
public class CustomDataEntryPersistenceImpl extends BasePersistenceImpl<CustomDataEntry>
	implements CustomDataEntryPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CustomDataEntryUtil} to access the custom data entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CustomDataEntryImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_KEY = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBykey",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEY = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBykey",
			new String[] { String.class.getName() },
			CustomDataEntryModelImpl.KEY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_KEY = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBykey",
			new String[] { String.class.getName() });

	/**
	 * Returns all the custom data entries where key = &#63;.
	 *
	 * @param key the key
	 * @return the matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findBykey(String key)
		throws SystemException {
		return findBykey(key, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the custom data entries where key = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param key the key
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @return the range of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findBykey(String key, int start, int end)
		throws SystemException {
		return findBykey(key, start, end, null);
	}

	/**
	 * Returns an ordered range of all the custom data entries where key = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param key the key
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findBykey(String key, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEY;
			finderArgs = new Object[] { key };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_KEY;
			finderArgs = new Object[] { key, start, end, orderByComparator };
		}

		List<CustomDataEntry> list = (List<CustomDataEntry>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CustomDataEntry customDataEntry : list) {
				if (!Validator.equals(key, customDataEntry.getKey())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CUSTOMDATAENTRY_WHERE);

			boolean bindKey = false;

			if (key == null) {
				query.append(_FINDER_COLUMN_KEY_KEY_1);
			}
			else if (key.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_KEY_KEY_3);
			}
			else {
				bindKey = true;

				query.append(_FINDER_COLUMN_KEY_KEY_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CustomDataEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindKey) {
					qPos.add(key);
				}

				if (!pagination) {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CustomDataEntry>(list);
				}
				else {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first custom data entry in the ordered set where key = &#63;.
	 *
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findBykey_First(String key,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchBykey_First(key,
				orderByComparator);

		if (customDataEntry != null) {
			return customDataEntry;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("key=");
		msg.append(key);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCustomDataEntryException(msg.toString());
	}

	/**
	 * Returns the first custom data entry in the ordered set where key = &#63;.
	 *
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchBykey_First(String key,
		OrderByComparator orderByComparator) throws SystemException {
		List<CustomDataEntry> list = findBykey(key, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last custom data entry in the ordered set where key = &#63;.
	 *
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findBykey_Last(String key,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchBykey_Last(key, orderByComparator);

		if (customDataEntry != null) {
			return customDataEntry;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("key=");
		msg.append(key);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCustomDataEntryException(msg.toString());
	}

	/**
	 * Returns the last custom data entry in the ordered set where key = &#63;.
	 *
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchBykey_Last(String key,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBykey(key);

		if (count == 0) {
			return null;
		}

		List<CustomDataEntry> list = findBykey(key, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the custom data entries before and after the current custom data entry in the ordered set where key = &#63;.
	 *
	 * @param entryId the primary key of the current custom data entry
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry[] findBykey_PrevAndNext(long entryId, String key,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = findByPrimaryKey(entryId);

		Session session = null;

		try {
			session = openSession();

			CustomDataEntry[] array = new CustomDataEntryImpl[3];

			array[0] = getBykey_PrevAndNext(session, customDataEntry, key,
					orderByComparator, true);

			array[1] = customDataEntry;

			array[2] = getBykey_PrevAndNext(session, customDataEntry, key,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CustomDataEntry getBykey_PrevAndNext(Session session,
		CustomDataEntry customDataEntry, String key,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CUSTOMDATAENTRY_WHERE);

		boolean bindKey = false;

		if (key == null) {
			query.append(_FINDER_COLUMN_KEY_KEY_1);
		}
		else if (key.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_KEY_KEY_3);
		}
		else {
			bindKey = true;

			query.append(_FINDER_COLUMN_KEY_KEY_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CustomDataEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindKey) {
			qPos.add(key);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(customDataEntry);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CustomDataEntry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the custom data entries where key = &#63; from the database.
	 *
	 * @param key the key
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBykey(String key) throws SystemException {
		for (CustomDataEntry customDataEntry : findBykey(key,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(customDataEntry);
		}
	}

	/**
	 * Returns the number of custom data entries where key = &#63;.
	 *
	 * @param key the key
	 * @return the number of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBykey(String key) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_KEY;

		Object[] finderArgs = new Object[] { key };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CUSTOMDATAENTRY_WHERE);

			boolean bindKey = false;

			if (key == null) {
				query.append(_FINDER_COLUMN_KEY_KEY_1);
			}
			else if (key.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_KEY_KEY_3);
			}
			else {
				bindKey = true;

				query.append(_FINDER_COLUMN_KEY_KEY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindKey) {
					qPos.add(key);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_KEY_KEY_1 = "customDataEntry.key IS NULL";
	private static final String _FINDER_COLUMN_KEY_KEY_2 = "customDataEntry.key = ?";
	private static final String _FINDER_COLUMN_KEY_KEY_3 = "(customDataEntry.key IS NULL OR customDataEntry.key = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByuserid",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserid",
			new String[] { Long.class.getName() },
			CustomDataEntryModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserid",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the custom data entries where userid = &#63;.
	 *
	 * @param userid the userid
	 * @return the matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByuserid(long userid)
		throws SystemException {
		return findByuserid(userid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the custom data entries where userid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userid the userid
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @return the range of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByuserid(long userid, int start, int end)
		throws SystemException {
		return findByuserid(userid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the custom data entries where userid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userid the userid
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByuserid(long userid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userid, start, end, orderByComparator };
		}

		List<CustomDataEntry> list = (List<CustomDataEntry>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CustomDataEntry customDataEntry : list) {
				if ((userid != customDataEntry.getUserid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CUSTOMDATAENTRY_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CustomDataEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userid);

				if (!pagination) {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CustomDataEntry>(list);
				}
				else {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first custom data entry in the ordered set where userid = &#63;.
	 *
	 * @param userid the userid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findByuserid_First(long userid,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchByuserid_First(userid,
				orderByComparator);

		if (customDataEntry != null) {
			return customDataEntry;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userid=");
		msg.append(userid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCustomDataEntryException(msg.toString());
	}

	/**
	 * Returns the first custom data entry in the ordered set where userid = &#63;.
	 *
	 * @param userid the userid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchByuserid_First(long userid,
		OrderByComparator orderByComparator) throws SystemException {
		List<CustomDataEntry> list = findByuserid(userid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last custom data entry in the ordered set where userid = &#63;.
	 *
	 * @param userid the userid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findByuserid_Last(long userid,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchByuserid_Last(userid,
				orderByComparator);

		if (customDataEntry != null) {
			return customDataEntry;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userid=");
		msg.append(userid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCustomDataEntryException(msg.toString());
	}

	/**
	 * Returns the last custom data entry in the ordered set where userid = &#63;.
	 *
	 * @param userid the userid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchByuserid_Last(long userid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserid(userid);

		if (count == 0) {
			return null;
		}

		List<CustomDataEntry> list = findByuserid(userid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the custom data entries before and after the current custom data entry in the ordered set where userid = &#63;.
	 *
	 * @param entryId the primary key of the current custom data entry
	 * @param userid the userid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry[] findByuserid_PrevAndNext(long entryId,
		long userid, OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = findByPrimaryKey(entryId);

		Session session = null;

		try {
			session = openSession();

			CustomDataEntry[] array = new CustomDataEntryImpl[3];

			array[0] = getByuserid_PrevAndNext(session, customDataEntry,
					userid, orderByComparator, true);

			array[1] = customDataEntry;

			array[2] = getByuserid_PrevAndNext(session, customDataEntry,
					userid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CustomDataEntry getByuserid_PrevAndNext(Session session,
		CustomDataEntry customDataEntry, long userid,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CUSTOMDATAENTRY_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CustomDataEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userid);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(customDataEntry);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CustomDataEntry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the custom data entries where userid = &#63; from the database.
	 *
	 * @param userid the userid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserid(long userid) throws SystemException {
		for (CustomDataEntry customDataEntry : findByuserid(userid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(customDataEntry);
		}
	}

	/**
	 * Returns the number of custom data entries where userid = &#63;.
	 *
	 * @param userid the userid
	 * @return the number of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserid(long userid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CUSTOMDATAENTRY_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userid);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "customDataEntry.userid = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTID = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByclientid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTID =
		new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByclientid",
			new String[] { String.class.getName() },
			CustomDataEntryModelImpl.CLIENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CLIENTID = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByclientid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the custom data entries where clientid = &#63;.
	 *
	 * @param clientid the clientid
	 * @return the matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByclientid(String clientid)
		throws SystemException {
		return findByclientid(clientid, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the custom data entries where clientid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param clientid the clientid
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @return the range of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByclientid(String clientid, int start,
		int end) throws SystemException {
		return findByclientid(clientid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the custom data entries where clientid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param clientid the clientid
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByclientid(String clientid, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTID;
			finderArgs = new Object[] { clientid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTID;
			finderArgs = new Object[] { clientid, start, end, orderByComparator };
		}

		List<CustomDataEntry> list = (List<CustomDataEntry>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CustomDataEntry customDataEntry : list) {
				if (!Validator.equals(clientid, customDataEntry.getClientid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CUSTOMDATAENTRY_WHERE);

			boolean bindClientid = false;

			if (clientid == null) {
				query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_1);
			}
			else if (clientid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_3);
			}
			else {
				bindClientid = true;

				query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CustomDataEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindClientid) {
					qPos.add(clientid);
				}

				if (!pagination) {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CustomDataEntry>(list);
				}
				else {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first custom data entry in the ordered set where clientid = &#63;.
	 *
	 * @param clientid the clientid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findByclientid_First(String clientid,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchByclientid_First(clientid,
				orderByComparator);

		if (customDataEntry != null) {
			return customDataEntry;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("clientid=");
		msg.append(clientid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCustomDataEntryException(msg.toString());
	}

	/**
	 * Returns the first custom data entry in the ordered set where clientid = &#63;.
	 *
	 * @param clientid the clientid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchByclientid_First(String clientid,
		OrderByComparator orderByComparator) throws SystemException {
		List<CustomDataEntry> list = findByclientid(clientid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last custom data entry in the ordered set where clientid = &#63;.
	 *
	 * @param clientid the clientid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findByclientid_Last(String clientid,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchByclientid_Last(clientid,
				orderByComparator);

		if (customDataEntry != null) {
			return customDataEntry;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("clientid=");
		msg.append(clientid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCustomDataEntryException(msg.toString());
	}

	/**
	 * Returns the last custom data entry in the ordered set where clientid = &#63;.
	 *
	 * @param clientid the clientid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchByclientid_Last(String clientid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByclientid(clientid);

		if (count == 0) {
			return null;
		}

		List<CustomDataEntry> list = findByclientid(clientid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the custom data entries before and after the current custom data entry in the ordered set where clientid = &#63;.
	 *
	 * @param entryId the primary key of the current custom data entry
	 * @param clientid the clientid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry[] findByclientid_PrevAndNext(long entryId,
		String clientid, OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = findByPrimaryKey(entryId);

		Session session = null;

		try {
			session = openSession();

			CustomDataEntry[] array = new CustomDataEntryImpl[3];

			array[0] = getByclientid_PrevAndNext(session, customDataEntry,
					clientid, orderByComparator, true);

			array[1] = customDataEntry;

			array[2] = getByclientid_PrevAndNext(session, customDataEntry,
					clientid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CustomDataEntry getByclientid_PrevAndNext(Session session,
		CustomDataEntry customDataEntry, String clientid,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CUSTOMDATAENTRY_WHERE);

		boolean bindClientid = false;

		if (clientid == null) {
			query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_1);
		}
		else if (clientid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_3);
		}
		else {
			bindClientid = true;

			query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CustomDataEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindClientid) {
			qPos.add(clientid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(customDataEntry);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CustomDataEntry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the custom data entries where clientid = &#63; from the database.
	 *
	 * @param clientid the clientid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByclientid(String clientid) throws SystemException {
		for (CustomDataEntry customDataEntry : findByclientid(clientid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(customDataEntry);
		}
	}

	/**
	 * Returns the number of custom data entries where clientid = &#63;.
	 *
	 * @param clientid the clientid
	 * @return the number of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByclientid(String clientid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CLIENTID;

		Object[] finderArgs = new Object[] { clientid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CUSTOMDATAENTRY_WHERE);

			boolean bindClientid = false;

			if (clientid == null) {
				query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_1);
			}
			else if (clientid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_3);
			}
			else {
				bindClientid = true;

				query.append(_FINDER_COLUMN_CLIENTID_CLIENTID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindClientid) {
					qPos.add(clientid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CLIENTID_CLIENTID_1 = "customDataEntry.clientid IS NULL";
	private static final String _FINDER_COLUMN_CLIENTID_CLIENTID_2 = "customDataEntry.clientid = ?";
	private static final String _FINDER_COLUMN_CLIENTID_CLIENTID_3 = "(customDataEntry.clientid IS NULL OR customDataEntry.clientid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTKEY =
		new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByclientKey",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTKEY =
		new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED,
			CustomDataEntryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByclientKey",
			new String[] { String.class.getName(), String.class.getName() },
			CustomDataEntryModelImpl.CLIENTID_COLUMN_BITMASK |
			CustomDataEntryModelImpl.KEY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CLIENTKEY = new FinderPath(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByclientKey",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the custom data entries where clientid = &#63; and key = &#63;.
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @return the matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByclientKey(String clientid, String key)
		throws SystemException {
		return findByclientKey(clientid, key, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the custom data entries where clientid = &#63; and key = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @return the range of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByclientKey(String clientid, String key,
		int start, int end) throws SystemException {
		return findByclientKey(clientid, key, start, end, null);
	}

	/**
	 * Returns an ordered range of all the custom data entries where clientid = &#63; and key = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findByclientKey(String clientid, String key,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTKEY;
			finderArgs = new Object[] { clientid, key };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTKEY;
			finderArgs = new Object[] {
					clientid, key,
					
					start, end, orderByComparator
				};
		}

		List<CustomDataEntry> list = (List<CustomDataEntry>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CustomDataEntry customDataEntry : list) {
				if (!Validator.equals(clientid, customDataEntry.getClientid()) ||
						!Validator.equals(key, customDataEntry.getKey())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CUSTOMDATAENTRY_WHERE);

			boolean bindClientid = false;

			if (clientid == null) {
				query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_1);
			}
			else if (clientid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_3);
			}
			else {
				bindClientid = true;

				query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_2);
			}

			boolean bindKey = false;

			if (key == null) {
				query.append(_FINDER_COLUMN_CLIENTKEY_KEY_1);
			}
			else if (key.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTKEY_KEY_3);
			}
			else {
				bindKey = true;

				query.append(_FINDER_COLUMN_CLIENTKEY_KEY_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CustomDataEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindClientid) {
					qPos.add(clientid);
				}

				if (bindKey) {
					qPos.add(key);
				}

				if (!pagination) {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CustomDataEntry>(list);
				}
				else {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findByclientKey_First(String clientid, String key,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchByclientKey_First(clientid, key,
				orderByComparator);

		if (customDataEntry != null) {
			return customDataEntry;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("clientid=");
		msg.append(clientid);

		msg.append(", key=");
		msg.append(key);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCustomDataEntryException(msg.toString());
	}

	/**
	 * Returns the first custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchByclientKey_First(String clientid, String key,
		OrderByComparator orderByComparator) throws SystemException {
		List<CustomDataEntry> list = findByclientKey(clientid, key, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findByclientKey_Last(String clientid, String key,
		OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchByclientKey_Last(clientid, key,
				orderByComparator);

		if (customDataEntry != null) {
			return customDataEntry;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("clientid=");
		msg.append(clientid);

		msg.append(", key=");
		msg.append(key);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCustomDataEntryException(msg.toString());
	}

	/**
	 * Returns the last custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchByclientKey_Last(String clientid, String key,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByclientKey(clientid, key);

		if (count == 0) {
			return null;
		}

		List<CustomDataEntry> list = findByclientKey(clientid, key, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the custom data entries before and after the current custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	 *
	 * @param entryId the primary key of the current custom data entry
	 * @param clientid the clientid
	 * @param key the key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry[] findByclientKey_PrevAndNext(long entryId,
		String clientid, String key, OrderByComparator orderByComparator)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = findByPrimaryKey(entryId);

		Session session = null;

		try {
			session = openSession();

			CustomDataEntry[] array = new CustomDataEntryImpl[3];

			array[0] = getByclientKey_PrevAndNext(session, customDataEntry,
					clientid, key, orderByComparator, true);

			array[1] = customDataEntry;

			array[2] = getByclientKey_PrevAndNext(session, customDataEntry,
					clientid, key, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CustomDataEntry getByclientKey_PrevAndNext(Session session,
		CustomDataEntry customDataEntry, String clientid, String key,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CUSTOMDATAENTRY_WHERE);

		boolean bindClientid = false;

		if (clientid == null) {
			query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_1);
		}
		else if (clientid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_3);
		}
		else {
			bindClientid = true;

			query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_2);
		}

		boolean bindKey = false;

		if (key == null) {
			query.append(_FINDER_COLUMN_CLIENTKEY_KEY_1);
		}
		else if (key.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CLIENTKEY_KEY_3);
		}
		else {
			bindKey = true;

			query.append(_FINDER_COLUMN_CLIENTKEY_KEY_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CustomDataEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindClientid) {
			qPos.add(clientid);
		}

		if (bindKey) {
			qPos.add(key);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(customDataEntry);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CustomDataEntry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the custom data entries where clientid = &#63; and key = &#63; from the database.
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByclientKey(String clientid, String key)
		throws SystemException {
		for (CustomDataEntry customDataEntry : findByclientKey(clientid, key,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(customDataEntry);
		}
	}

	/**
	 * Returns the number of custom data entries where clientid = &#63; and key = &#63;.
	 *
	 * @param clientid the clientid
	 * @param key the key
	 * @return the number of matching custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByclientKey(String clientid, String key)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CLIENTKEY;

		Object[] finderArgs = new Object[] { clientid, key };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CUSTOMDATAENTRY_WHERE);

			boolean bindClientid = false;

			if (clientid == null) {
				query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_1);
			}
			else if (clientid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_3);
			}
			else {
				bindClientid = true;

				query.append(_FINDER_COLUMN_CLIENTKEY_CLIENTID_2);
			}

			boolean bindKey = false;

			if (key == null) {
				query.append(_FINDER_COLUMN_CLIENTKEY_KEY_1);
			}
			else if (key.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTKEY_KEY_3);
			}
			else {
				bindKey = true;

				query.append(_FINDER_COLUMN_CLIENTKEY_KEY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindClientid) {
					qPos.add(clientid);
				}

				if (bindKey) {
					qPos.add(key);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CLIENTKEY_CLIENTID_1 = "customDataEntry.clientid IS NULL AND ";
	private static final String _FINDER_COLUMN_CLIENTKEY_CLIENTID_2 = "customDataEntry.clientid = ? AND ";
	private static final String _FINDER_COLUMN_CLIENTKEY_CLIENTID_3 = "(customDataEntry.clientid IS NULL OR customDataEntry.clientid = '') AND ";
	private static final String _FINDER_COLUMN_CLIENTKEY_KEY_1 = "customDataEntry.key IS NULL";
	private static final String _FINDER_COLUMN_CLIENTKEY_KEY_2 = "customDataEntry.key = ?";
	private static final String _FINDER_COLUMN_CLIENTKEY_KEY_3 = "(customDataEntry.key IS NULL OR customDataEntry.key = '')";

	public CustomDataEntryPersistenceImpl() {
		setModelClass(CustomDataEntry.class);
	}

	/**
	 * Caches the custom data entry in the entity cache if it is enabled.
	 *
	 * @param customDataEntry the custom data entry
	 */
	@Override
	public void cacheResult(CustomDataEntry customDataEntry) {
		EntityCacheUtil.putResult(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryImpl.class, customDataEntry.getPrimaryKey(),
			customDataEntry);

		customDataEntry.resetOriginalValues();
	}

	/**
	 * Caches the custom data entries in the entity cache if it is enabled.
	 *
	 * @param customDataEntries the custom data entries
	 */
	@Override
	public void cacheResult(List<CustomDataEntry> customDataEntries) {
		for (CustomDataEntry customDataEntry : customDataEntries) {
			if (EntityCacheUtil.getResult(
						CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
						CustomDataEntryImpl.class,
						customDataEntry.getPrimaryKey()) == null) {
				cacheResult(customDataEntry);
			}
			else {
				customDataEntry.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all custom data entries.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CustomDataEntryImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CustomDataEntryImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the custom data entry.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CustomDataEntry customDataEntry) {
		EntityCacheUtil.removeResult(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryImpl.class, customDataEntry.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CustomDataEntry> customDataEntries) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CustomDataEntry customDataEntry : customDataEntries) {
			EntityCacheUtil.removeResult(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
				CustomDataEntryImpl.class, customDataEntry.getPrimaryKey());
		}
	}

	/**
	 * Creates a new custom data entry with the primary key. Does not add the custom data entry to the database.
	 *
	 * @param entryId the primary key for the new custom data entry
	 * @return the new custom data entry
	 */
	@Override
	public CustomDataEntry create(long entryId) {
		CustomDataEntry customDataEntry = new CustomDataEntryImpl();

		customDataEntry.setNew(true);
		customDataEntry.setPrimaryKey(entryId);

		return customDataEntry;
	}

	/**
	 * Removes the custom data entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param entryId the primary key of the custom data entry
	 * @return the custom data entry that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry remove(long entryId)
		throws NoSuchCustomDataEntryException, SystemException {
		return remove((Serializable)entryId);
	}

	/**
	 * Removes the custom data entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the custom data entry
	 * @return the custom data entry that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry remove(Serializable primaryKey)
		throws NoSuchCustomDataEntryException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CustomDataEntry customDataEntry = (CustomDataEntry)session.get(CustomDataEntryImpl.class,
					primaryKey);

			if (customDataEntry == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCustomDataEntryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(customDataEntry);
		}
		catch (NoSuchCustomDataEntryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CustomDataEntry removeImpl(CustomDataEntry customDataEntry)
		throws SystemException {
		customDataEntry = toUnwrappedModel(customDataEntry);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(customDataEntry)) {
				customDataEntry = (CustomDataEntry)session.get(CustomDataEntryImpl.class,
						customDataEntry.getPrimaryKeyObj());
			}

			if (customDataEntry != null) {
				session.delete(customDataEntry);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (customDataEntry != null) {
			clearCache(customDataEntry);
		}

		return customDataEntry;
	}

	@Override
	public CustomDataEntry updateImpl(
		it.eng.rspa.cdv.datamodel.model.CustomDataEntry customDataEntry)
		throws SystemException {
		customDataEntry = toUnwrappedModel(customDataEntry);

		boolean isNew = customDataEntry.isNew();

		CustomDataEntryModelImpl customDataEntryModelImpl = (CustomDataEntryModelImpl)customDataEntry;

		Session session = null;

		try {
			session = openSession();

			if (customDataEntry.isNew()) {
				session.save(customDataEntry);

				customDataEntry.setNew(false);
			}
			else {
				session.merge(customDataEntry);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CustomDataEntryModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((customDataEntryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						customDataEntryModelImpl.getOriginalKey()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_KEY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEY,
					args);

				args = new Object[] { customDataEntryModelImpl.getKey() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_KEY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEY,
					args);
			}

			if ((customDataEntryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						customDataEntryModelImpl.getOriginalUserid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { customDataEntryModelImpl.getUserid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((customDataEntryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						customDataEntryModelImpl.getOriginalClientid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLIENTID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTID,
					args);

				args = new Object[] { customDataEntryModelImpl.getClientid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLIENTID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTID,
					args);
			}

			if ((customDataEntryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTKEY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						customDataEntryModelImpl.getOriginalClientid(),
						customDataEntryModelImpl.getOriginalKey()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLIENTKEY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTKEY,
					args);

				args = new Object[] {
						customDataEntryModelImpl.getClientid(),
						customDataEntryModelImpl.getKey()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLIENTKEY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTKEY,
					args);
			}
		}

		EntityCacheUtil.putResult(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
			CustomDataEntryImpl.class, customDataEntry.getPrimaryKey(),
			customDataEntry);

		return customDataEntry;
	}

	protected CustomDataEntry toUnwrappedModel(CustomDataEntry customDataEntry) {
		if (customDataEntry instanceof CustomDataEntryImpl) {
			return customDataEntry;
		}

		CustomDataEntryImpl customDataEntryImpl = new CustomDataEntryImpl();

		customDataEntryImpl.setNew(customDataEntry.isNew());
		customDataEntryImpl.setPrimaryKey(customDataEntry.getPrimaryKey());

		customDataEntryImpl.setEntryId(customDataEntry.getEntryId());
		customDataEntryImpl.setKey(customDataEntry.getKey());
		customDataEntryImpl.setValue(customDataEntry.getValue());
		customDataEntryImpl.setType(customDataEntry.getType());
		customDataEntryImpl.setUserid(customDataEntry.getUserid());
		customDataEntryImpl.setClientid(customDataEntry.getClientid());

		return customDataEntryImpl;
	}

	/**
	 * Returns the custom data entry with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the custom data entry
	 * @return the custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCustomDataEntryException, SystemException {
		CustomDataEntry customDataEntry = fetchByPrimaryKey(primaryKey);

		if (customDataEntry == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCustomDataEntryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return customDataEntry;
	}

	/**
	 * Returns the custom data entry with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException} if it could not be found.
	 *
	 * @param entryId the primary key of the custom data entry
	 * @return the custom data entry
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry findByPrimaryKey(long entryId)
		throws NoSuchCustomDataEntryException, SystemException {
		return findByPrimaryKey((Serializable)entryId);
	}

	/**
	 * Returns the custom data entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the custom data entry
	 * @return the custom data entry, or <code>null</code> if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CustomDataEntry customDataEntry = (CustomDataEntry)EntityCacheUtil.getResult(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
				CustomDataEntryImpl.class, primaryKey);

		if (customDataEntry == _nullCustomDataEntry) {
			return null;
		}

		if (customDataEntry == null) {
			Session session = null;

			try {
				session = openSession();

				customDataEntry = (CustomDataEntry)session.get(CustomDataEntryImpl.class,
						primaryKey);

				if (customDataEntry != null) {
					cacheResult(customDataEntry);
				}
				else {
					EntityCacheUtil.putResult(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
						CustomDataEntryImpl.class, primaryKey,
						_nullCustomDataEntry);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CustomDataEntryModelImpl.ENTITY_CACHE_ENABLED,
					CustomDataEntryImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return customDataEntry;
	}

	/**
	 * Returns the custom data entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param entryId the primary key of the custom data entry
	 * @return the custom data entry, or <code>null</code> if a custom data entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CustomDataEntry fetchByPrimaryKey(long entryId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)entryId);
	}

	/**
	 * Returns all the custom data entries.
	 *
	 * @return the custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the custom data entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @return the range of custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the custom data entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of custom data entries
	 * @param end the upper bound of the range of custom data entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CustomDataEntry> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CustomDataEntry> list = (List<CustomDataEntry>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CUSTOMDATAENTRY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CUSTOMDATAENTRY;

				if (pagination) {
					sql = sql.concat(CustomDataEntryModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CustomDataEntry>(list);
				}
				else {
					list = (List<CustomDataEntry>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the custom data entries from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CustomDataEntry customDataEntry : findAll()) {
			remove(customDataEntry);
		}
	}

	/**
	 * Returns the number of custom data entries.
	 *
	 * @return the number of custom data entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CUSTOMDATAENTRY);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the custom data entry persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.CustomDataEntry")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CustomDataEntry>> listenersList = new ArrayList<ModelListener<CustomDataEntry>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CustomDataEntry>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CustomDataEntryImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CUSTOMDATAENTRY = "SELECT customDataEntry FROM CustomDataEntry customDataEntry";
	private static final String _SQL_SELECT_CUSTOMDATAENTRY_WHERE = "SELECT customDataEntry FROM CustomDataEntry customDataEntry WHERE ";
	private static final String _SQL_COUNT_CUSTOMDATAENTRY = "SELECT COUNT(customDataEntry) FROM CustomDataEntry customDataEntry";
	private static final String _SQL_COUNT_CUSTOMDATAENTRY_WHERE = "SELECT COUNT(customDataEntry) FROM CustomDataEntry customDataEntry WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "customDataEntry.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CustomDataEntry exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CustomDataEntry exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CustomDataEntryPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"key", "type"
			});
	private static CustomDataEntry _nullCustomDataEntry = new CustomDataEntryImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CustomDataEntry> toCacheModel() {
				return _nullCustomDataEntryCacheModel;
			}
		};

	private static CacheModel<CustomDataEntry> _nullCustomDataEntryCacheModel = new CacheModel<CustomDataEntry>() {
			@Override
			public CustomDataEntry toEntityModel() {
				return _nullCustomDataEntry;
			}
		};
}