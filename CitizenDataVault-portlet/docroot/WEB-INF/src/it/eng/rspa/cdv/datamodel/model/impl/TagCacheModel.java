/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.Tag;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Tag in entity cache.
 *
 * @author Engineering
 * @see Tag
 * @generated
 */
public class TagCacheModel implements CacheModel<Tag>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{tagid=");
		sb.append(tagid);
		sb.append(", tagname=");
		sb.append(tagname);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Tag toEntityModel() {
		TagImpl tagImpl = new TagImpl();

		tagImpl.setTagid(tagid);

		if (tagname == null) {
			tagImpl.setTagname(StringPool.BLANK);
		}
		else {
			tagImpl.setTagname(tagname);
		}

		tagImpl.resetOriginalValues();

		return tagImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		tagid = objectInput.readLong();
		tagname = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(tagid);

		if (tagname == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tagname);
		}
	}

	public long tagid;
	public String tagname;
}