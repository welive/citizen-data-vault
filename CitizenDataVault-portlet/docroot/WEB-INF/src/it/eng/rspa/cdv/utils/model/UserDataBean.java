package it.eng.rspa.cdv.utils.model;

import java.util.HashSet;
import java.util.Set;

import com.liferay.portal.kernel.util.Validator;

public class UserDataBean {
	
	private Long dataId;
	private CustomEntry data;
	private String type;
	private Set<String> tags;
	
	public UserDataBean(){
		tags = new HashSet<String>();
		data = new CustomEntry();
		type = "String";
	}

	public Long getDataId() {
		return dataId;
	}

	public void setDataId(Long dataId) {
		this.dataId = dataId;
	}

	public CustomEntry getData() {
		return data;
	}

	public void setData(CustomEntry data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public boolean isValid() {
		return Validator.isNotNull(this.getTags()) && Validator.isNotNull(this.getData()) && this.getData().isValid();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		return prime + ((dataId == null) ? 0 : dataId.hashCode());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDataBean other = (UserDataBean) obj;
		
		if (dataId == null) {
			if (other.dataId != null)
				return false;
		} 
		else if (!dataId.equals(other.dataId))
			return false;
		
		return true;
	}

	
	
	
}
