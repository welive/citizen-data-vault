package it.eng.rspa.cdv.utils.model;

import java.util.HashSet;
import java.util.Set;

public class PushModel {

	private Long ccUserID;
	private String eventName;
	private Set<CustomEntry> entries;
	private Long endorserLiferayUserId;
	


	public PushModel(){
		entries = new HashSet<CustomEntry>();
	}

	public Long getCcUserID() {
		return ccUserID;
	}

	public void setCcUserID(Long ccUserID) {
		this.ccUserID = ccUserID;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Set<CustomEntry> getEntries() {
		return entries;
	}

	public void setEntries(Set<CustomEntry> entries) {
		this.entries = entries;
	}
	
	public Long getEndorserLiferayUserId() {
		return endorserLiferayUserId;
	}

	public void setEndorserLiferayUserId(Long endorserLiferayUserId) {
		this.endorserLiferayUserId = endorserLiferayUserId;
	}
}
