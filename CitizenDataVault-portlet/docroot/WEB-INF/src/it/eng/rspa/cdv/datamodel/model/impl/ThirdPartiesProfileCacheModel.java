/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ThirdPartiesProfile in entity cache.
 *
 * @author Engineering
 * @see ThirdPartiesProfile
 * @generated
 */
public class ThirdPartiesProfileCacheModel implements CacheModel<ThirdPartiesProfile>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{thirdPartyId=");
		sb.append(thirdPartyId);
		sb.append(", key=");
		sb.append(key);
		sb.append(", value=");
		sb.append(value);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ThirdPartiesProfile toEntityModel() {
		ThirdPartiesProfileImpl thirdPartiesProfileImpl = new ThirdPartiesProfileImpl();

		thirdPartiesProfileImpl.setThirdPartyId(thirdPartyId);

		if (key == null) {
			thirdPartiesProfileImpl.setKey(StringPool.BLANK);
		}
		else {
			thirdPartiesProfileImpl.setKey(key);
		}

		if (value == null) {
			thirdPartiesProfileImpl.setValue(StringPool.BLANK);
		}
		else {
			thirdPartiesProfileImpl.setValue(value);
		}

		thirdPartiesProfileImpl.resetOriginalValues();

		return thirdPartiesProfileImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		thirdPartyId = objectInput.readLong();
		key = objectInput.readUTF();
		value = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(thirdPartyId);

		if (key == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(key);
		}

		if (value == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(value);
		}
	}

	public long thirdPartyId;
	public String key;
	public String value;
}