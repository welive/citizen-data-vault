package it.eng.rspa.cdv.utils.model;

public class OutSkill {
	
	private String skillName;
	private Long endorsementCounter;
	
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public Long getEndorsementCounter() {
		return endorsementCounter;
	}
	public void setEndorsementCounter(Long endorsementCounter) {
		this.endorsementCounter = endorsementCounter;
	}
	
}
