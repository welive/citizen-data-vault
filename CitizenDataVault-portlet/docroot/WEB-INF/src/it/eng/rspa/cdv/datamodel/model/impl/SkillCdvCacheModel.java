/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.SkillCdv;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing SkillCdv in entity cache.
 *
 * @author Engineering
 * @see SkillCdv
 * @generated
 */
public class SkillCdvCacheModel implements CacheModel<SkillCdv>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{skillId=");
		sb.append(skillId);
		sb.append(", skillname=");
		sb.append(skillname);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public SkillCdv toEntityModel() {
		SkillCdvImpl skillCdvImpl = new SkillCdvImpl();

		skillCdvImpl.setSkillId(skillId);

		if (skillname == null) {
			skillCdvImpl.setSkillname(StringPool.BLANK);
		}
		else {
			skillCdvImpl.setSkillname(skillname);
		}

		skillCdvImpl.resetOriginalValues();

		return skillCdvImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		skillId = objectInput.readLong();
		skillname = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(skillId);

		if (skillname == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(skillname);
		}
	}

	public long skillId;
	public String skillname;
}