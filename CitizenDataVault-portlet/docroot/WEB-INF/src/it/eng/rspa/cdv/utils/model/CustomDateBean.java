package it.eng.rspa.cdv.utils.model;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.google.gson.annotations.Expose;

public class CustomDateBean {
	
	private Integer day;
	private Integer month;
	private Integer year;
	
	@Expose(serialize = false, deserialize = false)
	private boolean error;
	
	public CustomDateBean() {
		
		Calendar now = GregorianCalendar.getInstance();
		
		this.day = now.get(GregorianCalendar.DAY_OF_MONTH);
		this.month = 1 + now.get(GregorianCalendar.MONTH);
		this.year = now.get(GregorianCalendar.YEAR);
		
		this.error = false;
		System.out.println("[Valid: "+!this.error+"] Creating new instance "+this.toString());
	}
	
//	public CustomDateBean(Integer day, Integer month, Integer year) throws InvalidInputException {
//		this.setDay(day);
//		this.setMonth(month);
//		this.setYear(year);
//		
//		Calendar c = GregorianCalendar.getInstance();
//				c.set(this.getYear(), this.getMonth(), this.getDay());
//		Calendar now = GregorianCalendar.getInstance();
//		
//		if(now.compareTo(c) < 0)
//			this.error = true;
//		
//		System.out.println("2) Is error date: "+this.error);
//		
//	}
	
	public Integer getDay() {
		return day;
	}
//	public void setDay(Integer day) {
//		
//		System.out.println("Setting CustomDateBean.day: "+day);
//		
//		if(day>0 && day<32) 
//			this.day = day;
//		else 
//			this.error = true;
//		
//	}
	public Integer getMonth() {
		return month;
	}
//	public void setMonth(Integer month) {
//		
//		System.out.println("Setting CustomDateBean.month: "+month);
//		
//		if(month>0 && month<13) 
//			this.month = month;
//		else
//			this.error = true;
//	}
	public Integer getYear() {
		return year;
	}
//	public void setYear(Integer year) {
//		
//		System.out.println("Setting CustomDateBean.year: "+year);
//		
//		if(year/1000 > 0)
//			this.year = year;
//		else
//			this.error = true;
//	}
	
	public String toString(){
		return "[" + this.getClass().getSimpleName() + "] "+this.getDay()+"/"+this.getMonth()+"/"+this.getYear();
	}
	
	public Calendar getCalendar(){
		Calendar cal = GregorianCalendar.getInstance();
		cal.clear();
		cal.set(this.getYear(), this.getMonth()-1, this.getDay());
		
		return cal;
	}
	
	public Date getDate(){
		return this.getCalendar().getTime();
	}
	
//	public boolean isValid(){
//		
//		System.out.println("Checking if the date "+this.toString()+" is valid");
//		
//		Calendar c = this.getCalendar();
//		Calendar now = GregorianCalendar.getInstance();
//		
//		if(now.compareTo(c) < 0)
//			this.error = true;
//		
//		System.out.println(this.toString()+" is valid: "+!(this.error));
//		
//		return !(this.error);
//	}

}
