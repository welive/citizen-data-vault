package it.eng.rspa.cdv.utils.model;

import java.util.Calendar;
import java.util.GregorianCalendar;

import it.eng.rspa.cdv.utils.exception.InvalidInputException;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;

public class UpdateUserBean extends GenericUserBean {
	
	private String name;
	private String surname;
	private boolean isDeveloper;
	
	public UpdateUserBean(){
		super();
		
		/* Here you can set all the mandatory fields 
		 *  mandatory_fields.add("isMale");
		 */
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public boolean getIsDeveloper() {
		return isDeveloper;
	}

	public void setIsDeveloper(boolean isDeveloper) {
		this.isDeveloper = isDeveloper;
	}

	

	@Override
	public void check(JSONObject jin) 
			throws Exception {

		System.out.println("Checking "+jin);
		
		if(Validator.isNull(jin)){
			throw new InvalidInputException("The provided input is not valid");
		}
		
		try{
			
			if(jin.has("ccUserID"))
				jin.getLong("ccUserID");
			else throw new Exception("Missing mandatory field ccUserID.");
			
			if(jin.has("isMale")){
				jin.getBoolean("isMale");
			}
			
			if(jin.has("birthDate")){
				
				JSONObject date = jin.getJSONObject("birthDate");
				
				int day = date.getInt("day");
				int month = date.getInt("month")-1;
				int year = date.getInt("year");
				
				Calendar d = GregorianCalendar.getInstance();
				try{
					d.setLenient(false);
					d.clear();
					d.set(year, month, day);
					
					d.getTime();
				}
				catch(Exception e){
					throw new Exception("Invalid birthdate: "+e.getMessage());
				}
				
				Calendar now = GregorianCalendar.getInstance();
				if(now.compareTo(d) < 0)
					throw new Exception("Future birthdate are not allowed");
				
			}
			
			if(jin.has("address"))
				jin.getString("address");
			
			if(jin.has("city"))
				jin.getString("city");
			
			if(jin.has("country"))
				jin.getString("country");
			
			if(jin.has("zipCode"))
				jin.getString("zipCode");
			
			if(jin.has("languages")){
				JSONArray larr = jin.getJSONArray("languages");
				for(int i=0; i<larr.length(); i++){
					larr.getString(i);
				}
			}
			
			if(jin.has("userTags")){
				JSONArray tarr = jin.getJSONArray("userTags");
				for(int i=0; i<tarr.length(); i++){
					tarr.getString(i);
				}
			}
			
			if(jin.has("name")){
				String n = jin.getString("name");
				if(n.trim().isEmpty())
					throw new Exception("Name, if present, cannot be empty");
			}
			
			if(jin.has("surname"))
				jin.getString("surname");
			
			if(jin.has("isDeveloper")){
				jin.getBoolean("isDeveloper");
			}
			
		}
		catch(Exception e){
			throw new InvalidInputException(e.getMessage());
		}
	}
	
}
