/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchEndorsementException;
import it.eng.rspa.cdv.datamodel.model.Endorsement;
import it.eng.rspa.cdv.datamodel.model.impl.EndorsementImpl;
import it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the endorsement service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see EndorsementPersistence
 * @see EndorsementUtil
 * @generated
 */
public class EndorsementPersistenceImpl extends BasePersistenceImpl<Endorsement>
	implements EndorsementPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EndorsementUtil} to access the endorsement persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EndorsementImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, EndorsementImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, EndorsementImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SKILLID = new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, EndorsementImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByskillId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID =
		new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, EndorsementImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByskillId",
			new String[] { Long.class.getName() },
			EndorsementModelImpl.SKILLID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SKILLID = new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByskillId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the endorsements where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @return the matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByskillId(long skillId)
		throws SystemException {
		return findByskillId(skillId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the endorsements where skillId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param skillId the skill ID
	 * @param start the lower bound of the range of endorsements
	 * @param end the upper bound of the range of endorsements (not inclusive)
	 * @return the range of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByskillId(long skillId, int start, int end)
		throws SystemException {
		return findByskillId(skillId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the endorsements where skillId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param skillId the skill ID
	 * @param start the lower bound of the range of endorsements
	 * @param end the upper bound of the range of endorsements (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByskillId(long skillId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID;
			finderArgs = new Object[] { skillId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SKILLID;
			finderArgs = new Object[] { skillId, start, end, orderByComparator };
		}

		List<Endorsement> list = (List<Endorsement>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Endorsement endorsement : list) {
				if ((skillId != endorsement.getSkillId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ENDORSEMENT_WHERE);

			query.append(_FINDER_COLUMN_SKILLID_SKILLID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EndorsementModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(skillId);

				if (!pagination) {
					list = (List<Endorsement>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Endorsement>(list);
				}
				else {
					list = (List<Endorsement>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first endorsement in the ordered set where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement findByskillId_First(long skillId,
		OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = fetchByskillId_First(skillId,
				orderByComparator);

		if (endorsement != null) {
			return endorsement;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("skillId=");
		msg.append(skillId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEndorsementException(msg.toString());
	}

	/**
	 * Returns the first endorsement in the ordered set where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching endorsement, or <code>null</code> if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement fetchByskillId_First(long skillId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Endorsement> list = findByskillId(skillId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last endorsement in the ordered set where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement findByskillId_Last(long skillId,
		OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = fetchByskillId_Last(skillId, orderByComparator);

		if (endorsement != null) {
			return endorsement;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("skillId=");
		msg.append(skillId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEndorsementException(msg.toString());
	}

	/**
	 * Returns the last endorsement in the ordered set where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching endorsement, or <code>null</code> if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement fetchByskillId_Last(long skillId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByskillId(skillId);

		if (count == 0) {
			return null;
		}

		List<Endorsement> list = findByskillId(skillId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the endorsements before and after the current endorsement in the ordered set where skillId = &#63;.
	 *
	 * @param endorsementPK the primary key of the current endorsement
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement[] findByskillId_PrevAndNext(
		EndorsementPK endorsementPK, long skillId,
		OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = findByPrimaryKey(endorsementPK);

		Session session = null;

		try {
			session = openSession();

			Endorsement[] array = new EndorsementImpl[3];

			array[0] = getByskillId_PrevAndNext(session, endorsement, skillId,
					orderByComparator, true);

			array[1] = endorsement;

			array[2] = getByskillId_PrevAndNext(session, endorsement, skillId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Endorsement getByskillId_PrevAndNext(Session session,
		Endorsement endorsement, long skillId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ENDORSEMENT_WHERE);

		query.append(_FINDER_COLUMN_SKILLID_SKILLID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EndorsementModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(skillId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(endorsement);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Endorsement> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the endorsements where skillId = &#63; from the database.
	 *
	 * @param skillId the skill ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByskillId(long skillId) throws SystemException {
		for (Endorsement endorsement : findByskillId(skillId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(endorsement);
		}
	}

	/**
	 * Returns the number of endorsements where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @return the number of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByskillId(long skillId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SKILLID;

		Object[] finderArgs = new Object[] { skillId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ENDORSEMENT_WHERE);

			query.append(_FINDER_COLUMN_SKILLID_SKILLID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(skillId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SKILLID_SKILLID_2 = "endorsement.id.skillId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, EndorsementImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, EndorsementImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			EndorsementModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the endorsements where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByuserId(long userId)
		throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the endorsements where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of endorsements
	 * @param end the upper bound of the range of endorsements (not inclusive)
	 * @return the range of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the endorsements where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of endorsements
	 * @param end the upper bound of the range of endorsements (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByuserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<Endorsement> list = (List<Endorsement>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Endorsement endorsement : list) {
				if ((userId != endorsement.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ENDORSEMENT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EndorsementModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<Endorsement>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Endorsement>(list);
				}
				else {
					list = (List<Endorsement>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first endorsement in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = fetchByuserId_First(userId, orderByComparator);

		if (endorsement != null) {
			return endorsement;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEndorsementException(msg.toString());
	}

	/**
	 * Returns the first endorsement in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching endorsement, or <code>null</code> if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Endorsement> list = findByuserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last endorsement in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = fetchByuserId_Last(userId, orderByComparator);

		if (endorsement != null) {
			return endorsement;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEndorsementException(msg.toString());
	}

	/**
	 * Returns the last endorsement in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching endorsement, or <code>null</code> if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		if (count == 0) {
			return null;
		}

		List<Endorsement> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the endorsements before and after the current endorsement in the ordered set where userId = &#63;.
	 *
	 * @param endorsementPK the primary key of the current endorsement
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement[] findByuserId_PrevAndNext(EndorsementPK endorsementPK,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = findByPrimaryKey(endorsementPK);

		Session session = null;

		try {
			session = openSession();

			Endorsement[] array = new EndorsementImpl[3];

			array[0] = getByuserId_PrevAndNext(session, endorsement, userId,
					orderByComparator, true);

			array[1] = endorsement;

			array[2] = getByuserId_PrevAndNext(session, endorsement, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Endorsement getByuserId_PrevAndNext(Session session,
		Endorsement endorsement, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ENDORSEMENT_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EndorsementModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(endorsement);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Endorsement> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the endorsements where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserId(long userId) throws SystemException {
		for (Endorsement endorsement : findByuserId(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(endorsement);
		}
	}

	/**
	 * Returns the number of endorsements where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ENDORSEMENT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "endorsement.id.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SKILLIDANDUSERID =
		new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, EndorsementImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByskillIDAndUserID",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLIDANDUSERID =
		new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, EndorsementImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByskillIDAndUserID",
			new String[] { Long.class.getName(), Long.class.getName() },
			EndorsementModelImpl.SKILLID_COLUMN_BITMASK |
			EndorsementModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SKILLIDANDUSERID = new FinderPath(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByskillIDAndUserID",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the endorsements where skillId = &#63; and userId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @return the matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByskillIDAndUserID(long skillId, long userId)
		throws SystemException {
		return findByskillIDAndUserID(skillId, userId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the endorsements where skillId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of endorsements
	 * @param end the upper bound of the range of endorsements (not inclusive)
	 * @return the range of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByskillIDAndUserID(long skillId, long userId,
		int start, int end) throws SystemException {
		return findByskillIDAndUserID(skillId, userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the endorsements where skillId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of endorsements
	 * @param end the upper bound of the range of endorsements (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findByskillIDAndUserID(long skillId, long userId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLIDANDUSERID;
			finderArgs = new Object[] { skillId, userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SKILLIDANDUSERID;
			finderArgs = new Object[] {
					skillId, userId,
					
					start, end, orderByComparator
				};
		}

		List<Endorsement> list = (List<Endorsement>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Endorsement endorsement : list) {
				if ((skillId != endorsement.getSkillId()) ||
						(userId != endorsement.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ENDORSEMENT_WHERE);

			query.append(_FINDER_COLUMN_SKILLIDANDUSERID_SKILLID_2);

			query.append(_FINDER_COLUMN_SKILLIDANDUSERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EndorsementModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(skillId);

				qPos.add(userId);

				if (!pagination) {
					list = (List<Endorsement>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Endorsement>(list);
				}
				else {
					list = (List<Endorsement>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement findByskillIDAndUserID_First(long skillId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = fetchByskillIDAndUserID_First(skillId,
				userId, orderByComparator);

		if (endorsement != null) {
			return endorsement;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("skillId=");
		msg.append(skillId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEndorsementException(msg.toString());
	}

	/**
	 * Returns the first endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching endorsement, or <code>null</code> if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement fetchByskillIDAndUserID_First(long skillId, long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Endorsement> list = findByskillIDAndUserID(skillId, userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement findByskillIDAndUserID_Last(long skillId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = fetchByskillIDAndUserID_Last(skillId, userId,
				orderByComparator);

		if (endorsement != null) {
			return endorsement;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("skillId=");
		msg.append(skillId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEndorsementException(msg.toString());
	}

	/**
	 * Returns the last endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching endorsement, or <code>null</code> if a matching endorsement could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement fetchByskillIDAndUserID_Last(long skillId, long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByskillIDAndUserID(skillId, userId);

		if (count == 0) {
			return null;
		}

		List<Endorsement> list = findByskillIDAndUserID(skillId, userId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the endorsements before and after the current endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	 *
	 * @param endorsementPK the primary key of the current endorsement
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement[] findByskillIDAndUserID_PrevAndNext(
		EndorsementPK endorsementPK, long skillId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = findByPrimaryKey(endorsementPK);

		Session session = null;

		try {
			session = openSession();

			Endorsement[] array = new EndorsementImpl[3];

			array[0] = getByskillIDAndUserID_PrevAndNext(session, endorsement,
					skillId, userId, orderByComparator, true);

			array[1] = endorsement;

			array[2] = getByskillIDAndUserID_PrevAndNext(session, endorsement,
					skillId, userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Endorsement getByskillIDAndUserID_PrevAndNext(Session session,
		Endorsement endorsement, long skillId, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ENDORSEMENT_WHERE);

		query.append(_FINDER_COLUMN_SKILLIDANDUSERID_SKILLID_2);

		query.append(_FINDER_COLUMN_SKILLIDANDUSERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EndorsementModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(skillId);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(endorsement);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Endorsement> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the endorsements where skillId = &#63; and userId = &#63; from the database.
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByskillIDAndUserID(long skillId, long userId)
		throws SystemException {
		for (Endorsement endorsement : findByskillIDAndUserID(skillId, userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(endorsement);
		}
	}

	/**
	 * Returns the number of endorsements where skillId = &#63; and userId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param userId the user ID
	 * @return the number of matching endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByskillIDAndUserID(long skillId, long userId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SKILLIDANDUSERID;

		Object[] finderArgs = new Object[] { skillId, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ENDORSEMENT_WHERE);

			query.append(_FINDER_COLUMN_SKILLIDANDUSERID_SKILLID_2);

			query.append(_FINDER_COLUMN_SKILLIDANDUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(skillId);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SKILLIDANDUSERID_SKILLID_2 = "endorsement.id.skillId = ? AND ";
	private static final String _FINDER_COLUMN_SKILLIDANDUSERID_USERID_2 = "endorsement.id.userId = ?";

	public EndorsementPersistenceImpl() {
		setModelClass(Endorsement.class);
	}

	/**
	 * Caches the endorsement in the entity cache if it is enabled.
	 *
	 * @param endorsement the endorsement
	 */
	@Override
	public void cacheResult(Endorsement endorsement) {
		EntityCacheUtil.putResult(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementImpl.class, endorsement.getPrimaryKey(), endorsement);

		endorsement.resetOriginalValues();
	}

	/**
	 * Caches the endorsements in the entity cache if it is enabled.
	 *
	 * @param endorsements the endorsements
	 */
	@Override
	public void cacheResult(List<Endorsement> endorsements) {
		for (Endorsement endorsement : endorsements) {
			if (EntityCacheUtil.getResult(
						EndorsementModelImpl.ENTITY_CACHE_ENABLED,
						EndorsementImpl.class, endorsement.getPrimaryKey()) == null) {
				cacheResult(endorsement);
			}
			else {
				endorsement.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all endorsements.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EndorsementImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EndorsementImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the endorsement.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Endorsement endorsement) {
		EntityCacheUtil.removeResult(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementImpl.class, endorsement.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Endorsement> endorsements) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Endorsement endorsement : endorsements) {
			EntityCacheUtil.removeResult(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
				EndorsementImpl.class, endorsement.getPrimaryKey());
		}
	}

	/**
	 * Creates a new endorsement with the primary key. Does not add the endorsement to the database.
	 *
	 * @param endorsementPK the primary key for the new endorsement
	 * @return the new endorsement
	 */
	@Override
	public Endorsement create(EndorsementPK endorsementPK) {
		Endorsement endorsement = new EndorsementImpl();

		endorsement.setNew(true);
		endorsement.setPrimaryKey(endorsementPK);

		return endorsement;
	}

	/**
	 * Removes the endorsement with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param endorsementPK the primary key of the endorsement
	 * @return the endorsement that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement remove(EndorsementPK endorsementPK)
		throws NoSuchEndorsementException, SystemException {
		return remove((Serializable)endorsementPK);
	}

	/**
	 * Removes the endorsement with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the endorsement
	 * @return the endorsement that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement remove(Serializable primaryKey)
		throws NoSuchEndorsementException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Endorsement endorsement = (Endorsement)session.get(EndorsementImpl.class,
					primaryKey);

			if (endorsement == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEndorsementException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(endorsement);
		}
		catch (NoSuchEndorsementException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Endorsement removeImpl(Endorsement endorsement)
		throws SystemException {
		endorsement = toUnwrappedModel(endorsement);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(endorsement)) {
				endorsement = (Endorsement)session.get(EndorsementImpl.class,
						endorsement.getPrimaryKeyObj());
			}

			if (endorsement != null) {
				session.delete(endorsement);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (endorsement != null) {
			clearCache(endorsement);
		}

		return endorsement;
	}

	@Override
	public Endorsement updateImpl(
		it.eng.rspa.cdv.datamodel.model.Endorsement endorsement)
		throws SystemException {
		endorsement = toUnwrappedModel(endorsement);

		boolean isNew = endorsement.isNew();

		EndorsementModelImpl endorsementModelImpl = (EndorsementModelImpl)endorsement;

		Session session = null;

		try {
			session = openSession();

			if (endorsement.isNew()) {
				session.save(endorsement);

				endorsement.setNew(false);
			}
			else {
				session.merge(endorsement);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EndorsementModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((endorsementModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						endorsementModelImpl.getOriginalSkillId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKILLID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID,
					args);

				args = new Object[] { endorsementModelImpl.getSkillId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKILLID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID,
					args);
			}

			if ((endorsementModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						endorsementModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { endorsementModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((endorsementModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLIDANDUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						endorsementModelImpl.getOriginalSkillId(),
						endorsementModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKILLIDANDUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLIDANDUSERID,
					args);

				args = new Object[] {
						endorsementModelImpl.getSkillId(),
						endorsementModelImpl.getUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKILLIDANDUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLIDANDUSERID,
					args);
			}
		}

		EntityCacheUtil.putResult(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
			EndorsementImpl.class, endorsement.getPrimaryKey(), endorsement);

		return endorsement;
	}

	protected Endorsement toUnwrappedModel(Endorsement endorsement) {
		if (endorsement instanceof EndorsementImpl) {
			return endorsement;
		}

		EndorsementImpl endorsementImpl = new EndorsementImpl();

		endorsementImpl.setNew(endorsement.isNew());
		endorsementImpl.setPrimaryKey(endorsement.getPrimaryKey());

		endorsementImpl.setSkillId(endorsement.getSkillId());
		endorsementImpl.setUserId(endorsement.getUserId());
		endorsementImpl.setEndorserId(endorsement.getEndorserId());
		endorsementImpl.setDate(endorsement.getDate());

		return endorsementImpl;
	}

	/**
	 * Returns the endorsement with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the endorsement
	 * @return the endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEndorsementException, SystemException {
		Endorsement endorsement = fetchByPrimaryKey(primaryKey);

		if (endorsement == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEndorsementException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return endorsement;
	}

	/**
	 * Returns the endorsement with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchEndorsementException} if it could not be found.
	 *
	 * @param endorsementPK the primary key of the endorsement
	 * @return the endorsement
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement findByPrimaryKey(EndorsementPK endorsementPK)
		throws NoSuchEndorsementException, SystemException {
		return findByPrimaryKey((Serializable)endorsementPK);
	}

	/**
	 * Returns the endorsement with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the endorsement
	 * @return the endorsement, or <code>null</code> if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Endorsement endorsement = (Endorsement)EntityCacheUtil.getResult(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
				EndorsementImpl.class, primaryKey);

		if (endorsement == _nullEndorsement) {
			return null;
		}

		if (endorsement == null) {
			Session session = null;

			try {
				session = openSession();

				endorsement = (Endorsement)session.get(EndorsementImpl.class,
						primaryKey);

				if (endorsement != null) {
					cacheResult(endorsement);
				}
				else {
					EntityCacheUtil.putResult(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
						EndorsementImpl.class, primaryKey, _nullEndorsement);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EndorsementModelImpl.ENTITY_CACHE_ENABLED,
					EndorsementImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return endorsement;
	}

	/**
	 * Returns the endorsement with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param endorsementPK the primary key of the endorsement
	 * @return the endorsement, or <code>null</code> if a endorsement with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Endorsement fetchByPrimaryKey(EndorsementPK endorsementPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)endorsementPK);
	}

	/**
	 * Returns all the endorsements.
	 *
	 * @return the endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the endorsements.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of endorsements
	 * @param end the upper bound of the range of endorsements (not inclusive)
	 * @return the range of endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the endorsements.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of endorsements
	 * @param end the upper bound of the range of endorsements (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Endorsement> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Endorsement> list = (List<Endorsement>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ENDORSEMENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ENDORSEMENT;

				if (pagination) {
					sql = sql.concat(EndorsementModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Endorsement>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Endorsement>(list);
				}
				else {
					list = (List<Endorsement>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the endorsements from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Endorsement endorsement : findAll()) {
			remove(endorsement);
		}
	}

	/**
	 * Returns the number of endorsements.
	 *
	 * @return the number of endorsements
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ENDORSEMENT);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the endorsement persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.Endorsement")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Endorsement>> listenersList = new ArrayList<ModelListener<Endorsement>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Endorsement>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EndorsementImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ENDORSEMENT = "SELECT endorsement FROM Endorsement endorsement";
	private static final String _SQL_SELECT_ENDORSEMENT_WHERE = "SELECT endorsement FROM Endorsement endorsement WHERE ";
	private static final String _SQL_COUNT_ENDORSEMENT = "SELECT COUNT(endorsement) FROM Endorsement endorsement";
	private static final String _SQL_COUNT_ENDORSEMENT_WHERE = "SELECT COUNT(endorsement) FROM Endorsement endorsement WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "endorsement.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Endorsement exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Endorsement exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EndorsementPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"date"
			});
	private static Endorsement _nullEndorsement = new EndorsementImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Endorsement> toCacheModel() {
				return _nullEndorsementCacheModel;
			}
		};

	private static CacheModel<Endorsement> _nullEndorsementCacheModel = new CacheModel<Endorsement>() {
			@Override
			public Endorsement toEntityModel() {
				return _nullEndorsement;
			}
		};
}