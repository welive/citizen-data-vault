/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchLanguageException;
import it.eng.rspa.cdv.datamodel.model.Language;
import it.eng.rspa.cdv.datamodel.model.impl.LanguageImpl;
import it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the language service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see LanguagePersistence
 * @see LanguageUtil
 * @generated
 */
public class LanguagePersistenceImpl extends BasePersistenceImpl<Language>
	implements LanguagePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LanguageUtil} to access the language persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LanguageImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageModelImpl.FINDER_CACHE_ENABLED, LanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageModelImpl.FINDER_CACHE_ENABLED, LanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageModelImpl.FINDER_CACHE_ENABLED, LanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByname",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageModelImpl.FINDER_CACHE_ENABLED, LanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByname",
			new String[] { String.class.getName() },
			LanguageModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByname",
			new String[] { String.class.getName() });

	/**
	 * Returns all the languages where name = &#63;.
	 *
	 * @param name the name
	 * @return the matching languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Language> findByname(String name) throws SystemException {
		return findByname(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the languages where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of languages
	 * @param end the upper bound of the range of languages (not inclusive)
	 * @return the range of matching languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Language> findByname(String name, int start, int end)
		throws SystemException {
		return findByname(name, start, end, null);
	}

	/**
	 * Returns an ordered range of all the languages where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of languages
	 * @param end the upper bound of the range of languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Language> findByname(String name, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name, start, end, orderByComparator };
		}

		List<Language> list = (List<Language>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Language language : list) {
				if (!Validator.equals(name, language.getName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LANGUAGE_WHERE);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LanguageModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindName) {
					qPos.add(name.toLowerCase());
				}

				if (!pagination) {
					list = (List<Language>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Language>(list);
				}
				else {
					list = (List<Language>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first language in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a matching language could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language findByname_First(String name,
		OrderByComparator orderByComparator)
		throws NoSuchLanguageException, SystemException {
		Language language = fetchByname_First(name, orderByComparator);

		if (language != null) {
			return language;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLanguageException(msg.toString());
	}

	/**
	 * Returns the first language in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching language, or <code>null</code> if a matching language could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language fetchByname_First(String name,
		OrderByComparator orderByComparator) throws SystemException {
		List<Language> list = findByname(name, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last language in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a matching language could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language findByname_Last(String name,
		OrderByComparator orderByComparator)
		throws NoSuchLanguageException, SystemException {
		Language language = fetchByname_Last(name, orderByComparator);

		if (language != null) {
			return language;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLanguageException(msg.toString());
	}

	/**
	 * Returns the last language in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching language, or <code>null</code> if a matching language could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language fetchByname_Last(String name,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByname(name);

		if (count == 0) {
			return null;
		}

		List<Language> list = findByname(name, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the languages before and after the current language in the ordered set where name = &#63;.
	 *
	 * @param languageId the primary key of the current language
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language[] findByname_PrevAndNext(long languageId, String name,
		OrderByComparator orderByComparator)
		throws NoSuchLanguageException, SystemException {
		Language language = findByPrimaryKey(languageId);

		Session session = null;

		try {
			session = openSession();

			Language[] array = new LanguageImpl[3];

			array[0] = getByname_PrevAndNext(session, language, name,
					orderByComparator, true);

			array[1] = language;

			array[2] = getByname_PrevAndNext(session, language, name,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Language getByname_PrevAndNext(Session session,
		Language language, String name, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LANGUAGE_WHERE);

		boolean bindName = false;

		if (name == null) {
			query.append(_FINDER_COLUMN_NAME_NAME_1);
		}
		else if (name.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NAME_NAME_3);
		}
		else {
			bindName = true;

			query.append(_FINDER_COLUMN_NAME_NAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LanguageModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindName) {
			qPos.add(name.toLowerCase());
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(language);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Language> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the languages where name = &#63; from the database.
	 *
	 * @param name the name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByname(String name) throws SystemException {
		for (Language language : findByname(name, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(language);
		}
	}

	/**
	 * Returns the number of languages where name = &#63;.
	 *
	 * @param name the name
	 * @return the number of matching languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByname(String name) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

		Object[] finderArgs = new Object[] { name };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LANGUAGE_WHERE);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindName) {
					qPos.add(name.toLowerCase());
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NAME_NAME_1 = "language.name IS NULL";
	private static final String _FINDER_COLUMN_NAME_NAME_2 = "lower(language.name) = ?";
	private static final String _FINDER_COLUMN_NAME_NAME_3 = "(language.name IS NULL OR language.name = '')";

	public LanguagePersistenceImpl() {
		setModelClass(Language.class);
	}

	/**
	 * Caches the language in the entity cache if it is enabled.
	 *
	 * @param language the language
	 */
	@Override
	public void cacheResult(Language language) {
		EntityCacheUtil.putResult(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageImpl.class, language.getPrimaryKey(), language);

		language.resetOriginalValues();
	}

	/**
	 * Caches the languages in the entity cache if it is enabled.
	 *
	 * @param languages the languages
	 */
	@Override
	public void cacheResult(List<Language> languages) {
		for (Language language : languages) {
			if (EntityCacheUtil.getResult(
						LanguageModelImpl.ENTITY_CACHE_ENABLED,
						LanguageImpl.class, language.getPrimaryKey()) == null) {
				cacheResult(language);
			}
			else {
				language.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all languages.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LanguageImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LanguageImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the language.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Language language) {
		EntityCacheUtil.removeResult(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageImpl.class, language.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Language> languages) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Language language : languages) {
			EntityCacheUtil.removeResult(LanguageModelImpl.ENTITY_CACHE_ENABLED,
				LanguageImpl.class, language.getPrimaryKey());
		}
	}

	/**
	 * Creates a new language with the primary key. Does not add the language to the database.
	 *
	 * @param languageId the primary key for the new language
	 * @return the new language
	 */
	@Override
	public Language create(long languageId) {
		Language language = new LanguageImpl();

		language.setNew(true);
		language.setPrimaryKey(languageId);

		return language;
	}

	/**
	 * Removes the language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param languageId the primary key of the language
	 * @return the language that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language remove(long languageId)
		throws NoSuchLanguageException, SystemException {
		return remove((Serializable)languageId);
	}

	/**
	 * Removes the language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the language
	 * @return the language that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language remove(Serializable primaryKey)
		throws NoSuchLanguageException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Language language = (Language)session.get(LanguageImpl.class,
					primaryKey);

			if (language == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLanguageException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(language);
		}
		catch (NoSuchLanguageException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Language removeImpl(Language language) throws SystemException {
		language = toUnwrappedModel(language);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(language)) {
				language = (Language)session.get(LanguageImpl.class,
						language.getPrimaryKeyObj());
			}

			if (language != null) {
				session.delete(language);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (language != null) {
			clearCache(language);
		}

		return language;
	}

	@Override
	public Language updateImpl(
		it.eng.rspa.cdv.datamodel.model.Language language)
		throws SystemException {
		language = toUnwrappedModel(language);

		boolean isNew = language.isNew();

		LanguageModelImpl languageModelImpl = (LanguageModelImpl)language;

		Session session = null;

		try {
			session = openSession();

			if (language.isNew()) {
				session.save(language);

				language.setNew(false);
			}
			else {
				session.merge(language);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LanguageModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((languageModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { languageModelImpl.getOriginalName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);

				args = new Object[] { languageModelImpl.getName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);
			}
		}

		EntityCacheUtil.putResult(LanguageModelImpl.ENTITY_CACHE_ENABLED,
			LanguageImpl.class, language.getPrimaryKey(), language);

		return language;
	}

	protected Language toUnwrappedModel(Language language) {
		if (language instanceof LanguageImpl) {
			return language;
		}

		LanguageImpl languageImpl = new LanguageImpl();

		languageImpl.setNew(language.isNew());
		languageImpl.setPrimaryKey(language.getPrimaryKey());

		languageImpl.setLanguageId(language.getLanguageId());
		languageImpl.setName(language.getName());

		return languageImpl;
	}

	/**
	 * Returns the language with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the language
	 * @return the language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLanguageException, SystemException {
		Language language = fetchByPrimaryKey(primaryKey);

		if (language == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLanguageException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return language;
	}

	/**
	 * Returns the language with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchLanguageException} if it could not be found.
	 *
	 * @param languageId the primary key of the language
	 * @return the language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language findByPrimaryKey(long languageId)
		throws NoSuchLanguageException, SystemException {
		return findByPrimaryKey((Serializable)languageId);
	}

	/**
	 * Returns the language with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the language
	 * @return the language, or <code>null</code> if a language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Language language = (Language)EntityCacheUtil.getResult(LanguageModelImpl.ENTITY_CACHE_ENABLED,
				LanguageImpl.class, primaryKey);

		if (language == _nullLanguage) {
			return null;
		}

		if (language == null) {
			Session session = null;

			try {
				session = openSession();

				language = (Language)session.get(LanguageImpl.class, primaryKey);

				if (language != null) {
					cacheResult(language);
				}
				else {
					EntityCacheUtil.putResult(LanguageModelImpl.ENTITY_CACHE_ENABLED,
						LanguageImpl.class, primaryKey, _nullLanguage);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(LanguageModelImpl.ENTITY_CACHE_ENABLED,
					LanguageImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return language;
	}

	/**
	 * Returns the language with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param languageId the primary key of the language
	 * @return the language, or <code>null</code> if a language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Language fetchByPrimaryKey(long languageId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)languageId);
	}

	/**
	 * Returns all the languages.
	 *
	 * @return the languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Language> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of languages
	 * @param end the upper bound of the range of languages (not inclusive)
	 * @return the range of languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Language> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of languages
	 * @param end the upper bound of the range of languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Language> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Language> list = (List<Language>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LANGUAGE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LANGUAGE;

				if (pagination) {
					sql = sql.concat(LanguageModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Language>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Language>(list);
				}
				else {
					list = (List<Language>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the languages from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Language language : findAll()) {
			remove(language);
		}
	}

	/**
	 * Returns the number of languages.
	 *
	 * @return the number of languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LANGUAGE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the language persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.Language")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Language>> listenersList = new ArrayList<ModelListener<Language>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Language>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LanguageImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_LANGUAGE = "SELECT language FROM Language language";
	private static final String _SQL_SELECT_LANGUAGE_WHERE = "SELECT language FROM Language language WHERE ";
	private static final String _SQL_COUNT_LANGUAGE = "SELECT COUNT(language) FROM Language language";
	private static final String _SQL_COUNT_LANGUAGE_WHERE = "SELECT COUNT(language) FROM Language language WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "language.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Language exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Language exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LanguagePersistenceImpl.class);
	private static Language _nullLanguage = new LanguageImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Language> toCacheModel() {
				return _nullLanguageCacheModel;
			}
		};

	private static CacheModel<Language> _nullLanguageCacheModel = new CacheModel<Language>() {
			@Override
			public Language toEntityModel() {
				return _nullLanguage;
			}
		};
}