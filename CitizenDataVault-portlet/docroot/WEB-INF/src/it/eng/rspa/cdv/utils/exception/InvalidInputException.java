package it.eng.rspa.cdv.utils.exception;

public class InvalidInputException extends CustomException{

	private static final long serialVersionUID = 6L;

	public InvalidInputException(String message){
		super(message);
		setMessageNumber(6L);
	}
	
}
