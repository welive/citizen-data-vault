/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException;
import it.eng.rspa.cdv.datamodel.model.SkillCdv;
import it.eng.rspa.cdv.datamodel.model.impl.SkillCdvImpl;
import it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the skill cdv service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see SkillCdvPersistence
 * @see SkillCdvUtil
 * @generated
 */
public class SkillCdvPersistenceImpl extends BasePersistenceImpl<SkillCdv>
	implements SkillCdvPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SkillCdvUtil} to access the skill cdv persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SkillCdvImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvModelImpl.FINDER_CACHE_ENABLED, SkillCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvModelImpl.FINDER_CACHE_ENABLED, SkillCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SKILLNAME =
		new FinderPath(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvModelImpl.FINDER_CACHE_ENABLED, SkillCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByskillname",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLNAME =
		new FinderPath(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvModelImpl.FINDER_CACHE_ENABLED, SkillCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByskillname",
			new String[] { String.class.getName() },
			SkillCdvModelImpl.SKILLNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SKILLNAME = new FinderPath(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByskillname",
			new String[] { String.class.getName() });

	/**
	 * Returns all the skill cdvs where skillname = &#63;.
	 *
	 * @param skillname the skillname
	 * @return the matching skill cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkillCdv> findByskillname(String skillname)
		throws SystemException {
		return findByskillname(skillname, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the skill cdvs where skillname = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param skillname the skillname
	 * @param start the lower bound of the range of skill cdvs
	 * @param end the upper bound of the range of skill cdvs (not inclusive)
	 * @return the range of matching skill cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkillCdv> findByskillname(String skillname, int start, int end)
		throws SystemException {
		return findByskillname(skillname, start, end, null);
	}

	/**
	 * Returns an ordered range of all the skill cdvs where skillname = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param skillname the skillname
	 * @param start the lower bound of the range of skill cdvs
	 * @param end the upper bound of the range of skill cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching skill cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkillCdv> findByskillname(String skillname, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLNAME;
			finderArgs = new Object[] { skillname };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SKILLNAME;
			finderArgs = new Object[] { skillname, start, end, orderByComparator };
		}

		List<SkillCdv> list = (List<SkillCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (SkillCdv skillCdv : list) {
				if (!Validator.equals(skillname, skillCdv.getSkillname())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SKILLCDV_WHERE);

			boolean bindSkillname = false;

			if (skillname == null) {
				query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_1);
			}
			else if (skillname.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_3);
			}
			else {
				bindSkillname = true;

				query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(SkillCdvModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindSkillname) {
					qPos.add(skillname.toLowerCase());
				}

				if (!pagination) {
					list = (List<SkillCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<SkillCdv>(list);
				}
				else {
					list = (List<SkillCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first skill cdv in the ordered set where skillname = &#63;.
	 *
	 * @param skillname the skillname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching skill cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a matching skill cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv findByskillname_First(String skillname,
		OrderByComparator orderByComparator)
		throws NoSuchSkillCdvException, SystemException {
		SkillCdv skillCdv = fetchByskillname_First(skillname, orderByComparator);

		if (skillCdv != null) {
			return skillCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("skillname=");
		msg.append(skillname);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSkillCdvException(msg.toString());
	}

	/**
	 * Returns the first skill cdv in the ordered set where skillname = &#63;.
	 *
	 * @param skillname the skillname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching skill cdv, or <code>null</code> if a matching skill cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv fetchByskillname_First(String skillname,
		OrderByComparator orderByComparator) throws SystemException {
		List<SkillCdv> list = findByskillname(skillname, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last skill cdv in the ordered set where skillname = &#63;.
	 *
	 * @param skillname the skillname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching skill cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a matching skill cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv findByskillname_Last(String skillname,
		OrderByComparator orderByComparator)
		throws NoSuchSkillCdvException, SystemException {
		SkillCdv skillCdv = fetchByskillname_Last(skillname, orderByComparator);

		if (skillCdv != null) {
			return skillCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("skillname=");
		msg.append(skillname);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSkillCdvException(msg.toString());
	}

	/**
	 * Returns the last skill cdv in the ordered set where skillname = &#63;.
	 *
	 * @param skillname the skillname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching skill cdv, or <code>null</code> if a matching skill cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv fetchByskillname_Last(String skillname,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByskillname(skillname);

		if (count == 0) {
			return null;
		}

		List<SkillCdv> list = findByskillname(skillname, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the skill cdvs before and after the current skill cdv in the ordered set where skillname = &#63;.
	 *
	 * @param skillId the primary key of the current skill cdv
	 * @param skillname the skillname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next skill cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv[] findByskillname_PrevAndNext(long skillId,
		String skillname, OrderByComparator orderByComparator)
		throws NoSuchSkillCdvException, SystemException {
		SkillCdv skillCdv = findByPrimaryKey(skillId);

		Session session = null;

		try {
			session = openSession();

			SkillCdv[] array = new SkillCdvImpl[3];

			array[0] = getByskillname_PrevAndNext(session, skillCdv, skillname,
					orderByComparator, true);

			array[1] = skillCdv;

			array[2] = getByskillname_PrevAndNext(session, skillCdv, skillname,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected SkillCdv getByskillname_PrevAndNext(Session session,
		SkillCdv skillCdv, String skillname,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SKILLCDV_WHERE);

		boolean bindSkillname = false;

		if (skillname == null) {
			query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_1);
		}
		else if (skillname.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_3);
		}
		else {
			bindSkillname = true;

			query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(SkillCdvModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindSkillname) {
			qPos.add(skillname.toLowerCase());
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(skillCdv);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<SkillCdv> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the skill cdvs where skillname = &#63; from the database.
	 *
	 * @param skillname the skillname
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByskillname(String skillname) throws SystemException {
		for (SkillCdv skillCdv : findByskillname(skillname, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(skillCdv);
		}
	}

	/**
	 * Returns the number of skill cdvs where skillname = &#63;.
	 *
	 * @param skillname the skillname
	 * @return the number of matching skill cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByskillname(String skillname) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SKILLNAME;

		Object[] finderArgs = new Object[] { skillname };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SKILLCDV_WHERE);

			boolean bindSkillname = false;

			if (skillname == null) {
				query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_1);
			}
			else if (skillname.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_3);
			}
			else {
				bindSkillname = true;

				query.append(_FINDER_COLUMN_SKILLNAME_SKILLNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindSkillname) {
					qPos.add(skillname.toLowerCase());
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SKILLNAME_SKILLNAME_1 = "skillCdv.skillname IS NULL";
	private static final String _FINDER_COLUMN_SKILLNAME_SKILLNAME_2 = "lower(skillCdv.skillname) = ?";
	private static final String _FINDER_COLUMN_SKILLNAME_SKILLNAME_3 = "(skillCdv.skillname IS NULL OR skillCdv.skillname = '')";

	public SkillCdvPersistenceImpl() {
		setModelClass(SkillCdv.class);
	}

	/**
	 * Caches the skill cdv in the entity cache if it is enabled.
	 *
	 * @param skillCdv the skill cdv
	 */
	@Override
	public void cacheResult(SkillCdv skillCdv) {
		EntityCacheUtil.putResult(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvImpl.class, skillCdv.getPrimaryKey(), skillCdv);

		skillCdv.resetOriginalValues();
	}

	/**
	 * Caches the skill cdvs in the entity cache if it is enabled.
	 *
	 * @param skillCdvs the skill cdvs
	 */
	@Override
	public void cacheResult(List<SkillCdv> skillCdvs) {
		for (SkillCdv skillCdv : skillCdvs) {
			if (EntityCacheUtil.getResult(
						SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
						SkillCdvImpl.class, skillCdv.getPrimaryKey()) == null) {
				cacheResult(skillCdv);
			}
			else {
				skillCdv.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all skill cdvs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(SkillCdvImpl.class.getName());
		}

		EntityCacheUtil.clearCache(SkillCdvImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the skill cdv.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(SkillCdv skillCdv) {
		EntityCacheUtil.removeResult(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvImpl.class, skillCdv.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<SkillCdv> skillCdvs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (SkillCdv skillCdv : skillCdvs) {
			EntityCacheUtil.removeResult(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
				SkillCdvImpl.class, skillCdv.getPrimaryKey());
		}
	}

	/**
	 * Creates a new skill cdv with the primary key. Does not add the skill cdv to the database.
	 *
	 * @param skillId the primary key for the new skill cdv
	 * @return the new skill cdv
	 */
	@Override
	public SkillCdv create(long skillId) {
		SkillCdv skillCdv = new SkillCdvImpl();

		skillCdv.setNew(true);
		skillCdv.setPrimaryKey(skillId);

		return skillCdv;
	}

	/**
	 * Removes the skill cdv with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param skillId the primary key of the skill cdv
	 * @return the skill cdv that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv remove(long skillId)
		throws NoSuchSkillCdvException, SystemException {
		return remove((Serializable)skillId);
	}

	/**
	 * Removes the skill cdv with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the skill cdv
	 * @return the skill cdv that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv remove(Serializable primaryKey)
		throws NoSuchSkillCdvException, SystemException {
		Session session = null;

		try {
			session = openSession();

			SkillCdv skillCdv = (SkillCdv)session.get(SkillCdvImpl.class,
					primaryKey);

			if (skillCdv == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSkillCdvException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(skillCdv);
		}
		catch (NoSuchSkillCdvException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected SkillCdv removeImpl(SkillCdv skillCdv) throws SystemException {
		skillCdv = toUnwrappedModel(skillCdv);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(skillCdv)) {
				skillCdv = (SkillCdv)session.get(SkillCdvImpl.class,
						skillCdv.getPrimaryKeyObj());
			}

			if (skillCdv != null) {
				session.delete(skillCdv);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (skillCdv != null) {
			clearCache(skillCdv);
		}

		return skillCdv;
	}

	@Override
	public SkillCdv updateImpl(
		it.eng.rspa.cdv.datamodel.model.SkillCdv skillCdv)
		throws SystemException {
		skillCdv = toUnwrappedModel(skillCdv);

		boolean isNew = skillCdv.isNew();

		SkillCdvModelImpl skillCdvModelImpl = (SkillCdvModelImpl)skillCdv;

		Session session = null;

		try {
			session = openSession();

			if (skillCdv.isNew()) {
				session.save(skillCdv);

				skillCdv.setNew(false);
			}
			else {
				session.merge(skillCdv);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !SkillCdvModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((skillCdvModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						skillCdvModelImpl.getOriginalSkillname()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKILLNAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLNAME,
					args);

				args = new Object[] { skillCdvModelImpl.getSkillname() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKILLNAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLNAME,
					args);
			}
		}

		EntityCacheUtil.putResult(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
			SkillCdvImpl.class, skillCdv.getPrimaryKey(), skillCdv);

		return skillCdv;
	}

	protected SkillCdv toUnwrappedModel(SkillCdv skillCdv) {
		if (skillCdv instanceof SkillCdvImpl) {
			return skillCdv;
		}

		SkillCdvImpl skillCdvImpl = new SkillCdvImpl();

		skillCdvImpl.setNew(skillCdv.isNew());
		skillCdvImpl.setPrimaryKey(skillCdv.getPrimaryKey());

		skillCdvImpl.setSkillId(skillCdv.getSkillId());
		skillCdvImpl.setSkillname(skillCdv.getSkillname());

		return skillCdvImpl;
	}

	/**
	 * Returns the skill cdv with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the skill cdv
	 * @return the skill cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSkillCdvException, SystemException {
		SkillCdv skillCdv = fetchByPrimaryKey(primaryKey);

		if (skillCdv == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSkillCdvException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return skillCdv;
	}

	/**
	 * Returns the skill cdv with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException} if it could not be found.
	 *
	 * @param skillId the primary key of the skill cdv
	 * @return the skill cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv findByPrimaryKey(long skillId)
		throws NoSuchSkillCdvException, SystemException {
		return findByPrimaryKey((Serializable)skillId);
	}

	/**
	 * Returns the skill cdv with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the skill cdv
	 * @return the skill cdv, or <code>null</code> if a skill cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		SkillCdv skillCdv = (SkillCdv)EntityCacheUtil.getResult(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
				SkillCdvImpl.class, primaryKey);

		if (skillCdv == _nullSkillCdv) {
			return null;
		}

		if (skillCdv == null) {
			Session session = null;

			try {
				session = openSession();

				skillCdv = (SkillCdv)session.get(SkillCdvImpl.class, primaryKey);

				if (skillCdv != null) {
					cacheResult(skillCdv);
				}
				else {
					EntityCacheUtil.putResult(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
						SkillCdvImpl.class, primaryKey, _nullSkillCdv);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(SkillCdvModelImpl.ENTITY_CACHE_ENABLED,
					SkillCdvImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return skillCdv;
	}

	/**
	 * Returns the skill cdv with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param skillId the primary key of the skill cdv
	 * @return the skill cdv, or <code>null</code> if a skill cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkillCdv fetchByPrimaryKey(long skillId) throws SystemException {
		return fetchByPrimaryKey((Serializable)skillId);
	}

	/**
	 * Returns all the skill cdvs.
	 *
	 * @return the skill cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkillCdv> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the skill cdvs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of skill cdvs
	 * @param end the upper bound of the range of skill cdvs (not inclusive)
	 * @return the range of skill cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkillCdv> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the skill cdvs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of skill cdvs
	 * @param end the upper bound of the range of skill cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of skill cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkillCdv> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<SkillCdv> list = (List<SkillCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_SKILLCDV);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SKILLCDV;

				if (pagination) {
					sql = sql.concat(SkillCdvModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<SkillCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<SkillCdv>(list);
				}
				else {
					list = (List<SkillCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the skill cdvs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (SkillCdv skillCdv : findAll()) {
			remove(skillCdv);
		}
	}

	/**
	 * Returns the number of skill cdvs.
	 *
	 * @return the number of skill cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SKILLCDV);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the skill cdv persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.SkillCdv")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<SkillCdv>> listenersList = new ArrayList<ModelListener<SkillCdv>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<SkillCdv>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(SkillCdvImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_SKILLCDV = "SELECT skillCdv FROM SkillCdv skillCdv";
	private static final String _SQL_SELECT_SKILLCDV_WHERE = "SELECT skillCdv FROM SkillCdv skillCdv WHERE ";
	private static final String _SQL_COUNT_SKILLCDV = "SELECT COUNT(skillCdv) FROM SkillCdv skillCdv";
	private static final String _SQL_COUNT_SKILLCDV_WHERE = "SELECT COUNT(skillCdv) FROM SkillCdv skillCdv WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "skillCdv.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SkillCdv exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No SkillCdv exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(SkillCdvPersistenceImpl.class);
	private static SkillCdv _nullSkillCdv = new SkillCdvImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<SkillCdv> toCacheModel() {
				return _nullSkillCdvCacheModel;
			}
		};

	private static CacheModel<SkillCdv> _nullSkillCdvCacheModel = new CacheModel<SkillCdv>() {
			@Override
			public SkillCdv toEntityModel() {
				return _nullSkillCdv;
			}
		};
}