package it.eng.rspa.cdv.utils.exception;

public class WrongCcuseridException extends CustomException{

	private static final long serialVersionUID = 5L;

	public WrongCcuseridException(String message){
		super(message);
		setMessageNumber(5L);
	}
	
}
