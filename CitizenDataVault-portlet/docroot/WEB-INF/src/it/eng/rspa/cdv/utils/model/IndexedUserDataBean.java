package it.eng.rspa.cdv.utils.model;

public class IndexedUserDataBean extends UserDataBean {
	
	private Long index;
	
	public IndexedUserDataBean(){
		super();
	}

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	@Override
	public boolean isValid() {
		boolean superValid = super.isValid();
		boolean thisValid = (this.getIndex() != null);
		return superValid && thisValid;
	}
	
}
