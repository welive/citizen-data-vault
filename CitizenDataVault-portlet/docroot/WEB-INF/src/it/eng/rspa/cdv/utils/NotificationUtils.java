package it.eng.rspa.cdv.utils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.notifications.NotificationEvent;
import com.liferay.portal.kernel.notifications.NotificationEventFactoryUtil;
import com.liferay.portal.service.UserNotificationEventLocalServiceUtil;


public class NotificationUtils {
	
	/**
	 * @param textMessage
	 * @param destinationUderId
	 */
	public static void sendNotification(String textMessage, long destinationUserId) {
			
			JSONObject notificationEventJSONObject = JSONFactoryUtil.createJSONObject();
			
			String textMessageNotifica = textMessage;			
			 
			notificationEventJSONObject.put("text-message",textMessageNotifica);
			notificationEventJSONObject.put("userId",destinationUserId);
			notificationEventJSONObject.put("sender","CDV");	
			notificationEventJSONObject.put("destinationURL", Utils.getProfileUrl(destinationUserId));
				
		
			NotificationEvent notificationEvent = NotificationEventFactoryUtil.createNotificationEvent(System.currentTimeMillis(),PortletKeys.CDV, notificationEventJSONObject);
			notificationEvent.setDeliveryRequired(0);
			try {
				UserNotificationEventLocalServiceUtil.addUserNotificationEvent(destinationUserId,notificationEvent);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
			}
	
	}

}
