/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.Endorsement;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Endorsement in entity cache.
 *
 * @author Engineering
 * @see Endorsement
 * @generated
 */
public class EndorsementCacheModel implements CacheModel<Endorsement>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{skillId=");
		sb.append(skillId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", endorserId=");
		sb.append(endorserId);
		sb.append(", date=");
		sb.append(date);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Endorsement toEntityModel() {
		EndorsementImpl endorsementImpl = new EndorsementImpl();

		endorsementImpl.setSkillId(skillId);
		endorsementImpl.setUserId(userId);
		endorsementImpl.setEndorserId(endorserId);

		if (date == Long.MIN_VALUE) {
			endorsementImpl.setDate(null);
		}
		else {
			endorsementImpl.setDate(new Date(date));
		}

		endorsementImpl.resetOriginalValues();

		return endorsementImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		skillId = objectInput.readLong();
		userId = objectInput.readLong();
		endorserId = objectInput.readLong();
		date = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(skillId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(endorserId);
		objectOutput.writeLong(date);
	}

	public long skillId;
	public long userId;
	public long endorserId;
	public long date;
}