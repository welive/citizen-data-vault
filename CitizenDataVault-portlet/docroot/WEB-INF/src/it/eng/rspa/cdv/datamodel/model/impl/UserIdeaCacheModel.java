/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.UserIdea;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing UserIdea in entity cache.
 *
 * @author Engineering
 * @see UserIdea
 * @generated
 */
public class UserIdeaCacheModel implements CacheModel<UserIdea>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{ideaId=");
		sb.append(ideaId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", role=");
		sb.append(role);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserIdea toEntityModel() {
		UserIdeaImpl userIdeaImpl = new UserIdeaImpl();

		userIdeaImpl.setIdeaId(ideaId);
		userIdeaImpl.setUserId(userId);

		if (role == null) {
			userIdeaImpl.setRole(StringPool.BLANK);
		}
		else {
			userIdeaImpl.setRole(role);
		}

		userIdeaImpl.resetOriginalValues();

		return userIdeaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ideaId = objectInput.readLong();
		userId = objectInput.readLong();
		role = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ideaId);
		objectOutput.writeLong(userId);

		if (role == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(role);
		}
	}

	public long ideaId;
	public long userId;
	public String role;
}