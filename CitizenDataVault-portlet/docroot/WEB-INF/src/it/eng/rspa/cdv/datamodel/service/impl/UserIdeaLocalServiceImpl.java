/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.impl;

import java.util.ArrayList;
import java.util.List;

import it.eng.rspa.cdv.datamodel.model.UserIdea;
import it.eng.rspa.cdv.datamodel.service.base.UserIdeaLocalServiceBaseImpl;

/**
 * The implementation of the user idea local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.cdv.datamodel.service.UserIdeaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author SIRCHIA ANTONINO
 * @see it.eng.rspa.cdv.datamodel.service.base.UserIdeaLocalServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.UserIdeaLocalServiceUtil
 */
public class UserIdeaLocalServiceImpl extends UserIdeaLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.cdv.datamodel.service.UserIdeaLocalServiceUtil} to access the user idea local service.
	 */
	
	public List<UserIdea> getUserIdeaByUserId(long userid){
		try{ return userIdeaPersistence.findByuser(userid); }
		catch(Exception e){
			return new ArrayList<UserIdea>();
		}
	}
	
	public List<UserIdea> getUserIdeaByUserIdAndRole(long userid, String role){
		try{ return userIdeaPersistence.findByuserRole(userid, role); }
		catch(Exception e){
			return new ArrayList<UserIdea>();
		}
	}
	
	public List<UserIdea> getUserIdeaByIdeaId(long ideaid){
		try{ return userIdeaPersistence.findByideaId(ideaid); }
		catch(Exception e){
			return new ArrayList<UserIdea>();
		}
	}
	
}