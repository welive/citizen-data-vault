/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import it.eng.rspa.cdv.datamodel.model.CustomDataTag;
import it.eng.rspa.cdv.datamodel.service.CustomDataTagLocalServiceUtil;

/**
 * The extended model base implementation for the CustomDataTag service. Represents a row in the &quot;cdv_CustomDataTag&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CustomDataTagImpl}.
 * </p>
 *
 * @author Engineering
 * @see CustomDataTagImpl
 * @see it.eng.rspa.cdv.datamodel.model.CustomDataTag
 * @generated
 */
public abstract class CustomDataTagBaseImpl extends CustomDataTagModelImpl
	implements CustomDataTag {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a custom data tag model instance should use the {@link CustomDataTag} interface instead.
	 */
	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CustomDataTagLocalServiceUtil.addCustomDataTag(this);
		}
		else {
			CustomDataTagLocalServiceUtil.updateCustomDataTag(this);
		}
	}
}