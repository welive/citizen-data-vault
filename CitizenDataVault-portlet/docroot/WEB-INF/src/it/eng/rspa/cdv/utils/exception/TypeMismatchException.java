package it.eng.rspa.cdv.utils.exception;

public class TypeMismatchException extends CustomException {

	private static final long serialVersionUID = 3L;
	
	public TypeMismatchException(String message){
		super(message);
		setMessageNumber(3L);
	}

}