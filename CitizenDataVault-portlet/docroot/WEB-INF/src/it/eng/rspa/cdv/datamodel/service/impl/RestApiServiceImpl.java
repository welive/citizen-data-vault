/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.impl;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.ac.AccessControlled;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

import it.eng.rspa.cdv.conf.ConfUtils;
import it.eng.rspa.cdv.datamodel.model.CustomDataEntry;
import it.eng.rspa.cdv.datamodel.model.CustomDataTag;
import it.eng.rspa.cdv.datamodel.model.Language;
import it.eng.rspa.cdv.datamodel.model.Preference;
import it.eng.rspa.cdv.datamodel.model.SkillCdv;
import it.eng.rspa.cdv.datamodel.model.Tag;
import it.eng.rspa.cdv.datamodel.model.UsedApplication;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import it.eng.rspa.cdv.datamodel.model.UserIdea;
import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.model.UserPreference;
import it.eng.rspa.cdv.datamodel.model.UserSkill;
import it.eng.rspa.cdv.datamodel.service.CustomDataEntryLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.RestApiService;
import it.eng.rspa.cdv.datamodel.service.SkillCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.TagLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserIdeaLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserLanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserPreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.base.RestApiServiceBaseImpl;
import it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK;
import it.eng.rspa.cdv.utils.PlatformUserSynchronizer;
import it.eng.rspa.cdv.utils.RestApiUtils;
import it.eng.rspa.cdv.utils.Utils;
import it.eng.rspa.cdv.utils.exception.CustomException;
import it.eng.rspa.cdv.utils.exception.InternalErrorException;
import it.eng.rspa.cdv.utils.exception.InvalidInputException;
import it.eng.rspa.cdv.utils.exception.MissingParameterException;
import it.eng.rspa.cdv.utils.exception.TypeMismatchException;
import it.eng.rspa.cdv.utils.exception.WrongCcuseridException;
import it.eng.rspa.cdv.utils.model.AddUserBean;
import it.eng.rspa.cdv.utils.model.CustomEmployementBean;
import it.eng.rspa.cdv.utils.model.Gender;
import it.eng.rspa.cdv.utils.model.IndexedUserDataBean;
import it.eng.rspa.cdv.utils.model.Location;
import it.eng.rspa.cdv.utils.model.OutApp;
import it.eng.rspa.cdv.utils.model.OutSkill;
import it.eng.rspa.cdv.utils.model.PilotId;
import it.eng.rspa.cdv.utils.model.PushEvent;
import it.eng.rspa.cdv.utils.model.PushModel;
import it.eng.rspa.cdv.utils.model.UpdateUserBean;
import it.eng.rspa.cdv.utils.model.UserDataBean;
import it.eng.rspa.cdv.utils.model.UserMetadata;

/**
 * The implementation of the rest api remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.cdv.datamodel.service.RestApiService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author SIRCHIA ANTONINO
 * @see it.eng.rspa.cdv.datamodel.service.base.RestApiServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.RestApiServiceUtil
 */
public class RestApiServiceImpl extends RestApiServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.cdv.datamodel.service.RestApiServiceUtil} to access the rest api remote service.
	 */
	
	private static Log _log = LogFactoryUtil.getLog(RestApiService.class);
	private static boolean platformSync = Boolean.parseBoolean(PortletProps.get("platform.synchronization.active"));
	private static final Set<User> allowedBasicAuthUsers;
	
	static{
		allowedBasicAuthUsers = new HashSet<User>();
		try {
			User u = UserLocalServiceUtil.getUserByEmailAddress(Utils.getGenericPortalCompanyId(), "welive@welive.eu");
			allowedBasicAuthUsers.add(u);
		} 
		catch (Exception e) { e.printStackTrace(); }
		
	}
	
	private static boolean isBasicAuthorized(User u){
		return allowedBasicAuthUsers.contains(u);
	}
	
	/** DataSource Oriented **/
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/adduser")
	public JSONObject addUser(String body) {
		
		System.out.println("Adding user");
		System.out.println("\n"+body+"\n");
		
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		try { 
			JSONObject jin = null;
			if(!isBasicAuthorized(this.getUser())){
				throw new InternalErrorException("The user "+this.getUser().getFullName() +" is not authorized to invoke this API");
			}
			AddUserBean aub = null;
			try{
				jin = JSONFactoryUtil.createJSONObject(body);
				
				Gson gson = new Gson();
				aub = gson.fromJson(body, AddUserBean.class);
			}
			catch(JsonSyntaxException jse){
				throw new InvalidInputException(jse.getMessage());
			}
			catch(NumberFormatException | IllegalStateException tme){
				throw new TypeMismatchException(tme.getMessage());
			}
			catch(Exception e){
				throw new InternalErrorException(e.getMessage());
			}
			aub.check(jin);
			List<UserCdv> prev = userCdvPersistence.findByccuid(aub.getCcUserID());
			if(!prev.isEmpty()){
				throw new WrongCcuseridException("User with ccUserID "+aub.getCcUserID()+" already registered with the CDV");
			}
			
			UserCdvPK pk = new UserCdvPK(aub.getCcUserID(), aub.getId());
			UserCdv newUser = UserCdvLocalServiceUtil.createUserCdv(pk);
					newUser.setUsername(aub.getLiferayScreenName());
					newUser.setAddress(aub.getAddress());
					newUser.setCity(aub.getCity());
					newUser.setCountry(aub.getCountry());
					newUser.setZipcode(aub.getZipCode());
					newUser.setEmail(aub.getEmail());
					if(Validator.isNotNull(aub.getIsMale())){
						newUser.setGender(aub.getIsMale() ? Gender.MALE.toString() : Gender.FEMALE.toString());
					}
					newUser.setPilot(aub.getReferredPilot());
					newUser.setName(aub.getFirstName());
					newUser.setSurname(aub.getLastName());
					newUser.setReputation(0L);
					newUser.setLastKnownLatitude(0.0);
					newUser.setLastKnownLongitude(0.0);
					newUser.setCompanyId(Utils.getGenericPortalCompanyId());
					
					if(Validator.isNotNull(aub.getBirthDate())){
						newUser.setBirthdate(aub.getBirthDate().getDate());
					}
					
					String employement = "";
					if(Validator.isNotNull(aub.getEmployement())){
						employement = aub.getEmployement().toString();
					}
					newUser.setEmployement(employement);
					
			UserCdvLocalServiceUtil.addUserCdv(newUser);
			//Languages handler
			Set<String> langs = aub.getLanguages();
			if(langs!=null){
				for(String lang:langs){
					String currlang = lang.trim().toLowerCase();
					Language dtoLang = RestApiUtils.getLanguage(currlang);
					RestApiUtils.associateUserLanguage(newUser, dtoLang);
				}
			}
			//Preference handler
			Set<String> preferences = aub.getUserTags();
			if(preferences!=null){
				for(String preference:preferences){
					String currpref = preference.trim().toLowerCase();
					Preference dtoPref = RestApiUtils.getPreference(currpref);
					RestApiUtils.associateUserPreference(newUser, dtoPref);
				}
			}
			response.put("response", 0);
			response.put("message", 0);
		}
		catch(CustomException ce){
			ce.printStackTrace();
			response.put("message", ce.getMessageNumber());
			response.put("text", ce.getMessage());
			response.put("response", 1);
		}
		catch (Exception e) {
			e.printStackTrace();
			if(!response.has("message"))
				response.put("message", 4);
			
			if(!response.has("text"))
				response.put("text", e.getMessage());
			
			response.put("response", 1);
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/updateuser")
	public JSONObject updateUser(String body){
		JSONObject response = JSONFactoryUtil.createJSONObject();
		try { 
			if(!isBasicAuthorized(this.getUser())){
				throw new InternalErrorException("The user "+this.getUser().getFullName() +" is not authorized to invoke this API");
			}
			
			System.out.println(body);
			JSONObject jin = null;
			UpdateUserBean uub = null;
			
			try{
				jin = JSONFactoryUtil.createJSONObject(body);
				
				Gson gson = new Gson();
				uub = gson.fromJson(body, UpdateUserBean.class);
				
			}
			catch(JsonSyntaxException jse){
				throw new InvalidInputException(jse.getMessage());
			}
			catch(NumberFormatException | IllegalStateException tme){
				throw new TypeMismatchException(tme.getMessage());
			}
			catch(Exception e){
				throw new InternalErrorException(e.getMessage());
			}
			
			if(Validator.isNull(uub)){
				throw new InvalidInputException("Unable parse the input");
			}
			
			uub.check(jin);
			
			List<UserCdv> list = userCdvPersistence.findByccuid(uub.getCcUserID());
			if(list==null || list.isEmpty()){
				throw new WrongCcuseridException("The ccUserID "+ uub.getCcUserID() +" is not valid");
			}
				
			UserCdv _user = list.get(0);
			
			if(Validator.isNotNull(uub.getBirthDate())){
				
				if(platformSync)
					PlatformUserSynchronizer.setBirthdate(_user.getUserId(), uub.getBirthDate().getDate());
				
				_user.setBirthdate(uub.getBirthDate().getDate());
				
			}
			
			if(Validator.isNotNull(uub.getAddress())){
				_user.setAddress(uub.getAddress());
			}
			
			if(Validator.isNotNull(uub.getCity())){
				_user.setCity(uub.getCity());
			}
			
			if(Validator.isNotNull(uub.getCountry())){
				_user.setCountry(uub.getCountry());
			}
			
			if(Validator.isNotNull(uub.getZipCode())){
				_user.setZipcode(uub.getZipCode());
			}
			
			if(Validator.isNotNull(uub.getIsMale())){
				if(uub.getIsMale()){
					_user.setGender(Gender.MALE.toString());  
				}
				else{
					_user.setGender(Gender.FEMALE.toString());
				}
			}
			
			if(Validator.isNotNull(uub.getIsDeveloper())){
				
				if(platformSync)
					PlatformUserSynchronizer.isDeveloper(_user.getUserId(), uub.getIsDeveloper());
				
				_user.setIsDeveloper(uub.getIsDeveloper());  
			}
			
			if(Validator.isNotNull(uub.getName())){
				if(platformSync)
					PlatformUserSynchronizer.setName(_user.getUserId(), uub.getName());
				
				_user.setName(uub.getName());
			}
			
			if(Validator.isNotNull(uub.getSurname())){
				
				if(platformSync)
					PlatformUserSynchronizer.setSurname(_user.getUserId(), uub.getSurname());
				
				_user.setSurname(uub.getSurname());
			}
			
			if(Validator.isNotNull(uub.getLanguages())){
				
				//Remove all previous languages
				List<UserLanguage> uls = userLanguagePersistence.findByuserId(_user.getUserId());
				if(uls!=null){
					for(UserLanguage ul : uls){
						UserLanguageLocalServiceUtil.deleteUserLanguage(ul);
					}
				}
				
				//Add all current languages
				Set<String> langs = uub.getLanguages();
				for(String lang:langs){
					
					String currlang = lang.trim().toLowerCase();
					Language dtoLang = RestApiUtils.getLanguage(currlang);
					
					RestApiUtils.associateUserLanguage(_user, dtoLang);
				}
			}
			
			if(Validator.isNotNull(uub.getUserTags())){

				//Remove all previous preferences
				List<UserPreference> ups = userPreferencePersistence.findByuserId(_user.getUserId());
				if(ups!=null){
					for(UserPreference up : ups){
						UserPreferenceLocalServiceUtil.deleteUserPreference(up);
					}
				}
				
				//Add all current preferences
				Set<String> prefs = uub.getUserTags();
				for(String pref:prefs){
					
					String currpref = pref.trim().toLowerCase();
					Preference dtoPref = RestApiUtils.getPreference(currpref);
					
					RestApiUtils.associateUserPreference(_user, dtoPref);
				}
			}
			
			UserCdvLocalServiceUtil.updateUserCdv(_user);
			
			response.put("response", 0);
			response.put("message", 0);
		}
		catch(CustomException ce){
			response.put("message", ce.getMessageNumber());
			response.put("text", ce.getMessage());
			response.put("response", 1);
		}
		catch (Exception e) {
			e.printStackTrace();
			if(!response.has("message"))
				response.put("message", 4);
			
			if(!response.has("text"))
				response.put("text", e.getMessage());
			
			response.put("response", 1);
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/push")
	public JSONObject push(String body){
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		Type type = new TypeToken<Set<PushModel>>() {}.getType();
		Set<PushModel> input = new Gson().fromJson(body, type);
		for(PushModel pm : input){
			try{
				PushEvent pe = PushEvent.valueOf(pm.getEventName());
				RestApiUtils.pushEventHandler(pm);
				response.put("response", 0);
				response.put("message", 0);
				
			}
			catch(CustomException ce){
				response.put("response", "1");
				response.put("message", ce.getMessageNumber());
				response.put("text", ce.getMessage());
			}
			catch(Exception e){
				response.put("response", "1");
				response.put("message", 4L);
				response.put("text", e.getMessage());
			}
		}
		
		return response;
	}
	
	
	/** DataConsumer Oriented **/
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getusersskills")
	public JSONArray getUsersSkills(){
		
		JSONArray response = JSONFactoryUtil.createJSONArray();
		
		try {
			List<UserCdv> users = UserCdvLocalServiceUtil.getUserCdvs(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
			for(UserCdv user : users){
				
				try{
				
					//The item for a specific user
					JSONObject json = JSONFactoryUtil.createJSONObject();
							json.put("ccUserID", user.getCcuid());
					
					//Languages array
					JSONArray langsArr = JSONFactoryUtil.createJSONArray();
					try{
						List<UserLanguage> uls = userLanguagePersistence.findByuserId(user.getUserId());
						for(UserLanguage ul : uls){
							try{
								Language l = languagePersistence.fetchByPrimaryKey(ul.getLanguageId());
								langsArr.put(l.getName());
							}
							catch(Exception e){
								_log.warn(e.getClass().getSimpleName()+": "+e.getMessage());
								//Skip this language
							}
						}
					}
					catch(Exception e){
						_log.warn(e.getClass().getSimpleName()+": "+e.getMessage());
						
						//No lang found for this user
						//Skip the languages for this user
					}
					
					json.put("languages", langsArr);
					
	
					//Skills array
					JSONArray skillsArr = JSONFactoryUtil.createJSONArray();
					
					try{
						
						List<UserSkill> uss = userSkillPersistence.findByuserId(user.getUserId());
						for(UserSkill us:uss){
							try{
								JSONObject skilljson = JSONFactoryUtil.createJSONObject();
								SkillCdv s = SkillCdvLocalServiceUtil.getSkillCdv(us.getSkillId());
								skilljson.put("skillName", s.getSkillname());
								skilljson.put("endorsementCounter", us.getEndorsmentCounter());
								
								skillsArr.put(skilljson);
							}
							catch(Exception e){
								//Skip this skill
								_log.warn(e.getClass().getSimpleName()+": "+e.getMessage());
							}
						}
					}
					catch(Exception e){
						//No skill found for this user
						//Skip the skills for this user
						_log.warn(e.getClass().getSimpleName()+": "+e.getMessage());
					}
					
					json.put("skills", skillsArr);
					
					response.put(json);
				}
				catch(Exception e){
					_log.warn(e.getClass().getSimpleName()+": "+e.getMessage());
					//Somenthing goes wrong with this user
					//Skip this user
				}
			}
		}
		catch (Exception e) {
			//Unable to retrieve the list of all users
			e.printStackTrace();
			
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("response", 1);
			json.put("message", 5);
			json.put("text", "Error: "+e.getMessage());
			
			response.put(json);
		
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getusermetadata")
	public JSONObject getUserMetadata(String ccuserid) {
		JSONObject json = null;
		
		try{
			UserMetadata meta = new UserMetadata(); 
			UserCdv ucdv = Utils.getUserCdvByCcUserId(Long.parseLong(ccuserid));
			
			meta.setAddress(ucdv.getAddress());
			if(ucdv.getBirthdate()!=null){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
				meta.setBirthdate(sdf.format(ucdv.getBirthdate()));
			}
			else{
				meta.setBirthdate(null);
			}
			meta.setCity(ucdv.getCity());
			meta.setLastKnownLocation(new Location(String.valueOf(ucdv.getLastKnownLatitude()), String.valueOf(ucdv.getLastKnownLongitude())));
			
			//Retrieve skills
			try{
				List<UserSkill> uss = userSkillPersistence.findByuserId(ucdv.getUserId());
				for(UserSkill us : uss){
					try{
						OutSkill os = new OutSkill();
						os.setEndorsementCounter(us.getEndorsmentCounter());
						os.setSkillName(skillCdvPersistence.findByPrimaryKey(us.getSkillId()).getSkillname());
						
						meta.getSkills().add(os);
					}
					catch(Exception e){
						/* Skip this skill */
					}
				}
			}
			catch(Exception e){
				/* Skip the skill section */
			}
			
			//Retrieve usedApps
			try{
				List<UsedApplication> uas = usedApplicationPersistence.findByuserId(ucdv.getUserId());
				for(UsedApplication ua : uas){
					try{
						OutApp oa = new OutApp();
						oa.setAppID(ua.getAppId());
						oa.setAppName(applicationPersistence.findByPrimaryKey(ua.getAppId()).getAppName());
						
						meta.getUsedApps().add(oa);
					}
					catch(Exception e){
						/* Skip this app */
					}
				}
			}
			catch(Exception e){
				/* Skip the usedApps section */
			}
			
			
			//Retrieve preferences
			try{
				List<UserPreference> ups = userPreferencePersistence.findByuserId(ucdv.getUserId());
				for(UserPreference up : ups){
					try{
						String prefname = preferencePersistence.findByPrimaryKey(up.getPreferenceId()).getName();
						
						meta.getUserTags().add(prefname);
					}
					catch(Exception e){
						/* Skip this app */
					}
				}
			}
			catch(Exception e){
				/* Skip the usedApps section */
			}
			
			Gson gson = new Gson();
			String sJson = gson.toJson(meta);
			try{ json = JSONFactoryUtil.createJSONObject(sJson); }
			catch(Exception e){
				throw new InternalErrorException(e.getMessage());
			}
		}
		catch(CustomException ce){
			ce.printStackTrace();
			
			json = JSONFactoryUtil.createJSONObject();
			json.put("message", ce.getMessageNumber());
			json.put("response", 1);
			json.put("text", ce.getMessage());
		}
		catch(Exception ge){
			ge.printStackTrace();
		}
		
		return json;
	}
	
	//Mapped on swagger
	//API Oauth Protected
	@JSONWebService(value="/cdv/getuserprofile")
	@AccessControlled(guestAccessEnabled=true)
	public JSONObject getUserProfile(String uid){
		
		JSONObject response = Utils.getUserProfile(uid);
		return response;
		
	}
	
	
	/** Statistics Oriented **/
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getkpi11")
	public JSONObject getKPI11(String pilotid, String from, String to) {
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		if(Validator.isNull(pilotid)){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", "Error: Invalid pilotID");
			
			return response;
		}
		
		try{
			String pilot = "all";
			List<UserCdv> users = null;
			if(!pilotid.equalsIgnoreCase("all")){
				pilot = PilotId.valueOf(pilotid.toLowerCase()).toString();
				
				System.out.println("Pilot: "+pilot);
				
				users = UserCdvLocalServiceUtil.getUserCdvByPilot(pilot);
			}
			else{ users = UserCdvLocalServiceUtil.getUserCdvs(QueryUtil.ALL_POS, QueryUtil.ALL_POS); }
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);
			Date dFrom = null;
			if(from!=null && !from.trim().equalsIgnoreCase("")){
				dFrom = sdf.parse(from);
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dFrom);
				
				d.getTime();
			}
			
			Date dTo = null;
			if(to!=null && !to.trim().equalsIgnoreCase("")){
				dTo = sdf.parse(to);
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dTo);
				
				d.getTime();
			}
			
			for(UserCdv u : users){
				
				try{
				
					long uid = u.getUserId();
					User lifUser = UserLocalServiceUtil.getUser(uid);
					Date registrationDate = lifUser.getCreateDate();
					
					if(dFrom!=null && !registrationDate.after(dFrom)){
						continue;
					}
					
					if(dTo!=null && !registrationDate.before(dTo)){
						continue;
					}
					
					List<UserIdea> uis = UserIdeaLocalServiceUtil.getUserIdeaByUserId(uid);
					if(uis.isEmpty()){
						continue;
					}
					
					if(response.has(u.getPilot())){
						response.put(u.getPilot(), response.getInt(u.getPilot())+1);
					}
					else{ response.put(u.getPilot(), 1); }
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
				
			}
			
			for(PilotId pid : PilotId.values()){
				try{
					String spid = pid.toString();
					if(	!response.has(spid) && ( pilot.equalsIgnoreCase("all") || pilot.equalsIgnoreCase(spid)	)){ 
						response.put(spid, 0);
					}
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		}
		catch(Exception e){
			response.put("response", 1);
			response.put("message", 4);
			response.put("text", e.getMessage());
			
			return response;
		}
		
		return response;
		
	}		
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getkpi43")
	public JSONObject getKPI43(String pilotid, String from, String to){
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		if(Validator.isNull(pilotid)){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", "Error: Invalid pilotID");
			
			return response;
		}
		
		try{
			String pilot = "all";
			List<UserCdv> users = null;
			if(!pilotid.equalsIgnoreCase("all")){
				pilot = PilotId.valueOf(pilotid.toLowerCase()).toString();
				users = UserCdvLocalServiceUtil.getUserCdvByPilot(pilot);
			}
			else{ users = UserCdvLocalServiceUtil.getUserCdvs(QueryUtil.ALL_POS, QueryUtil.ALL_POS); }
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);
			Date dFrom = null;
			if(from!=null && !from.trim().equalsIgnoreCase("")){
				dFrom = sdf.parse(from);
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dFrom);
				
				d.getTime();
			}
			
			Date dTo = null;
			if(to!=null && !to.trim().equalsIgnoreCase("")){
				dTo = sdf.parse(to);
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dTo);
				
				d.getTime();
			}
			
			for(UserCdv u : users){
				try{
					long uid = u.getUserId();
					User lifUser = UserLocalServiceUtil.getUser(uid);
					Date registrationDate = lifUser.getCreateDate();
					
					if(dFrom!=null && !registrationDate.after(dFrom)){
						continue;
					}
					
					if(dTo!=null && !registrationDate.before(dTo)){
						continue;
					}
					
					if(response.has(u.getPilot())){
						response.put(u.getPilot(), response.getInt(u.getPilot())+1);
					}
					else{ response.put(u.getPilot(), 1); }
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
				
			}
			
			for(PilotId pid : PilotId.values()){
				try{
					String spid = pid.toString();
					if(	!response.has(spid) && ( pilot.equalsIgnoreCase("all") || pilot.equalsIgnoreCase(spid)	)){ 
						response.put(spid, 0);
					}
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			
		}
		catch(Exception e){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", e.getMessage());
			
			return response;
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getkpi111")
	public JSONObject getKPI111(String pilotid){
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		if(Validator.isNull(pilotid)){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", "Error: Invalid pilotID");
			
			return response;
		}
		
		try{
			String pilot = "all";
			List<UserCdv> users = null;
			if(!pilotid.equalsIgnoreCase("all")){
				pilot = PilotId.valueOf(pilotid.toLowerCase()).toString();
				users = UserCdvLocalServiceUtil.getUserCdvByPilot(pilot);
			}
			else{ users = UserCdvLocalServiceUtil.getUserCdvs(QueryUtil.ALL_POS, QueryUtil.ALL_POS); }
			
			
			for(UserCdv u : users){
				try{
					Date birthdate = u.getBirthdate();
					String key = "noanswer";
					
					if(birthdate!=null){
						
						int age = RestApiUtils.getAge(birthdate);
						
						if(age<20){
							key = "<20";
						}
						else if(age<40){
							key = "20-40";
						}
						else if(age<60){
							key = "40-60";
						}
						else if(age>=60){
							key = ">60";
						}
						
					}
					
					JSONObject pj = JSONFactoryUtil.createJSONObject();
					if(response.has(u.getPilot())){
						pj = response.getJSONObject(u.getPilot());
						if(pj.has(key)){ pj.put(key, pj.getInt(key)+1); }
						else{ pj.put(key, 1); }
					}
					else{ pj.put(key, 1); }
					response.put(u.getPilot(), pj);	
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			
			//Fulfill missing values			
			Iterator<String> ks = response.keys();
			while(ks.hasNext()){
				try{
					String k = ks.next();
					JSONObject jv = response.getJSONObject(k);
					
					if(!jv.has("<20")){
						jv.put("<20", 0);
					}
					if(!jv.has("20-40")){
						jv.put("20-40", 0);
					}
					if(!jv.has("40-60")){
						jv.put("40-60", 0);
					}
					if(!jv.has(">60")){
						jv.put(">60", 0);
					}
					if(!jv.has("noanswer")){
						jv.put("noanswer", 0);
					}
					
					response.put(k, jv);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			
			JSONObject emptypj = JSONFactoryUtil.createJSONObject();
					emptypj.put("<20", 0);
					emptypj.put("20-40", 0);
					emptypj.put("40-60", 0);
					emptypj.put(">60", 0);
					emptypj.put("noanswer", 0);
			
			for(PilotId pid : PilotId.values()){
				try{
					String spid = pid.toString();
					if(	!response.has(spid) && ( pilot.equalsIgnoreCase("all") || pilot.equalsIgnoreCase(spid)	)){ 
						response.put(spid, emptypj);
					}
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			
		}
		catch(Exception e){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", e.getMessage());
			
			return response;
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getkpi112")
	public JSONObject getKPI112(String pilotid) {
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		if(Validator.isNull(pilotid)){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", "Error: Invalid pilotID");
			
			return response;
		}
		
		try{
			String pilot = "all";
			List<UserCdv> users = null;
			if(!pilotid.equalsIgnoreCase("all")){
				pilot = PilotId.valueOf(pilotid.toLowerCase()).toString();
				users = UserCdvLocalServiceUtil.getUserCdvByPilot(pilot);
			}
			else{ users = UserCdvLocalServiceUtil.getUserCdvs(QueryUtil.ALL_POS, QueryUtil.ALL_POS); }
			
			for(UserCdv u : users){
				try{
					String gender = u.getGender().toLowerCase();
					if(gender.trim().equalsIgnoreCase("")){
						gender = "noanswer";
					}
					JSONObject sj = JSONFactoryUtil.createJSONObject();
					
					if(response.has(u.getPilot())){
						sj = response.getJSONObject(u.getPilot());
						if(sj.has(gender)){ sj.put(gender, sj.getInt(gender)+1); }
						else{ sj.put(gender, 1); }
						
					}
					else{ sj.put(gender, 1); }
					
					response.put(u.getPilot(), sj);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			
			//Fulfill missing values			
			Iterator<String> ks = response.keys();
			while(ks.hasNext()){
				try{
					String k = ks.next();
					JSONObject jv = response.getJSONObject(k);
					
					if(!jv.has("male")){
						jv.put("male", 0);
					}
					if(!jv.has("female")){
						jv.put("female", 0);
					}
					if(!jv.has("noanswer")){
						jv.put("noanswer", 0);
					}
					response.put(k, jv);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			
			JSONObject emptypj = JSONFactoryUtil.createJSONObject();
					emptypj.put("male", 0);
					emptypj.put("female", 0);
					emptypj.put("noanswer", 0);
			
			for(PilotId pid : PilotId.values()){
				try{	
					String spid = pid.toString();
					if(	!response.has(spid) && ( pilot.equalsIgnoreCase("all") || pilot.equalsIgnoreCase(spid)	)){ 
						response.put(spid, emptypj);
					}
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			
		}
		catch(Exception e){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", e.getMessage());
			
			return response;
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getkpi113")
	public JSONObject getKPI113(String pilotid, String from, String to) {
		System.out.println("Getting KPI 11.3");
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		if(Validator.isNull(pilotid)){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", "Error: Invalid pilotID");
			
			return response;
		}
		
		try{
			String pilot = "all";
			List<UserCdv> users = null;
			if(!pilotid.equalsIgnoreCase("all")){
				pilot = PilotId.valueOf(pilotid.toLowerCase()).toString();
				
				System.out.println("Pilot: "+pilot);
				
				users = UserCdvLocalServiceUtil.getUserCdvByPilot(pilot);
			}
			else{ users = UserCdvLocalServiceUtil.getUserCdvs(QueryUtil.ALL_POS, QueryUtil.ALL_POS); }

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);
			Date dFrom = null;
			if(from!=null && !from.trim().equalsIgnoreCase("")){
				dFrom = sdf.parse(from);
				
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dFrom);
				
				d.getTime();
			}
			
			Date dTo = null;
			if(to!=null && !to.trim().equalsIgnoreCase("")){
				dTo = sdf.parse(to);
				
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dTo);
				
				d.getTime();
			}
			
			JSONObject pilotitemvalue = JSONFactoryUtil.createJSONObject();
						pilotitemvalue.put("noanswer", 0);
						pilotitemvalue.put("Student", 0);
						pilotitemvalue.put("Unemployed", 0);
						pilotitemvalue.put("Employed by third party", 0);
						pilotitemvalue.put("Self-employed / Entrepreneur", 0);
						pilotitemvalue.put("Retired", 0);
						pilotitemvalue.put("Other", 0);
			
			for(UserCdv u : users){
				try{
					long uid = u.getUserId();
					User lifUser = UserLocalServiceUtil.getUser(uid);
					Date registrationDate = lifUser.getCreateDate();
					
					if(dFrom!=null && !registrationDate.after(dFrom)){
						continue;
					}
					
					if(dTo!=null && !registrationDate.before(dTo)){
						continue;
					}
					
					String emp = u.getEmployement();
					String k = "Other";
					if(emp == null || emp.trim().equalsIgnoreCase("")){
						k = "noanswer";
					}
					else{
						try{
							k = CustomEmployementBean.valueOf(emp.toLowerCase()).toString();
						}
						catch(Exception e){
							System.out.println("Error: "+e);
							/* Do nothing */
						}
					}
					
					if(!response.has(u.getPilot())){
						response.put(u.getPilot(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
					}
					
					int prev_val = response.getJSONObject(u.getPilot()).getInt(k);
					int new_val = prev_val +1;
					
					response.put(u.getPilot(), response.getJSONObject(u.getPilot()).put(k, new_val));
					
				}
				catch(Exception e){
					System.out.println("Error: "+e);
				}
				
			}
			
			if(pilotid.equalsIgnoreCase("all")){
				for(PilotId pid : PilotId.values()){
					if(!response.has(pid.toString())){
						response.put(pid.toString(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
					}
				}
			}
			else{
				if(!response.has(PilotId.valueOf(pilotid.trim().toLowerCase()).toString())){
					response.put(PilotId.valueOf(pilotid.trim().toLowerCase()).toString(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
				}
			}
			
		}
		catch(Exception e){
			System.out.println("Error: "+e);
			
			response = JSONFactoryUtil.createJSONObject();
			response.put("message", 6);
			response.put("response", 1);
			response.put("text", e.getMessage());
		}
		
		return response;
		
	}
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getkpi114")
	public JSONObject getKPI114(String pilotid, String from, String to) {
		System.out.println("Getting KPI 11.4");
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		if(Validator.isNull(pilotid)){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", "Error: Invalid pilotID");
			
			return response;
		}
		
		try{
			String pilot = "all";
			List<UserCdv> users = null;
			if(!pilotid.equalsIgnoreCase("all")){
				pilot = PilotId.valueOf(pilotid.toLowerCase()).toString();
				
				System.out.println("Pilot: "+pilot);
				
				users = UserCdvLocalServiceUtil.getUserCdvByPilot(pilot);
			}
			else{ users = UserCdvLocalServiceUtil.getUserCdvs(QueryUtil.ALL_POS, QueryUtil.ALL_POS); }

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);
			Date dFrom = null;
			if(from!=null && !from.trim().equalsIgnoreCase("")){
				dFrom = sdf.parse(from);
				
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dFrom);
				
				d.getTime();
			}
			
			Date dTo = null;
			if(to!=null && !to.trim().equalsIgnoreCase("")){
				dTo = sdf.parse(to);
				
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dTo);
				
				d.getTime();
			}
			
			JSONObject pilotitemvalue = JSONFactoryUtil.createJSONObject();
						pilotitemvalue.put("Citizen", 0);
						pilotitemvalue.put("Academy", 0);
						pilotitemvalue.put("Business", 0);
						pilotitemvalue.put("Entrepreneur", 0);

			Role rCitizen = ConfUtils.getRoleCitizen();
			Role rAcademy = ConfUtils.getRoleAcademy();
			Role rBusiness = ConfUtils.getRoleBusiness();
			Role rEntrepreneur = ConfUtils.getRoleEntrepreneur();

			for(UserCdv u : users){
				try{
					long uid = u.getUserId();
					User lifUser = UserLocalServiceUtil.getUser(uid);
					Date registrationDate = lifUser.getCreateDate();
					
					if(dFrom!=null && !registrationDate.after(dFrom)){
						continue;
					}
					
					if(dTo!=null && !registrationDate.before(dTo)){
						continue;
					}
					
					String k = null;
					List<Role> userRoles = lifUser.getRoles();
					if(userRoles.contains(rCitizen)){
						k = rCitizen.getName();
					}
					else if(userRoles.contains(rAcademy)){
						k = rAcademy.getName();
					}
					else if(userRoles.contains(rBusiness)){
						k = rBusiness.getName();
					}
					else if(userRoles.contains(rEntrepreneur)){
						k = rEntrepreneur.getName();
					}
					
					if(!response.has(u.getPilot())){
						response.put(u.getPilot(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
					}
					
					int prev_val = response.getJSONObject(u.getPilot()).getInt(k);
					int new_val = prev_val +1;
					
					response.put(u.getPilot(), response.getJSONObject(u.getPilot()).put(k, new_val));
					
				}
				catch(Exception e){
					System.out.println("Error: "+e);
				}
				
			}
			
			if(pilotid.equalsIgnoreCase("all")){
				for(PilotId pid : PilotId.values()){
					if(!response.has(pid.toString())){
						response.put(pid.toString(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
					}
				}
			}
			else{
				if(!response.has(PilotId.valueOf(pilotid.trim().toLowerCase()).toString())){
					response.put(PilotId.valueOf(pilotid.trim().toLowerCase()).toString(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
				}
			}
			
			
		}
		catch(Exception e){
			
			System.out.println("Error: "+e);
			
			response = JSONFactoryUtil.createJSONObject();
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", e.getMessage());
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Basic Authentication
	@JSONWebService(value="/cdv/getkpi115")
	public JSONObject getKPI115(String pilotid, String from, String to) {
		
		System.out.println("Getting KPI 11.5");
		
		JSONObject response = JSONFactoryUtil.createJSONObject();
		
		if(Validator.isNull(pilotid)){
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", "Error: Invalid pilotID");
			
			return response;
		}
		
		try{
			String pilot = "all";
			List<UserCdv> users = null;
			if(!pilotid.equalsIgnoreCase("all")){
				pilot = PilotId.valueOf(pilotid.toLowerCase()).toString();
				
				System.out.println("Pilot: "+pilot);
				
				users = UserCdvLocalServiceUtil.getUserCdvByPilot(pilot);
			}
			else{ users = UserCdvLocalServiceUtil.getUserCdvs(QueryUtil.ALL_POS, QueryUtil.ALL_POS); }

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);
			Date dFrom = null;
			if(from!=null && !from.trim().equalsIgnoreCase("")){
				dFrom = sdf.parse(from);
				
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dFrom);
				
				d.getTime();
			}
			
			Date dTo = null;
			if(to!=null && !to.trim().equalsIgnoreCase("")){
				dTo = sdf.parse(to);
				
				Calendar d = GregorianCalendar.getInstance();
				d.setLenient(false);
				d.clear();
				d.setTime(dTo);
				
				d.getTime();
			}
			
			JSONObject pilotitemvalue = JSONFactoryUtil.createJSONObject();
						pilotitemvalue.put("Yes", 0);
						pilotitemvalue.put("No", 0);

			Role rDeveloper = ConfUtils.getRoleDeveloper();

			for(UserCdv u : users){
				try{
					long uid = u.getUserId();
					User lifUser = UserLocalServiceUtil.getUser(uid);
					Date registrationDate = lifUser.getCreateDate();
					
					if(dFrom!=null && !registrationDate.after(dFrom)){
						continue;
					}
					
					if(dTo!=null && !registrationDate.before(dTo)){
						continue;
					}
					
					String k = null;
					List<Role> userRoles = lifUser.getRoles();
					if(userRoles.contains(rDeveloper)){
						k = "Yes";
					}
					else{
						k = "No";
					}
					
					if(!response.has(u.getPilot())){
						response.put(u.getPilot(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
					}
					
					int prev_val = response.getJSONObject(u.getPilot()).getInt(k);
					int new_val = prev_val +1;
					
					response.put(u.getPilot(), response.getJSONObject(u.getPilot()).put(k, new_val));
					
				}
				catch(Exception e){
					System.out.println("Error: "+e);
				}
				
			}
			
			if(pilotid.equalsIgnoreCase("all")){
				for(PilotId pid : PilotId.values()){
					if(!response.has(pid.toString())){
						response.put(pid.toString(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
					}
				}
			}
			else{
				if(!response.has(PilotId.valueOf(pilotid.trim().toLowerCase()).toString())){
					response.put(PilotId.valueOf(pilotid.trim().toLowerCase()).toString(), JSONFactoryUtil.createJSONObject(pilotitemvalue.toString()));
				}
			}
			
			
		}
		catch(Exception e){
			
			System.out.println("Error: "+e);
			
			response = JSONFactoryUtil.createJSONObject();
			response.put("response", 1);
			response.put("message", 6);
			response.put("text", e.getMessage());
		}
		
		return response;	
	}
	
	/** Data Oriented **/
	
	//Mapped on swagger
	//Oauth Authentication
	@JSONWebService(value="/cdv/userdataupdate")
	@AccessControlled(guestAccessEnabled=true)
	public JSONArray userDataUpdate(String body, String uid, String client_id) {
		
		JSONArray response = JSONFactoryUtil.createJSONArray();
		
		try{
			
			long userid = Utils.getUserIdByCcUserId(uid);
			
			Set<IndexedUserDataBean> iudbs = null;
			try{
				Type type = new TypeToken<Set<IndexedUserDataBean>>() {}.getType();
				iudbs = new Gson().fromJson(body, type);
			}
			catch(JsonSyntaxException jse){
				throw new InvalidInputException(jse.getMessage());
			}
			catch(NumberFormatException | IllegalStateException tme){
				throw new TypeMismatchException(tme.getMessage());
			}
			catch(Exception e){
				throw new InternalErrorException(e.getMessage());
			}
			
			for(IndexedUserDataBean iudb : iudbs){
				JSONObject item = JSONFactoryUtil.createJSONObject();
				try{
					if(!iudb.isValid()){
						throw new MissingParameterException("A mandatory field is missing. Please read the documentation.");
					}
					
					item.put("index", iudb.getIndex());
					
					Long dataid = null; 
					if(Validator.isNull(iudb.getDataId())){
						dataid = RestApiUtils.createUserData(iudb, userid, client_id);
					}
					else{
						dataid = RestApiUtils.updateUserData(iudb, userid, client_id);
					}
					
					item.put("dataId", dataid);
					
				}
				catch(CustomException ce){
					
					if(Validator.isNotNull(iudb.getIndex()))
						item.put("index", iudb.getIndex());	
					
					item.put("message", ce.getMessageNumber());
					item.put("response", 1);
					item.put("text", ce.getMessage());
					
				}
				
				response.put(item);
			}
		}
		catch(CustomException ce){
			JSONObject item = JSONFactoryUtil.createJSONObject();
			item.put("message", ce.getMessageNumber());
			item.put("response", 1);
			item.put("text", ce.getMessage());
			
			response.put(item);
		}
		
		return response;
		
	}
	
	//Mapped on swagger
	//Oauth Authentication
	@JSONWebService(value="/cdv/userdataget")
	@AccessControlled(guestAccessEnabled=true)
	public JSONArray getUserData(String dataid, String uid, String client_id) {
		
		System.out.println(dataid+" "+uid+" "+client_id);
		
		JSONArray response = JSONFactoryUtil.createJSONArray();
		Set<UserDataBean> jResp = new HashSet<UserDataBean>();
		
		try{
			if(Validator.isNull(dataid))
				throw new MissingParameterException("The mandatory field dataId is missing");
			
			Long userid = Utils.getUserIdByCcUserId(uid);
			
			if(dataid.equalsIgnoreCase("all")){
				List<CustomDataEntry> ucds = null;
				try{
					ucds = customDataEntryPersistence.findByclientid(client_id);
					if(Validator.isNull(ucds))
						throw new Exception();
				}
				catch(Exception e){
					ucds = new ArrayList<CustomDataEntry>();
				}
				
				for(CustomDataEntry cde : ucds){
					try{
						UserDataBean udb = new UserDataBean();
						udb.setDataId(cde.getEntryId());
						udb.setType(cde.getType());
						udb.getData().setKey(cde.getKey());
						udb.getData().setValue(cde.getValue());
						
						List<CustomDataTag> cdts = null;
						try{ 
							cdts = customDataTagPersistence.findByentryId(cde.getEntryId());
							if(Validator.isNull(cdts))
								throw new Exception();
							
						}
						catch(Exception e){
							cdts = new ArrayList<CustomDataTag>();
						}
						
						for(CustomDataTag cdt : cdts){
							try{
								Tag t = TagLocalServiceUtil.getTag(cdt.getTagid());
								udb.getTags().add(t.getTagname().toLowerCase());
							}
							catch(Exception e){
								_log.error(e.getClass()+": "+e.getMessage());
							}
						}
						
						jResp.add(udb);
					}
					catch(Exception e){
						e.printStackTrace();
						_log.error(e.getClass()+": "+e.getMessage());
					}
				}
			}
			else{
				
				CustomDataEntry cde = null;
				try{ 
					cde = CustomDataEntryLocalServiceUtil.getCustomDataEntry(Long.parseLong(dataid));
					if(!cde.getClientid().equals(client_id)){
						throw new Exception();
					}
				}
				catch(Exception e){ cde = null; }
				
				if(Validator.isNotNull(cde) && cde.getUserid() == userid){
						
					UserDataBean udb = new UserDataBean();
					udb.setDataId(cde.getEntryId());
					udb.setType(cde.getType());
					udb.getData().setKey(cde.getKey());
					udb.getData().setValue(cde.getValue());
	
					List<CustomDataTag> cdts = null;
					try{  
						cdts = customDataTagPersistence.findByentryId(cde.getEntryId());
						if(Validator.isNull(cdts))
							throw new Exception();
					}
					catch(Exception e){ cdts = new ArrayList<CustomDataTag>(); }
					
					for(CustomDataTag cdt : cdts){
						try{
							Tag t = TagLocalServiceUtil.getTag(cdt.getTagid());
							udb.getTags().add(t.getTagname());
						}
						catch(Exception e){
							_log.error(e.getClass()+": "+e.getMessage());
						}
					}
					
					jResp.add(udb);
				}
			}
			
			String jarr = new Gson().toJson(jResp);
			response = JSONFactoryUtil.createJSONArray(jarr);
			
		}
		catch(CustomException ce){
			response = JSONFactoryUtil.createJSONArray();
			JSONObject item = JSONFactoryUtil.createJSONObject();
			item.put("message", ce.getMessageNumber());
			item.put("response", 1);
			item.put("text", ce.getMessage());
			
			response.put(item);
		}
		catch(Exception e){
			response = JSONFactoryUtil.createJSONArray();
			JSONObject item = JSONFactoryUtil.createJSONObject();
			item.put("message", 4);
			item.put("response", 1);
			item.put("text", e.getMessage());
			
			response.put(item);
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Oauth Authentication
	@JSONWebService(value="/cdv/userdatasearch")
	@AccessControlled(guestAccessEnabled=true)
	public JSONArray getUserDataSearchResult(String tags, String uid, String client_id)  {
		
		JSONArray response = JSONFactoryUtil.createJSONArray();
		
		try{
			
			if(Validator.isNull(tags))
				throw new MissingParameterException("The mandatory field tags is missing");
			
			Long userid = Utils.getUserIdByCcUserId(uid);
			Set<UserDataBean> jResp = new HashSet<UserDataBean>();
			
			//Get all tag IDs
			String[] tag_array = tags.split(",");
			Set<Long> tags_ids = new HashSet<Long>();
			for(String tag : tag_array){
				try{
					List<Tag> ts = tagPersistence.findBytagname(tag.toLowerCase());
					if(Validator.isNull(ts) || ts.isEmpty())
						throw new Exception();
					
					Tag t = ts.get(0);
					tags_ids.add(t.getTagid());
				}
				catch(Exception e){
					/* No tag found with the provided name */
				}
			}
			
			//Find each customdata tagged with each provided tag (via previously found IDs)
			for(Long tagid : tags_ids){
				List<CustomDataTag> cdts = null;
				try{ cdts = customDataTagPersistence.findBytagid(tagid); }
				catch(Exception e){ cdts = new ArrayList<CustomDataTag>(); }
				
				for(CustomDataTag cdt : cdts){
					try{
						CustomDataEntry cde = CustomDataEntryLocalServiceUtil.getCustomDataEntry(cdt.getEntryId());
						
						if(cde.getUserid() != userid || !cde.getClientid().equals(client_id))
							continue;
						
						UserDataBean udb = new UserDataBean();
						udb.setDataId(cde.getEntryId());
						udb.setType(cde.getType());
						udb.getData().setKey(cde.getKey());
						udb.getData().setValue(cde.getValue());
						
						List<CustomDataTag> currcdts = null;
						try{  
							currcdts = customDataTagPersistence.findByentryId(cde.getEntryId());
							if(Validator.isNull(currcdts))
								throw new Exception();
						}
						catch(Exception e){ currcdts = new ArrayList<CustomDataTag>(); }
						
						for(CustomDataTag currcdt : currcdts){
							try{
								Tag t = TagLocalServiceUtil.getTag(currcdt.getTagid());
								udb.getTags().add(t.getTagname());
							}
							catch(Exception e){
								_log.error(e.getClass()+": "+e.getMessage());
							}
						}
						
						jResp.add(udb);
					}
					catch(Exception e){
						/* The cde does not exist any more */
					}
				}
				
			}
	
			String jarr = new Gson().toJson(jResp);
			response = JSONFactoryUtil.createJSONArray(jarr);
			
		}
		catch(CustomException ce){
			response = JSONFactoryUtil.createJSONArray();
			JSONObject item = JSONFactoryUtil.createJSONObject();
			item.put("message", ce.getMessageNumber());
			item.put("response", 1);
			item.put("text", ce.getMessage());
			
			response.put(item);
		}
		catch(Exception e){
			response = JSONFactoryUtil.createJSONArray();
			JSONObject item = JSONFactoryUtil.createJSONObject();
			item.put("message", 4);
			item.put("response", 1);
			item.put("text", e.getMessage());
			
			response.put(item);
		}
		
		return response;
	}
	
	//Mapped on swagger
	//Oauth Authentication
	@JSONWebService(value="/cdv/userdatadelete", method = "DELETE")
	@AccessControlled(guestAccessEnabled=true)
	public JSONArray deleteUserData(String dataid, String uid, String client_id) {
		
//		System.out.println(dataid+" "+uid+" "+client_id);
	
		JSONArray response = JSONFactoryUtil.createJSONArray();
		
		try{
			
			Set<UserDataBean> jResp = new HashSet<UserDataBean>();
			
			if(Validator.isNull(dataid))
				throw new MissingParameterException("The mandatory field dataId is missing");
			
			Long userid = Utils.getUserIdByCcUserId(uid);
			
			CustomDataEntry cde = null;
			try{ 
				cde = CustomDataEntryLocalServiceUtil.getCustomDataEntry(Long.parseLong(dataid));
				if(!cde.getClientid().equals(client_id) || cde.getUserid() != userid)
					throw new Exception();
			}
			catch(Exception e){ cde = null; }
			
			if(Validator.isNotNull(cde)){
					
				UserDataBean udb = new UserDataBean();
				udb.setDataId(cde.getEntryId());
				udb.setType(cde.getType());
				udb.getData().setKey(cde.getKey());
				udb.getData().setValue(cde.getValue());

				List<CustomDataTag> cdts = null;
				try{  
					cdts = customDataTagPersistence.findByentryId(cde.getEntryId());
					if(Validator.isNull(cdts))
						throw new Exception();
				}
				catch(Exception e){ cdts = new ArrayList<CustomDataTag>(); }
				
				for(CustomDataTag cdt : cdts){
					try{
						Tag t = TagLocalServiceUtil.getTag(cdt.getTagid());
						udb.getTags().add(t.getTagname());
					}
					catch(Exception e){
						_log.error(e.getClass()+": "+e.getMessage());
					}
				}
				
				long entryId = cde.getEntryId();
				CustomDataEntryLocalServiceUtil.deleteCustomDataEntry(cde);
				customDataTagPersistence.removeByentryId(entryId);
				
				jResp.add(udb);
				
			}

			String jarr = new Gson().toJson(jResp);
			response = JSONFactoryUtil.createJSONArray(jarr);
			
		}
		catch(CustomException ce){
			response = JSONFactoryUtil.createJSONArray();
			JSONObject item = JSONFactoryUtil.createJSONObject();
			item.put("message", ce.getMessageNumber());
			item.put("response", 1);
			item.put("text", ce.getMessage());
			
			response.put(item);
		}
		catch(Exception e){
			response = JSONFactoryUtil.createJSONArray();
			JSONObject item = JSONFactoryUtil.createJSONObject();
			item.put("message", 4);
			item.put("response", 1);
			item.put("text", e.getMessage());
			
			response.put(item);
		}
		
		return response;
	}
	
}