/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.IdeaCdv;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing IdeaCdv in entity cache.
 *
 * @author Engineering
 * @see IdeaCdv
 * @generated
 */
public class IdeaCdvCacheModel implements CacheModel<IdeaCdv>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{ideaId=");
		sb.append(ideaId);
		sb.append(", ideaName=");
		sb.append(ideaName);
		sb.append(", need=");
		sb.append(need);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public IdeaCdv toEntityModel() {
		IdeaCdvImpl ideaCdvImpl = new IdeaCdvImpl();

		ideaCdvImpl.setIdeaId(ideaId);

		if (ideaName == null) {
			ideaCdvImpl.setIdeaName(StringPool.BLANK);
		}
		else {
			ideaCdvImpl.setIdeaName(ideaName);
		}

		ideaCdvImpl.setNeed(need);

		ideaCdvImpl.resetOriginalValues();

		return ideaCdvImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ideaId = objectInput.readLong();
		ideaName = objectInput.readUTF();
		need = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ideaId);

		if (ideaName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ideaName);
		}

		objectOutput.writeBoolean(need);
	}

	public long ideaId;
	public String ideaName;
	public boolean need;
}