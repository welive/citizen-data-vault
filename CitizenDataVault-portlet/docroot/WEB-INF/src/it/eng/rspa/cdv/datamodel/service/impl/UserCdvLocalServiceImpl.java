/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

import it.eng.rspa.cdv.datamodel.model.CustomDataEntry;
import it.eng.rspa.cdv.datamodel.model.CustomDataTag;
import it.eng.rspa.cdv.datamodel.model.UsedApplication;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import it.eng.rspa.cdv.datamodel.model.UserIdea;
import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.model.UserPreference;
import it.eng.rspa.cdv.datamodel.model.UserSkill;
import it.eng.rspa.cdv.datamodel.model.UserThirdProfile;
import it.eng.rspa.cdv.datamodel.service.base.UserCdvLocalServiceBaseImpl;
import it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK;

/**
 * The implementation of the user cdv local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.cdv.datamodel.service.UserCdvLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author SIRCHIA ANTONINO
 * @see it.eng.rspa.cdv.datamodel.service.base.UserCdvLocalServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil
 */
public class UserCdvLocalServiceImpl extends UserCdvLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil} to access the user cdv local service.
	 */
	
	private enum ForgetErrorCodes {
		user_not_found,
		user_idea,
		used_application,
		user_custom_data_entry,
		user_preference,
		user_thirdprofile,
		user_skills,
		user_lang,
		user_custom_data_tag
	}
	
	public List<UserCdv> getUserCdvByPilot(String pilot){
		try { return userCdvPersistence.findBypilot(pilot); }
		catch(Exception e){ return new ArrayList<UserCdv>(); }
	}
	
	public UserCdv getUserCdvByPK(long ccuserid, long userid){
		try{
			UserCdvPK ucdvpk = new UserCdvPK(ccuserid, userid);
			return userCdvPersistence.fetchByPrimaryKey(ucdvpk);
		}
		catch(Exception e){ return null; }
	}
	
	public JSONObject forget(long liferayUserID){
		
		JSONObject out = JSONFactoryUtil.createJSONObject();
		out.put("success", true);
		out.put("errors", JSONFactoryUtil.createJSONArray());
		
		try{
			List<UserCdv> uCdvs = getUserCdvPersistence().findByuserId(liferayUserID);
			if(uCdvs == null || uCdvs.isEmpty()){
				throw new Exception("No user exists for id "+liferayUserID);
			}
			UserCdv uCdv = uCdvs.get(0);
			if(uCdv==null){
				throw new Exception("No user exists for id "+liferayUserID);
			}
			
			long cdvUserID = uCdv.getUserId();
			
			
			/** Forget assets **/
			
				//Forget ideas
					List<UserIdea> ideas = new ArrayList<UserIdea>();
					try{ ideas = getUserIdeaPersistence().findByuser(cdvUserID); }
					catch(Exception e){ e.printStackTrace(); }
					
					for(UserIdea ui : ideas){
						try{ getUserIdeaPersistence().remove(ui); }
						catch(Exception e){
							e.printStackTrace();
							
							out.put("success", false);
							JSONObject j = JSONFactoryUtil.createJSONObject();
							j.put("code", ForgetErrorCodes.user_idea.toString());
							j.put("message", e.toString());
							
							out.getJSONArray("errors").put(j);
						}
					}
				
				//Forget used applications
					List<UsedApplication> apps = new ArrayList<UsedApplication>();
					try { apps = getUsedApplicationPersistence().findByuserId(cdvUserID); }
					catch(Exception e){ e.printStackTrace(); }
					
					for(UsedApplication app : apps){
						try{ getUsedApplicationPersistence().remove(app); }
						catch(Exception e){
							e.printStackTrace();
							
							out.put("success", false);
							JSONObject j = JSONFactoryUtil.createJSONObject();
							j.put("code", ForgetErrorCodes.used_application.toString());
							j.put("message", e.toString());
							
							out.getJSONArray("errors").put(j);
						}
					}
				
				//Forget custom entries
					List<CustomDataEntry> customentries = new ArrayList<CustomDataEntry>();
					try { customentries = getCustomDataEntryPersistence().findByuserid(cdvUserID); }
					catch(Exception e){ e.printStackTrace(); }
					
					for(CustomDataEntry entry : customentries){
						try{ 
							getCustomDataEntryPersistence().remove(entry);
							
							//Forget Custom Data Tag
								List<CustomDataTag> tags = new ArrayList<CustomDataTag>();
								try{ tags =  getCustomDataTagPersistence().findByentryId(entry.getEntryId()); }
								catch(Exception e){ e.printStackTrace(); }
								
								for(CustomDataTag tag : tags){
									try{ getCustomDataTagPersistence().remove(tag); }
									catch(Exception e){ 
										e.printStackTrace();
										
										out.put("success", false);
										JSONObject j = JSONFactoryUtil.createJSONObject();
										j.put("code", ForgetErrorCodes.user_custom_data_tag.toString());
										j.put("message", e.toString());
										
										out.getJSONArray("errors").put(j);
									}
								}
							
						}
						catch(Exception e){
							e.printStackTrace();
							
							out.put("success", false);
							JSONObject j = JSONFactoryUtil.createJSONObject();
							j.put("code", ForgetErrorCodes.user_custom_data_entry.toString());
							j.put("message", e.toString());
							
							out.getJSONArray("errors").put(j);
						}
					}
					
				//Forget user preferences
					List<UserPreference> prefs = new ArrayList<UserPreference>(); 
					try{ prefs = getUserPreferencePersistence().findByuserId(cdvUserID); }
					catch(Exception e){ e.printStackTrace(); }
					
					for(UserPreference pref : prefs){
						try{ getUserPreferencePersistence().remove(pref); }
						catch(Exception e){
							e.printStackTrace();
							
							out.put("success", false);
							JSONObject j = JSONFactoryUtil.createJSONObject();
							j.put("code", ForgetErrorCodes.user_preference.toString());
							j.put("message", e.toString());
							
							out.getJSONArray("errors").put(j);
						}
					}
			
				//Forget User third profile
					List<UserThirdProfile> profiles = new ArrayList<UserThirdProfile>(); 
					try{ profiles = getUserThirdProfilePersistence().findByuserId(cdvUserID); }
					catch(Exception e){ e.printStackTrace(); }
					
					for(UserThirdProfile profile : profiles){
						try{ getUserThirdProfilePersistence().remove(profile); }
						catch(Exception e){
							e.printStackTrace();
							
							out.put("success", false);
							JSONObject j = JSONFactoryUtil.createJSONObject();
							j.put("code", ForgetErrorCodes.user_thirdprofile.toString());
							j.put("message", e.toString());
							
							out.getJSONArray("errors").put(j);
						}
					}
					
				//Forget User skills
					List<UserSkill> skills = new ArrayList<UserSkill>(); 
					try{ skills = getUserSkillPersistence().findByuserId(cdvUserID); }
					catch(Exception e){ e.printStackTrace(); }
					
					for(UserSkill sk : skills){
						try{ getUserSkillPersistence().remove(sk); }
						catch(Exception e){
							e.printStackTrace();
							
							out.put("success", false);
							JSONObject j = JSONFactoryUtil.createJSONObject();
							j.put("code", ForgetErrorCodes.user_skills.toString());
							j.put("message", e.toString());
							
							out.getJSONArray("errors").put(j);
						}
					}
					
				//Forget User languages
				List<UserLanguage> langs = new ArrayList<UserLanguage>();
				try{ langs = getUserLanguagePersistence().findByuserId(cdvUserID); }
				catch(Exception e){ e.printStackTrace();  }
				
				for(UserLanguage l : langs){
					try{ getUserLanguagePersistence().remove(l); }
					catch(Exception e){
						e.printStackTrace();
						
						out.put("success", false);
						JSONObject j = JSONFactoryUtil.createJSONObject();
						j.put("code", ForgetErrorCodes.user_lang.toString());
						j.put("message", e.toString());
						
						out.getJSONArray("errors").put(j);
					}
				}
				
				
				//Forget the User
				getUserCdvPersistence().remove(uCdv);
			
		}
		catch(Exception e){
			e.printStackTrace();
			
			out.put("success", false);
			JSONObject j = JSONFactoryUtil.createJSONObject();
			j.put("code", ForgetErrorCodes.user_not_found.toString());
			j.put("message", e.toString());
			
			out.getJSONArray("errors").put(j);
			
		}
		
		return out;
	}
	
}