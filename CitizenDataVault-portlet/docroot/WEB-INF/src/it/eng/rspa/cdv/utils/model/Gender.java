package it.eng.rspa.cdv.utils.model;

public enum Gender {
	MALE("MALE"),
	FEMALE("FEMALE");
	
	private String txt;
	
	private Gender(String t){
		this.txt = t.trim();
	}
	
	public String toString(){
		return this.txt;
	}
}
