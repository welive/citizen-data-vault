package it.eng.rspa.cdv.utils.model;

import com.liferay.portal.kernel.util.Validator;

public class CustomEntry {

	private String key;
	private String value;
	
	public CustomEntry(String key, String value){
		this.key = key;
		this.value = value;
	}
	
	public CustomEntry(){
		//Do Nothing;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isValid() {
		return Validator.isNotNull(key) && Validator.isNotNull(value);
	}
	
	
	
}
