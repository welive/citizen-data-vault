/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.eng.rspa.cdv.datamodel.service.RestApiServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link it.eng.rspa.cdv.datamodel.service.RestApiServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Engineering
 * @see RestApiServiceHttp
 * @see it.eng.rspa.cdv.datamodel.service.RestApiServiceUtil
 * @generated
 */
public class RestApiServiceSoap {
	/**
	* DataSource Oriented
	*/
	public static java.lang.String addUser(java.lang.String body)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.addUser(body);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateUser(java.lang.String body)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.updateUser(body);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String push(java.lang.String body)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.push(body);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* DataConsumer Oriented *
	*/
	public static java.lang.String getUsersSkills() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestApiServiceUtil.getUsersSkills();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getUserMetadata(java.lang.String ccuserid)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getUserMetadata(ccuserid);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getUserProfile(java.lang.String uid)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getUserProfile(uid);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* Statistics Oriented *
	*/
	public static java.lang.String getKPI11(java.lang.String pilotid,
		java.lang.String from, java.lang.String to) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getKPI11(pilotid,
					from, to);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getKPI43(java.lang.String pilotid,
		java.lang.String from, java.lang.String to) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getKPI43(pilotid,
					from, to);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getKPI111(java.lang.String pilotid)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getKPI111(pilotid);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getKPI112(java.lang.String pilotid)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getKPI112(pilotid);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getKPI113(java.lang.String pilotid,
		java.lang.String from, java.lang.String to) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getKPI113(pilotid,
					from, to);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getKPI114(java.lang.String pilotid,
		java.lang.String from, java.lang.String to) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getKPI114(pilotid,
					from, to);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getKPI115(java.lang.String pilotid,
		java.lang.String from, java.lang.String to) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestApiServiceUtil.getKPI115(pilotid,
					from, to);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* Data Oriented *
	*/
	public static java.lang.String userDataUpdate(java.lang.String body,
		java.lang.String uid, java.lang.String client_id)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestApiServiceUtil.userDataUpdate(body,
					uid, client_id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getUserData(java.lang.String dataid,
		java.lang.String uid, java.lang.String client_id)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestApiServiceUtil.getUserData(dataid,
					uid, client_id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getUserDataSearchResult(
		java.lang.String tags, java.lang.String uid, java.lang.String client_id)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestApiServiceUtil.getUserDataSearchResult(tags,
					uid, client_id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String deleteUserData(java.lang.String dataid,
		java.lang.String uid, java.lang.String client_id)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestApiServiceUtil.deleteUserData(dataid,
					uid, client_id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(RestApiServiceSoap.class);
}