package it.eng.rspa.cdv.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import it.eng.rspa.cdv.datamodel.model.Application;
import it.eng.rspa.cdv.datamodel.model.CustomDataEntry;
import it.eng.rspa.cdv.datamodel.model.CustomDataTag;
import it.eng.rspa.cdv.datamodel.model.Endorsement;
import it.eng.rspa.cdv.datamodel.model.IdeaCdv;
import it.eng.rspa.cdv.datamodel.model.Language;
import it.eng.rspa.cdv.datamodel.model.Preference;
import it.eng.rspa.cdv.datamodel.model.SkillCdv;
import it.eng.rspa.cdv.datamodel.model.Tag;
import it.eng.rspa.cdv.datamodel.model.UsedApplication;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import it.eng.rspa.cdv.datamodel.model.UserIdea;
import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.model.UserPreference;
import it.eng.rspa.cdv.datamodel.model.UserSkill;
import it.eng.rspa.cdv.datamodel.model.impl.ApplicationImpl;
import it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UserIdeaImpl;
import it.eng.rspa.cdv.datamodel.service.ApplicationLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.CustomDataEntryLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.CustomDataTagLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.IdeaCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.LanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.PreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.SkillCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.TagLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UsedApplicationLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserIdeaLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserLanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserPreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserSkillLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK;
import it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK;
import it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK;
import it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK;
import it.eng.rspa.cdv.datamodel.service.persistence.UserLanguagePK;
import it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK;
import it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK;
import it.eng.rspa.cdv.utils.exception.CustomException;
import it.eng.rspa.cdv.utils.exception.InternalErrorException;
import it.eng.rspa.cdv.utils.exception.InvalidInputException;
import it.eng.rspa.cdv.utils.exception.MissingParameterException;
import it.eng.rspa.cdv.utils.exception.TypeMismatchException;
import it.eng.rspa.cdv.utils.exception.WrongCcuseridException;
import it.eng.rspa.cdv.utils.model.CustomEntry;
import it.eng.rspa.cdv.utils.model.IndexedUserDataBean;
import it.eng.rspa.cdv.utils.model.PushEvent;
import it.eng.rspa.cdv.utils.model.PushModel;
import it.eng.rspa.cdv.utils.model.UserIdeaRole;


public abstract class RestApiUtils {
	
	
	public static void pushEventHandler(PushModel pm) 
			throws InternalErrorException, MissingParameterException, WrongCcuseridException, TypeMismatchException, InvalidInputException{
		
			PushEvent pe = PushEvent.valueOf(pm.getEventName());
			switch(pe){
				case CollaborationRemoved:
					RestApiUtils.onCollaborationRemove(pm);
					break;
				case IdeaCreated:
					RestApiUtils.onIdeaCreate(pm);
					break;
				case IdeaDeleted:
					RestApiUtils.onIdeaRemove(pm);
					break;
				case NewAppInstalled:
					RestApiUtils.onAppInstall(pm);
					break;
				case NewCollaboration:
					RestApiUtils.onCollaborationCreate(pm);
					break;
				case NewKnownLocation:
					RestApiUtils.onLocationUpdate(pm);
					break;
				case ReputationUpdated:
					RestApiUtils.onReputationUpdate(pm);
					break;
				case SkillsUpdated:
					RestApiUtils.onSkillsUpdate(pm);
					break;
				case SkillsRemoved:
					RestApiUtils.onSkillRemoved(pm);
					break;
				default:
					throw new IllegalArgumentException("The event " + pm.getEventName() + " cannot be handled.");
			}
		
		return;
		
	}
	/**
	 * @param langname The name of the language to be retrieved
	 * @return The it.eng.rspa.cdv.datamodel.model.Language object associated to the provided langname (case-insensitive)
	 * 
	 * If the langname is not present in the database, it is created.
	 */
	public static Language getLanguage(String langname){
		Language lang = LanguageLocalServiceUtil.getLanguageByName(langname);
		
		if(lang == null){
			try{
				
				Language thisLang = LanguageLocalServiceUtil.createLanguage(CounterLocalServiceUtil.increment(Language.class.getName()));
				thisLang.setName(langname);
				return LanguageLocalServiceUtil.addLanguage(thisLang);
				
			}
			catch(Exception e){
				return null;
			}
			
		}
		
		return lang;
			
	}
	
	/**
	 * @param user The user who must be associated to the language
	 * @param lang The language that must be associated to the user
	 * @return The generated Database entry of type it.eng.rspa.cdv.datamodel.model.UserLanguage
	 */
	public static UserLanguage associateUserLanguage(UserCdv user, Language lang){
		
		if(user==null || lang==null)
			return null;
		
		try{
			UserLanguagePK ulpk = new UserLanguagePK();
			ulpk.setLanguageId(lang.getLanguageId());
			ulpk.setUserId(user.getUserId());
			
			UserLanguage ul = UserLanguageLocalServiceUtil.createUserLanguage(ulpk);
			return UserLanguageLocalServiceUtil.addUserLanguage(ul);
		}
		catch(Exception e){
			return null;
		}
		
	}
	
	/**
	 * @param prefname The name of the preference to be retrieved
	 * @return The it.eng.rspa.cdv.datamodel.model.Preference object associated to the provided prefname (case-insensitive)
	 * 
	 * If the prefname is not present in the database, it is created.
	 */
	public static Preference getPreference(String prefname) {
		Preference pref = PreferenceLocalServiceUtil.getPreferenceByName(prefname);
		
		if(Validator.isNull(pref)){
			try{
				
				Preference thisPref = PreferenceLocalServiceUtil.createPreference(CounterLocalServiceUtil.increment(Preference.class.getName()));
				thisPref.setName(prefname);
				return PreferenceLocalServiceUtil.addPreference(thisPref);
				
			}
			catch(Exception e){
				return null;
			}
			
		}
		
		return pref;
	}
	
	/**
	 * @param user The user who must be associated to the preference
	 * @param dtoPref The preference that must be associated to the user
	 * @return The generated Database entry of type it.eng.rspa.cdv.datamodel.model.UserPreference
	 */
	public static UserPreference associateUserPreference(UserCdv user, Preference pref) {
		
		if(user==null || pref==null)
			return null;
		
		try{
			UserPreferencePK uppk = new UserPreferencePK();
			uppk.setPreferenceId(pref.getPreferenceId());
			uppk.setUserId(user.getUserId());
			
			UserPreference up = UserPreferenceLocalServiceUtil.createUserPreference(uppk);
			return UserPreferenceLocalServiceUtil.addUserPreference(up);
		}
		catch(Exception e){
			return null;
		}
	}
	
	/**
	 * @param skillname The name of the skill to be retrieved
	 * @return the it.eng.rspa.cdv.datamodel.model.SkillCdv object associated to the provided skillname (case-insensitive)
	 * 
	 * If the skillname is not present in the database, it is created.
	 */
	public static SkillCdv getSkill(String skillname){
		SkillCdv skill = SkillCdvLocalServiceUtil.getSkillByName(skillname);
		
		if(skill == null){
			try{
				
				SkillCdv thisSkill = SkillCdvLocalServiceUtil.createSkillCdv(CounterLocalServiceUtil.increment(SkillCdv.class.getName()));
				thisSkill.setSkillname(skillname);
				return SkillCdvLocalServiceUtil.addSkillCdv(thisSkill);
				
			}
			catch(Exception e){
				return null;
			}
			
		}
		
		return skill;

	}
	
	/**
	 * @param user The user who must be associated to the skill
	 * @param skill The skill that must be associated to the user
	 * @return The generated Database entry of type it.eng.rspa.cdv.datamodel.model.UserSkill
	 */
	public static UserSkill associateUserSkill(UserCdv user, SkillCdv skill, long endorserUserId){
		
		
		if(user==null || skill==null)
			return null;
		
		try{
			UserSkillPK uspk = new UserSkillPK();
			uspk.setSkillId(skill.getSkillId());
			uspk.setUserId(user.getUserId());
			
			UserSkill us = UserSkillLocalServiceUtil.createUserSkill(uspk);
			Date now = new Date();
			us.setDate(now);
			
			if (user.getUserId() != endorserUserId){
				us.setEndorsmentCounter(1L);
			}else{
				us.setEndorsmentCounter(0L);
			}
			
			//Create endorsement, useful in order to know who endorse
			EndorsementPK endorsementPK = new EndorsementPK();
			endorsementPK.setEndorserId(endorserUserId);
			endorsementPK.setSkillId(skill.getSkillId());
			endorsementPK.setUserId(user.getUserId());
			
			Endorsement endors = EndorsementLocalServiceUtil.createEndorsement(endorsementPK);
			endors.setDate(now);
			EndorsementLocalServiceUtil.updateEndorsement(endors);
			
			return UserSkillLocalServiceUtil.addUserSkill(us);
		}
		catch(Exception e){
			return null;
		}
		
	}
	
	/**
	 * @param pushmodel the object containing the push event
	 * It removes the collaboration pair <user, idea> from the DB.
	 * It's necessary that the input pushmodel contains the entries with at least the ideaid
	 * @throws InternalErrorException 
	 * @throws MissingParameterException 
	 * @throws WrongCcuseridException 
	 * @throws TypeMismatchException 
	 * @throws InvalidInputException 
	 */
	public static void onCollaborationRemove(PushModel pushmodel) 
			throws InternalErrorException, MissingParameterException, WrongCcuseridException, TypeMismatchException, InvalidInputException{
		
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries) || entries.isEmpty()){
			throw new MissingParameterException("Missing parameter ideaId");
		}
		
		Long ideaid = null;
		for(CustomEntry ce : entries){
			if(ce.getKey().equalsIgnoreCase("ideaid")){
				try{ ideaid = Long.parseLong(ce.getValue()); }
				catch(NumberFormatException e){
					throw new TypeMismatchException("The entry ideaid is not a number");
				}
			}
		}
		
		if(Validator.isNull(ideaid))
			throw new MissingParameterException("The mandatory entry ideaid is missing");
		
		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
		
		UserIdeaPK uipk = new UserIdeaPK();
		uipk.setRole(UserIdeaRole.Collaborator.toString());
		uipk.setIdeaId(ideaid);
		uipk.setUserId(uid);
		
		try{ UserIdeaLocalServiceUtil.deleteUserIdea(uipk); }
		catch(Exception e){
			throw new InvalidInputException("No collaboration exists for the pair [" + uid + ", " + ideaid + "]");
		}
		
		return;
	}
	
	/**
	 * @param pushmodel the object containing the push event
	 * It adds the authorship pair <user, idea> into the DB.
	 * It's necessary that the input pushmodel contains the entries with at least the ideaid, ideatitle and isneed.
	 * @throws InternalErrorException
	 * @throws MissingParameterException
	 * @throws TypeMismatchException
	 * @throws WrongCcuseridException
	 * @throws InvalidInputException
	 */
	public static void onIdeaCreate(PushModel pushmodel) 
			throws InternalErrorException, MissingParameterException, TypeMismatchException, WrongCcuseridException, InvalidInputException {
		
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries) || entries.isEmpty()){
			throw new MissingParameterException("Missing parameter ideaId");
		}
		
		Long ideaid = null;
		String ideatitle = null;
		Boolean isNeed = null;
		
		for(CustomEntry ce : entries){
			if(ce.getKey().equalsIgnoreCase("ideaid")){
				try{ ideaid = Long.parseLong(ce.getValue()); }
				catch(NumberFormatException e){
					throw new TypeMismatchException("The entry ideaid is not a number");
				}
			}
			else if(ce.getKey().equalsIgnoreCase("ideatitle")){
				ideatitle = ce.getValue();
			}
			else if(ce.getKey().equalsIgnoreCase("isneed")){
				try{ isNeed = Boolean.parseBoolean(ce.getValue()); }
				catch(Exception e){
					throw new TypeMismatchException("The entry isNeed is not a boolean");
				}
			}
		}
		
		if(Validator.isNull(ideaid))
			throw new MissingParameterException("The mandatory entry ideaid is missing");
		
		if(Validator.isNull(ideatitle))
			throw new MissingParameterException("The mandatory entry ideatitle is missing");
		
		if(Validator.isNull(isNeed))
			throw new MissingParameterException("The mandatory entry isNeed is missing");
		
		
		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
		
		IdeaCdv previous_idea = null;
		try{  previous_idea = IdeaCdvLocalServiceUtil.getIdeaCdv(ideaid); }
		catch(Exception e){ /* It should be the normal flow */ }
		
		if(Validator.isNotNull(previous_idea)){
			throw new InvalidInputException("The idea \""+ ideatitle + "\" with id " + ideaid + " is already registered in the CDV.");
		}
		
		IdeaCdv icdv = new IdeaCdvImpl();
		icdv.setIdeaId(ideaid);
		icdv.setIdeaName(ideatitle);
		icdv.setNeed(isNeed);
		
		try{ IdeaCdvLocalServiceUtil.addIdeaCdv(icdv); }
		catch(Exception e){
			throw new InvalidInputException(e.getMessage());
		}
		
		UserIdea ui = new UserIdeaImpl();
		ui.setRole(UserIdeaRole.Author.toString());
		ui.setIdeaId(icdv.getIdeaId());
		ui.setUserId(uid);
		
		try{ UserIdeaLocalServiceUtil.addUserIdea(ui); }
		catch(Exception e){
			throw new InvalidInputException("No authorship for the pair [" + uid + ", " + ideaid + "] already exists.");
		}
		
		return;
	}
	
	/**
	 * @param pushmodel the object containing the push event
	 * It removes the authorship pair <user, idea> from the DB.
	 * It's necessary that the input pushmodel contains the entries with at least the ideaid.
	 * 
	 * @throws InternalErrorException 
	 * @throws MissingParameterException 
	 * @throws InvalidInputException 
	 * @throws TypeMismatchException 
	 * @throws WrongCcuseridException 
	 */
	public static void onIdeaRemove(PushModel pushmodel) 
			throws InternalErrorException, MissingParameterException, InvalidInputException, TypeMismatchException, WrongCcuseridException {
		
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries) || entries.isEmpty()){
			throw new MissingParameterException("Missing parameter ideaId");
		}
		
		Long ideaid = null;
		
		for(CustomEntry ce : entries){
			if(ce.getKey().equalsIgnoreCase("ideaid")){
				try{ ideaid = Long.parseLong(ce.getValue()); }
				catch(NumberFormatException e){
					throw new TypeMismatchException("The entry ideaid is not a number");
				}
			}
		}
		
		if(Validator.isNull(ideaid))
			throw new MissingParameterException("The mandatory entry ideaid is missing");
		
		
		List<UserIdea> userideas = UserIdeaLocalServiceUtil.getUserIdeaByIdeaId(ideaid); 
		
		for(UserIdea ui : userideas){
			try{ UserIdeaLocalServiceUtil.deleteUserIdea(ui); }
			catch(Exception e){
				System.out.println(e);
			}
		}

//		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
//		UserIdeaPK uipk = new UserIdeaPK();
//		uipk.setIdeaId(ideaid);
//		uipk.setUserId(uid);
//		uipk.setRole(UserIdeaRole.Author.toString());
//		
//		try{ UserIdeaLocalServiceUtil.deleteUserIdea(uipk); }
//		catch(PortalException pe){
//			throw new InvalidInputException("The authorship ["+ ideaid + ", " + uid +"] is not defined");
//		}
//		catch(Exception e){
//			throw new InternalErrorException(e.getMessage());
//		}
		
		return;
	}

	/**
	 * @param pushmodel the object containing the push event
	 * It adds the usage data pair <user, app> into the DB.
	 * It's necessary that the input pushmodel contains the entries with at least the appID, appName.
	 *  
	 * @throws InternalErrorException
	 * @throws MissingParameterException 
	 * @throws InvalidInputException 
	 * @throws TypeMismatchException 
	 * @throws WrongCcuseridException 
	 */
	public static void onAppInstall(PushModel pushmodel) 
			throws InternalErrorException, MissingParameterException, InvalidInputException, TypeMismatchException, WrongCcuseridException{
		
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries) || entries.isEmpty()){
			throw new MissingParameterException("Missing parameter appID");
		}
		
		Long appID = null;
		String appName = null;
		
		for(CustomEntry ce : entries){
			if(ce.getKey().equalsIgnoreCase("appID")){
				try{ appID = Long.parseLong(ce.getValue()); }
				catch(NumberFormatException e){
					throw new TypeMismatchException("The entry appID is not a number");
				}
			}
			else if(ce.getKey().equalsIgnoreCase("appName")){
				appName = ce.getValue();
			}
		}
		
		if(Validator.isNull(appID))
			throw new MissingParameterException("The mandatory entry appID is missing");
		
		if(Validator.isNull(appName))
			throw new MissingParameterException("The mandatory entry appName is missing");
		
		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
		
		Application previous_app = null;
		try{ previous_app = ApplicationLocalServiceUtil.getApplication(appID); }
		catch(Exception e){ /* It should be the normal flow */ }
		
		if(Validator.isNotNull(previous_app)){
			throw new InvalidInputException("The application \""+ appName + "\" with id " + appID + " is already registered in the CDV.");
		}
		
		Application appcdv = new ApplicationImpl();
		appcdv.setAppId(appID);
		appcdv.setAppName(appName);
		
		try{ ApplicationLocalServiceUtil.addApplication(appcdv); }
		catch(Exception e){
			throw new InvalidInputException(e.getMessage());
		}
		
		UsedApplication ua = new UsedApplicationImpl();
		ua.setAppId(appcdv.getAppId());
		ua.setUserId(uid);
		
		try{ UsedApplicationLocalServiceUtil.addUsedApplication(ua); }
		catch(Exception e){
			throw new InternalErrorException(e.getMessage());
		}
		
		return;
	}
	
	/**
	 * @param pushmodel the object containing the push event
	 * It creates a collaboration pair <user, idea>
	 * It's necessary that the input pushmodel contains the entries with at least the ideaid
	 * 
	 * @throws InternalErrorException 
	 * @throws MissingParameterException 
	 * @throws TypeMismatchException 
	 * @throws WrongCcuseridException 
	 * @throws InvalidInputException 
	 */
	public static void onCollaborationCreate(PushModel pushmodel) 
			throws InternalErrorException, MissingParameterException, TypeMismatchException, WrongCcuseridException, InvalidInputException{
		
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries) || entries.isEmpty()){
			throw new MissingParameterException("Missing parameter ideaId");
		}
		
		Long ideaid = null;
		String ideatitle = null;
		Boolean isNeed = null;
		
		for(CustomEntry ce : entries){
			if(ce.getKey().equalsIgnoreCase("ideaid")){
				try{ ideaid = Long.parseLong(ce.getValue()); }
				catch(NumberFormatException e){
					throw new TypeMismatchException("The entry ideaid is not a number");
				}
			}
			else if(ce.getKey().equalsIgnoreCase("ideatitle")){
				ideatitle = ce.getValue();
			}
			else if(ce.getKey().equalsIgnoreCase("isneed")){
				try{ isNeed = Boolean.parseBoolean(ce.getValue()); }
				catch(Exception e){
					throw new TypeMismatchException("The entry isNeed is not a boolean");
				}
			}
		}
		
		if(Validator.isNull(ideaid))
			throw new MissingParameterException("The mandatory entry ideaid is missing");
		
		if(Validator.isNull(ideatitle))
			throw new MissingParameterException("The mandatory entry ideatitle is missing");
		
		if(Validator.isNull(isNeed))
			throw new MissingParameterException("The mandatory entry isNeed is missing");
		
		IdeaCdv icdv = null;
		try{  icdv = IdeaCdvLocalServiceUtil.getIdeaCdv(ideaid); }
		catch(PortalException e){ 
			
			//The idea is not yet registered in the CDV
			icdv = new IdeaCdvImpl();
			icdv.setIdeaId(ideaid);
			icdv.setIdeaName(ideatitle);
			icdv.setNeed(isNeed);
			try{ IdeaCdvLocalServiceUtil.addIdeaCdv(icdv); }
			catch(Exception e1){
				throw new InternalErrorException(e.getMessage());
			}
		}
		catch(Exception e){
			throw new InternalErrorException(e.getMessage());
		}
		
		if(Validator.isNull(icdv)){
			throw new InternalError("The idea \""+ ideatitle + "\" with id " + ideaid + " cannot be registered in the CDV.");
		}
		
		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
		
		UserIdea ui = new UserIdeaImpl();
		ui.setRole(UserIdeaRole.Collaborator.toString());
		ui.setIdeaId(ideaid);
		ui.setUserId(uid);
		
		try{ UserIdeaLocalServiceUtil.addUserIdea(ui); }
		catch(Exception e){
			throw new InternalError("The collaboration pair [" + ideaid + ", " + uid + "] cannot be registered in the CDV.");
		}
		
		return;
	}
	
	/**
	 * @param pushmodel the object containing the push event
	 * It updates the user's Last known location.
	 * It's necessary that the input pushmodel contains the entries with at least the lat and lng keys.
	 * 
	 * @throws InternalErrorException
	 * @throws MissingParameterException 
	 * @throws TypeMismatchException 
	 * @throws WrongCcuseridException 
	 */
	public static void onLocationUpdate(PushModel pushmodel) 
			throws InternalErrorException, MissingParameterException, TypeMismatchException, WrongCcuseridException{
		
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries) || entries.isEmpty()){
			throw new MissingParameterException("Missing parameter ideaId");
		}
		
		Double lat = null;
		Double lng = null;
		
		for(CustomEntry ce : entries){
			if(ce.getKey().equalsIgnoreCase("lat")){
				try{ lat = Double.parseDouble(ce.getValue()); }
				catch(NumberFormatException e){
					throw new TypeMismatchException("The entry lat is not a number");
				}
			}
			else if(ce.getKey().equalsIgnoreCase("lng")){
				try{ lng = Double.parseDouble(ce.getValue()); }
				catch(NumberFormatException e){
					throw new TypeMismatchException("The entry lng is not a number");
				}
			}
		}
		
		if(Validator.isNull(lat))
			throw new MissingParameterException("The mandatory entry lat is missing");
		
		if(Validator.isNull(lng))
			throw new MissingParameterException("The mandatory entry lng is missing");
		
		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
		
		UserCdvPK upk = new UserCdvPK();
		upk.setCcuid(pushmodel.getCcUserID());
		upk.setUserId(uid);
		
		UserCdv u = null;
		try{  
			u = UserCdvLocalServiceUtil.getUserCdv(upk);
			if(Validator.isNull(u))
				throw new Exception();
		}
		catch(Exception e){
			throw new WrongCcuseridException("The provided CcUserId " + pushmodel.getCcUserID() + " does not correspond to any CDV user.");
		}
		
		u.setLastKnownLatitude(lat);
		u.setLastKnownLongitude(lng);
		try{ UserCdvLocalServiceUtil.updateUserCdv(u); }
		catch(Exception e){
			throw new InternalErrorException("Cannot update the user " + u.getCcuid() + " LKL: "+e.getMessage());
		}
		
	}
	
	/**
	 * @param pushmodel the object containing the push event
	 * It updates the user's reputation.
	 * It's necessary that the input pushmodel contains the entries with at least the 'newReputation' keys.
	 * 
	 * @throws TypeMismatchException
	 * @throws MissingParameterException
	 * @throws InternalErrorException
	 * @throws WrongCcuseridException
	 */
	public static void onReputationUpdate(PushModel pushmodel) 
			throws TypeMismatchException, MissingParameterException, InternalErrorException, WrongCcuseridException{
		
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries) || entries.isEmpty()){
			throw new MissingParameterException("Missing parameter newReputation");
		}
		
		Float rep = null;
		
		for(CustomEntry ce : entries){
			if(ce.getKey().equalsIgnoreCase("newReputation")){
				try{ rep = Float.parseFloat(ce.getValue()); }
				catch(NumberFormatException e){
					throw new TypeMismatchException("The entry newReputation is not a number");
				}
			}
		}
		
		if(Validator.isNull(rep))
			throw new MissingParameterException("The mandatory entry newReputation is missing");
		
		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
		
		UserCdvPK upk = new UserCdvPK();
		upk.setCcuid(pushmodel.getCcUserID());
		upk.setUserId(uid);
		
		UserCdv u = null;
		try{  
			u = UserCdvLocalServiceUtil.getUserCdv(upk);
			if(Validator.isNull(u))
				throw new Exception();
		}
		catch(Exception e){
			throw new WrongCcuseridException("The provided CcUserId " + pushmodel.getCcUserID() + " does not correspond to any CDV user.");
		}
		
		Long lRep = new Long(Math.round(rep));
		u.setReputation(lRep);
		try{ UserCdvLocalServiceUtil.updateUserCdv(u); }
		catch(Exception e){
			throw new InternalErrorException("Cannot update the user " + u.getCcuid() + " reputation: "+e.getMessage());
		}
	}
	
	/**
	 * @param pushmodel the object containing the push event
	 * It updates the user's skill.
	 * The input pushmodel may contain the a set of entries whose keys are the Skillname and the value is the endorsement counter.
	 * Whenever the one of the provided skills is not yet present in the set of user's skills, it is created in the DB with endorsement 
	 * counter equal to zero. 
	 * 
	 * @throws InternalErrorException
	 * @throws MissingParameterException
	 * @throws WrongCcuseridException
	 */
	public static void onSkillsUpdate(PushModel pushmodel)	throws InternalErrorException, MissingParameterException, WrongCcuseridException{
		
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries)){
			throw new MissingParameterException("Entries missing");
		}
		
		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
		
		UserCdv _user = null;
		try{
			UserCdvPK upk = new UserCdvPK();
			upk.setCcuid(pushmodel.getCcUserID());
			upk.setUserId(uid);
			_user = UserCdvLocalServiceUtil.getUserCdv(upk);
			
			if(Validator.isNull(_user))
				throw new Exception();
		}
		catch(Exception e){
			throw new WrongCcuseridException("No user exists in the CDV with the provided CCUserID");
		}
		
		for(CustomEntry ce : entries){
			String skillname = ce.getKey().trim().toLowerCase();
			SkillCdv dtoSkill = RestApiUtils.getSkill(skillname);
			
			UserSkillPK uspk = new UserSkillPK();
			uspk.setSkillId(dtoSkill.getSkillId());
			uspk.setUserId(uid);
			UserSkill us = null;
			try{ us = UserSkillLocalServiceUtil.getUserSkill(uspk); }
			catch(Exception e){ /* The variable remains NULL */ }
			
			User userCdv = null;
			try {
				userCdv = UserLocalServiceUtil.getUser(uid);
			} catch (PortalException | SystemException e1) {
				e1.printStackTrace();
			}
			
			
			User endorser = null;
			try {
				endorser = UserLocalServiceUtil.getUser(pushmodel.getEndorserLiferayUserId());
			} catch (PortalException | SystemException e1) {
				e1.printStackTrace();
			}
			
			String endorserNameSurname = endorser.getFullName();
			
			
			String msgNotificationText = LanguageUtil.get(userCdv.getLocale(), "cdv.new-skill");
			
			if(Validator.isNull(us)){
				RestApiUtils.associateUserSkill(_user, dtoSkill, pushmodel.getEndorserLiferayUserId());
			}
			else{
				Long endrsmnt = Long.parseLong(ce.getValue());
				us.setEndorsmentCounter(endrsmnt);
				Date now = new Date();
				us.setDate(now);//update Date
				try{ UserSkillLocalServiceUtil.updateUserSkill(us); }
				catch(Exception e){
					throw new InternalErrorException(e.getMessage());
				}
				
				EndorsementPK ePK = new EndorsementPK();
				ePK.setSkillId(dtoSkill.getSkillId());
				ePK.setUserId(uid);
				ePK.setEndorserId(pushmodel.getEndorserLiferayUserId());
				
				Endorsement endorsement = EndorsementLocalServiceUtil.createEndorsement(ePK);
				endorsement.setDate(now);
				try {
					EndorsementLocalServiceUtil.updateEndorsement(endorsement);
				} catch (SystemException e) {
					e.printStackTrace();
				}
				
				 msgNotificationText = LanguageUtil.get(userCdv.getLocale(), "cdv.new-endorsement");
				
				
			}
			
			msgNotificationText = endorserNameSurname + " "+msgNotificationText+": '"+skillname+"'";
			
			if (pushmodel.getEndorserLiferayUserId() !=uid)
					NotificationUtils.sendNotification(msgNotificationText, uid );
			
			
		}
		
		return;
	}
	
	public static Tag getTag(String tagname){
		try{
			Tag tag = TagLocalServiceUtil.getTagByName(tagname);
			if(Validator.isNull(tag)){
				tag = TagLocalServiceUtil.createTag(CounterLocalServiceUtil.increment(Tag.class.getName()));
				tag.setTagname(tagname.toLowerCase());
				TagLocalServiceUtil.addTag(tag);
			}
			
			return tag;
		}
		catch(Exception e){
			/* Do Nothing */
		}
		
		return null;
	}
	
	public static Long createUserData(IndexedUserDataBean iudb, Long userid, String client_id) {
		Long ret = null;
		try{
			//Store CustomData
			CustomDataEntry cde = CustomDataEntryLocalServiceUtil.createCustomDataEntry(CounterLocalServiceUtil.increment(CustomDataEntry.class.getName()));
			cde.setType(iudb.getType());
			cde.setKey(iudb.getData().getKey());
			cde.setValue(iudb.getData().getValue());
			cde.setUserid(userid);
			cde.setClientid(client_id);
			CustomDataEntryLocalServiceUtil.addCustomDataEntry(cde);
			
			ret = cde.getEntryId();
			
			Set<String> tags = iudb.getTags();
			for(String tag : tags){
				
				//Retrive/Store Tag
				Tag t = getTag(tag);
				
				//Associate Tag to the CustomData
				CustomDataTagPK cdtpk = new CustomDataTagPK();
				cdtpk.setEntryId(cde.getEntryId());
				cdtpk.setTagid(t.getTagid());
				
				CustomDataTag cdt = null;
				try{ 
					cdt = CustomDataTagLocalServiceUtil.getCustomDataTag(cdtpk);
					if(Validator.isNull(cdt))
						throw new Exception();
				}
				catch(Exception e){
					cdt = CustomDataTagLocalServiceUtil.createCustomDataTag(cdtpk);
					CustomDataTagLocalServiceUtil.addCustomDataTag(cdt);
				}
			}			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return ret;
	}

	public static Long updateUserData(IndexedUserDataBean iudb, Long userid, String client_id) throws CustomException {
		Long ret = null;
		try{
			//Retrieve CustomData
			CustomDataEntry cde = CustomDataEntryLocalServiceUtil.getCustomDataEntry(iudb.getDataId());
			
			if(cde.getUserid() != userid)
				throw new InvalidInputException("Unauthorized to update the UserData "+cde.getEntryId());
			
			//Update data
			cde.setType(iudb.getType());
			cde.setKey(iudb.getData().getKey());
			cde.setValue(iudb.getData().getValue());
			cde.setUserid(userid);
			cde.setClientid(client_id);
			CustomDataEntryLocalServiceUtil.updateCustomDataEntry(cde);
			
			ret = cde.getEntryId();
			
			Set<String> tags = iudb.getTags();
			for(String tag : tags){
				
				//Retrive/Store Tag
				Tag t = getTag(tag);
				
				//Associate Tag to the CustomData
				CustomDataTagPK cdtpk = new CustomDataTagPK();
				cdtpk.setEntryId(cde.getEntryId());
				cdtpk.setTagid(t.getTagid());
				
				CustomDataTag cdt = null;
				try{ 
					cdt = CustomDataTagLocalServiceUtil.getCustomDataTag(cdtpk);
					if(Validator.isNull(cdt))
						throw new Exception();
				}
				catch(Exception e){
					cdt = CustomDataTagLocalServiceUtil.createCustomDataTag(cdtpk);
					CustomDataTagLocalServiceUtil.addCustomDataTag(cdt);
				}
			}
		}
		catch(CustomException ce){
			throw ce;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return ret;
	}

	public static void onSkillRemoved(PushModel pushmodel) 
			throws InternalErrorException, MissingParameterException, WrongCcuseridException {
		if(Validator.isNull(pushmodel))
			throw new InternalErrorException("Internal Error");
		
		Set<CustomEntry> entries = pushmodel.getEntries();
		if(Validator.isNull(entries)){
			throw new MissingParameterException("Entries missing");
		}
		
		
		long uid = Utils.getUserIdByCcUserId(String.valueOf(pushmodel.getCcUserID()));
		
		UserCdv _user = null;
		try{
			UserCdvPK upk = new UserCdvPK();
			upk.setCcuid(pushmodel.getCcUserID());
			upk.setUserId(uid);
			_user = UserCdvLocalServiceUtil.getUserCdv(upk);
			
			if(Validator.isNull(_user))
				throw new Exception();
		}
		catch(Exception e){
			throw new WrongCcuseridException("No user exists in the CDV with the provided CCUserID");
		}
		
		for(CustomEntry ce : entries){
			if(!ce.getKey().equalsIgnoreCase("skill_name")){
				//The parameter is not correct
				continue;
			}
			
			String skillname = ce.getValue().trim().toLowerCase();
			SkillCdv dtoSkill = RestApiUtils.getSkill(skillname);
			
			
			
			
			if(Validator.isNull(dtoSkill)){
				//The skill is not present in the DB or it's mispelled
				continue;
			}
				
			
			UserSkillPK uspk = new UserSkillPK();
			uspk.setSkillId(dtoSkill.getSkillId());
			uspk.setUserId(uid);
			
			UserSkill us = null;
			try{ us = UserSkillLocalServiceUtil.getUserSkill(uspk); }
			catch(Exception e){
				//The user does not have that skill
				continue; 
			}
			
			if(!Validator.isNull(us)){
				
				//self-delete
				if (uid == pushmodel.getEndorserLiferayUserId()){
				
					try{ UserSkillLocalServiceUtil.deleteUserSkill(us); }
					catch(Exception e){
						throw new InternalErrorException("Unable to delete the association between User and Skill.");
					}
					
					EndorsementLocalServiceUtil.deleteAllEndorsementsBySkillAndUser(dtoSkill.getSkillId(), uid);	
					
				}else{//endorser != user
					
					EndorsementLocalServiceUtil.deleteEndorsementByPKs(dtoSkill.getSkillId(), uid, pushmodel.getEndorserLiferayUserId());
					long count = us.getEndorsmentCounter();
					
					List<Endorsement> ends = EndorsementLocalServiceUtil.getEndorsementsBySkillIdAndUserIdNoSelf (dtoSkill.getSkillId(), uid);
					
					
					if (ends.size()>0){//only if it has other endorsements
						
						long countnew = count -1;
						us.setEndorsmentCounter(countnew);
						try {
							UserSkillLocalServiceUtil.updateUserSkill(us);
						} catch (SystemException e) {
							
							e.printStackTrace();
						}
					}
					
					
					List<Endorsement> allEnds = EndorsementLocalServiceUtil.getEndorsementsBySkillIdAndUserId(dtoSkill.getSkillId(), uid);
					
					
					if (allEnds.size() == 0){// if there isn't other endorement, I remove the skill
						try {
							UserSkillLocalServiceUtil.deleteUserSkill(us);
						} catch (SystemException e) {
							e.printStackTrace();
						}
					}	
					
					
					
					
				}
			}
			else{
				//The user does not have that skill
				continue;
			}
		}
		
		return;
	}
	
	public static int getAge(Date birthday){
		
		Calendar birthDay = GregorianCalendar.getInstance();
		birthDay.setTime(birthday);
		Calendar now = GregorianCalendar.getInstance();
		
		//Get difference between years
		int years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
		int currMonth = now.get(Calendar.MONTH) + 1;
		int birthMonth = birthDay.get(Calendar.MONTH) + 1;
      
		//Get difference between months
		int months = currMonth - birthMonth;
      
		//if month difference is in negative then reduce years by one and calculate the number of months.
		if (months < 0){
			years--;
			months = 12 - birthMonth + currMonth;
			if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
				months--;
		} 
		else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)){
			years--;
			months = 11;
		}
      
		//Calculate the days
		int days = 0;
		if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
			days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
		else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)){
			int today = now.get(Calendar.DAY_OF_MONTH);
			now.add(Calendar.MONTH, -1);
			days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
		} 
		else{
			days = 0;
			if (months == 12){
				years++;
				months = 0;
			}
		}
		
		return years;
	}
	
}
