package it.eng.rspa.cdv.utils.model;

public enum UserIdeaRole {

	Author("Author"),
	Collaborator("Collaborator");
	
	private String text;
	
	private UserIdeaRole(String t){
		this.text = t;
	}
	
	public String toString(){
		return this.text;
	}
	
}
