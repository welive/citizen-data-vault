/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchPreferenceException;
import it.eng.rspa.cdv.datamodel.model.Preference;
import it.eng.rspa.cdv.datamodel.model.impl.PreferenceImpl;
import it.eng.rspa.cdv.datamodel.model.impl.PreferenceModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the preference service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see PreferencePersistence
 * @see PreferenceUtil
 * @generated
 */
public class PreferencePersistenceImpl extends BasePersistenceImpl<Preference>
	implements PreferencePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PreferenceUtil} to access the preference persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PreferenceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceModelImpl.FINDER_CACHE_ENABLED, PreferenceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceModelImpl.FINDER_CACHE_ENABLED, PreferenceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceModelImpl.FINDER_CACHE_ENABLED, PreferenceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByname",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceModelImpl.FINDER_CACHE_ENABLED, PreferenceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByname",
			new String[] { String.class.getName() },
			PreferenceModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByname",
			new String[] { String.class.getName() });

	/**
	 * Returns all the preferences where name = &#63;.
	 *
	 * @param name the name
	 * @return the matching preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Preference> findByname(String name) throws SystemException {
		return findByname(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the preferences where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.PreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of preferences
	 * @param end the upper bound of the range of preferences (not inclusive)
	 * @return the range of matching preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Preference> findByname(String name, int start, int end)
		throws SystemException {
		return findByname(name, start, end, null);
	}

	/**
	 * Returns an ordered range of all the preferences where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.PreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of preferences
	 * @param end the upper bound of the range of preferences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Preference> findByname(String name, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name, start, end, orderByComparator };
		}

		List<Preference> list = (List<Preference>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Preference preference : list) {
				if (!Validator.equals(name, preference.getName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PREFERENCE_WHERE);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PreferenceModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindName) {
					qPos.add(name.toLowerCase());
				}

				if (!pagination) {
					list = (List<Preference>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Preference>(list);
				}
				else {
					list = (List<Preference>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first preference in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchPreferenceException if a matching preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference findByname_First(String name,
		OrderByComparator orderByComparator)
		throws NoSuchPreferenceException, SystemException {
		Preference preference = fetchByname_First(name, orderByComparator);

		if (preference != null) {
			return preference;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPreferenceException(msg.toString());
	}

	/**
	 * Returns the first preference in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching preference, or <code>null</code> if a matching preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference fetchByname_First(String name,
		OrderByComparator orderByComparator) throws SystemException {
		List<Preference> list = findByname(name, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last preference in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchPreferenceException if a matching preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference findByname_Last(String name,
		OrderByComparator orderByComparator)
		throws NoSuchPreferenceException, SystemException {
		Preference preference = fetchByname_Last(name, orderByComparator);

		if (preference != null) {
			return preference;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPreferenceException(msg.toString());
	}

	/**
	 * Returns the last preference in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching preference, or <code>null</code> if a matching preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference fetchByname_Last(String name,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByname(name);

		if (count == 0) {
			return null;
		}

		List<Preference> list = findByname(name, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the preferences before and after the current preference in the ordered set where name = &#63;.
	 *
	 * @param preferenceId the primary key of the current preference
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchPreferenceException if a preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference[] findByname_PrevAndNext(long preferenceId, String name,
		OrderByComparator orderByComparator)
		throws NoSuchPreferenceException, SystemException {
		Preference preference = findByPrimaryKey(preferenceId);

		Session session = null;

		try {
			session = openSession();

			Preference[] array = new PreferenceImpl[3];

			array[0] = getByname_PrevAndNext(session, preference, name,
					orderByComparator, true);

			array[1] = preference;

			array[2] = getByname_PrevAndNext(session, preference, name,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Preference getByname_PrevAndNext(Session session,
		Preference preference, String name,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PREFERENCE_WHERE);

		boolean bindName = false;

		if (name == null) {
			query.append(_FINDER_COLUMN_NAME_NAME_1);
		}
		else if (name.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NAME_NAME_3);
		}
		else {
			bindName = true;

			query.append(_FINDER_COLUMN_NAME_NAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PreferenceModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindName) {
			qPos.add(name.toLowerCase());
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(preference);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Preference> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the preferences where name = &#63; from the database.
	 *
	 * @param name the name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByname(String name) throws SystemException {
		for (Preference preference : findByname(name, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(preference);
		}
	}

	/**
	 * Returns the number of preferences where name = &#63;.
	 *
	 * @param name the name
	 * @return the number of matching preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByname(String name) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

		Object[] finderArgs = new Object[] { name };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PREFERENCE_WHERE);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindName) {
					qPos.add(name.toLowerCase());
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NAME_NAME_1 = "preference.name IS NULL";
	private static final String _FINDER_COLUMN_NAME_NAME_2 = "lower(preference.name) = ?";
	private static final String _FINDER_COLUMN_NAME_NAME_3 = "(preference.name IS NULL OR preference.name = '')";

	public PreferencePersistenceImpl() {
		setModelClass(Preference.class);
	}

	/**
	 * Caches the preference in the entity cache if it is enabled.
	 *
	 * @param preference the preference
	 */
	@Override
	public void cacheResult(Preference preference) {
		EntityCacheUtil.putResult(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceImpl.class, preference.getPrimaryKey(), preference);

		preference.resetOriginalValues();
	}

	/**
	 * Caches the preferences in the entity cache if it is enabled.
	 *
	 * @param preferences the preferences
	 */
	@Override
	public void cacheResult(List<Preference> preferences) {
		for (Preference preference : preferences) {
			if (EntityCacheUtil.getResult(
						PreferenceModelImpl.ENTITY_CACHE_ENABLED,
						PreferenceImpl.class, preference.getPrimaryKey()) == null) {
				cacheResult(preference);
			}
			else {
				preference.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all preferences.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PreferenceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PreferenceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the preference.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Preference preference) {
		EntityCacheUtil.removeResult(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceImpl.class, preference.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Preference> preferences) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Preference preference : preferences) {
			EntityCacheUtil.removeResult(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
				PreferenceImpl.class, preference.getPrimaryKey());
		}
	}

	/**
	 * Creates a new preference with the primary key. Does not add the preference to the database.
	 *
	 * @param preferenceId the primary key for the new preference
	 * @return the new preference
	 */
	@Override
	public Preference create(long preferenceId) {
		Preference preference = new PreferenceImpl();

		preference.setNew(true);
		preference.setPrimaryKey(preferenceId);

		return preference;
	}

	/**
	 * Removes the preference with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param preferenceId the primary key of the preference
	 * @return the preference that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchPreferenceException if a preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference remove(long preferenceId)
		throws NoSuchPreferenceException, SystemException {
		return remove((Serializable)preferenceId);
	}

	/**
	 * Removes the preference with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the preference
	 * @return the preference that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchPreferenceException if a preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference remove(Serializable primaryKey)
		throws NoSuchPreferenceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Preference preference = (Preference)session.get(PreferenceImpl.class,
					primaryKey);

			if (preference == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPreferenceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(preference);
		}
		catch (NoSuchPreferenceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Preference removeImpl(Preference preference)
		throws SystemException {
		preference = toUnwrappedModel(preference);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(preference)) {
				preference = (Preference)session.get(PreferenceImpl.class,
						preference.getPrimaryKeyObj());
			}

			if (preference != null) {
				session.delete(preference);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (preference != null) {
			clearCache(preference);
		}

		return preference;
	}

	@Override
	public Preference updateImpl(
		it.eng.rspa.cdv.datamodel.model.Preference preference)
		throws SystemException {
		preference = toUnwrappedModel(preference);

		boolean isNew = preference.isNew();

		PreferenceModelImpl preferenceModelImpl = (PreferenceModelImpl)preference;

		Session session = null;

		try {
			session = openSession();

			if (preference.isNew()) {
				session.save(preference);

				preference.setNew(false);
			}
			else {
				session.merge(preference);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PreferenceModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((preferenceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						preferenceModelImpl.getOriginalName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);

				args = new Object[] { preferenceModelImpl.getName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);
			}
		}

		EntityCacheUtil.putResult(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
			PreferenceImpl.class, preference.getPrimaryKey(), preference);

		return preference;
	}

	protected Preference toUnwrappedModel(Preference preference) {
		if (preference instanceof PreferenceImpl) {
			return preference;
		}

		PreferenceImpl preferenceImpl = new PreferenceImpl();

		preferenceImpl.setNew(preference.isNew());
		preferenceImpl.setPrimaryKey(preference.getPrimaryKey());

		preferenceImpl.setPreferenceId(preference.getPreferenceId());
		preferenceImpl.setName(preference.getName());

		return preferenceImpl;
	}

	/**
	 * Returns the preference with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the preference
	 * @return the preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchPreferenceException if a preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPreferenceException, SystemException {
		Preference preference = fetchByPrimaryKey(primaryKey);

		if (preference == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPreferenceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return preference;
	}

	/**
	 * Returns the preference with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchPreferenceException} if it could not be found.
	 *
	 * @param preferenceId the primary key of the preference
	 * @return the preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchPreferenceException if a preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference findByPrimaryKey(long preferenceId)
		throws NoSuchPreferenceException, SystemException {
		return findByPrimaryKey((Serializable)preferenceId);
	}

	/**
	 * Returns the preference with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the preference
	 * @return the preference, or <code>null</code> if a preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Preference preference = (Preference)EntityCacheUtil.getResult(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
				PreferenceImpl.class, primaryKey);

		if (preference == _nullPreference) {
			return null;
		}

		if (preference == null) {
			Session session = null;

			try {
				session = openSession();

				preference = (Preference)session.get(PreferenceImpl.class,
						primaryKey);

				if (preference != null) {
					cacheResult(preference);
				}
				else {
					EntityCacheUtil.putResult(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
						PreferenceImpl.class, primaryKey, _nullPreference);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PreferenceModelImpl.ENTITY_CACHE_ENABLED,
					PreferenceImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return preference;
	}

	/**
	 * Returns the preference with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param preferenceId the primary key of the preference
	 * @return the preference, or <code>null</code> if a preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Preference fetchByPrimaryKey(long preferenceId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)preferenceId);
	}

	/**
	 * Returns all the preferences.
	 *
	 * @return the preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Preference> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the preferences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.PreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of preferences
	 * @param end the upper bound of the range of preferences (not inclusive)
	 * @return the range of preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Preference> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the preferences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.PreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of preferences
	 * @param end the upper bound of the range of preferences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Preference> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Preference> list = (List<Preference>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PREFERENCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PREFERENCE;

				if (pagination) {
					sql = sql.concat(PreferenceModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Preference>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Preference>(list);
				}
				else {
					list = (List<Preference>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the preferences from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Preference preference : findAll()) {
			remove(preference);
		}
	}

	/**
	 * Returns the number of preferences.
	 *
	 * @return the number of preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PREFERENCE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the preference persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.Preference")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Preference>> listenersList = new ArrayList<ModelListener<Preference>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Preference>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PreferenceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PREFERENCE = "SELECT preference FROM Preference preference";
	private static final String _SQL_SELECT_PREFERENCE_WHERE = "SELECT preference FROM Preference preference WHERE ";
	private static final String _SQL_COUNT_PREFERENCE = "SELECT COUNT(preference) FROM Preference preference";
	private static final String _SQL_COUNT_PREFERENCE_WHERE = "SELECT COUNT(preference) FROM Preference preference WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "preference.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Preference exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Preference exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PreferencePersistenceImpl.class);
	private static Preference _nullPreference = new PreferenceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Preference> toCacheModel() {
				return _nullPreferenceCacheModel;
			}
		};

	private static CacheModel<Preference> _nullPreferenceCacheModel = new CacheModel<Preference>() {
			@Override
			public Preference toEntityModel() {
				return _nullPreference;
			}
		};
}