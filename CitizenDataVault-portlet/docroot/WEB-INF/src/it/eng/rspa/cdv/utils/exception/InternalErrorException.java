package it.eng.rspa.cdv.utils.exception;

public class InternalErrorException extends CustomException{

	private static final long serialVersionUID = 4L;
	
	public InternalErrorException(String message){
		super(message);
		setMessageNumber(4L);
	}
	
}
