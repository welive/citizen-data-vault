/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import it.eng.rspa.cdv.datamodel.model.impl.UserCdvImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the user cdv service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserCdvPersistence
 * @see UserCdvUtil
 * @generated
 */
public class UserCdvPersistenceImpl extends BasePersistenceImpl<UserCdv>
	implements UserCdvPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserCdvUtil} to access the user cdv persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserCdvImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CCUID = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByccuid",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CCUID = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByccuid",
			new String[] { Long.class.getName() },
			UserCdvModelImpl.CCUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CCUID = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByccuid",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user cdvs where ccuid = &#63;.
	 *
	 * @param ccuid the ccuid
	 * @return the matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByccuid(long ccuid) throws SystemException {
		return findByccuid(ccuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user cdvs where ccuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ccuid the ccuid
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @return the range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByccuid(long ccuid, int start, int end)
		throws SystemException {
		return findByccuid(ccuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user cdvs where ccuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ccuid the ccuid
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByccuid(long ccuid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CCUID;
			finderArgs = new Object[] { ccuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CCUID;
			finderArgs = new Object[] { ccuid, start, end, orderByComparator };
		}

		List<UserCdv> list = (List<UserCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserCdv userCdv : list) {
				if ((ccuid != userCdv.getCcuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERCDV_WHERE);

			query.append(_FINDER_COLUMN_CCUID_CCUID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserCdvModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ccuid);

				if (!pagination) {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserCdv>(list);
				}
				else {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user cdv in the ordered set where ccuid = &#63;.
	 *
	 * @param ccuid the ccuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByccuid_First(long ccuid,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByccuid_First(ccuid, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ccuid=");
		msg.append(ccuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the first user cdv in the ordered set where ccuid = &#63;.
	 *
	 * @param ccuid the ccuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByccuid_First(long ccuid,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserCdv> list = findByccuid(ccuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user cdv in the ordered set where ccuid = &#63;.
	 *
	 * @param ccuid the ccuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByccuid_Last(long ccuid,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByccuid_Last(ccuid, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ccuid=");
		msg.append(ccuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the last user cdv in the ordered set where ccuid = &#63;.
	 *
	 * @param ccuid the ccuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByccuid_Last(long ccuid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByccuid(ccuid);

		if (count == 0) {
			return null;
		}

		List<UserCdv> list = findByccuid(ccuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user cdvs before and after the current user cdv in the ordered set where ccuid = &#63;.
	 *
	 * @param userCdvPK the primary key of the current user cdv
	 * @param ccuid the ccuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv[] findByccuid_PrevAndNext(UserCdvPK userCdvPK, long ccuid,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = findByPrimaryKey(userCdvPK);

		Session session = null;

		try {
			session = openSession();

			UserCdv[] array = new UserCdvImpl[3];

			array[0] = getByccuid_PrevAndNext(session, userCdv, ccuid,
					orderByComparator, true);

			array[1] = userCdv;

			array[2] = getByccuid_PrevAndNext(session, userCdv, ccuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserCdv getByccuid_PrevAndNext(Session session, UserCdv userCdv,
		long ccuid, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERCDV_WHERE);

		query.append(_FINDER_COLUMN_CCUID_CCUID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserCdvModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ccuid);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userCdv);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserCdv> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user cdvs where ccuid = &#63; from the database.
	 *
	 * @param ccuid the ccuid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByccuid(long ccuid) throws SystemException {
		for (UserCdv userCdv : findByccuid(ccuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userCdv);
		}
	}

	/**
	 * Returns the number of user cdvs where ccuid = &#63;.
	 *
	 * @param ccuid the ccuid
	 * @return the number of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByccuid(long ccuid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CCUID;

		Object[] finderArgs = new Object[] { ccuid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERCDV_WHERE);

			query.append(_FINDER_COLUMN_CCUID_CCUID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ccuid);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CCUID_CCUID_2 = "userCdv.id.ccuid = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			UserCdvModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user cdvs where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByuserId(long userId) throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user cdvs where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @return the range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user cdvs where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByuserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UserCdv> list = (List<UserCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserCdv userCdv : list) {
				if ((userId != userCdv.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERCDV_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserCdvModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserCdv>(list);
				}
				else {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user cdv in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByuserId_First(userId, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the first user cdv in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserCdv> list = findByuserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user cdv in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByuserId_Last(userId, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the last user cdv in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		if (count == 0) {
			return null;
		}

		List<UserCdv> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user cdvs before and after the current user cdv in the ordered set where userId = &#63;.
	 *
	 * @param userCdvPK the primary key of the current user cdv
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv[] findByuserId_PrevAndNext(UserCdvPK userCdvPK, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = findByPrimaryKey(userCdvPK);

		Session session = null;

		try {
			session = openSession();

			UserCdv[] array = new UserCdvImpl[3];

			array[0] = getByuserId_PrevAndNext(session, userCdv, userId,
					orderByComparator, true);

			array[1] = userCdv;

			array[2] = getByuserId_PrevAndNext(session, userCdv, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserCdv getByuserId_PrevAndNext(Session session, UserCdv userCdv,
		long userId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERCDV_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserCdvModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userCdv);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserCdv> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user cdvs where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserId(long userId) throws SystemException {
		for (UserCdv userCdv : findByuserId(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userCdv);
		}
	}

	/**
	 * Returns the number of user cdvs where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERCDV_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "userCdv.id.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERNAME = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByusername",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERNAME =
		new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByusername",
			new String[] { String.class.getName() },
			UserCdvModelImpl.USERNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERNAME = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByusername",
			new String[] { String.class.getName() });

	/**
	 * Returns all the user cdvs where username = &#63;.
	 *
	 * @param username the username
	 * @return the matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByusername(String username)
		throws SystemException {
		return findByusername(username, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the user cdvs where username = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param username the username
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @return the range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByusername(String username, int start, int end)
		throws SystemException {
		return findByusername(username, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user cdvs where username = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param username the username
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByusername(String username, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERNAME;
			finderArgs = new Object[] { username };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERNAME;
			finderArgs = new Object[] { username, start, end, orderByComparator };
		}

		List<UserCdv> list = (List<UserCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserCdv userCdv : list) {
				if (!Validator.equals(username, userCdv.getUsername())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERCDV_WHERE);

			boolean bindUsername = false;

			if (username == null) {
				query.append(_FINDER_COLUMN_USERNAME_USERNAME_1);
			}
			else if (username.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERNAME_USERNAME_3);
			}
			else {
				bindUsername = true;

				query.append(_FINDER_COLUMN_USERNAME_USERNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserCdvModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUsername) {
					qPos.add(username);
				}

				if (!pagination) {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserCdv>(list);
				}
				else {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user cdv in the ordered set where username = &#63;.
	 *
	 * @param username the username
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByusername_First(String username,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByusername_First(username, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("username=");
		msg.append(username);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the first user cdv in the ordered set where username = &#63;.
	 *
	 * @param username the username
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByusername_First(String username,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserCdv> list = findByusername(username, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user cdv in the ordered set where username = &#63;.
	 *
	 * @param username the username
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByusername_Last(String username,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByusername_Last(username, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("username=");
		msg.append(username);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the last user cdv in the ordered set where username = &#63;.
	 *
	 * @param username the username
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByusername_Last(String username,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByusername(username);

		if (count == 0) {
			return null;
		}

		List<UserCdv> list = findByusername(username, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user cdvs before and after the current user cdv in the ordered set where username = &#63;.
	 *
	 * @param userCdvPK the primary key of the current user cdv
	 * @param username the username
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv[] findByusername_PrevAndNext(UserCdvPK userCdvPK,
		String username, OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = findByPrimaryKey(userCdvPK);

		Session session = null;

		try {
			session = openSession();

			UserCdv[] array = new UserCdvImpl[3];

			array[0] = getByusername_PrevAndNext(session, userCdv, username,
					orderByComparator, true);

			array[1] = userCdv;

			array[2] = getByusername_PrevAndNext(session, userCdv, username,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserCdv getByusername_PrevAndNext(Session session,
		UserCdv userCdv, String username, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERCDV_WHERE);

		boolean bindUsername = false;

		if (username == null) {
			query.append(_FINDER_COLUMN_USERNAME_USERNAME_1);
		}
		else if (username.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_USERNAME_USERNAME_3);
		}
		else {
			bindUsername = true;

			query.append(_FINDER_COLUMN_USERNAME_USERNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserCdvModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUsername) {
			qPos.add(username);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userCdv);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserCdv> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user cdvs where username = &#63; from the database.
	 *
	 * @param username the username
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByusername(String username) throws SystemException {
		for (UserCdv userCdv : findByusername(username, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userCdv);
		}
	}

	/**
	 * Returns the number of user cdvs where username = &#63;.
	 *
	 * @param username the username
	 * @return the number of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByusername(String username) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERNAME;

		Object[] finderArgs = new Object[] { username };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERCDV_WHERE);

			boolean bindUsername = false;

			if (username == null) {
				query.append(_FINDER_COLUMN_USERNAME_USERNAME_1);
			}
			else if (username.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERNAME_USERNAME_3);
			}
			else {
				bindUsername = true;

				query.append(_FINDER_COLUMN_USERNAME_USERNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUsername) {
					qPos.add(username);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERNAME_USERNAME_1 = "userCdv.username IS NULL";
	private static final String _FINDER_COLUMN_USERNAME_USERNAME_2 = "userCdv.username = ?";
	private static final String _FINDER_COLUMN_USERNAME_USERNAME_3 = "(userCdv.username IS NULL OR userCdv.username = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMAIL = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByemail",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByemail",
			new String[] { String.class.getName() },
			UserCdvModelImpl.EMAIL_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMAIL = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByemail",
			new String[] { String.class.getName() });

	/**
	 * Returns all the user cdvs where email = &#63;.
	 *
	 * @param email the email
	 * @return the matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByemail(String email) throws SystemException {
		return findByemail(email, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user cdvs where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @return the range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByemail(String email, int start, int end)
		throws SystemException {
		return findByemail(email, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user cdvs where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findByemail(String email, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL;
			finderArgs = new Object[] { email };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMAIL;
			finderArgs = new Object[] { email, start, end, orderByComparator };
		}

		List<UserCdv> list = (List<UserCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserCdv userCdv : list) {
				if (!Validator.equals(email, userCdv.getEmail())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERCDV_WHERE);

			boolean bindEmail = false;

			if (email == null) {
				query.append(_FINDER_COLUMN_EMAIL_EMAIL_1);
			}
			else if (email.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EMAIL_EMAIL_3);
			}
			else {
				bindEmail = true;

				query.append(_FINDER_COLUMN_EMAIL_EMAIL_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserCdvModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEmail) {
					qPos.add(email);
				}

				if (!pagination) {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserCdv>(list);
				}
				else {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user cdv in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByemail_First(String email,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByemail_First(email, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("email=");
		msg.append(email);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the first user cdv in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByemail_First(String email,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserCdv> list = findByemail(email, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user cdv in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByemail_Last(String email,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByemail_Last(email, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("email=");
		msg.append(email);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the last user cdv in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByemail_Last(String email,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByemail(email);

		if (count == 0) {
			return null;
		}

		List<UserCdv> list = findByemail(email, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user cdvs before and after the current user cdv in the ordered set where email = &#63;.
	 *
	 * @param userCdvPK the primary key of the current user cdv
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv[] findByemail_PrevAndNext(UserCdvPK userCdvPK, String email,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = findByPrimaryKey(userCdvPK);

		Session session = null;

		try {
			session = openSession();

			UserCdv[] array = new UserCdvImpl[3];

			array[0] = getByemail_PrevAndNext(session, userCdv, email,
					orderByComparator, true);

			array[1] = userCdv;

			array[2] = getByemail_PrevAndNext(session, userCdv, email,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserCdv getByemail_PrevAndNext(Session session, UserCdv userCdv,
		String email, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERCDV_WHERE);

		boolean bindEmail = false;

		if (email == null) {
			query.append(_FINDER_COLUMN_EMAIL_EMAIL_1);
		}
		else if (email.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_EMAIL_EMAIL_3);
		}
		else {
			bindEmail = true;

			query.append(_FINDER_COLUMN_EMAIL_EMAIL_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserCdvModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindEmail) {
			qPos.add(email);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userCdv);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserCdv> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user cdvs where email = &#63; from the database.
	 *
	 * @param email the email
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByemail(String email) throws SystemException {
		for (UserCdv userCdv : findByemail(email, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userCdv);
		}
	}

	/**
	 * Returns the number of user cdvs where email = &#63;.
	 *
	 * @param email the email
	 * @return the number of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByemail(String email) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_EMAIL;

		Object[] finderArgs = new Object[] { email };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERCDV_WHERE);

			boolean bindEmail = false;

			if (email == null) {
				query.append(_FINDER_COLUMN_EMAIL_EMAIL_1);
			}
			else if (email.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EMAIL_EMAIL_3);
			}
			else {
				bindEmail = true;

				query.append(_FINDER_COLUMN_EMAIL_EMAIL_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEmail) {
					qPos.add(email);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EMAIL_EMAIL_1 = "userCdv.email IS NULL";
	private static final String _FINDER_COLUMN_EMAIL_EMAIL_2 = "userCdv.email = ?";
	private static final String _FINDER_COLUMN_EMAIL_EMAIL_3 = "(userCdv.email IS NULL OR userCdv.email = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOT = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBypilot",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, UserCdvImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBypilot",
			new String[] { String.class.getName() },
			UserCdvModelImpl.PILOT_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PILOT = new FinderPath(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBypilot",
			new String[] { String.class.getName() });

	/**
	 * Returns all the user cdvs where pilot = &#63;.
	 *
	 * @param pilot the pilot
	 * @return the matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findBypilot(String pilot) throws SystemException {
		return findBypilot(pilot, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user cdvs where pilot = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilot the pilot
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @return the range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findBypilot(String pilot, int start, int end)
		throws SystemException {
		return findBypilot(pilot, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user cdvs where pilot = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilot the pilot
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findBypilot(String pilot, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT;
			finderArgs = new Object[] { pilot };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOT;
			finderArgs = new Object[] { pilot, start, end, orderByComparator };
		}

		List<UserCdv> list = (List<UserCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserCdv userCdv : list) {
				if (!Validator.equals(pilot, userCdv.getPilot())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERCDV_WHERE);

			boolean bindPilot = false;

			if (pilot == null) {
				query.append(_FINDER_COLUMN_PILOT_PILOT_1);
			}
			else if (pilot.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOT_PILOT_3);
			}
			else {
				bindPilot = true;

				query.append(_FINDER_COLUMN_PILOT_PILOT_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserCdvModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilot) {
					qPos.add(pilot);
				}

				if (!pagination) {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserCdv>(list);
				}
				else {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user cdv in the ordered set where pilot = &#63;.
	 *
	 * @param pilot the pilot
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findBypilot_First(String pilot,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchBypilot_First(pilot, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilot=");
		msg.append(pilot);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the first user cdv in the ordered set where pilot = &#63;.
	 *
	 * @param pilot the pilot
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchBypilot_First(String pilot,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserCdv> list = findBypilot(pilot, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user cdv in the ordered set where pilot = &#63;.
	 *
	 * @param pilot the pilot
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findBypilot_Last(String pilot,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchBypilot_Last(pilot, orderByComparator);

		if (userCdv != null) {
			return userCdv;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilot=");
		msg.append(pilot);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserCdvException(msg.toString());
	}

	/**
	 * Returns the last user cdv in the ordered set where pilot = &#63;.
	 *
	 * @param pilot the pilot
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchBypilot_Last(String pilot,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBypilot(pilot);

		if (count == 0) {
			return null;
		}

		List<UserCdv> list = findBypilot(pilot, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user cdvs before and after the current user cdv in the ordered set where pilot = &#63;.
	 *
	 * @param userCdvPK the primary key of the current user cdv
	 * @param pilot the pilot
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv[] findBypilot_PrevAndNext(UserCdvPK userCdvPK, String pilot,
		OrderByComparator orderByComparator)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = findByPrimaryKey(userCdvPK);

		Session session = null;

		try {
			session = openSession();

			UserCdv[] array = new UserCdvImpl[3];

			array[0] = getBypilot_PrevAndNext(session, userCdv, pilot,
					orderByComparator, true);

			array[1] = userCdv;

			array[2] = getBypilot_PrevAndNext(session, userCdv, pilot,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserCdv getBypilot_PrevAndNext(Session session, UserCdv userCdv,
		String pilot, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERCDV_WHERE);

		boolean bindPilot = false;

		if (pilot == null) {
			query.append(_FINDER_COLUMN_PILOT_PILOT_1);
		}
		else if (pilot.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PILOT_PILOT_3);
		}
		else {
			bindPilot = true;

			query.append(_FINDER_COLUMN_PILOT_PILOT_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserCdvModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindPilot) {
			qPos.add(pilot);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userCdv);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserCdv> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user cdvs where pilot = &#63; from the database.
	 *
	 * @param pilot the pilot
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBypilot(String pilot) throws SystemException {
		for (UserCdv userCdv : findBypilot(pilot, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userCdv);
		}
	}

	/**
	 * Returns the number of user cdvs where pilot = &#63;.
	 *
	 * @param pilot the pilot
	 * @return the number of matching user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBypilot(String pilot) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PILOT;

		Object[] finderArgs = new Object[] { pilot };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERCDV_WHERE);

			boolean bindPilot = false;

			if (pilot == null) {
				query.append(_FINDER_COLUMN_PILOT_PILOT_1);
			}
			else if (pilot.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOT_PILOT_3);
			}
			else {
				bindPilot = true;

				query.append(_FINDER_COLUMN_PILOT_PILOT_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilot) {
					qPos.add(pilot);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PILOT_PILOT_1 = "userCdv.pilot IS NULL";
	private static final String _FINDER_COLUMN_PILOT_PILOT_2 = "userCdv.pilot = ?";
	private static final String _FINDER_COLUMN_PILOT_PILOT_3 = "(userCdv.pilot IS NULL OR userCdv.pilot = '')";

	public UserCdvPersistenceImpl() {
		setModelClass(UserCdv.class);
	}

	/**
	 * Caches the user cdv in the entity cache if it is enabled.
	 *
	 * @param userCdv the user cdv
	 */
	@Override
	public void cacheResult(UserCdv userCdv) {
		EntityCacheUtil.putResult(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvImpl.class, userCdv.getPrimaryKey(), userCdv);

		userCdv.resetOriginalValues();
	}

	/**
	 * Caches the user cdvs in the entity cache if it is enabled.
	 *
	 * @param userCdvs the user cdvs
	 */
	@Override
	public void cacheResult(List<UserCdv> userCdvs) {
		for (UserCdv userCdv : userCdvs) {
			if (EntityCacheUtil.getResult(
						UserCdvModelImpl.ENTITY_CACHE_ENABLED,
						UserCdvImpl.class, userCdv.getPrimaryKey()) == null) {
				cacheResult(userCdv);
			}
			else {
				userCdv.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user cdvs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UserCdvImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UserCdvImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user cdv.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserCdv userCdv) {
		EntityCacheUtil.removeResult(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvImpl.class, userCdv.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserCdv> userCdvs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserCdv userCdv : userCdvs) {
			EntityCacheUtil.removeResult(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
				UserCdvImpl.class, userCdv.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user cdv with the primary key. Does not add the user cdv to the database.
	 *
	 * @param userCdvPK the primary key for the new user cdv
	 * @return the new user cdv
	 */
	@Override
	public UserCdv create(UserCdvPK userCdvPK) {
		UserCdv userCdv = new UserCdvImpl();

		userCdv.setNew(true);
		userCdv.setPrimaryKey(userCdvPK);

		return userCdv;
	}

	/**
	 * Removes the user cdv with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userCdvPK the primary key of the user cdv
	 * @return the user cdv that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv remove(UserCdvPK userCdvPK)
		throws NoSuchUserCdvException, SystemException {
		return remove((Serializable)userCdvPK);
	}

	/**
	 * Removes the user cdv with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user cdv
	 * @return the user cdv that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv remove(Serializable primaryKey)
		throws NoSuchUserCdvException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UserCdv userCdv = (UserCdv)session.get(UserCdvImpl.class, primaryKey);

			if (userCdv == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserCdvException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userCdv);
		}
		catch (NoSuchUserCdvException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserCdv removeImpl(UserCdv userCdv) throws SystemException {
		userCdv = toUnwrappedModel(userCdv);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userCdv)) {
				userCdv = (UserCdv)session.get(UserCdvImpl.class,
						userCdv.getPrimaryKeyObj());
			}

			if (userCdv != null) {
				session.delete(userCdv);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userCdv != null) {
			clearCache(userCdv);
		}

		return userCdv;
	}

	@Override
	public UserCdv updateImpl(it.eng.rspa.cdv.datamodel.model.UserCdv userCdv)
		throws SystemException {
		userCdv = toUnwrappedModel(userCdv);

		boolean isNew = userCdv.isNew();

		UserCdvModelImpl userCdvModelImpl = (UserCdvModelImpl)userCdv;

		Session session = null;

		try {
			session = openSession();

			if (userCdv.isNew()) {
				session.save(userCdv);

				userCdv.setNew(false);
			}
			else {
				session.merge(userCdv);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UserCdvModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((userCdvModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CCUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { userCdvModelImpl.getOriginalCcuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CCUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CCUID,
					args);

				args = new Object[] { userCdvModelImpl.getCcuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CCUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CCUID,
					args);
			}

			if ((userCdvModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userCdvModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { userCdvModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((userCdvModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userCdvModelImpl.getOriginalUsername()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERNAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERNAME,
					args);

				args = new Object[] { userCdvModelImpl.getUsername() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERNAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERNAME,
					args);
			}

			if ((userCdvModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { userCdvModelImpl.getOriginalEmail() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMAIL, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL,
					args);

				args = new Object[] { userCdvModelImpl.getEmail() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMAIL, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL,
					args);
			}

			if ((userCdvModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { userCdvModelImpl.getOriginalPilot() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT,
					args);

				args = new Object[] { userCdvModelImpl.getPilot() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT,
					args);
			}
		}

		EntityCacheUtil.putResult(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
			UserCdvImpl.class, userCdv.getPrimaryKey(), userCdv);

		return userCdv;
	}

	protected UserCdv toUnwrappedModel(UserCdv userCdv) {
		if (userCdv instanceof UserCdvImpl) {
			return userCdv;
		}

		UserCdvImpl userCdvImpl = new UserCdvImpl();

		userCdvImpl.setNew(userCdv.isNew());
		userCdvImpl.setPrimaryKey(userCdv.getPrimaryKey());

		userCdvImpl.setCcuid(userCdv.getCcuid());
		userCdvImpl.setUserId(userCdv.getUserId());
		userCdvImpl.setCompanyId(userCdv.getCompanyId());
		userCdvImpl.setUsername(userCdv.getUsername());
		userCdvImpl.setPassword(userCdv.getPassword());
		userCdvImpl.setName(userCdv.getName());
		userCdvImpl.setSurname(userCdv.getSurname());
		userCdvImpl.setGender(userCdv.getGender());
		userCdvImpl.setBirthdate(userCdv.getBirthdate());
		userCdvImpl.setAddress(userCdv.getAddress());
		userCdvImpl.setCity(userCdv.getCity());
		userCdvImpl.setCountry(userCdv.getCountry());
		userCdvImpl.setZipcode(userCdv.getZipcode());
		userCdvImpl.setEmail(userCdv.getEmail());
		userCdvImpl.setIsDeveloper(userCdv.isIsDeveloper());
		userCdvImpl.setLastKnownLatitude(userCdv.getLastKnownLatitude());
		userCdvImpl.setLastKnownLongitude(userCdv.getLastKnownLongitude());
		userCdvImpl.setPilot(userCdv.getPilot());
		userCdvImpl.setReputation(userCdv.getReputation());
		userCdvImpl.setEmployement(userCdv.getEmployement());

		return userCdvImpl;
	}

	/**
	 * Returns the user cdv with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user cdv
	 * @return the user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserCdvException, SystemException {
		UserCdv userCdv = fetchByPrimaryKey(primaryKey);

		if (userCdv == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserCdvException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userCdv;
	}

	/**
	 * Returns the user cdv with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserCdvException} if it could not be found.
	 *
	 * @param userCdvPK the primary key of the user cdv
	 * @return the user cdv
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv findByPrimaryKey(UserCdvPK userCdvPK)
		throws NoSuchUserCdvException, SystemException {
		return findByPrimaryKey((Serializable)userCdvPK);
	}

	/**
	 * Returns the user cdv with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user cdv
	 * @return the user cdv, or <code>null</code> if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UserCdv userCdv = (UserCdv)EntityCacheUtil.getResult(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
				UserCdvImpl.class, primaryKey);

		if (userCdv == _nullUserCdv) {
			return null;
		}

		if (userCdv == null) {
			Session session = null;

			try {
				session = openSession();

				userCdv = (UserCdv)session.get(UserCdvImpl.class, primaryKey);

				if (userCdv != null) {
					cacheResult(userCdv);
				}
				else {
					EntityCacheUtil.putResult(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
						UserCdvImpl.class, primaryKey, _nullUserCdv);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UserCdvModelImpl.ENTITY_CACHE_ENABLED,
					UserCdvImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userCdv;
	}

	/**
	 * Returns the user cdv with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userCdvPK the primary key of the user cdv
	 * @return the user cdv, or <code>null</code> if a user cdv with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserCdv fetchByPrimaryKey(UserCdvPK userCdvPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)userCdvPK);
	}

	/**
	 * Returns all the user cdvs.
	 *
	 * @return the user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user cdvs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @return the range of user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user cdvs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user cdvs
	 * @param end the upper bound of the range of user cdvs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserCdv> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserCdv> list = (List<UserCdv>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_USERCDV);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERCDV;

				if (pagination) {
					sql = sql.concat(UserCdvModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserCdv>(list);
				}
				else {
					list = (List<UserCdv>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user cdvs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UserCdv userCdv : findAll()) {
			remove(userCdv);
		}
	}

	/**
	 * Returns the number of user cdvs.
	 *
	 * @return the number of user cdvs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERCDV);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the user cdv persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.UserCdv")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UserCdv>> listenersList = new ArrayList<ModelListener<UserCdv>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UserCdv>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UserCdvImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_USERCDV = "SELECT userCdv FROM UserCdv userCdv";
	private static final String _SQL_SELECT_USERCDV_WHERE = "SELECT userCdv FROM UserCdv userCdv WHERE ";
	private static final String _SQL_COUNT_USERCDV = "SELECT COUNT(userCdv) FROM UserCdv userCdv";
	private static final String _SQL_COUNT_USERCDV_WHERE = "SELECT COUNT(userCdv) FROM UserCdv userCdv WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userCdv.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserCdv exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserCdv exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UserCdvPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"password"
			});
	private static UserCdv _nullUserCdv = new UserCdvImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UserCdv> toCacheModel() {
				return _nullUserCdvCacheModel;
			}
		};

	private static CacheModel<UserCdv> _nullUserCdvCacheModel = new CacheModel<UserCdv>() {
			@Override
			public UserCdv toEntityModel() {
				return _nullUserCdv;
			}
		};
}