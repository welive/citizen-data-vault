/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException;
import it.eng.rspa.cdv.datamodel.model.UserThirdProfile;
import it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the user third profile service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserThirdProfilePersistence
 * @see UserThirdProfileUtil
 * @generated
 */
public class UserThirdProfilePersistenceImpl extends BasePersistenceImpl<UserThirdProfile>
	implements UserThirdProfilePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserThirdProfileUtil} to access the user third profile persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserThirdProfileImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileModelImpl.FINDER_CACHE_ENABLED,
			UserThirdProfileImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileModelImpl.FINDER_CACHE_ENABLED,
			UserThirdProfileImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileModelImpl.FINDER_CACHE_ENABLED,
			UserThirdProfileImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByuserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileModelImpl.FINDER_CACHE_ENABLED,
			UserThirdProfileImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			UserThirdProfileModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user third profiles where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching user third profiles
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserThirdProfile> findByuserId(long userId)
		throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user third profiles where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user third profiles
	 * @param end the upper bound of the range of user third profiles (not inclusive)
	 * @return the range of matching user third profiles
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserThirdProfile> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user third profiles where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user third profiles
	 * @param end the upper bound of the range of user third profiles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user third profiles
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserThirdProfile> findByuserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UserThirdProfile> list = (List<UserThirdProfile>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserThirdProfile userThirdProfile : list) {
				if ((userId != userThirdProfile.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERTHIRDPROFILE_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserThirdProfileModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UserThirdProfile>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserThirdProfile>(list);
				}
				else {
					list = (List<UserThirdProfile>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user third profile in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user third profile
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException if a matching user third profile could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserThirdProfileException, SystemException {
		UserThirdProfile userThirdProfile = fetchByuserId_First(userId,
				orderByComparator);

		if (userThirdProfile != null) {
			return userThirdProfile;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserThirdProfileException(msg.toString());
	}

	/**
	 * Returns the first user third profile in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user third profile, or <code>null</code> if a matching user third profile could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserThirdProfile> list = findByuserId(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user third profile in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user third profile
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException if a matching user third profile could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserThirdProfileException, SystemException {
		UserThirdProfile userThirdProfile = fetchByuserId_Last(userId,
				orderByComparator);

		if (userThirdProfile != null) {
			return userThirdProfile;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserThirdProfileException(msg.toString());
	}

	/**
	 * Returns the last user third profile in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user third profile, or <code>null</code> if a matching user third profile could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		if (count == 0) {
			return null;
		}

		List<UserThirdProfile> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user third profiles before and after the current user third profile in the ordered set where userId = &#63;.
	 *
	 * @param userThirdProfilePK the primary key of the current user third profile
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user third profile
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException if a user third profile with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile[] findByuserId_PrevAndNext(
		UserThirdProfilePK userThirdProfilePK, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserThirdProfileException, SystemException {
		UserThirdProfile userThirdProfile = findByPrimaryKey(userThirdProfilePK);

		Session session = null;

		try {
			session = openSession();

			UserThirdProfile[] array = new UserThirdProfileImpl[3];

			array[0] = getByuserId_PrevAndNext(session, userThirdProfile,
					userId, orderByComparator, true);

			array[1] = userThirdProfile;

			array[2] = getByuserId_PrevAndNext(session, userThirdProfile,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserThirdProfile getByuserId_PrevAndNext(Session session,
		UserThirdProfile userThirdProfile, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERTHIRDPROFILE_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserThirdProfileModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userThirdProfile);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserThirdProfile> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user third profiles where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserId(long userId) throws SystemException {
		for (UserThirdProfile userThirdProfile : findByuserId(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userThirdProfile);
		}
	}

	/**
	 * Returns the number of user third profiles where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching user third profiles
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERTHIRDPROFILE_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "userThirdProfile.id.userId = ?";

	public UserThirdProfilePersistenceImpl() {
		setModelClass(UserThirdProfile.class);
	}

	/**
	 * Caches the user third profile in the entity cache if it is enabled.
	 *
	 * @param userThirdProfile the user third profile
	 */
	@Override
	public void cacheResult(UserThirdProfile userThirdProfile) {
		EntityCacheUtil.putResult(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileImpl.class, userThirdProfile.getPrimaryKey(),
			userThirdProfile);

		userThirdProfile.resetOriginalValues();
	}

	/**
	 * Caches the user third profiles in the entity cache if it is enabled.
	 *
	 * @param userThirdProfiles the user third profiles
	 */
	@Override
	public void cacheResult(List<UserThirdProfile> userThirdProfiles) {
		for (UserThirdProfile userThirdProfile : userThirdProfiles) {
			if (EntityCacheUtil.getResult(
						UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
						UserThirdProfileImpl.class,
						userThirdProfile.getPrimaryKey()) == null) {
				cacheResult(userThirdProfile);
			}
			else {
				userThirdProfile.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user third profiles.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UserThirdProfileImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UserThirdProfileImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user third profile.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserThirdProfile userThirdProfile) {
		EntityCacheUtil.removeResult(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileImpl.class, userThirdProfile.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserThirdProfile> userThirdProfiles) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserThirdProfile userThirdProfile : userThirdProfiles) {
			EntityCacheUtil.removeResult(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
				UserThirdProfileImpl.class, userThirdProfile.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user third profile with the primary key. Does not add the user third profile to the database.
	 *
	 * @param userThirdProfilePK the primary key for the new user third profile
	 * @return the new user third profile
	 */
	@Override
	public UserThirdProfile create(UserThirdProfilePK userThirdProfilePK) {
		UserThirdProfile userThirdProfile = new UserThirdProfileImpl();

		userThirdProfile.setNew(true);
		userThirdProfile.setPrimaryKey(userThirdProfilePK);

		return userThirdProfile;
	}

	/**
	 * Removes the user third profile with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userThirdProfilePK the primary key of the user third profile
	 * @return the user third profile that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException if a user third profile with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile remove(UserThirdProfilePK userThirdProfilePK)
		throws NoSuchUserThirdProfileException, SystemException {
		return remove((Serializable)userThirdProfilePK);
	}

	/**
	 * Removes the user third profile with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user third profile
	 * @return the user third profile that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException if a user third profile with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile remove(Serializable primaryKey)
		throws NoSuchUserThirdProfileException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UserThirdProfile userThirdProfile = (UserThirdProfile)session.get(UserThirdProfileImpl.class,
					primaryKey);

			if (userThirdProfile == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserThirdProfileException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userThirdProfile);
		}
		catch (NoSuchUserThirdProfileException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserThirdProfile removeImpl(UserThirdProfile userThirdProfile)
		throws SystemException {
		userThirdProfile = toUnwrappedModel(userThirdProfile);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userThirdProfile)) {
				userThirdProfile = (UserThirdProfile)session.get(UserThirdProfileImpl.class,
						userThirdProfile.getPrimaryKeyObj());
			}

			if (userThirdProfile != null) {
				session.delete(userThirdProfile);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userThirdProfile != null) {
			clearCache(userThirdProfile);
		}

		return userThirdProfile;
	}

	@Override
	public UserThirdProfile updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserThirdProfile userThirdProfile)
		throws SystemException {
		userThirdProfile = toUnwrappedModel(userThirdProfile);

		boolean isNew = userThirdProfile.isNew();

		UserThirdProfileModelImpl userThirdProfileModelImpl = (UserThirdProfileModelImpl)userThirdProfile;

		Session session = null;

		try {
			session = openSession();

			if (userThirdProfile.isNew()) {
				session.save(userThirdProfile);

				userThirdProfile.setNew(false);
			}
			else {
				session.merge(userThirdProfile);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UserThirdProfileModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((userThirdProfileModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userThirdProfileModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { userThirdProfileModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		EntityCacheUtil.putResult(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
			UserThirdProfileImpl.class, userThirdProfile.getPrimaryKey(),
			userThirdProfile);

		return userThirdProfile;
	}

	protected UserThirdProfile toUnwrappedModel(
		UserThirdProfile userThirdProfile) {
		if (userThirdProfile instanceof UserThirdProfileImpl) {
			return userThirdProfile;
		}

		UserThirdProfileImpl userThirdProfileImpl = new UserThirdProfileImpl();

		userThirdProfileImpl.setNew(userThirdProfile.isNew());
		userThirdProfileImpl.setPrimaryKey(userThirdProfile.getPrimaryKey());

		userThirdProfileImpl.setThirdPartyId(userThirdProfile.getThirdPartyId());
		userThirdProfileImpl.setKey(userThirdProfile.getKey());
		userThirdProfileImpl.setUserId(userThirdProfile.getUserId());

		return userThirdProfileImpl;
	}

	/**
	 * Returns the user third profile with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user third profile
	 * @return the user third profile
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException if a user third profile with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserThirdProfileException, SystemException {
		UserThirdProfile userThirdProfile = fetchByPrimaryKey(primaryKey);

		if (userThirdProfile == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserThirdProfileException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userThirdProfile;
	}

	/**
	 * Returns the user third profile with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException} if it could not be found.
	 *
	 * @param userThirdProfilePK the primary key of the user third profile
	 * @return the user third profile
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException if a user third profile with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile findByPrimaryKey(
		UserThirdProfilePK userThirdProfilePK)
		throws NoSuchUserThirdProfileException, SystemException {
		return findByPrimaryKey((Serializable)userThirdProfilePK);
	}

	/**
	 * Returns the user third profile with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user third profile
	 * @return the user third profile, or <code>null</code> if a user third profile with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UserThirdProfile userThirdProfile = (UserThirdProfile)EntityCacheUtil.getResult(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
				UserThirdProfileImpl.class, primaryKey);

		if (userThirdProfile == _nullUserThirdProfile) {
			return null;
		}

		if (userThirdProfile == null) {
			Session session = null;

			try {
				session = openSession();

				userThirdProfile = (UserThirdProfile)session.get(UserThirdProfileImpl.class,
						primaryKey);

				if (userThirdProfile != null) {
					cacheResult(userThirdProfile);
				}
				else {
					EntityCacheUtil.putResult(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
						UserThirdProfileImpl.class, primaryKey,
						_nullUserThirdProfile);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UserThirdProfileModelImpl.ENTITY_CACHE_ENABLED,
					UserThirdProfileImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userThirdProfile;
	}

	/**
	 * Returns the user third profile with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userThirdProfilePK the primary key of the user third profile
	 * @return the user third profile, or <code>null</code> if a user third profile with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserThirdProfile fetchByPrimaryKey(
		UserThirdProfilePK userThirdProfilePK) throws SystemException {
		return fetchByPrimaryKey((Serializable)userThirdProfilePK);
	}

	/**
	 * Returns all the user third profiles.
	 *
	 * @return the user third profiles
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserThirdProfile> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user third profiles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user third profiles
	 * @param end the upper bound of the range of user third profiles (not inclusive)
	 * @return the range of user third profiles
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserThirdProfile> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user third profiles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user third profiles
	 * @param end the upper bound of the range of user third profiles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user third profiles
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserThirdProfile> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserThirdProfile> list = (List<UserThirdProfile>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_USERTHIRDPROFILE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERTHIRDPROFILE;

				if (pagination) {
					sql = sql.concat(UserThirdProfileModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserThirdProfile>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserThirdProfile>(list);
				}
				else {
					list = (List<UserThirdProfile>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user third profiles from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UserThirdProfile userThirdProfile : findAll()) {
			remove(userThirdProfile);
		}
	}

	/**
	 * Returns the number of user third profiles.
	 *
	 * @return the number of user third profiles
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERTHIRDPROFILE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the user third profile persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.UserThirdProfile")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UserThirdProfile>> listenersList = new ArrayList<ModelListener<UserThirdProfile>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UserThirdProfile>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UserThirdProfileImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_USERTHIRDPROFILE = "SELECT userThirdProfile FROM UserThirdProfile userThirdProfile";
	private static final String _SQL_SELECT_USERTHIRDPROFILE_WHERE = "SELECT userThirdProfile FROM UserThirdProfile userThirdProfile WHERE ";
	private static final String _SQL_COUNT_USERTHIRDPROFILE = "SELECT COUNT(userThirdProfile) FROM UserThirdProfile userThirdProfile";
	private static final String _SQL_COUNT_USERTHIRDPROFILE_WHERE = "SELECT COUNT(userThirdProfile) FROM UserThirdProfile userThirdProfile WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userThirdProfile.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserThirdProfile exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserThirdProfile exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UserThirdProfilePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"key"
			});
	private static UserThirdProfile _nullUserThirdProfile = new UserThirdProfileImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UserThirdProfile> toCacheModel() {
				return _nullUserThirdProfileCacheModel;
			}
		};

	private static CacheModel<UserThirdProfile> _nullUserThirdProfileCacheModel = new CacheModel<UserThirdProfile>() {
			@Override
			public UserThirdProfile toEntityModel() {
				return _nullUserThirdProfile;
			}
		};
}