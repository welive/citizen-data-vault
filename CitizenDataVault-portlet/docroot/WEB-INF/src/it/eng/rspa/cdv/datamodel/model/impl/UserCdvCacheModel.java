/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.UserCdv;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserCdv in entity cache.
 *
 * @author Engineering
 * @see UserCdv
 * @generated
 */
public class UserCdvCacheModel implements CacheModel<UserCdv>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{ccuid=");
		sb.append(ccuid);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", username=");
		sb.append(username);
		sb.append(", password=");
		sb.append(password);
		sb.append(", name=");
		sb.append(name);
		sb.append(", surname=");
		sb.append(surname);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", birthdate=");
		sb.append(birthdate);
		sb.append(", address=");
		sb.append(address);
		sb.append(", city=");
		sb.append(city);
		sb.append(", country=");
		sb.append(country);
		sb.append(", zipcode=");
		sb.append(zipcode);
		sb.append(", email=");
		sb.append(email);
		sb.append(", isDeveloper=");
		sb.append(isDeveloper);
		sb.append(", lastKnownLatitude=");
		sb.append(lastKnownLatitude);
		sb.append(", lastKnownLongitude=");
		sb.append(lastKnownLongitude);
		sb.append(", pilot=");
		sb.append(pilot);
		sb.append(", reputation=");
		sb.append(reputation);
		sb.append(", employement=");
		sb.append(employement);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserCdv toEntityModel() {
		UserCdvImpl userCdvImpl = new UserCdvImpl();

		userCdvImpl.setCcuid(ccuid);
		userCdvImpl.setUserId(userId);
		userCdvImpl.setCompanyId(companyId);

		if (username == null) {
			userCdvImpl.setUsername(StringPool.BLANK);
		}
		else {
			userCdvImpl.setUsername(username);
		}

		if (password == null) {
			userCdvImpl.setPassword(StringPool.BLANK);
		}
		else {
			userCdvImpl.setPassword(password);
		}

		if (name == null) {
			userCdvImpl.setName(StringPool.BLANK);
		}
		else {
			userCdvImpl.setName(name);
		}

		if (surname == null) {
			userCdvImpl.setSurname(StringPool.BLANK);
		}
		else {
			userCdvImpl.setSurname(surname);
		}

		if (gender == null) {
			userCdvImpl.setGender(StringPool.BLANK);
		}
		else {
			userCdvImpl.setGender(gender);
		}

		if (birthdate == Long.MIN_VALUE) {
			userCdvImpl.setBirthdate(null);
		}
		else {
			userCdvImpl.setBirthdate(new Date(birthdate));
		}

		if (address == null) {
			userCdvImpl.setAddress(StringPool.BLANK);
		}
		else {
			userCdvImpl.setAddress(address);
		}

		if (city == null) {
			userCdvImpl.setCity(StringPool.BLANK);
		}
		else {
			userCdvImpl.setCity(city);
		}

		if (country == null) {
			userCdvImpl.setCountry(StringPool.BLANK);
		}
		else {
			userCdvImpl.setCountry(country);
		}

		if (zipcode == null) {
			userCdvImpl.setZipcode(StringPool.BLANK);
		}
		else {
			userCdvImpl.setZipcode(zipcode);
		}

		if (email == null) {
			userCdvImpl.setEmail(StringPool.BLANK);
		}
		else {
			userCdvImpl.setEmail(email);
		}

		userCdvImpl.setIsDeveloper(isDeveloper);
		userCdvImpl.setLastKnownLatitude(lastKnownLatitude);
		userCdvImpl.setLastKnownLongitude(lastKnownLongitude);

		if (pilot == null) {
			userCdvImpl.setPilot(StringPool.BLANK);
		}
		else {
			userCdvImpl.setPilot(pilot);
		}

		userCdvImpl.setReputation(reputation);

		if (employement == null) {
			userCdvImpl.setEmployement(StringPool.BLANK);
		}
		else {
			userCdvImpl.setEmployement(employement);
		}

		userCdvImpl.resetOriginalValues();

		return userCdvImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ccuid = objectInput.readLong();
		userId = objectInput.readLong();
		companyId = objectInput.readLong();
		username = objectInput.readUTF();
		password = objectInput.readUTF();
		name = objectInput.readUTF();
		surname = objectInput.readUTF();
		gender = objectInput.readUTF();
		birthdate = objectInput.readLong();
		address = objectInput.readUTF();
		city = objectInput.readUTF();
		country = objectInput.readUTF();
		zipcode = objectInput.readUTF();
		email = objectInput.readUTF();
		isDeveloper = objectInput.readBoolean();
		lastKnownLatitude = objectInput.readDouble();
		lastKnownLongitude = objectInput.readDouble();
		pilot = objectInput.readUTF();
		reputation = objectInput.readLong();
		employement = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ccuid);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(companyId);

		if (username == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(username);
		}

		if (password == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(password);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (surname == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(surname);
		}

		if (gender == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(gender);
		}

		objectOutput.writeLong(birthdate);

		if (address == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(address);
		}

		if (city == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(city);
		}

		if (country == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(country);
		}

		if (zipcode == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(zipcode);
		}

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		objectOutput.writeBoolean(isDeveloper);
		objectOutput.writeDouble(lastKnownLatitude);
		objectOutput.writeDouble(lastKnownLongitude);

		if (pilot == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pilot);
		}

		objectOutput.writeLong(reputation);

		if (employement == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(employement);
		}
	}

	public long ccuid;
	public long userId;
	public long companyId;
	public String username;
	public String password;
	public String name;
	public String surname;
	public String gender;
	public long birthdate;
	public String address;
	public String city;
	public String country;
	public String zipcode;
	public String email;
	public boolean isDeveloper;
	public double lastKnownLatitude;
	public double lastKnownLongitude;
	public String pilot;
	public long reputation;
	public String employement;
}