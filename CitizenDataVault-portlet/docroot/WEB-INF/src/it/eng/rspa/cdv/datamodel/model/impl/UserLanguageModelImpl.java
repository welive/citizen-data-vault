/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.model.UserLanguageModel;
import it.eng.rspa.cdv.datamodel.service.persistence.UserLanguagePK;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the UserLanguage service. Represents a row in the &quot;cdv_UserLanguage&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.eng.rspa.cdv.datamodel.model.UserLanguageModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link UserLanguageImpl}.
 * </p>
 *
 * @author Engineering
 * @see UserLanguageImpl
 * @see it.eng.rspa.cdv.datamodel.model.UserLanguage
 * @see it.eng.rspa.cdv.datamodel.model.UserLanguageModel
 * @generated
 */
public class UserLanguageModelImpl extends BaseModelImpl<UserLanguage>
	implements UserLanguageModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a user language model instance should use the {@link it.eng.rspa.cdv.datamodel.model.UserLanguage} interface instead.
	 */
	public static final String TABLE_NAME = "cdv_UserLanguage";
	public static final Object[][] TABLE_COLUMNS = {
			{ "languageId", Types.BIGINT },
			{ "userId", Types.BIGINT }
		};
	public static final String TABLE_SQL_CREATE = "create table cdv_UserLanguage (languageId LONG not null,userId LONG not null,primary key (languageId, userId))";
	public static final String TABLE_SQL_DROP = "drop table cdv_UserLanguage";
	public static final String ORDER_BY_JPQL = " ORDER BY userLanguage.id.languageId ASC, userLanguage.id.userId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY cdv_UserLanguage.languageId ASC, cdv_UserLanguage.userId ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.it.eng.rspa.cdv.datamodel.model.UserLanguage"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.it.eng.rspa.cdv.datamodel.model.UserLanguage"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.it.eng.rspa.cdv.datamodel.model.UserLanguage"),
			true);
	public static long USERID_COLUMN_BITMASK = 1L;
	public static long LANGUAGEID_COLUMN_BITMASK = 2L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.it.eng.rspa.cdv.datamodel.model.UserLanguage"));

	public UserLanguageModelImpl() {
	}

	@Override
	public UserLanguagePK getPrimaryKey() {
		return new UserLanguagePK(_languageId, _userId);
	}

	@Override
	public void setPrimaryKey(UserLanguagePK primaryKey) {
		setLanguageId(primaryKey.languageId);
		setUserId(primaryKey.userId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UserLanguagePK(_languageId, _userId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UserLanguagePK)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return UserLanguage.class;
	}

	@Override
	public String getModelClassName() {
		return UserLanguage.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("languageId", getLanguageId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long languageId = (Long)attributes.get("languageId");

		if (languageId != null) {
			setLanguageId(languageId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	@Override
	public long getLanguageId() {
		return _languageId;
	}

	@Override
	public void setLanguageId(long languageId) {
		_languageId = languageId;
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_columnBitmask |= USERID_COLUMN_BITMASK;

		if (!_setOriginalUserId) {
			_setOriginalUserId = true;

			_originalUserId = _userId;
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getOriginalUserId() {
		return _originalUserId;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public UserLanguage toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (UserLanguage)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		UserLanguageImpl userLanguageImpl = new UserLanguageImpl();

		userLanguageImpl.setLanguageId(getLanguageId());
		userLanguageImpl.setUserId(getUserId());

		userLanguageImpl.resetOriginalValues();

		return userLanguageImpl;
	}

	@Override
	public int compareTo(UserLanguage userLanguage) {
		UserLanguagePK primaryKey = userLanguage.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserLanguage)) {
			return false;
		}

		UserLanguage userLanguage = (UserLanguage)obj;

		UserLanguagePK primaryKey = userLanguage.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public void resetOriginalValues() {
		UserLanguageModelImpl userLanguageModelImpl = this;

		userLanguageModelImpl._originalUserId = userLanguageModelImpl._userId;

		userLanguageModelImpl._setOriginalUserId = false;

		userLanguageModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<UserLanguage> toCacheModel() {
		UserLanguageCacheModel userLanguageCacheModel = new UserLanguageCacheModel();

		userLanguageCacheModel.languageId = getLanguageId();

		userLanguageCacheModel.userId = getUserId();

		return userLanguageCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{languageId=");
		sb.append(getLanguageId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.UserLanguage");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>languageId</column-name><column-value><![CDATA[");
		sb.append(getLanguageId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = UserLanguage.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			UserLanguage.class
		};
	private long _languageId;
	private long _userId;
	private String _userUuid;
	private long _originalUserId;
	private boolean _setOriginalUserId;
	private long _columnBitmask;
	private UserLanguage _escapedModel;
}