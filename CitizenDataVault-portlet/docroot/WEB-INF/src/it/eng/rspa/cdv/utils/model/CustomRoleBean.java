package it.eng.rspa.cdv.utils.model;

public enum CustomRoleBean {
	
	Academy("Academy"),
	Citizen("Citizen"),
	Business("Business"),
	Entrepreneur("Entrepreneur");
	
	private String txt;
	
	private CustomRoleBean(String t){
		this.txt = t.trim();
	}
	
	public String toString(){
		return this.txt;
	}

}
