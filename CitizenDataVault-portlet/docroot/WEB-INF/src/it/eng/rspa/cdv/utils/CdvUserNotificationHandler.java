package it.eng.rspa.cdv.utils;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.notifications.BaseUserNotificationHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.UserNotificationEvent;
import com.liferay.portal.service.ServiceContext;


public class CdvUserNotificationHandler extends BaseUserNotificationHandler{

		
	public CdvUserNotificationHandler() {
		setPortletId(PortletKeys.CDV);
	}
	
	@Override
	protected String getBody(UserNotificationEvent userNotificationEvent, ServiceContext serviceContext) throws Exception {
		

		JSONObject jsonObject = JSONFactoryUtil.createJSONObject(userNotificationEvent.getPayload());
		
		StringBundler sb = new StringBundler(5);
		sb.append("<div class=\"title\">");
		sb.append(HtmlUtil.escape(jsonObject.getString("sender")));
		sb.append("</div><div class=\"body\">");
		sb.append(HtmlUtil.escape(jsonObject.getString("text-message")));
		sb.append("</div>");
		
		return sb.toString();
	}
	

	@Override
	protected String getLink(UserNotificationEvent userNotificationEvent, ServiceContext serviceContext) throws Exception {
		
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject(userNotificationEvent.getPayload());
		 String currentURL = jsonObject.getString("destinationURL");
		 
		
		return currentURL;
	}
	
}
