package it.eng.rspa.cdv.utils.model;

import java.util.HashSet;
import java.util.Set;

public class UserProfile {
	
	private Long ccUserID;
	private String referredPilot;
	private String name;
	private String surname;
	private String gender;
	private String birthdate;
	private String address;
	private String city;
	private String country;
	private String zipCode;
	private String email;
	private Boolean isDeveloper;
	private Set<String> languages;
	private Set<String> userTags;
	private Set<OutSkill> skills;
	private Set<OutApp> usedApps;
	private ProfileData profileData;
	private Location lastKnownLocation;
	
	public UserProfile(){
		languages = new HashSet<String>();
		userTags = new HashSet<String>();
		skills = new HashSet<OutSkill>();
		usedApps = new HashSet<OutApp>();
	}

	public Long getCcUserID() {
		return ccUserID;
	}

	public void setCcUserID(Long ccUserID) {
		this.ccUserID = ccUserID;
	}

	public String getReferredPilot() {
		return referredPilot;
	}

	public void setReferredPilot(String referredPilot) {
		this.referredPilot = referredPilot;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsDeveloper() {
		return isDeveloper;
	}

	public void setIsDeveloper(Boolean isDeveloper) {
		this.isDeveloper = isDeveloper;
	}

	public Set<String> getLanguages() {
		return languages;
	}

	public void setLanguages(Set<String> languages) {
		this.languages = languages;
	}

	public Set<String> getUserTags() {
		return userTags;
	}

	public void setUserTags(Set<String> userTags) {
		this.userTags = userTags;
	}

	public Set<OutSkill> getSkills() {
		return skills;
	}

	public void setSkills(Set<OutSkill> skills) {
		this.skills = skills;
	}

	public Set<OutApp> getUsedApps() {
		return usedApps;
	}

	public void setUsedApps(Set<OutApp> usedApps) {
		this.usedApps = usedApps;
	}

	public ProfileData getProfileData() {
		return profileData;
	}

	public void setProfileData(ProfileData profileData) {
		this.profileData = profileData;
	}

	public Location getLastKnownLocation() {
		return lastKnownLocation;
	}

	public void setLastKnownLocation(Location lastKnownLocation) {
		this.lastKnownLocation = lastKnownLocation;
	}
}
