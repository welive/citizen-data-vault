package it.eng.rspa.cdv.utils.model;

public enum PilotId {
	
	bilbao("Bilbao"),
	novisad("Novisad"),
	uusimaa("Uusimaa"),
	trento("Trento");
	
	private String text;
	
	private PilotId(String s){
		this.text = s;
	}
	
	public String toString(){
		return this.text;
	}
	

}
