package it.eng.rspa.cdv.utils;

import java.util.Date;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public abstract class PlatformUserSynchronizer {

	public static void setName(Long userid, String name) 
			throws PortalException, SystemException{
		
		User u = UserLocalServiceUtil.getUser(userid);
		u.setFirstName(name);
		
		UserLocalServiceUtil.updateUser(u);
		
		return;
		
	}
	
	public static  void setSurname(Long userid, String surname) 
			throws PortalException, SystemException{
		
		User u = UserLocalServiceUtil.getUser(userid);
		u.setLastName(surname);
		
		UserLocalServiceUtil.updateUser(u);
		
		return;
		
	}
	
	public static void setBirthdate(Long userid, Date birthdate) 
			throws PortalException, SystemException{
		
		User u = UserLocalServiceUtil.getUser(userid);
		if(birthdate != null){
			u.getBirthday().setTime(birthdate.getTime());
		}
		else{ u.getBirthday().setTime(0); }
		
		UserLocalServiceUtil.updateUser(u);
		
		return;
	}
	
	public static void isDeveloper(Long userid, boolean isDeveloper) 
			throws Exception{
		
		Role rDeveloper = RoleLocalServiceUtil.getRole(CompanyLocalServiceUtil.getCompanyIdByUserId(userid), PortletProps.get("role.developer"));
		
		if(isDeveloper){ RoleLocalServiceUtil.addUserRole(userid, rDeveloper); }
		else{ RoleLocalServiceUtil.deleteUserRole(userid, rDeveloper); }
		
		return;
		
	}
	
}
