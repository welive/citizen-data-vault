/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;
import it.eng.rspa.cdv.datamodel.model.UserPreference;
import it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the user preference service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserPreferencePersistence
 * @see UserPreferenceUtil
 * @generated
 */
public class UserPreferencePersistenceImpl extends BasePersistenceImpl<UserPreference>
	implements UserPreferencePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserPreferenceUtil} to access the user preference persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserPreferenceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED,
			UserPreferenceImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED,
			UserPreferenceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED,
			UserPreferenceImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByuserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED,
			UserPreferenceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			UserPreferenceModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user preferences where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findByuserId(long userId)
		throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user preferences where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user preferences
	 * @param end the upper bound of the range of user preferences (not inclusive)
	 * @return the range of matching user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user preferences where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user preferences
	 * @param end the upper bound of the range of user preferences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findByuserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UserPreference> list = (List<UserPreference>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserPreference userPreference : list) {
				if ((userId != userPreference.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERPREFERENCE_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserPreferenceModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UserPreference>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserPreference>(list);
				}
				else {
					list = (List<UserPreference>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user preference in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a matching user preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserPreferenceException, SystemException {
		UserPreference userPreference = fetchByuserId_First(userId,
				orderByComparator);

		if (userPreference != null) {
			return userPreference;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserPreferenceException(msg.toString());
	}

	/**
	 * Returns the first user preference in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user preference, or <code>null</code> if a matching user preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserPreference> list = findByuserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user preference in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a matching user preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserPreferenceException, SystemException {
		UserPreference userPreference = fetchByuserId_Last(userId,
				orderByComparator);

		if (userPreference != null) {
			return userPreference;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserPreferenceException(msg.toString());
	}

	/**
	 * Returns the last user preference in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user preference, or <code>null</code> if a matching user preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		if (count == 0) {
			return null;
		}

		List<UserPreference> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user preferences before and after the current user preference in the ordered set where userId = &#63;.
	 *
	 * @param userPreferencePK the primary key of the current user preference
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference[] findByuserId_PrevAndNext(
		UserPreferencePK userPreferencePK, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserPreferenceException, SystemException {
		UserPreference userPreference = findByPrimaryKey(userPreferencePK);

		Session session = null;

		try {
			session = openSession();

			UserPreference[] array = new UserPreferenceImpl[3];

			array[0] = getByuserId_PrevAndNext(session, userPreference, userId,
					orderByComparator, true);

			array[1] = userPreference;

			array[2] = getByuserId_PrevAndNext(session, userPreference, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserPreference getByuserId_PrevAndNext(Session session,
		UserPreference userPreference, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERPREFERENCE_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserPreferenceModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userPreference);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserPreference> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user preferences where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserId(long userId) throws SystemException {
		for (UserPreference userPreference : findByuserId(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userPreference);
		}
	}

	/**
	 * Returns the number of user preferences where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERPREFERENCE_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "userPreference.id.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PREFERENCEID =
		new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED,
			UserPreferenceImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBypreferenceId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PREFERENCEID =
		new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED,
			UserPreferenceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBypreferenceId",
			new String[] { Long.class.getName() },
			UserPreferenceModelImpl.PREFERENCEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PREFERENCEID = new FinderPath(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBypreferenceId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user preferences where preferenceId = &#63;.
	 *
	 * @param preferenceId the preference ID
	 * @return the matching user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findBypreferenceId(long preferenceId)
		throws SystemException {
		return findBypreferenceId(preferenceId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user preferences where preferenceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param preferenceId the preference ID
	 * @param start the lower bound of the range of user preferences
	 * @param end the upper bound of the range of user preferences (not inclusive)
	 * @return the range of matching user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findBypreferenceId(long preferenceId,
		int start, int end) throws SystemException {
		return findBypreferenceId(preferenceId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user preferences where preferenceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param preferenceId the preference ID
	 * @param start the lower bound of the range of user preferences
	 * @param end the upper bound of the range of user preferences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findBypreferenceId(long preferenceId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PREFERENCEID;
			finderArgs = new Object[] { preferenceId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PREFERENCEID;
			finderArgs = new Object[] {
					preferenceId,
					
					start, end, orderByComparator
				};
		}

		List<UserPreference> list = (List<UserPreference>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserPreference userPreference : list) {
				if ((preferenceId != userPreference.getPreferenceId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERPREFERENCE_WHERE);

			query.append(_FINDER_COLUMN_PREFERENCEID_PREFERENCEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserPreferenceModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(preferenceId);

				if (!pagination) {
					list = (List<UserPreference>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserPreference>(list);
				}
				else {
					list = (List<UserPreference>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user preference in the ordered set where preferenceId = &#63;.
	 *
	 * @param preferenceId the preference ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a matching user preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference findBypreferenceId_First(long preferenceId,
		OrderByComparator orderByComparator)
		throws NoSuchUserPreferenceException, SystemException {
		UserPreference userPreference = fetchBypreferenceId_First(preferenceId,
				orderByComparator);

		if (userPreference != null) {
			return userPreference;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("preferenceId=");
		msg.append(preferenceId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserPreferenceException(msg.toString());
	}

	/**
	 * Returns the first user preference in the ordered set where preferenceId = &#63;.
	 *
	 * @param preferenceId the preference ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user preference, or <code>null</code> if a matching user preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference fetchBypreferenceId_First(long preferenceId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserPreference> list = findBypreferenceId(preferenceId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user preference in the ordered set where preferenceId = &#63;.
	 *
	 * @param preferenceId the preference ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a matching user preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference findBypreferenceId_Last(long preferenceId,
		OrderByComparator orderByComparator)
		throws NoSuchUserPreferenceException, SystemException {
		UserPreference userPreference = fetchBypreferenceId_Last(preferenceId,
				orderByComparator);

		if (userPreference != null) {
			return userPreference;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("preferenceId=");
		msg.append(preferenceId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserPreferenceException(msg.toString());
	}

	/**
	 * Returns the last user preference in the ordered set where preferenceId = &#63;.
	 *
	 * @param preferenceId the preference ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user preference, or <code>null</code> if a matching user preference could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference fetchBypreferenceId_Last(long preferenceId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBypreferenceId(preferenceId);

		if (count == 0) {
			return null;
		}

		List<UserPreference> list = findBypreferenceId(preferenceId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user preferences before and after the current user preference in the ordered set where preferenceId = &#63;.
	 *
	 * @param userPreferencePK the primary key of the current user preference
	 * @param preferenceId the preference ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference[] findBypreferenceId_PrevAndNext(
		UserPreferencePK userPreferencePK, long preferenceId,
		OrderByComparator orderByComparator)
		throws NoSuchUserPreferenceException, SystemException {
		UserPreference userPreference = findByPrimaryKey(userPreferencePK);

		Session session = null;

		try {
			session = openSession();

			UserPreference[] array = new UserPreferenceImpl[3];

			array[0] = getBypreferenceId_PrevAndNext(session, userPreference,
					preferenceId, orderByComparator, true);

			array[1] = userPreference;

			array[2] = getBypreferenceId_PrevAndNext(session, userPreference,
					preferenceId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserPreference getBypreferenceId_PrevAndNext(Session session,
		UserPreference userPreference, long preferenceId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERPREFERENCE_WHERE);

		query.append(_FINDER_COLUMN_PREFERENCEID_PREFERENCEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserPreferenceModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(preferenceId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userPreference);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserPreference> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user preferences where preferenceId = &#63; from the database.
	 *
	 * @param preferenceId the preference ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBypreferenceId(long preferenceId)
		throws SystemException {
		for (UserPreference userPreference : findBypreferenceId(preferenceId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userPreference);
		}
	}

	/**
	 * Returns the number of user preferences where preferenceId = &#63;.
	 *
	 * @param preferenceId the preference ID
	 * @return the number of matching user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBypreferenceId(long preferenceId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PREFERENCEID;

		Object[] finderArgs = new Object[] { preferenceId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERPREFERENCE_WHERE);

			query.append(_FINDER_COLUMN_PREFERENCEID_PREFERENCEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(preferenceId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PREFERENCEID_PREFERENCEID_2 = "userPreference.id.preferenceId = ?";

	public UserPreferencePersistenceImpl() {
		setModelClass(UserPreference.class);
	}

	/**
	 * Caches the user preference in the entity cache if it is enabled.
	 *
	 * @param userPreference the user preference
	 */
	@Override
	public void cacheResult(UserPreference userPreference) {
		EntityCacheUtil.putResult(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceImpl.class, userPreference.getPrimaryKey(),
			userPreference);

		userPreference.resetOriginalValues();
	}

	/**
	 * Caches the user preferences in the entity cache if it is enabled.
	 *
	 * @param userPreferences the user preferences
	 */
	@Override
	public void cacheResult(List<UserPreference> userPreferences) {
		for (UserPreference userPreference : userPreferences) {
			if (EntityCacheUtil.getResult(
						UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
						UserPreferenceImpl.class, userPreference.getPrimaryKey()) == null) {
				cacheResult(userPreference);
			}
			else {
				userPreference.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user preferences.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UserPreferenceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UserPreferenceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user preference.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserPreference userPreference) {
		EntityCacheUtil.removeResult(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceImpl.class, userPreference.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserPreference> userPreferences) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserPreference userPreference : userPreferences) {
			EntityCacheUtil.removeResult(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
				UserPreferenceImpl.class, userPreference.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user preference with the primary key. Does not add the user preference to the database.
	 *
	 * @param userPreferencePK the primary key for the new user preference
	 * @return the new user preference
	 */
	@Override
	public UserPreference create(UserPreferencePK userPreferencePK) {
		UserPreference userPreference = new UserPreferenceImpl();

		userPreference.setNew(true);
		userPreference.setPrimaryKey(userPreferencePK);

		return userPreference;
	}

	/**
	 * Removes the user preference with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userPreferencePK the primary key of the user preference
	 * @return the user preference that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference remove(UserPreferencePK userPreferencePK)
		throws NoSuchUserPreferenceException, SystemException {
		return remove((Serializable)userPreferencePK);
	}

	/**
	 * Removes the user preference with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user preference
	 * @return the user preference that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference remove(Serializable primaryKey)
		throws NoSuchUserPreferenceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UserPreference userPreference = (UserPreference)session.get(UserPreferenceImpl.class,
					primaryKey);

			if (userPreference == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserPreferenceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userPreference);
		}
		catch (NoSuchUserPreferenceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserPreference removeImpl(UserPreference userPreference)
		throws SystemException {
		userPreference = toUnwrappedModel(userPreference);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userPreference)) {
				userPreference = (UserPreference)session.get(UserPreferenceImpl.class,
						userPreference.getPrimaryKeyObj());
			}

			if (userPreference != null) {
				session.delete(userPreference);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userPreference != null) {
			clearCache(userPreference);
		}

		return userPreference;
	}

	@Override
	public UserPreference updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserPreference userPreference)
		throws SystemException {
		userPreference = toUnwrappedModel(userPreference);

		boolean isNew = userPreference.isNew();

		UserPreferenceModelImpl userPreferenceModelImpl = (UserPreferenceModelImpl)userPreference;

		Session session = null;

		try {
			session = openSession();

			if (userPreference.isNew()) {
				session.save(userPreference);

				userPreference.setNew(false);
			}
			else {
				session.merge(userPreference);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UserPreferenceModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((userPreferenceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userPreferenceModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { userPreferenceModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((userPreferenceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PREFERENCEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userPreferenceModelImpl.getOriginalPreferenceId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PREFERENCEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PREFERENCEID,
					args);

				args = new Object[] { userPreferenceModelImpl.getPreferenceId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PREFERENCEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PREFERENCEID,
					args);
			}
		}

		EntityCacheUtil.putResult(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
			UserPreferenceImpl.class, userPreference.getPrimaryKey(),
			userPreference);

		return userPreference;
	}

	protected UserPreference toUnwrappedModel(UserPreference userPreference) {
		if (userPreference instanceof UserPreferenceImpl) {
			return userPreference;
		}

		UserPreferenceImpl userPreferenceImpl = new UserPreferenceImpl();

		userPreferenceImpl.setNew(userPreference.isNew());
		userPreferenceImpl.setPrimaryKey(userPreference.getPrimaryKey());

		userPreferenceImpl.setPreferenceId(userPreference.getPreferenceId());
		userPreferenceImpl.setUserId(userPreference.getUserId());

		return userPreferenceImpl;
	}

	/**
	 * Returns the user preference with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user preference
	 * @return the user preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserPreferenceException, SystemException {
		UserPreference userPreference = fetchByPrimaryKey(primaryKey);

		if (userPreference == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserPreferenceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userPreference;
	}

	/**
	 * Returns the user preference with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException} if it could not be found.
	 *
	 * @param userPreferencePK the primary key of the user preference
	 * @return the user preference
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference findByPrimaryKey(UserPreferencePK userPreferencePK)
		throws NoSuchUserPreferenceException, SystemException {
		return findByPrimaryKey((Serializable)userPreferencePK);
	}

	/**
	 * Returns the user preference with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user preference
	 * @return the user preference, or <code>null</code> if a user preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UserPreference userPreference = (UserPreference)EntityCacheUtil.getResult(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
				UserPreferenceImpl.class, primaryKey);

		if (userPreference == _nullUserPreference) {
			return null;
		}

		if (userPreference == null) {
			Session session = null;

			try {
				session = openSession();

				userPreference = (UserPreference)session.get(UserPreferenceImpl.class,
						primaryKey);

				if (userPreference != null) {
					cacheResult(userPreference);
				}
				else {
					EntityCacheUtil.putResult(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
						UserPreferenceImpl.class, primaryKey,
						_nullUserPreference);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UserPreferenceModelImpl.ENTITY_CACHE_ENABLED,
					UserPreferenceImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userPreference;
	}

	/**
	 * Returns the user preference with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userPreferencePK the primary key of the user preference
	 * @return the user preference, or <code>null</code> if a user preference with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserPreference fetchByPrimaryKey(UserPreferencePK userPreferencePK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)userPreferencePK);
	}

	/**
	 * Returns all the user preferences.
	 *
	 * @return the user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user preferences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user preferences
	 * @param end the upper bound of the range of user preferences (not inclusive)
	 * @return the range of user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user preferences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user preferences
	 * @param end the upper bound of the range of user preferences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserPreference> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserPreference> list = (List<UserPreference>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_USERPREFERENCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERPREFERENCE;

				if (pagination) {
					sql = sql.concat(UserPreferenceModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserPreference>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserPreference>(list);
				}
				else {
					list = (List<UserPreference>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user preferences from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UserPreference userPreference : findAll()) {
			remove(userPreference);
		}
	}

	/**
	 * Returns the number of user preferences.
	 *
	 * @return the number of user preferences
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERPREFERENCE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the user preference persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.UserPreference")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UserPreference>> listenersList = new ArrayList<ModelListener<UserPreference>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UserPreference>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UserPreferenceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_USERPREFERENCE = "SELECT userPreference FROM UserPreference userPreference";
	private static final String _SQL_SELECT_USERPREFERENCE_WHERE = "SELECT userPreference FROM UserPreference userPreference WHERE ";
	private static final String _SQL_COUNT_USERPREFERENCE = "SELECT COUNT(userPreference) FROM UserPreference userPreference";
	private static final String _SQL_COUNT_USERPREFERENCE_WHERE = "SELECT COUNT(userPreference) FROM UserPreference userPreference WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userPreference.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserPreference exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserPreference exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UserPreferencePersistenceImpl.class);
	private static UserPreference _nullUserPreference = new UserPreferenceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UserPreference> toCacheModel() {
				return _nullUserPreferenceCacheModel;
			}
		};

	private static CacheModel<UserPreference> _nullUserPreferenceCacheModel = new CacheModel<UserPreference>() {
			@Override
			public UserPreference toEntityModel() {
				return _nullUserPreference;
			}
		};
}