package it.eng.rspa.cdv.utils.model;

public class OutApp {
	
	private String appName;
	private Long appID;
	
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public Long getAppID() {
		return appID;
	}
	public void setAppID(Long appID) {
		this.appID = appID;
	}
	
}
