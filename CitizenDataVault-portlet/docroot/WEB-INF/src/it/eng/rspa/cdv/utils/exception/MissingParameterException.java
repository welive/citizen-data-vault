package it.eng.rspa.cdv.utils.exception;

public class MissingParameterException extends CustomException {

	private static final long serialVersionUID = 1L;
	
	public MissingParameterException(String message){
		super(message);
		setMessageNumber(2L);
	}

}
