package it.eng.rspa.cdv.utils.model;

import java.util.Set;

import com.liferay.portal.kernel.json.JSONObject;

public abstract class GenericUserBean {

	protected Long ccUserID;
	protected Boolean isMale;
	protected CustomDateBean birthDate;
	protected String address;
	protected String city;
	protected String country;
	protected String zipCode;
	protected Set<String> languages;
	protected Set<String> userTags;
	
	protected GenericUserBean(){
		//Allocating instance
	}
	
	public Long getCcUserID() {
		return ccUserID;
	}


	public Boolean getIsMale() {
		return isMale;
	}


	public CustomDateBean getBirthDate() {
		return birthDate;
	}


	
	public String getAddress() {
		return address;
	}


	public String getCity() {
		return city;
	}


	public String getCountry() {
		return country;
	}


	public String getZipCode() {
		return zipCode;
	}


	public Set<String> getLanguages() {
		return languages;
	}


	public Set<String> getUserTags() {
		return userTags;
	}
	
	public abstract void check(JSONObject jin) throws Exception;
}
