/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;
import it.eng.rspa.cdv.datamodel.model.UserIdea;
import it.eng.rspa.cdv.datamodel.model.impl.UserIdeaImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the user idea service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserIdeaPersistence
 * @see UserIdeaUtil
 * @generated
 */
public class UserIdeaPersistenceImpl extends BasePersistenceImpl<UserIdea>
	implements UserIdeaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserIdeaUtil} to access the user idea persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserIdeaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ROLE = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByrole",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROLE = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByrole",
			new String[] { String.class.getName() },
			UserIdeaModelImpl.ROLE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ROLE = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByrole",
			new String[] { String.class.getName() });

	/**
	 * Returns all the user ideas where role = &#63;.
	 *
	 * @param role the role
	 * @return the matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByrole(String role) throws SystemException {
		return findByrole(role, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user ideas where role = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param role the role
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @return the range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByrole(String role, int start, int end)
		throws SystemException {
		return findByrole(role, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user ideas where role = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param role the role
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByrole(String role, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROLE;
			finderArgs = new Object[] { role };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ROLE;
			finderArgs = new Object[] { role, start, end, orderByComparator };
		}

		List<UserIdea> list = (List<UserIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserIdea userIdea : list) {
				if (!Validator.equals(role, userIdea.getRole())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERIDEA_WHERE);

			boolean bindRole = false;

			if (role == null) {
				query.append(_FINDER_COLUMN_ROLE_ROLE_1);
			}
			else if (role.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ROLE_ROLE_3);
			}
			else {
				bindRole = true;

				query.append(_FINDER_COLUMN_ROLE_ROLE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindRole) {
					qPos.add(role);
				}

				if (!pagination) {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserIdea>(list);
				}
				else {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user idea in the ordered set where role = &#63;.
	 *
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByrole_First(String role,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByrole_First(role, orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("role=");
		msg.append(role);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the first user idea in the ordered set where role = &#63;.
	 *
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByrole_First(String role,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserIdea> list = findByrole(role, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user idea in the ordered set where role = &#63;.
	 *
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByrole_Last(String role,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByrole_Last(role, orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("role=");
		msg.append(role);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the last user idea in the ordered set where role = &#63;.
	 *
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByrole_Last(String role,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByrole(role);

		if (count == 0) {
			return null;
		}

		List<UserIdea> list = findByrole(role, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user ideas before and after the current user idea in the ordered set where role = &#63;.
	 *
	 * @param userIdeaPK the primary key of the current user idea
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea[] findByrole_PrevAndNext(UserIdeaPK userIdeaPK,
		String role, OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = findByPrimaryKey(userIdeaPK);

		Session session = null;

		try {
			session = openSession();

			UserIdea[] array = new UserIdeaImpl[3];

			array[0] = getByrole_PrevAndNext(session, userIdea, role,
					orderByComparator, true);

			array[1] = userIdea;

			array[2] = getByrole_PrevAndNext(session, userIdea, role,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserIdea getByrole_PrevAndNext(Session session,
		UserIdea userIdea, String role, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERIDEA_WHERE);

		boolean bindRole = false;

		if (role == null) {
			query.append(_FINDER_COLUMN_ROLE_ROLE_1);
		}
		else if (role.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ROLE_ROLE_3);
		}
		else {
			bindRole = true;

			query.append(_FINDER_COLUMN_ROLE_ROLE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindRole) {
			qPos.add(role);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user ideas where role = &#63; from the database.
	 *
	 * @param role the role
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByrole(String role) throws SystemException {
		for (UserIdea userIdea : findByrole(role, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userIdea);
		}
	}

	/**
	 * Returns the number of user ideas where role = &#63;.
	 *
	 * @param role the role
	 * @return the number of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByrole(String role) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ROLE;

		Object[] finderArgs = new Object[] { role };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERIDEA_WHERE);

			boolean bindRole = false;

			if (role == null) {
				query.append(_FINDER_COLUMN_ROLE_ROLE_1);
			}
			else if (role.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ROLE_ROLE_3);
			}
			else {
				bindRole = true;

				query.append(_FINDER_COLUMN_ROLE_ROLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindRole) {
					qPos.add(role);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ROLE_ROLE_1 = "userIdea.id.role IS NULL";
	private static final String _FINDER_COLUMN_ROLE_ROLE_2 = "userIdea.id.role = ?";
	private static final String _FINDER_COLUMN_ROLE_ROLE_3 = "(userIdea.id.role IS NULL OR userIdea.id.role = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USER = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuser",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuser",
			new String[] { Long.class.getName() },
			UserIdeaModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USER = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuser",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user ideas where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByuser(long userId) throws SystemException {
		return findByuser(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user ideas where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @return the range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByuser(long userId, int start, int end)
		throws SystemException {
		return findByuser(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user ideas where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByuser(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USER;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UserIdea> list = (List<UserIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserIdea userIdea : list) {
				if ((userId != userIdea.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERIDEA_WHERE);

			query.append(_FINDER_COLUMN_USER_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserIdea>(list);
				}
				else {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user idea in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByuser_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByuser_First(userId, orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the first user idea in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByuser_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserIdea> list = findByuser(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user idea in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByuser_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByuser_Last(userId, orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the last user idea in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByuser_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuser(userId);

		if (count == 0) {
			return null;
		}

		List<UserIdea> list = findByuser(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user ideas before and after the current user idea in the ordered set where userId = &#63;.
	 *
	 * @param userIdeaPK the primary key of the current user idea
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea[] findByuser_PrevAndNext(UserIdeaPK userIdeaPK,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = findByPrimaryKey(userIdeaPK);

		Session session = null;

		try {
			session = openSession();

			UserIdea[] array = new UserIdeaImpl[3];

			array[0] = getByuser_PrevAndNext(session, userIdea, userId,
					orderByComparator, true);

			array[1] = userIdea;

			array[2] = getByuser_PrevAndNext(session, userIdea, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserIdea getByuser_PrevAndNext(Session session,
		UserIdea userIdea, long userId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERIDEA_WHERE);

		query.append(_FINDER_COLUMN_USER_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user ideas where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuser(long userId) throws SystemException {
		for (UserIdea userIdea : findByuser(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userIdea);
		}
	}

	/**
	 * Returns the number of user ideas where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuser(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USER;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERIDEA_WHERE);

			query.append(_FINDER_COLUMN_USER_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USER_USERID_2 = "userIdea.id.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERROLE = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserRole",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERROLE =
		new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserRole",
			new String[] { Long.class.getName(), String.class.getName() },
			UserIdeaModelImpl.USERID_COLUMN_BITMASK |
			UserIdeaModelImpl.ROLE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERROLE = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserRole",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the user ideas where userId = &#63; and role = &#63;.
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @return the matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByuserRole(long userId, String role)
		throws SystemException {
		return findByuserRole(userId, role, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user ideas where userId = &#63; and role = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @return the range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByuserRole(long userId, String role, int start,
		int end) throws SystemException {
		return findByuserRole(userId, role, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user ideas where userId = &#63; and role = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByuserRole(long userId, String role, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERROLE;
			finderArgs = new Object[] { userId, role };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERROLE;
			finderArgs = new Object[] {
					userId, role,
					
					start, end, orderByComparator
				};
		}

		List<UserIdea> list = (List<UserIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserIdea userIdea : list) {
				if ((userId != userIdea.getUserId()) ||
						!Validator.equals(role, userIdea.getRole())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_USERIDEA_WHERE);

			query.append(_FINDER_COLUMN_USERROLE_USERID_2);

			boolean bindRole = false;

			if (role == null) {
				query.append(_FINDER_COLUMN_USERROLE_ROLE_1);
			}
			else if (role.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERROLE_ROLE_3);
			}
			else {
				bindRole = true;

				query.append(_FINDER_COLUMN_USERROLE_ROLE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (bindRole) {
					qPos.add(role);
				}

				if (!pagination) {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserIdea>(list);
				}
				else {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user idea in the ordered set where userId = &#63; and role = &#63;.
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByuserRole_First(long userId, String role,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByuserRole_First(userId, role,
				orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", role=");
		msg.append(role);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the first user idea in the ordered set where userId = &#63; and role = &#63;.
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByuserRole_First(long userId, String role,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserIdea> list = findByuserRole(userId, role, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user idea in the ordered set where userId = &#63; and role = &#63;.
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByuserRole_Last(long userId, String role,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByuserRole_Last(userId, role, orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", role=");
		msg.append(role);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the last user idea in the ordered set where userId = &#63; and role = &#63;.
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByuserRole_Last(long userId, String role,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserRole(userId, role);

		if (count == 0) {
			return null;
		}

		List<UserIdea> list = findByuserRole(userId, role, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user ideas before and after the current user idea in the ordered set where userId = &#63; and role = &#63;.
	 *
	 * @param userIdeaPK the primary key of the current user idea
	 * @param userId the user ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea[] findByuserRole_PrevAndNext(UserIdeaPK userIdeaPK,
		long userId, String role, OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = findByPrimaryKey(userIdeaPK);

		Session session = null;

		try {
			session = openSession();

			UserIdea[] array = new UserIdeaImpl[3];

			array[0] = getByuserRole_PrevAndNext(session, userIdea, userId,
					role, orderByComparator, true);

			array[1] = userIdea;

			array[2] = getByuserRole_PrevAndNext(session, userIdea, userId,
					role, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserIdea getByuserRole_PrevAndNext(Session session,
		UserIdea userIdea, long userId, String role,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERIDEA_WHERE);

		query.append(_FINDER_COLUMN_USERROLE_USERID_2);

		boolean bindRole = false;

		if (role == null) {
			query.append(_FINDER_COLUMN_USERROLE_ROLE_1);
		}
		else if (role.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_USERROLE_ROLE_3);
		}
		else {
			bindRole = true;

			query.append(_FINDER_COLUMN_USERROLE_ROLE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (bindRole) {
			qPos.add(role);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user ideas where userId = &#63; and role = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserRole(long userId, String role)
		throws SystemException {
		for (UserIdea userIdea : findByuserRole(userId, role,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userIdea);
		}
	}

	/**
	 * Returns the number of user ideas where userId = &#63; and role = &#63;.
	 *
	 * @param userId the user ID
	 * @param role the role
	 * @return the number of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserRole(long userId, String role)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERROLE;

		Object[] finderArgs = new Object[] { userId, role };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERIDEA_WHERE);

			query.append(_FINDER_COLUMN_USERROLE_USERID_2);

			boolean bindRole = false;

			if (role == null) {
				query.append(_FINDER_COLUMN_USERROLE_ROLE_1);
			}
			else if (role.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERROLE_ROLE_3);
			}
			else {
				bindRole = true;

				query.append(_FINDER_COLUMN_USERROLE_ROLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (bindRole) {
					qPos.add(role);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERROLE_USERID_2 = "userIdea.id.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERROLE_ROLE_1 = "userIdea.id.role IS NULL";
	private static final String _FINDER_COLUMN_USERROLE_ROLE_2 = "userIdea.id.role = ?";
	private static final String _FINDER_COLUMN_USERROLE_ROLE_3 = "(userIdea.id.role IS NULL OR userIdea.id.role = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByideaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID =
		new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByideaId",
			new String[] { Long.class.getName() },
			UserIdeaModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAID = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByideaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user ideas where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByideaId(long ideaId) throws SystemException {
		return findByideaId(ideaId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user ideas where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @return the range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByideaId(long ideaId, int start, int end)
		throws SystemException {
		return findByideaId(ideaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user ideas where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByideaId(long ideaId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaId, start, end, orderByComparator };
		}

		List<UserIdea> list = (List<UserIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserIdea userIdea : list) {
				if ((ideaId != userIdea.getIdeaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (!pagination) {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserIdea>(list);
				}
				else {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user idea in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByideaId_First(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByideaId_First(ideaId, orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the first user idea in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByideaId_First(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserIdea> list = findByideaId(ideaId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user idea in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByideaId_Last(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByideaId_Last(ideaId, orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the last user idea in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByideaId_Last(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByideaId(ideaId);

		if (count == 0) {
			return null;
		}

		List<UserIdea> list = findByideaId(ideaId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user ideas before and after the current user idea in the ordered set where ideaId = &#63;.
	 *
	 * @param userIdeaPK the primary key of the current user idea
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea[] findByideaId_PrevAndNext(UserIdeaPK userIdeaPK,
		long ideaId, OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = findByPrimaryKey(userIdeaPK);

		Session session = null;

		try {
			session = openSession();

			UserIdea[] array = new UserIdeaImpl[3];

			array[0] = getByideaId_PrevAndNext(session, userIdea, ideaId,
					orderByComparator, true);

			array[1] = userIdea;

			array[2] = getByideaId_PrevAndNext(session, userIdea, ideaId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserIdea getByideaId_PrevAndNext(Session session,
		UserIdea userIdea, long ideaId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERIDEA_WHERE);

		query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user ideas where ideaId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByideaId(long ideaId) throws SystemException {
		for (UserIdea userIdea : findByideaId(ideaId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userIdea);
		}
	}

	/**
	 * Returns the number of user ideas where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the number of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByideaId(long ideaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAID;

		Object[] finderArgs = new Object[] { ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAID_IDEAID_2 = "userIdea.id.ideaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAROLE = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByideaRole",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAROLE =
		new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, UserIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByideaRole",
			new String[] { Long.class.getName(), String.class.getName() },
			UserIdeaModelImpl.IDEAID_COLUMN_BITMASK |
			UserIdeaModelImpl.ROLE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAROLE = new FinderPath(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByideaRole",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the user ideas where ideaId = &#63; and role = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @return the matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByideaRole(long ideaId, String role)
		throws SystemException {
		return findByideaRole(ideaId, role, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user ideas where ideaId = &#63; and role = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @return the range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByideaRole(long ideaId, String role, int start,
		int end) throws SystemException {
		return findByideaRole(ideaId, role, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user ideas where ideaId = &#63; and role = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findByideaRole(long ideaId, String role, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAROLE;
			finderArgs = new Object[] { ideaId, role };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAROLE;
			finderArgs = new Object[] {
					ideaId, role,
					
					start, end, orderByComparator
				};
		}

		List<UserIdea> list = (List<UserIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserIdea userIdea : list) {
				if ((ideaId != userIdea.getIdeaId()) ||
						!Validator.equals(role, userIdea.getRole())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_USERIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAROLE_IDEAID_2);

			boolean bindRole = false;

			if (role == null) {
				query.append(_FINDER_COLUMN_IDEAROLE_ROLE_1);
			}
			else if (role.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEAROLE_ROLE_3);
			}
			else {
				bindRole = true;

				query.append(_FINDER_COLUMN_IDEAROLE_ROLE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (bindRole) {
					qPos.add(role);
				}

				if (!pagination) {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserIdea>(list);
				}
				else {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user idea in the ordered set where ideaId = &#63; and role = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByideaRole_First(long ideaId, String role,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByideaRole_First(ideaId, role,
				orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", role=");
		msg.append(role);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the first user idea in the ordered set where ideaId = &#63; and role = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByideaRole_First(long ideaId, String role,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserIdea> list = findByideaRole(ideaId, role, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user idea in the ordered set where ideaId = &#63; and role = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByideaRole_Last(long ideaId, String role,
		OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByideaRole_Last(ideaId, role, orderByComparator);

		if (userIdea != null) {
			return userIdea;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", role=");
		msg.append(role);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserIdeaException(msg.toString());
	}

	/**
	 * Returns the last user idea in the ordered set where ideaId = &#63; and role = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByideaRole_Last(long ideaId, String role,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByideaRole(ideaId, role);

		if (count == 0) {
			return null;
		}

		List<UserIdea> list = findByideaRole(ideaId, role, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user ideas before and after the current user idea in the ordered set where ideaId = &#63; and role = &#63;.
	 *
	 * @param userIdeaPK the primary key of the current user idea
	 * @param ideaId the idea ID
	 * @param role the role
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea[] findByideaRole_PrevAndNext(UserIdeaPK userIdeaPK,
		long ideaId, String role, OrderByComparator orderByComparator)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = findByPrimaryKey(userIdeaPK);

		Session session = null;

		try {
			session = openSession();

			UserIdea[] array = new UserIdeaImpl[3];

			array[0] = getByideaRole_PrevAndNext(session, userIdea, ideaId,
					role, orderByComparator, true);

			array[1] = userIdea;

			array[2] = getByideaRole_PrevAndNext(session, userIdea, ideaId,
					role, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserIdea getByideaRole_PrevAndNext(Session session,
		UserIdea userIdea, long ideaId, String role,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERIDEA_WHERE);

		query.append(_FINDER_COLUMN_IDEAROLE_IDEAID_2);

		boolean bindRole = false;

		if (role == null) {
			query.append(_FINDER_COLUMN_IDEAROLE_ROLE_1);
		}
		else if (role.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_IDEAROLE_ROLE_3);
		}
		else {
			bindRole = true;

			query.append(_FINDER_COLUMN_IDEAROLE_ROLE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (bindRole) {
			qPos.add(role);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user ideas where ideaId = &#63; and role = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByideaRole(long ideaId, String role)
		throws SystemException {
		for (UserIdea userIdea : findByideaRole(ideaId, role,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userIdea);
		}
	}

	/**
	 * Returns the number of user ideas where ideaId = &#63; and role = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param role the role
	 * @return the number of matching user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByideaRole(long ideaId, String role)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAROLE;

		Object[] finderArgs = new Object[] { ideaId, role };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAROLE_IDEAID_2);

			boolean bindRole = false;

			if (role == null) {
				query.append(_FINDER_COLUMN_IDEAROLE_ROLE_1);
			}
			else if (role.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEAROLE_ROLE_3);
			}
			else {
				bindRole = true;

				query.append(_FINDER_COLUMN_IDEAROLE_ROLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (bindRole) {
					qPos.add(role);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAROLE_IDEAID_2 = "userIdea.id.ideaId = ? AND ";
	private static final String _FINDER_COLUMN_IDEAROLE_ROLE_1 = "userIdea.id.role IS NULL";
	private static final String _FINDER_COLUMN_IDEAROLE_ROLE_2 = "userIdea.id.role = ?";
	private static final String _FINDER_COLUMN_IDEAROLE_ROLE_3 = "(userIdea.id.role IS NULL OR userIdea.id.role = '')";

	public UserIdeaPersistenceImpl() {
		setModelClass(UserIdea.class);
	}

	/**
	 * Caches the user idea in the entity cache if it is enabled.
	 *
	 * @param userIdea the user idea
	 */
	@Override
	public void cacheResult(UserIdea userIdea) {
		EntityCacheUtil.putResult(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaImpl.class, userIdea.getPrimaryKey(), userIdea);

		userIdea.resetOriginalValues();
	}

	/**
	 * Caches the user ideas in the entity cache if it is enabled.
	 *
	 * @param userIdeas the user ideas
	 */
	@Override
	public void cacheResult(List<UserIdea> userIdeas) {
		for (UserIdea userIdea : userIdeas) {
			if (EntityCacheUtil.getResult(
						UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
						UserIdeaImpl.class, userIdea.getPrimaryKey()) == null) {
				cacheResult(userIdea);
			}
			else {
				userIdea.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user ideas.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UserIdeaImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UserIdeaImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user idea.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserIdea userIdea) {
		EntityCacheUtil.removeResult(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaImpl.class, userIdea.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserIdea> userIdeas) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserIdea userIdea : userIdeas) {
			EntityCacheUtil.removeResult(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
				UserIdeaImpl.class, userIdea.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user idea with the primary key. Does not add the user idea to the database.
	 *
	 * @param userIdeaPK the primary key for the new user idea
	 * @return the new user idea
	 */
	@Override
	public UserIdea create(UserIdeaPK userIdeaPK) {
		UserIdea userIdea = new UserIdeaImpl();

		userIdea.setNew(true);
		userIdea.setPrimaryKey(userIdeaPK);

		return userIdea;
	}

	/**
	 * Removes the user idea with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userIdeaPK the primary key of the user idea
	 * @return the user idea that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea remove(UserIdeaPK userIdeaPK)
		throws NoSuchUserIdeaException, SystemException {
		return remove((Serializable)userIdeaPK);
	}

	/**
	 * Removes the user idea with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user idea
	 * @return the user idea that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea remove(Serializable primaryKey)
		throws NoSuchUserIdeaException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UserIdea userIdea = (UserIdea)session.get(UserIdeaImpl.class,
					primaryKey);

			if (userIdea == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserIdeaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userIdea);
		}
		catch (NoSuchUserIdeaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserIdea removeImpl(UserIdea userIdea) throws SystemException {
		userIdea = toUnwrappedModel(userIdea);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userIdea)) {
				userIdea = (UserIdea)session.get(UserIdeaImpl.class,
						userIdea.getPrimaryKeyObj());
			}

			if (userIdea != null) {
				session.delete(userIdea);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userIdea != null) {
			clearCache(userIdea);
		}

		return userIdea;
	}

	@Override
	public UserIdea updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserIdea userIdea)
		throws SystemException {
		userIdea = toUnwrappedModel(userIdea);

		boolean isNew = userIdea.isNew();

		UserIdeaModelImpl userIdeaModelImpl = (UserIdeaModelImpl)userIdea;

		Session session = null;

		try {
			session = openSession();

			if (userIdea.isNew()) {
				session.save(userIdea);

				userIdea.setNew(false);
			}
			else {
				session.merge(userIdea);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UserIdeaModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((userIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { userIdeaModelImpl.getOriginalRole() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ROLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROLE,
					args);

				args = new Object[] { userIdeaModelImpl.getRole() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ROLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROLE,
					args);
			}

			if ((userIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userIdeaModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USER, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
					args);

				args = new Object[] { userIdeaModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USER, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
					args);
			}

			if ((userIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERROLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userIdeaModelImpl.getOriginalUserId(),
						userIdeaModelImpl.getOriginalRole()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERROLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERROLE,
					args);

				args = new Object[] {
						userIdeaModelImpl.getUserId(),
						userIdeaModelImpl.getRole()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERROLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERROLE,
					args);
			}

			if ((userIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userIdeaModelImpl.getOriginalIdeaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);

				args = new Object[] { userIdeaModelImpl.getIdeaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);
			}

			if ((userIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAROLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userIdeaModelImpl.getOriginalIdeaId(),
						userIdeaModelImpl.getOriginalRole()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAROLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAROLE,
					args);

				args = new Object[] {
						userIdeaModelImpl.getIdeaId(),
						userIdeaModelImpl.getRole()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAROLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAROLE,
					args);
			}
		}

		EntityCacheUtil.putResult(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
			UserIdeaImpl.class, userIdea.getPrimaryKey(), userIdea);

		return userIdea;
	}

	protected UserIdea toUnwrappedModel(UserIdea userIdea) {
		if (userIdea instanceof UserIdeaImpl) {
			return userIdea;
		}

		UserIdeaImpl userIdeaImpl = new UserIdeaImpl();

		userIdeaImpl.setNew(userIdea.isNew());
		userIdeaImpl.setPrimaryKey(userIdea.getPrimaryKey());

		userIdeaImpl.setIdeaId(userIdea.getIdeaId());
		userIdeaImpl.setUserId(userIdea.getUserId());
		userIdeaImpl.setRole(userIdea.getRole());

		return userIdeaImpl;
	}

	/**
	 * Returns the user idea with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user idea
	 * @return the user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserIdeaException, SystemException {
		UserIdea userIdea = fetchByPrimaryKey(primaryKey);

		if (userIdea == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserIdeaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userIdea;
	}

	/**
	 * Returns the user idea with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException} if it could not be found.
	 *
	 * @param userIdeaPK the primary key of the user idea
	 * @return the user idea
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea findByPrimaryKey(UserIdeaPK userIdeaPK)
		throws NoSuchUserIdeaException, SystemException {
		return findByPrimaryKey((Serializable)userIdeaPK);
	}

	/**
	 * Returns the user idea with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user idea
	 * @return the user idea, or <code>null</code> if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UserIdea userIdea = (UserIdea)EntityCacheUtil.getResult(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
				UserIdeaImpl.class, primaryKey);

		if (userIdea == _nullUserIdea) {
			return null;
		}

		if (userIdea == null) {
			Session session = null;

			try {
				session = openSession();

				userIdea = (UserIdea)session.get(UserIdeaImpl.class, primaryKey);

				if (userIdea != null) {
					cacheResult(userIdea);
				}
				else {
					EntityCacheUtil.putResult(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
						UserIdeaImpl.class, primaryKey, _nullUserIdea);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UserIdeaModelImpl.ENTITY_CACHE_ENABLED,
					UserIdeaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userIdea;
	}

	/**
	 * Returns the user idea with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userIdeaPK the primary key of the user idea
	 * @return the user idea, or <code>null</code> if a user idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserIdea fetchByPrimaryKey(UserIdeaPK userIdeaPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)userIdeaPK);
	}

	/**
	 * Returns all the user ideas.
	 *
	 * @return the user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user ideas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @return the range of user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user ideas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user ideas
	 * @param end the upper bound of the range of user ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserIdea> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserIdea> list = (List<UserIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_USERIDEA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERIDEA;

				if (pagination) {
					sql = sql.concat(UserIdeaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserIdea>(list);
				}
				else {
					list = (List<UserIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user ideas from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UserIdea userIdea : findAll()) {
			remove(userIdea);
		}
	}

	/**
	 * Returns the number of user ideas.
	 *
	 * @return the number of user ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERIDEA);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the user idea persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.UserIdea")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UserIdea>> listenersList = new ArrayList<ModelListener<UserIdea>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UserIdea>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UserIdeaImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_USERIDEA = "SELECT userIdea FROM UserIdea userIdea";
	private static final String _SQL_SELECT_USERIDEA_WHERE = "SELECT userIdea FROM UserIdea userIdea WHERE ";
	private static final String _SQL_COUNT_USERIDEA = "SELECT COUNT(userIdea) FROM UserIdea userIdea";
	private static final String _SQL_COUNT_USERIDEA_WHERE = "SELECT COUNT(userIdea) FROM UserIdea userIdea WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userIdea.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserIdea exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserIdea exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UserIdeaPersistenceImpl.class);
	private static UserIdea _nullUserIdea = new UserIdeaImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UserIdea> toCacheModel() {
				return _nullUserIdeaCacheModel;
			}
		};

	private static CacheModel<UserIdea> _nullUserIdeaCacheModel = new CacheModel<UserIdea>() {
			@Override
			public UserIdea toEntityModel() {
				return _nullUserIdea;
			}
		};
}