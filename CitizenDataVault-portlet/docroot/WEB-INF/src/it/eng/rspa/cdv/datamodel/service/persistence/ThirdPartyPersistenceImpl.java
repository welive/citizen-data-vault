/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException;
import it.eng.rspa.cdv.datamodel.model.ThirdParty;
import it.eng.rspa.cdv.datamodel.model.impl.ThirdPartyImpl;
import it.eng.rspa.cdv.datamodel.model.impl.ThirdPartyModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the third party service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see ThirdPartyPersistence
 * @see ThirdPartyUtil
 * @generated
 */
public class ThirdPartyPersistenceImpl extends BasePersistenceImpl<ThirdParty>
	implements ThirdPartyPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ThirdPartyUtil} to access the third party persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ThirdPartyImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
			ThirdPartyModelImpl.FINDER_CACHE_ENABLED, ThirdPartyImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
			ThirdPartyModelImpl.FINDER_CACHE_ENABLED, ThirdPartyImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
			ThirdPartyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public ThirdPartyPersistenceImpl() {
		setModelClass(ThirdParty.class);
	}

	/**
	 * Caches the third party in the entity cache if it is enabled.
	 *
	 * @param thirdParty the third party
	 */
	@Override
	public void cacheResult(ThirdParty thirdParty) {
		EntityCacheUtil.putResult(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
			ThirdPartyImpl.class, thirdParty.getPrimaryKey(), thirdParty);

		thirdParty.resetOriginalValues();
	}

	/**
	 * Caches the third parties in the entity cache if it is enabled.
	 *
	 * @param thirdParties the third parties
	 */
	@Override
	public void cacheResult(List<ThirdParty> thirdParties) {
		for (ThirdParty thirdParty : thirdParties) {
			if (EntityCacheUtil.getResult(
						ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
						ThirdPartyImpl.class, thirdParty.getPrimaryKey()) == null) {
				cacheResult(thirdParty);
			}
			else {
				thirdParty.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all third parties.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ThirdPartyImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ThirdPartyImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the third party.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ThirdParty thirdParty) {
		EntityCacheUtil.removeResult(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
			ThirdPartyImpl.class, thirdParty.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ThirdParty> thirdParties) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ThirdParty thirdParty : thirdParties) {
			EntityCacheUtil.removeResult(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
				ThirdPartyImpl.class, thirdParty.getPrimaryKey());
		}
	}

	/**
	 * Creates a new third party with the primary key. Does not add the third party to the database.
	 *
	 * @param thirdPartyId the primary key for the new third party
	 * @return the new third party
	 */
	@Override
	public ThirdParty create(long thirdPartyId) {
		ThirdParty thirdParty = new ThirdPartyImpl();

		thirdParty.setNew(true);
		thirdParty.setPrimaryKey(thirdPartyId);

		return thirdParty;
	}

	/**
	 * Removes the third party with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param thirdPartyId the primary key of the third party
	 * @return the third party that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException if a third party with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThirdParty remove(long thirdPartyId)
		throws NoSuchThirdPartyException, SystemException {
		return remove((Serializable)thirdPartyId);
	}

	/**
	 * Removes the third party with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the third party
	 * @return the third party that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException if a third party with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThirdParty remove(Serializable primaryKey)
		throws NoSuchThirdPartyException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ThirdParty thirdParty = (ThirdParty)session.get(ThirdPartyImpl.class,
					primaryKey);

			if (thirdParty == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchThirdPartyException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(thirdParty);
		}
		catch (NoSuchThirdPartyException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ThirdParty removeImpl(ThirdParty thirdParty)
		throws SystemException {
		thirdParty = toUnwrappedModel(thirdParty);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(thirdParty)) {
				thirdParty = (ThirdParty)session.get(ThirdPartyImpl.class,
						thirdParty.getPrimaryKeyObj());
			}

			if (thirdParty != null) {
				session.delete(thirdParty);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (thirdParty != null) {
			clearCache(thirdParty);
		}

		return thirdParty;
	}

	@Override
	public ThirdParty updateImpl(
		it.eng.rspa.cdv.datamodel.model.ThirdParty thirdParty)
		throws SystemException {
		thirdParty = toUnwrappedModel(thirdParty);

		boolean isNew = thirdParty.isNew();

		Session session = null;

		try {
			session = openSession();

			if (thirdParty.isNew()) {
				session.save(thirdParty);

				thirdParty.setNew(false);
			}
			else {
				session.merge(thirdParty);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
			ThirdPartyImpl.class, thirdParty.getPrimaryKey(), thirdParty);

		return thirdParty;
	}

	protected ThirdParty toUnwrappedModel(ThirdParty thirdParty) {
		if (thirdParty instanceof ThirdPartyImpl) {
			return thirdParty;
		}

		ThirdPartyImpl thirdPartyImpl = new ThirdPartyImpl();

		thirdPartyImpl.setNew(thirdParty.isNew());
		thirdPartyImpl.setPrimaryKey(thirdParty.getPrimaryKey());

		thirdPartyImpl.setThirdPartyId(thirdParty.getThirdPartyId());
		thirdPartyImpl.setThirdPartyName(thirdParty.getThirdPartyName());

		return thirdPartyImpl;
	}

	/**
	 * Returns the third party with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the third party
	 * @return the third party
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException if a third party with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThirdParty findByPrimaryKey(Serializable primaryKey)
		throws NoSuchThirdPartyException, SystemException {
		ThirdParty thirdParty = fetchByPrimaryKey(primaryKey);

		if (thirdParty == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchThirdPartyException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return thirdParty;
	}

	/**
	 * Returns the third party with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException} if it could not be found.
	 *
	 * @param thirdPartyId the primary key of the third party
	 * @return the third party
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException if a third party with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThirdParty findByPrimaryKey(long thirdPartyId)
		throws NoSuchThirdPartyException, SystemException {
		return findByPrimaryKey((Serializable)thirdPartyId);
	}

	/**
	 * Returns the third party with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the third party
	 * @return the third party, or <code>null</code> if a third party with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThirdParty fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ThirdParty thirdParty = (ThirdParty)EntityCacheUtil.getResult(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
				ThirdPartyImpl.class, primaryKey);

		if (thirdParty == _nullThirdParty) {
			return null;
		}

		if (thirdParty == null) {
			Session session = null;

			try {
				session = openSession();

				thirdParty = (ThirdParty)session.get(ThirdPartyImpl.class,
						primaryKey);

				if (thirdParty != null) {
					cacheResult(thirdParty);
				}
				else {
					EntityCacheUtil.putResult(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
						ThirdPartyImpl.class, primaryKey, _nullThirdParty);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ThirdPartyModelImpl.ENTITY_CACHE_ENABLED,
					ThirdPartyImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return thirdParty;
	}

	/**
	 * Returns the third party with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param thirdPartyId the primary key of the third party
	 * @return the third party, or <code>null</code> if a third party with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ThirdParty fetchByPrimaryKey(long thirdPartyId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)thirdPartyId);
	}

	/**
	 * Returns all the third parties.
	 *
	 * @return the third parties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThirdParty> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the third parties.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of third parties
	 * @param end the upper bound of the range of third parties (not inclusive)
	 * @return the range of third parties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThirdParty> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the third parties.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of third parties
	 * @param end the upper bound of the range of third parties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of third parties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ThirdParty> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ThirdParty> list = (List<ThirdParty>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_THIRDPARTY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_THIRDPARTY;

				if (pagination) {
					sql = sql.concat(ThirdPartyModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ThirdParty>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ThirdParty>(list);
				}
				else {
					list = (List<ThirdParty>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the third parties from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ThirdParty thirdParty : findAll()) {
			remove(thirdParty);
		}
	}

	/**
	 * Returns the number of third parties.
	 *
	 * @return the number of third parties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_THIRDPARTY);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the third party persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.ThirdParty")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ThirdParty>> listenersList = new ArrayList<ModelListener<ThirdParty>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ThirdParty>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ThirdPartyImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_THIRDPARTY = "SELECT thirdParty FROM ThirdParty thirdParty";
	private static final String _SQL_COUNT_THIRDPARTY = "SELECT COUNT(thirdParty) FROM ThirdParty thirdParty";
	private static final String _ORDER_BY_ENTITY_ALIAS = "thirdParty.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ThirdParty exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ThirdPartyPersistenceImpl.class);
	private static ThirdParty _nullThirdParty = new ThirdPartyImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ThirdParty> toCacheModel() {
				return _nullThirdPartyCacheModel;
			}
		};

	private static CacheModel<ThirdParty> _nullThirdPartyCacheModel = new CacheModel<ThirdParty>() {
			@Override
			public ThirdParty toEntityModel() {
				return _nullThirdParty;
			}
		};
}