/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;
import it.eng.rspa.cdv.datamodel.model.UserSkill;
import it.eng.rspa.cdv.datamodel.model.impl.UserSkillImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the user skill service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserSkillPersistence
 * @see UserSkillUtil
 * @generated
 */
public class UserSkillPersistenceImpl extends BasePersistenceImpl<UserSkill>
	implements UserSkillPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserSkillUtil} to access the user skill persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserSkillImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, UserSkillImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, UserSkillImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SKILLID = new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, UserSkillImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByskillId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID =
		new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, UserSkillImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByskillId",
			new String[] { Long.class.getName() },
			UserSkillModelImpl.SKILLID_COLUMN_BITMASK |
			UserSkillModelImpl.ENDORSMENTCOUNTER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SKILLID = new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByskillId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user skills where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @return the matching user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findByskillId(long skillId)
		throws SystemException {
		return findByskillId(skillId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user skills where skillId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param skillId the skill ID
	 * @param start the lower bound of the range of user skills
	 * @param end the upper bound of the range of user skills (not inclusive)
	 * @return the range of matching user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findByskillId(long skillId, int start, int end)
		throws SystemException {
		return findByskillId(skillId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user skills where skillId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param skillId the skill ID
	 * @param start the lower bound of the range of user skills
	 * @param end the upper bound of the range of user skills (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findByskillId(long skillId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID;
			finderArgs = new Object[] { skillId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SKILLID;
			finderArgs = new Object[] { skillId, start, end, orderByComparator };
		}

		List<UserSkill> list = (List<UserSkill>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserSkill userSkill : list) {
				if ((skillId != userSkill.getSkillId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERSKILL_WHERE);

			query.append(_FINDER_COLUMN_SKILLID_SKILLID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserSkillModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(skillId);

				if (!pagination) {
					list = (List<UserSkill>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserSkill>(list);
				}
				else {
					list = (List<UserSkill>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user skill in the ordered set where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user skill
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a matching user skill could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill findByskillId_First(long skillId,
		OrderByComparator orderByComparator)
		throws NoSuchUserSkillException, SystemException {
		UserSkill userSkill = fetchByskillId_First(skillId, orderByComparator);

		if (userSkill != null) {
			return userSkill;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("skillId=");
		msg.append(skillId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserSkillException(msg.toString());
	}

	/**
	 * Returns the first user skill in the ordered set where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user skill, or <code>null</code> if a matching user skill could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill fetchByskillId_First(long skillId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserSkill> list = findByskillId(skillId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user skill in the ordered set where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user skill
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a matching user skill could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill findByskillId_Last(long skillId,
		OrderByComparator orderByComparator)
		throws NoSuchUserSkillException, SystemException {
		UserSkill userSkill = fetchByskillId_Last(skillId, orderByComparator);

		if (userSkill != null) {
			return userSkill;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("skillId=");
		msg.append(skillId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserSkillException(msg.toString());
	}

	/**
	 * Returns the last user skill in the ordered set where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user skill, or <code>null</code> if a matching user skill could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill fetchByskillId_Last(long skillId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByskillId(skillId);

		if (count == 0) {
			return null;
		}

		List<UserSkill> list = findByskillId(skillId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user skills before and after the current user skill in the ordered set where skillId = &#63;.
	 *
	 * @param userSkillPK the primary key of the current user skill
	 * @param skillId the skill ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user skill
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill[] findByskillId_PrevAndNext(UserSkillPK userSkillPK,
		long skillId, OrderByComparator orderByComparator)
		throws NoSuchUserSkillException, SystemException {
		UserSkill userSkill = findByPrimaryKey(userSkillPK);

		Session session = null;

		try {
			session = openSession();

			UserSkill[] array = new UserSkillImpl[3];

			array[0] = getByskillId_PrevAndNext(session, userSkill, skillId,
					orderByComparator, true);

			array[1] = userSkill;

			array[2] = getByskillId_PrevAndNext(session, userSkill, skillId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserSkill getByskillId_PrevAndNext(Session session,
		UserSkill userSkill, long skillId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERSKILL_WHERE);

		query.append(_FINDER_COLUMN_SKILLID_SKILLID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserSkillModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(skillId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userSkill);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserSkill> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user skills where skillId = &#63; from the database.
	 *
	 * @param skillId the skill ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByskillId(long skillId) throws SystemException {
		for (UserSkill userSkill : findByskillId(skillId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userSkill);
		}
	}

	/**
	 * Returns the number of user skills where skillId = &#63;.
	 *
	 * @param skillId the skill ID
	 * @return the number of matching user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByskillId(long skillId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SKILLID;

		Object[] finderArgs = new Object[] { skillId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERSKILL_WHERE);

			query.append(_FINDER_COLUMN_SKILLID_SKILLID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(skillId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SKILLID_SKILLID_2 = "userSkill.id.skillId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, UserSkillImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, UserSkillImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			UserSkillModelImpl.USERID_COLUMN_BITMASK |
			UserSkillModelImpl.ENDORSMENTCOUNTER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user skills where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findByuserId(long userId) throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user skills where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user skills
	 * @param end the upper bound of the range of user skills (not inclusive)
	 * @return the range of matching user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user skills where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user skills
	 * @param end the upper bound of the range of user skills (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findByuserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UserSkill> list = (List<UserSkill>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserSkill userSkill : list) {
				if ((userId != userSkill.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERSKILL_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserSkillModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UserSkill>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserSkill>(list);
				}
				else {
					list = (List<UserSkill>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user skill in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user skill
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a matching user skill could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserSkillException, SystemException {
		UserSkill userSkill = fetchByuserId_First(userId, orderByComparator);

		if (userSkill != null) {
			return userSkill;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserSkillException(msg.toString());
	}

	/**
	 * Returns the first user skill in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user skill, or <code>null</code> if a matching user skill could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserSkill> list = findByuserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user skill in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user skill
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a matching user skill could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserSkillException, SystemException {
		UserSkill userSkill = fetchByuserId_Last(userId, orderByComparator);

		if (userSkill != null) {
			return userSkill;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserSkillException(msg.toString());
	}

	/**
	 * Returns the last user skill in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user skill, or <code>null</code> if a matching user skill could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		if (count == 0) {
			return null;
		}

		List<UserSkill> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user skills before and after the current user skill in the ordered set where userId = &#63;.
	 *
	 * @param userSkillPK the primary key of the current user skill
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user skill
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill[] findByuserId_PrevAndNext(UserSkillPK userSkillPK,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchUserSkillException, SystemException {
		UserSkill userSkill = findByPrimaryKey(userSkillPK);

		Session session = null;

		try {
			session = openSession();

			UserSkill[] array = new UserSkillImpl[3];

			array[0] = getByuserId_PrevAndNext(session, userSkill, userId,
					orderByComparator, true);

			array[1] = userSkill;

			array[2] = getByuserId_PrevAndNext(session, userSkill, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserSkill getByuserId_PrevAndNext(Session session,
		UserSkill userSkill, long userId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERSKILL_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserSkillModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userSkill);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserSkill> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user skills where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserId(long userId) throws SystemException {
		for (UserSkill userSkill : findByuserId(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userSkill);
		}
	}

	/**
	 * Returns the number of user skills where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERSKILL_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "userSkill.id.userId = ?";

	public UserSkillPersistenceImpl() {
		setModelClass(UserSkill.class);
	}

	/**
	 * Caches the user skill in the entity cache if it is enabled.
	 *
	 * @param userSkill the user skill
	 */
	@Override
	public void cacheResult(UserSkill userSkill) {
		EntityCacheUtil.putResult(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillImpl.class, userSkill.getPrimaryKey(), userSkill);

		userSkill.resetOriginalValues();
	}

	/**
	 * Caches the user skills in the entity cache if it is enabled.
	 *
	 * @param userSkills the user skills
	 */
	@Override
	public void cacheResult(List<UserSkill> userSkills) {
		for (UserSkill userSkill : userSkills) {
			if (EntityCacheUtil.getResult(
						UserSkillModelImpl.ENTITY_CACHE_ENABLED,
						UserSkillImpl.class, userSkill.getPrimaryKey()) == null) {
				cacheResult(userSkill);
			}
			else {
				userSkill.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user skills.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UserSkillImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UserSkillImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user skill.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserSkill userSkill) {
		EntityCacheUtil.removeResult(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillImpl.class, userSkill.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserSkill> userSkills) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserSkill userSkill : userSkills) {
			EntityCacheUtil.removeResult(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
				UserSkillImpl.class, userSkill.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user skill with the primary key. Does not add the user skill to the database.
	 *
	 * @param userSkillPK the primary key for the new user skill
	 * @return the new user skill
	 */
	@Override
	public UserSkill create(UserSkillPK userSkillPK) {
		UserSkill userSkill = new UserSkillImpl();

		userSkill.setNew(true);
		userSkill.setPrimaryKey(userSkillPK);

		return userSkill;
	}

	/**
	 * Removes the user skill with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userSkillPK the primary key of the user skill
	 * @return the user skill that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill remove(UserSkillPK userSkillPK)
		throws NoSuchUserSkillException, SystemException {
		return remove((Serializable)userSkillPK);
	}

	/**
	 * Removes the user skill with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user skill
	 * @return the user skill that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill remove(Serializable primaryKey)
		throws NoSuchUserSkillException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UserSkill userSkill = (UserSkill)session.get(UserSkillImpl.class,
					primaryKey);

			if (userSkill == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserSkillException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userSkill);
		}
		catch (NoSuchUserSkillException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserSkill removeImpl(UserSkill userSkill)
		throws SystemException {
		userSkill = toUnwrappedModel(userSkill);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userSkill)) {
				userSkill = (UserSkill)session.get(UserSkillImpl.class,
						userSkill.getPrimaryKeyObj());
			}

			if (userSkill != null) {
				session.delete(userSkill);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userSkill != null) {
			clearCache(userSkill);
		}

		return userSkill;
	}

	@Override
	public UserSkill updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserSkill userSkill)
		throws SystemException {
		userSkill = toUnwrappedModel(userSkill);

		boolean isNew = userSkill.isNew();

		UserSkillModelImpl userSkillModelImpl = (UserSkillModelImpl)userSkill;

		Session session = null;

		try {
			session = openSession();

			if (userSkill.isNew()) {
				session.save(userSkill);

				userSkill.setNew(false);
			}
			else {
				session.merge(userSkill);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UserSkillModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((userSkillModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userSkillModelImpl.getOriginalSkillId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKILLID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID,
					args);

				args = new Object[] { userSkillModelImpl.getSkillId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKILLID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SKILLID,
					args);
			}

			if ((userSkillModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userSkillModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { userSkillModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		EntityCacheUtil.putResult(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
			UserSkillImpl.class, userSkill.getPrimaryKey(), userSkill);

		return userSkill;
	}

	protected UserSkill toUnwrappedModel(UserSkill userSkill) {
		if (userSkill instanceof UserSkillImpl) {
			return userSkill;
		}

		UserSkillImpl userSkillImpl = new UserSkillImpl();

		userSkillImpl.setNew(userSkill.isNew());
		userSkillImpl.setPrimaryKey(userSkill.getPrimaryKey());

		userSkillImpl.setSkillId(userSkill.getSkillId());
		userSkillImpl.setUserId(userSkill.getUserId());
		userSkillImpl.setDate(userSkill.getDate());
		userSkillImpl.setEndorsmentCounter(userSkill.getEndorsmentCounter());

		return userSkillImpl;
	}

	/**
	 * Returns the user skill with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user skill
	 * @return the user skill
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserSkillException, SystemException {
		UserSkill userSkill = fetchByPrimaryKey(primaryKey);

		if (userSkill == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserSkillException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userSkill;
	}

	/**
	 * Returns the user skill with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserSkillException} if it could not be found.
	 *
	 * @param userSkillPK the primary key of the user skill
	 * @return the user skill
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill findByPrimaryKey(UserSkillPK userSkillPK)
		throws NoSuchUserSkillException, SystemException {
		return findByPrimaryKey((Serializable)userSkillPK);
	}

	/**
	 * Returns the user skill with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user skill
	 * @return the user skill, or <code>null</code> if a user skill with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UserSkill userSkill = (UserSkill)EntityCacheUtil.getResult(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
				UserSkillImpl.class, primaryKey);

		if (userSkill == _nullUserSkill) {
			return null;
		}

		if (userSkill == null) {
			Session session = null;

			try {
				session = openSession();

				userSkill = (UserSkill)session.get(UserSkillImpl.class,
						primaryKey);

				if (userSkill != null) {
					cacheResult(userSkill);
				}
				else {
					EntityCacheUtil.putResult(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
						UserSkillImpl.class, primaryKey, _nullUserSkill);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UserSkillModelImpl.ENTITY_CACHE_ENABLED,
					UserSkillImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userSkill;
	}

	/**
	 * Returns the user skill with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userSkillPK the primary key of the user skill
	 * @return the user skill, or <code>null</code> if a user skill with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserSkill fetchByPrimaryKey(UserSkillPK userSkillPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)userSkillPK);
	}

	/**
	 * Returns all the user skills.
	 *
	 * @return the user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user skills.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user skills
	 * @param end the upper bound of the range of user skills (not inclusive)
	 * @return the range of user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user skills.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user skills
	 * @param end the upper bound of the range of user skills (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserSkill> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserSkill> list = (List<UserSkill>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_USERSKILL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERSKILL;

				if (pagination) {
					sql = sql.concat(UserSkillModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserSkill>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserSkill>(list);
				}
				else {
					list = (List<UserSkill>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user skills from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UserSkill userSkill : findAll()) {
			remove(userSkill);
		}
	}

	/**
	 * Returns the number of user skills.
	 *
	 * @return the number of user skills
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERSKILL);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the user skill persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.UserSkill")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UserSkill>> listenersList = new ArrayList<ModelListener<UserSkill>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UserSkill>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UserSkillImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_USERSKILL = "SELECT userSkill FROM UserSkill userSkill";
	private static final String _SQL_SELECT_USERSKILL_WHERE = "SELECT userSkill FROM UserSkill userSkill WHERE ";
	private static final String _SQL_COUNT_USERSKILL = "SELECT COUNT(userSkill) FROM UserSkill userSkill";
	private static final String _SQL_COUNT_USERSKILL_WHERE = "SELECT COUNT(userSkill) FROM UserSkill userSkill WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userSkill.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserSkill exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserSkill exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UserSkillPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"date"
			});
	private static UserSkill _nullUserSkill = new UserSkillImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UserSkill> toCacheModel() {
				return _nullUserSkillCacheModel;
			}
		};

	private static CacheModel<UserSkill> _nullUserSkillCacheModel = new CacheModel<UserSkill>() {
			@Override
			public UserSkill toEntityModel() {
				return _nullUserSkill;
			}
		};
}