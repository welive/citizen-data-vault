/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.CustomDataEntry;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CustomDataEntry in entity cache.
 *
 * @author Engineering
 * @see CustomDataEntry
 * @generated
 */
public class CustomDataEntryCacheModel implements CacheModel<CustomDataEntry>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{entryId=");
		sb.append(entryId);
		sb.append(", key=");
		sb.append(key);
		sb.append(", value=");
		sb.append(value);
		sb.append(", type=");
		sb.append(type);
		sb.append(", userid=");
		sb.append(userid);
		sb.append(", clientid=");
		sb.append(clientid);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CustomDataEntry toEntityModel() {
		CustomDataEntryImpl customDataEntryImpl = new CustomDataEntryImpl();

		customDataEntryImpl.setEntryId(entryId);

		if (key == null) {
			customDataEntryImpl.setKey(StringPool.BLANK);
		}
		else {
			customDataEntryImpl.setKey(key);
		}

		if (value == null) {
			customDataEntryImpl.setValue(StringPool.BLANK);
		}
		else {
			customDataEntryImpl.setValue(value);
		}

		if (type == null) {
			customDataEntryImpl.setType(StringPool.BLANK);
		}
		else {
			customDataEntryImpl.setType(type);
		}

		customDataEntryImpl.setUserid(userid);

		if (clientid == null) {
			customDataEntryImpl.setClientid(StringPool.BLANK);
		}
		else {
			customDataEntryImpl.setClientid(clientid);
		}

		customDataEntryImpl.resetOriginalValues();

		return customDataEntryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		entryId = objectInput.readLong();
		key = objectInput.readUTF();
		value = objectInput.readUTF();
		type = objectInput.readUTF();
		userid = objectInput.readLong();
		clientid = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(entryId);

		if (key == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(key);
		}

		if (value == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(value);
		}

		if (type == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(type);
		}

		objectOutput.writeLong(userid);

		if (clientid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(clientid);
		}
	}

	public long entryId;
	public String key;
	public String value;
	public String type;
	public long userid;
	public String clientid;
}