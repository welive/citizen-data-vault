package it.eng.rspa.cdv;

import com.liferay.portal.kernel.struts.BaseStrutsPortletAction;
import com.liferay.portal.kernel.struts.StrutsPortletAction;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;


public class CustomStrutsAction extends BaseStrutsPortletAction {
	
	public void processAction(
            StrutsPortletAction originalStrutsPortletAction,
            PortletConfig portletConfig, ActionRequest actionRequest,
            ActionResponse actionResponse) throws Exception{
		
		super.processAction(originalStrutsPortletAction, portletConfig, actionRequest, actionResponse);
		
		System.out.println(this.getClass().getName()+" --> processAction()");
		
        return ;
        
	}
	
	public String render(
            StrutsPortletAction originalStrutsPortletAction,
            PortletConfig portletConfig, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws Exception {
		
		super.render(originalStrutsPortletAction, portletConfig, renderRequest, renderResponse);
        
		System.out.println(this.getClass().getName()+" --> render()");
		
        return "/custom/redirect/path";
		
	}
	
}
