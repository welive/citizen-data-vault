/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.cdv.datamodel.model.UserSkill;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserSkill in entity cache.
 *
 * @author Engineering
 * @see UserSkill
 * @generated
 */
public class UserSkillCacheModel implements CacheModel<UserSkill>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{skillId=");
		sb.append(skillId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", date=");
		sb.append(date);
		sb.append(", endorsmentCounter=");
		sb.append(endorsmentCounter);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserSkill toEntityModel() {
		UserSkillImpl userSkillImpl = new UserSkillImpl();

		userSkillImpl.setSkillId(skillId);
		userSkillImpl.setUserId(userId);

		if (date == Long.MIN_VALUE) {
			userSkillImpl.setDate(null);
		}
		else {
			userSkillImpl.setDate(new Date(date));
		}

		userSkillImpl.setEndorsmentCounter(endorsmentCounter);

		userSkillImpl.resetOriginalValues();

		return userSkillImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		skillId = objectInput.readLong();
		userId = objectInput.readLong();
		date = objectInput.readLong();
		endorsmentCounter = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(skillId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(date);
		objectOutput.writeLong(endorsmentCounter);
	}

	public long skillId;
	public long userId;
	public long date;
	public long endorsmentCounter;
}