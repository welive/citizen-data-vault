package it.eng.rspa.cdv.conf;

import it.eng.rspa.cdv.datamodel.model.IdeaCdv;
import it.eng.rspa.cdv.datamodel.model.Language;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import it.eng.rspa.cdv.datamodel.model.UserIdea;
import it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UserIdeaImpl;
import it.eng.rspa.cdv.datamodel.service.IdeaCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserIdeaLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK;
import it.eng.rspa.cdv.utils.RestApiUtils;
import it.eng.rspa.cdv.utils.Utils;
import it.eng.rspa.cdv.utils.exception.InvalidInputException;
import it.eng.rspa.cdv.utils.model.Gender;
import it.eng.rspa.cdv.utils.model.UserIdeaRole;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.management.relation.RoleNotFoundException;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.Country;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public abstract class ConfUtils extends Utils{

	public static Role getRoleCitizen(){
		try{ return RoleLocalServiceUtil.getRole(Utils.getGenericPortalCompanyId(), PortletProps.get("role.citizen")); }
		catch(Exception e){
			Logger.getLogger(ConfUtils.class).error(e.getMessage());
			return null; 
		}
	}
	
	public static Role getRoleAcademy(){
		try{ return RoleLocalServiceUtil.getRole(Utils.getGenericPortalCompanyId(), PortletProps.get("role.academy")); }
		catch(Exception e){
			Logger.getLogger(ConfUtils.class).error(e.getMessage());
			return null; 
		}
	}
	
	public static Role getRoleEntrepreneur(){
		try{ return RoleLocalServiceUtil.getRole(Utils.getGenericPortalCompanyId(), PortletProps.get("role.entrepreneur")); }
		catch(Exception e){
			Logger.getLogger(ConfUtils.class).error(e.getMessage());
			return null; 
		}
	}
	
	public static Role getRoleBusiness(){
		try{ return RoleLocalServiceUtil.getRole(Utils.getGenericPortalCompanyId(), PortletProps.get("role.business")); }
		catch(Exception e){
			Logger.getLogger(ConfUtils.class).error(e.getMessage());
			return null; 
		}
	}
	
	public static Role getRoleDeveloper(){
		try{ return RoleLocalServiceUtil.getRole(Utils.getGenericPortalCompanyId(), PortletProps.get("role.developer")); }
		catch(Exception e){
			Logger.getLogger(ConfUtils.class).error(e.getMessage());
			return null; 
		}
	}
	
	public static boolean isDeveloper(User u){
		try { return u.getRoles().contains(getRoleDeveloper()); }
		catch(Exception e){ return false; }
	}
	
	public static  long getRoleCitizenID(){
		
		Role rCitizen = getRoleCitizen();
		if(Validator.isNull(rCitizen))
			return -1L;
		else
			return rCitizen.getRoleId();
	}
	
	public static  List<User> getPlatformCitizens() 
			throws RoleNotFoundException{
		
		List<User> citizens = new ArrayList<User>();
		
		long rCitizenID = getRoleCitizenID();
		if(rCitizenID == -1){
			throw new RoleNotFoundException();
		}
		
		try{ citizens = UserLocalServiceUtil.getRoleUsers(rCitizenID); }
		catch(Exception e){ 
			Logger.getLogger(ConfUtils.class).warn(e.getMessage());	
		}
		
		return citizens;
	}
	
	public static void addOrUpdateUserCdv(UserCdv ucdv) 
			throws SystemException{
		
		boolean isUpdate = false;
		try { 
			UserCdv oldUserCdv = UserCdvLocalServiceUtil.getUserCdv(ucdv.getPrimaryKey());
			if(Validator.isNotNull(oldUserCdv))
				isUpdate = true;
		}
		catch(Exception e){ isUpdate = false; }
		
		if(isUpdate)
			UserCdvLocalServiceUtil.updateUserCdv(ucdv);
		else
			UserCdvLocalServiceUtil.addUserCdv(ucdv);
		
		return;
	}
	
	protected static UserCdv getUserCdvInstance(long ccuid, long uid){
		UserCdvPK ucdvpk = new UserCdvPK();
		ucdvpk.setCcuid(ccuid);
		ucdvpk.setUserId(uid);
				
		UserCdv u = UserCdvLocalServiceUtil.createUserCdv(ucdvpk);
		u.setCompanyId(getGenericPortalCompanyId());
		
		return u;
	}
	
	public static String getPilotId(User u){
		String pilotId = "";
		String[] pilots = ((String[])u.getExpandoBridge().getAttribute("pilot"));
		if(pilots!=null && pilots.length > 0) pilotId = pilots[0];
		
		return pilotId;
	}
	
	public static String[] getLanguages(User u){
		String[] out = new String[]{"English"};
		
		Serializable sLangs = u.getExpandoBridge().getAttribute("languages");
		if(Validator.isNotNull(sLangs)){
			String[] langs = (String[])sLangs ;
			if(langs.length>0){
				out = langs;
			}
		}
		
		return out;
	}
	
	public static UserCdv generateUserCdv(User citizen) 
			throws Exception{
		
		UserCdv ucdv = getUserCdvInstance(getUserCcUserId(citizen), citizen.getUserId());
		
		//Birthday
		Date birthday = citizen.getBirthday();
		if(Validator.isNotNull(birthday)){
			ucdv.setBirthdate(birthday);
		}
		
		//Address
		List<Address> addresses = citizen.getAddresses();
		if(Validator.isNotNull(addresses) && !addresses.isEmpty()){
			
			Address address = addresses.get(0);
			if(Validator.isNotNull(address)){
				
				ucdv.setAddress(address.getStreet1());
				ucdv.setZipcode(address.getZip());
				ucdv.setCity(address.getCity());
				
				Country country = address.getCountry();
				if(Validator.isNotNull(country)){
					ucdv.setCountry(country.getName());
				}
			}
		}
		
		//Others
		ucdv.setEmail(citizen.getEmailAddress());
		ucdv.setIsDeveloper(isDeveloper(citizen));
		ucdv.setPilot(getPilotId(citizen));
		ucdv.setName(citizen.getFirstName());
		ucdv.setSurname(citizen.getLastName());
		ucdv.setLastKnownLatitude(0.0);
		ucdv.setLastKnownLongitude(0.0);
		ucdv.setUsername(citizen.getScreenName());
		ucdv.setReputation(0);
		ucdv.setGender(citizen.isMale() ? Gender.MALE.toString() : Gender.FEMALE.toString());
		
		return ucdv;
		
	}
	
	public static void alignLanguages(User citizen, UserCdv ucdv){
		
		String[] langs = ConfUtils.getLanguages(citizen);
		for(String lang:langs){
			
			String currlang = lang.trim().toLowerCase();
			Language dtoLang = RestApiUtils.getLanguage(currlang);
			
			RestApiUtils.associateUserLanguage(ucdv, dtoLang);
		}
	}
	
	protected static void alignAuthorships(long userid){
		
		List<CLSIdea> author_ideas = new ArrayList<CLSIdea>();
		try{ author_ideas = CLSIdeaLocalServiceUtil.getIdeasByUserId(userid); }
		catch(Exception e){ /* Do nothing */ }
		
		for(CLSIdea idea : author_ideas){
			
			try{
				String ideatitle = idea.getIdeaTitle();
				Long ideaid = idea.getIdeaID();
				Boolean isNeed = idea.isIsNeed();
				
				IdeaCdv previous_idea = null;
				try{  previous_idea = IdeaCdvLocalServiceUtil.getIdeaCdv(ideaid); }
				catch(Exception e){ /* It should be the normal flow */ }
				
				
				if(Validator.isNull(previous_idea)){
					
					/* It should be the normal flow */
					
					IdeaCdv icdv = new IdeaCdvImpl();
					icdv.setIdeaId(ideaid);
					icdv.setIdeaName(ideatitle);
					icdv.setNeed(isNeed);
					
					try{ previous_idea = IdeaCdvLocalServiceUtil.addIdeaCdv(icdv); }
					catch(Exception e){
						throw new InvalidInputException(e.getMessage());
					}
					
				}
				
				long _ideaid = previous_idea.getIdeaId();
				
				UserIdea ui = new UserIdeaImpl();
				ui.setRole(UserIdeaRole.Author.toString());
				ui.setIdeaId(_ideaid);
				ui.setUserId(userid);
				
				try{ UserIdeaLocalServiceUtil.addUserIdea(ui); }
				catch(Exception e){
					throw new InvalidInputException("Authorship for the pair [" + userid + ", " + _ideaid + "] already exists.");
				}
			}
			catch(Exception e){
				Logger.getLogger(ConfUtils.class).error(e.getMessage());
			}
		}
	}
	
	protected static void alignCollaborations(long userid){
		
		List<CLSCoworker> cws = new ArrayList<CLSCoworker>(); 
		
		try{ cws = CLSCoworkerLocalServiceUtil.getCoworkersByUserId(userid); }
		catch(Exception e){ /* No collaboration */ }
		
		for(CLSCoworker cw : cws){
			try{
				long ideaid = cw.getIdeaID();
				
				CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaid);
				
				String ideatitle = idea.getIdeaTitle();
				Boolean isNeed = idea.isIsNeed();
					
				IdeaCdv previous_idea = null;
				try{  previous_idea = IdeaCdvLocalServiceUtil.getIdeaCdv(ideaid); }
				catch(Exception e){ /* It should be the normal flow */ }
				
				if(Validator.isNull(previous_idea)){
					
					/* It should be the normal flow */
					
					IdeaCdv icdv = new IdeaCdvImpl();
					icdv.setIdeaId(ideaid);
					icdv.setIdeaName(ideatitle);
					icdv.setNeed(isNeed);
					
					try{ previous_idea = IdeaCdvLocalServiceUtil.addIdeaCdv(icdv); }
					catch(Exception e){
						throw new InvalidInputException(e.getMessage());
					}
					
				}
				
				long _ideaid = previous_idea.getIdeaId();
				
				UserIdea ui = new UserIdeaImpl();
				ui.setRole(UserIdeaRole.Collaborator.toString());
				ui.setIdeaId(_ideaid);
				ui.setUserId(userid);
				
				try{ UserIdeaLocalServiceUtil.addUserIdea(ui); }
				catch(Exception e){
					throw new InvalidInputException("Collaboration for the pair [" + userid + ", " + _ideaid + "] already exists.");
				}
			}
			catch(Exception e){
				Logger.getLogger(ConfUtils.class).error(e.getMessage());
			}
		}
	}
	
	public static void alignActivities(UserCdv ucdv){
		
		long userid = ucdv.getUserId();
		
		alignAuthorships(userid);
		alignCollaborations(userid);
		
		return;

	}
}
