/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException;
import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.model.impl.UserLanguageImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UserLanguageModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the user language service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserLanguagePersistence
 * @see UserLanguageUtil
 * @generated
 */
public class UserLanguagePersistenceImpl extends BasePersistenceImpl<UserLanguage>
	implements UserLanguagePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserLanguageUtil} to access the user language persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserLanguageImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageModelImpl.FINDER_CACHE_ENABLED, UserLanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageModelImpl.FINDER_CACHE_ENABLED, UserLanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageModelImpl.FINDER_CACHE_ENABLED, UserLanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageModelImpl.FINDER_CACHE_ENABLED, UserLanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			UserLanguageModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user languages where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching user languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserLanguage> findByuserId(long userId)
		throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user languages where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserLanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user languages
	 * @param end the upper bound of the range of user languages (not inclusive)
	 * @return the range of matching user languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserLanguage> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user languages where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserLanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user languages
	 * @param end the upper bound of the range of user languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserLanguage> findByuserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UserLanguage> list = (List<UserLanguage>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UserLanguage userLanguage : list) {
				if ((userId != userLanguage.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERLANGUAGE_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserLanguageModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UserLanguage>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserLanguage>(list);
				}
				else {
					list = (List<UserLanguage>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user language in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException if a matching user language could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserLanguageException, SystemException {
		UserLanguage userLanguage = fetchByuserId_First(userId,
				orderByComparator);

		if (userLanguage != null) {
			return userLanguage;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserLanguageException(msg.toString());
	}

	/**
	 * Returns the first user language in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user language, or <code>null</code> if a matching user language could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UserLanguage> list = findByuserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user language in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException if a matching user language could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserLanguageException, SystemException {
		UserLanguage userLanguage = fetchByuserId_Last(userId, orderByComparator);

		if (userLanguage != null) {
			return userLanguage;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserLanguageException(msg.toString());
	}

	/**
	 * Returns the last user language in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user language, or <code>null</code> if a matching user language could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		if (count == 0) {
			return null;
		}

		List<UserLanguage> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user languages before and after the current user language in the ordered set where userId = &#63;.
	 *
	 * @param userLanguagePK the primary key of the current user language
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException if a user language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage[] findByuserId_PrevAndNext(
		UserLanguagePK userLanguagePK, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUserLanguageException, SystemException {
		UserLanguage userLanguage = findByPrimaryKey(userLanguagePK);

		Session session = null;

		try {
			session = openSession();

			UserLanguage[] array = new UserLanguageImpl[3];

			array[0] = getByuserId_PrevAndNext(session, userLanguage, userId,
					orderByComparator, true);

			array[1] = userLanguage;

			array[2] = getByuserId_PrevAndNext(session, userLanguage, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserLanguage getByuserId_PrevAndNext(Session session,
		UserLanguage userLanguage, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERLANGUAGE_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserLanguageModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userLanguage);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserLanguage> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user languages where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserId(long userId) throws SystemException {
		for (UserLanguage userLanguage : findByuserId(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userLanguage);
		}
	}

	/**
	 * Returns the number of user languages where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching user languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERLANGUAGE_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "userLanguage.id.userId = ?";

	public UserLanguagePersistenceImpl() {
		setModelClass(UserLanguage.class);
	}

	/**
	 * Caches the user language in the entity cache if it is enabled.
	 *
	 * @param userLanguage the user language
	 */
	@Override
	public void cacheResult(UserLanguage userLanguage) {
		EntityCacheUtil.putResult(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageImpl.class, userLanguage.getPrimaryKey(), userLanguage);

		userLanguage.resetOriginalValues();
	}

	/**
	 * Caches the user languages in the entity cache if it is enabled.
	 *
	 * @param userLanguages the user languages
	 */
	@Override
	public void cacheResult(List<UserLanguage> userLanguages) {
		for (UserLanguage userLanguage : userLanguages) {
			if (EntityCacheUtil.getResult(
						UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
						UserLanguageImpl.class, userLanguage.getPrimaryKey()) == null) {
				cacheResult(userLanguage);
			}
			else {
				userLanguage.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user languages.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UserLanguageImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UserLanguageImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user language.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserLanguage userLanguage) {
		EntityCacheUtil.removeResult(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageImpl.class, userLanguage.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserLanguage> userLanguages) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserLanguage userLanguage : userLanguages) {
			EntityCacheUtil.removeResult(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
				UserLanguageImpl.class, userLanguage.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user language with the primary key. Does not add the user language to the database.
	 *
	 * @param userLanguagePK the primary key for the new user language
	 * @return the new user language
	 */
	@Override
	public UserLanguage create(UserLanguagePK userLanguagePK) {
		UserLanguage userLanguage = new UserLanguageImpl();

		userLanguage.setNew(true);
		userLanguage.setPrimaryKey(userLanguagePK);

		return userLanguage;
	}

	/**
	 * Removes the user language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userLanguagePK the primary key of the user language
	 * @return the user language that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException if a user language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage remove(UserLanguagePK userLanguagePK)
		throws NoSuchUserLanguageException, SystemException {
		return remove((Serializable)userLanguagePK);
	}

	/**
	 * Removes the user language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user language
	 * @return the user language that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException if a user language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage remove(Serializable primaryKey)
		throws NoSuchUserLanguageException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UserLanguage userLanguage = (UserLanguage)session.get(UserLanguageImpl.class,
					primaryKey);

			if (userLanguage == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserLanguageException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userLanguage);
		}
		catch (NoSuchUserLanguageException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserLanguage removeImpl(UserLanguage userLanguage)
		throws SystemException {
		userLanguage = toUnwrappedModel(userLanguage);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userLanguage)) {
				userLanguage = (UserLanguage)session.get(UserLanguageImpl.class,
						userLanguage.getPrimaryKeyObj());
			}

			if (userLanguage != null) {
				session.delete(userLanguage);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userLanguage != null) {
			clearCache(userLanguage);
		}

		return userLanguage;
	}

	@Override
	public UserLanguage updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserLanguage userLanguage)
		throws SystemException {
		userLanguage = toUnwrappedModel(userLanguage);

		boolean isNew = userLanguage.isNew();

		UserLanguageModelImpl userLanguageModelImpl = (UserLanguageModelImpl)userLanguage;

		Session session = null;

		try {
			session = openSession();

			if (userLanguage.isNew()) {
				session.save(userLanguage);

				userLanguage.setNew(false);
			}
			else {
				session.merge(userLanguage);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UserLanguageModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((userLanguageModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userLanguageModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { userLanguageModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		EntityCacheUtil.putResult(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
			UserLanguageImpl.class, userLanguage.getPrimaryKey(), userLanguage);

		return userLanguage;
	}

	protected UserLanguage toUnwrappedModel(UserLanguage userLanguage) {
		if (userLanguage instanceof UserLanguageImpl) {
			return userLanguage;
		}

		UserLanguageImpl userLanguageImpl = new UserLanguageImpl();

		userLanguageImpl.setNew(userLanguage.isNew());
		userLanguageImpl.setPrimaryKey(userLanguage.getPrimaryKey());

		userLanguageImpl.setLanguageId(userLanguage.getLanguageId());
		userLanguageImpl.setUserId(userLanguage.getUserId());

		return userLanguageImpl;
	}

	/**
	 * Returns the user language with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user language
	 * @return the user language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException if a user language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserLanguageException, SystemException {
		UserLanguage userLanguage = fetchByPrimaryKey(primaryKey);

		if (userLanguage == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserLanguageException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userLanguage;
	}

	/**
	 * Returns the user language with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException} if it could not be found.
	 *
	 * @param userLanguagePK the primary key of the user language
	 * @return the user language
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException if a user language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage findByPrimaryKey(UserLanguagePK userLanguagePK)
		throws NoSuchUserLanguageException, SystemException {
		return findByPrimaryKey((Serializable)userLanguagePK);
	}

	/**
	 * Returns the user language with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user language
	 * @return the user language, or <code>null</code> if a user language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UserLanguage userLanguage = (UserLanguage)EntityCacheUtil.getResult(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
				UserLanguageImpl.class, primaryKey);

		if (userLanguage == _nullUserLanguage) {
			return null;
		}

		if (userLanguage == null) {
			Session session = null;

			try {
				session = openSession();

				userLanguage = (UserLanguage)session.get(UserLanguageImpl.class,
						primaryKey);

				if (userLanguage != null) {
					cacheResult(userLanguage);
				}
				else {
					EntityCacheUtil.putResult(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
						UserLanguageImpl.class, primaryKey, _nullUserLanguage);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UserLanguageModelImpl.ENTITY_CACHE_ENABLED,
					UserLanguageImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userLanguage;
	}

	/**
	 * Returns the user language with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userLanguagePK the primary key of the user language
	 * @return the user language, or <code>null</code> if a user language with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UserLanguage fetchByPrimaryKey(UserLanguagePK userLanguagePK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)userLanguagePK);
	}

	/**
	 * Returns all the user languages.
	 *
	 * @return the user languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserLanguage> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserLanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user languages
	 * @param end the upper bound of the range of user languages (not inclusive)
	 * @return the range of user languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserLanguage> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserLanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user languages
	 * @param end the upper bound of the range of user languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UserLanguage> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserLanguage> list = (List<UserLanguage>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_USERLANGUAGE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERLANGUAGE;

				if (pagination) {
					sql = sql.concat(UserLanguageModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserLanguage>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UserLanguage>(list);
				}
				else {
					list = (List<UserLanguage>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user languages from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UserLanguage userLanguage : findAll()) {
			remove(userLanguage);
		}
	}

	/**
	 * Returns the number of user languages.
	 *
	 * @return the number of user languages
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERLANGUAGE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the user language persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.UserLanguage")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UserLanguage>> listenersList = new ArrayList<ModelListener<UserLanguage>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UserLanguage>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UserLanguageImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_USERLANGUAGE = "SELECT userLanguage FROM UserLanguage userLanguage";
	private static final String _SQL_SELECT_USERLANGUAGE_WHERE = "SELECT userLanguage FROM UserLanguage userLanguage WHERE ";
	private static final String _SQL_COUNT_USERLANGUAGE = "SELECT COUNT(userLanguage) FROM UserLanguage userLanguage";
	private static final String _SQL_COUNT_USERLANGUAGE_WHERE = "SELECT COUNT(userLanguage) FROM UserLanguage userLanguage WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userLanguage.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserLanguage exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserLanguage exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UserLanguagePersistenceImpl.class);
	private static UserLanguage _nullUserLanguage = new UserLanguageImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UserLanguage> toCacheModel() {
				return _nullUserLanguageCacheModel;
			}
		};

	private static CacheModel<UserLanguage> _nullUserLanguageCacheModel = new CacheModel<UserLanguage>() {
			@Override
			public UserLanguage toEntityModel() {
				return _nullUserLanguage;
			}
		};
}