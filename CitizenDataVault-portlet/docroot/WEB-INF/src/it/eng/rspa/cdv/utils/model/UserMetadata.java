package it.eng.rspa.cdv.utils.model;

import java.util.HashSet;
import java.util.Set;

public class UserMetadata {

	private String birthdate;
	private String address;
	private String city;
	private Set<String> userTags;
	private Set<OutSkill> skills;
	private Set<OutApp> usedApps;
	private Location lastKnownLocation;
	
	public UserMetadata(){
		userTags = new HashSet<String>();
		skills = new HashSet<OutSkill>(); 
		usedApps = new HashSet<OutApp>();
	}
	
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Set<String> getUserTags() {
		return userTags;
	}
	public void setUserTags(Set<String> userTags) {
		this.userTags = userTags;
	}
	public Set<OutSkill> getSkills() {
		return skills;
	}
	public void setSkills(Set<OutSkill> skills) {
		this.skills = skills;
	}
	public Set<OutApp> getUsedApps() {
		return usedApps;
	}
	public void setUsedApps(Set<OutApp> usedApps) {
		this.usedApps = usedApps;
	}
	public Location getLastKnownLocation() {
		return lastKnownLocation;
	}
	public void setLastKnownLocation(Location lastKnownLocation) {
		this.lastKnownLocation = lastKnownLocation;
	}
	
}
