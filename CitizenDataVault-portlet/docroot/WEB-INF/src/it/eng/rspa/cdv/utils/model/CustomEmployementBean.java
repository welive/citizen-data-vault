package it.eng.rspa.cdv.utils.model;

public enum CustomEmployementBean {

	Student("Student"),
	Unemployed("Unemployed"),
	Employedbythirdparty("Employed by third party"),
	Selfemployedentrepreneur("Self-employed / entrepreneur"),
	Retired("Retired"),
	Other("Other");
	
	private String txt;
	
	private CustomEmployementBean(String t){
		this.txt = t.trim();
	}
	
	public String toString(){
		return this.txt;
	}
	
}
