package it.eng.rspa.cdv.conf;

import java.util.List;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import javax.management.relation.RoleNotFoundException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.model.User;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class CDVConfiguration
 */
public class CDVConfiguration extends MVCPortlet {
 
	public void init(ActionRequest request, ActionResponse response){
		
		try{ 
			List<User> citizens = ConfUtils.getPlatformCitizens();
			
			for(User citizen : citizens){
				try{
					UserCdv ucdv = ConfUtils.generateUserCdv(citizen);
					ConfUtils.addOrUpdateUserCdv(ucdv);
					ConfUtils.alignLanguages(citizen, ucdv);
					
					ConfUtils.alignActivities(ucdv);
					
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
		}
		catch(RoleNotFoundException re){
			Logger.getLogger(this.getClass()).error("No citizen role found");
			SessionErrors.add(request, "no-role");
		}
		
		return;
	}
	

}
