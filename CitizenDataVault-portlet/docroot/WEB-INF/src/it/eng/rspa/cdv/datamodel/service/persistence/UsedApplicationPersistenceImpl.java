/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;
import it.eng.rspa.cdv.datamodel.model.UsedApplication;
import it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationImpl;
import it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the used application service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UsedApplicationPersistence
 * @see UsedApplicationUtil
 * @generated
 */
public class UsedApplicationPersistenceImpl extends BasePersistenceImpl<UsedApplication>
	implements UsedApplicationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UsedApplicationUtil} to access the used application persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UsedApplicationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED,
			UsedApplicationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED,
			UsedApplicationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED,
			UsedApplicationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByuserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED,
			UsedApplicationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			UsedApplicationModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the used applications where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findByuserId(long userId)
		throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the used applications where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of used applications
	 * @param end the upper bound of the range of used applications (not inclusive)
	 * @return the range of matching used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the used applications where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of used applications
	 * @param end the upper bound of the range of used applications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findByuserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UsedApplication> list = (List<UsedApplication>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UsedApplication usedApplication : list) {
				if ((userId != usedApplication.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USEDAPPLICATION_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UsedApplicationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UsedApplication>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UsedApplication>(list);
				}
				else {
					list = (List<UsedApplication>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first used application in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching used application
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a matching used application could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUsedApplicationException, SystemException {
		UsedApplication usedApplication = fetchByuserId_First(userId,
				orderByComparator);

		if (usedApplication != null) {
			return usedApplication;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUsedApplicationException(msg.toString());
	}

	/**
	 * Returns the first used application in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching used application, or <code>null</code> if a matching used application could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UsedApplication> list = findByuserId(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last used application in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching used application
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a matching used application could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUsedApplicationException, SystemException {
		UsedApplication usedApplication = fetchByuserId_Last(userId,
				orderByComparator);

		if (usedApplication != null) {
			return usedApplication;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUsedApplicationException(msg.toString());
	}

	/**
	 * Returns the last used application in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching used application, or <code>null</code> if a matching used application could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		if (count == 0) {
			return null;
		}

		List<UsedApplication> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the used applications before and after the current used application in the ordered set where userId = &#63;.
	 *
	 * @param usedApplicationPK the primary key of the current used application
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next used application
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication[] findByuserId_PrevAndNext(
		UsedApplicationPK usedApplicationPK, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchUsedApplicationException, SystemException {
		UsedApplication usedApplication = findByPrimaryKey(usedApplicationPK);

		Session session = null;

		try {
			session = openSession();

			UsedApplication[] array = new UsedApplicationImpl[3];

			array[0] = getByuserId_PrevAndNext(session, usedApplication,
					userId, orderByComparator, true);

			array[1] = usedApplication;

			array[2] = getByuserId_PrevAndNext(session, usedApplication,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UsedApplication getByuserId_PrevAndNext(Session session,
		UsedApplication usedApplication, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USEDAPPLICATION_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UsedApplicationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(usedApplication);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UsedApplication> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the used applications where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByuserId(long userId) throws SystemException {
		for (UsedApplication usedApplication : findByuserId(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(usedApplication);
		}
	}

	/**
	 * Returns the number of used applications where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USEDAPPLICATION_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "usedApplication.id.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_APPID = new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED,
			UsedApplicationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByappId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPID = new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED,
			UsedApplicationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByappId",
			new String[] { Long.class.getName() },
			UsedApplicationModelImpl.APPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_APPID = new FinderPath(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByappId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the used applications where appId = &#63;.
	 *
	 * @param appId the app ID
	 * @return the matching used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findByappId(long appId)
		throws SystemException {
		return findByappId(appId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the used applications where appId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param appId the app ID
	 * @param start the lower bound of the range of used applications
	 * @param end the upper bound of the range of used applications (not inclusive)
	 * @return the range of matching used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findByappId(long appId, int start, int end)
		throws SystemException {
		return findByappId(appId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the used applications where appId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param appId the app ID
	 * @param start the lower bound of the range of used applications
	 * @param end the upper bound of the range of used applications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findByappId(long appId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPID;
			finderArgs = new Object[] { appId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_APPID;
			finderArgs = new Object[] { appId, start, end, orderByComparator };
		}

		List<UsedApplication> list = (List<UsedApplication>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UsedApplication usedApplication : list) {
				if ((appId != usedApplication.getAppId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USEDAPPLICATION_WHERE);

			query.append(_FINDER_COLUMN_APPID_APPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UsedApplicationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(appId);

				if (!pagination) {
					list = (List<UsedApplication>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UsedApplication>(list);
				}
				else {
					list = (List<UsedApplication>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first used application in the ordered set where appId = &#63;.
	 *
	 * @param appId the app ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching used application
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a matching used application could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication findByappId_First(long appId,
		OrderByComparator orderByComparator)
		throws NoSuchUsedApplicationException, SystemException {
		UsedApplication usedApplication = fetchByappId_First(appId,
				orderByComparator);

		if (usedApplication != null) {
			return usedApplication;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("appId=");
		msg.append(appId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUsedApplicationException(msg.toString());
	}

	/**
	 * Returns the first used application in the ordered set where appId = &#63;.
	 *
	 * @param appId the app ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching used application, or <code>null</code> if a matching used application could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication fetchByappId_First(long appId,
		OrderByComparator orderByComparator) throws SystemException {
		List<UsedApplication> list = findByappId(appId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last used application in the ordered set where appId = &#63;.
	 *
	 * @param appId the app ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching used application
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a matching used application could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication findByappId_Last(long appId,
		OrderByComparator orderByComparator)
		throws NoSuchUsedApplicationException, SystemException {
		UsedApplication usedApplication = fetchByappId_Last(appId,
				orderByComparator);

		if (usedApplication != null) {
			return usedApplication;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("appId=");
		msg.append(appId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUsedApplicationException(msg.toString());
	}

	/**
	 * Returns the last used application in the ordered set where appId = &#63;.
	 *
	 * @param appId the app ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching used application, or <code>null</code> if a matching used application could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication fetchByappId_Last(long appId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByappId(appId);

		if (count == 0) {
			return null;
		}

		List<UsedApplication> list = findByappId(appId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the used applications before and after the current used application in the ordered set where appId = &#63;.
	 *
	 * @param usedApplicationPK the primary key of the current used application
	 * @param appId the app ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next used application
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication[] findByappId_PrevAndNext(
		UsedApplicationPK usedApplicationPK, long appId,
		OrderByComparator orderByComparator)
		throws NoSuchUsedApplicationException, SystemException {
		UsedApplication usedApplication = findByPrimaryKey(usedApplicationPK);

		Session session = null;

		try {
			session = openSession();

			UsedApplication[] array = new UsedApplicationImpl[3];

			array[0] = getByappId_PrevAndNext(session, usedApplication, appId,
					orderByComparator, true);

			array[1] = usedApplication;

			array[2] = getByappId_PrevAndNext(session, usedApplication, appId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UsedApplication getByappId_PrevAndNext(Session session,
		UsedApplication usedApplication, long appId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USEDAPPLICATION_WHERE);

		query.append(_FINDER_COLUMN_APPID_APPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UsedApplicationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(appId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(usedApplication);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UsedApplication> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the used applications where appId = &#63; from the database.
	 *
	 * @param appId the app ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByappId(long appId) throws SystemException {
		for (UsedApplication usedApplication : findByappId(appId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(usedApplication);
		}
	}

	/**
	 * Returns the number of used applications where appId = &#63;.
	 *
	 * @param appId the app ID
	 * @return the number of matching used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByappId(long appId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_APPID;

		Object[] finderArgs = new Object[] { appId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USEDAPPLICATION_WHERE);

			query.append(_FINDER_COLUMN_APPID_APPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(appId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPID_APPID_2 = "usedApplication.id.appId = ?";

	public UsedApplicationPersistenceImpl() {
		setModelClass(UsedApplication.class);
	}

	/**
	 * Caches the used application in the entity cache if it is enabled.
	 *
	 * @param usedApplication the used application
	 */
	@Override
	public void cacheResult(UsedApplication usedApplication) {
		EntityCacheUtil.putResult(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationImpl.class, usedApplication.getPrimaryKey(),
			usedApplication);

		usedApplication.resetOriginalValues();
	}

	/**
	 * Caches the used applications in the entity cache if it is enabled.
	 *
	 * @param usedApplications the used applications
	 */
	@Override
	public void cacheResult(List<UsedApplication> usedApplications) {
		for (UsedApplication usedApplication : usedApplications) {
			if (EntityCacheUtil.getResult(
						UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
						UsedApplicationImpl.class,
						usedApplication.getPrimaryKey()) == null) {
				cacheResult(usedApplication);
			}
			else {
				usedApplication.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all used applications.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UsedApplicationImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UsedApplicationImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the used application.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UsedApplication usedApplication) {
		EntityCacheUtil.removeResult(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationImpl.class, usedApplication.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UsedApplication> usedApplications) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UsedApplication usedApplication : usedApplications) {
			EntityCacheUtil.removeResult(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
				UsedApplicationImpl.class, usedApplication.getPrimaryKey());
		}
	}

	/**
	 * Creates a new used application with the primary key. Does not add the used application to the database.
	 *
	 * @param usedApplicationPK the primary key for the new used application
	 * @return the new used application
	 */
	@Override
	public UsedApplication create(UsedApplicationPK usedApplicationPK) {
		UsedApplication usedApplication = new UsedApplicationImpl();

		usedApplication.setNew(true);
		usedApplication.setPrimaryKey(usedApplicationPK);

		return usedApplication;
	}

	/**
	 * Removes the used application with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param usedApplicationPK the primary key of the used application
	 * @return the used application that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication remove(UsedApplicationPK usedApplicationPK)
		throws NoSuchUsedApplicationException, SystemException {
		return remove((Serializable)usedApplicationPK);
	}

	/**
	 * Removes the used application with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the used application
	 * @return the used application that was removed
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication remove(Serializable primaryKey)
		throws NoSuchUsedApplicationException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UsedApplication usedApplication = (UsedApplication)session.get(UsedApplicationImpl.class,
					primaryKey);

			if (usedApplication == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUsedApplicationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(usedApplication);
		}
		catch (NoSuchUsedApplicationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UsedApplication removeImpl(UsedApplication usedApplication)
		throws SystemException {
		usedApplication = toUnwrappedModel(usedApplication);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(usedApplication)) {
				usedApplication = (UsedApplication)session.get(UsedApplicationImpl.class,
						usedApplication.getPrimaryKeyObj());
			}

			if (usedApplication != null) {
				session.delete(usedApplication);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (usedApplication != null) {
			clearCache(usedApplication);
		}

		return usedApplication;
	}

	@Override
	public UsedApplication updateImpl(
		it.eng.rspa.cdv.datamodel.model.UsedApplication usedApplication)
		throws SystemException {
		usedApplication = toUnwrappedModel(usedApplication);

		boolean isNew = usedApplication.isNew();

		UsedApplicationModelImpl usedApplicationModelImpl = (UsedApplicationModelImpl)usedApplication;

		Session session = null;

		try {
			session = openSession();

			if (usedApplication.isNew()) {
				session.save(usedApplication);

				usedApplication.setNew(false);
			}
			else {
				session.merge(usedApplication);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UsedApplicationModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((usedApplicationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						usedApplicationModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { usedApplicationModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((usedApplicationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						usedApplicationModelImpl.getOriginalAppId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_APPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPID,
					args);

				args = new Object[] { usedApplicationModelImpl.getAppId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_APPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_APPID,
					args);
			}
		}

		EntityCacheUtil.putResult(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
			UsedApplicationImpl.class, usedApplication.getPrimaryKey(),
			usedApplication);

		return usedApplication;
	}

	protected UsedApplication toUnwrappedModel(UsedApplication usedApplication) {
		if (usedApplication instanceof UsedApplicationImpl) {
			return usedApplication;
		}

		UsedApplicationImpl usedApplicationImpl = new UsedApplicationImpl();

		usedApplicationImpl.setNew(usedApplication.isNew());
		usedApplicationImpl.setPrimaryKey(usedApplication.getPrimaryKey());

		usedApplicationImpl.setAppId(usedApplication.getAppId());
		usedApplicationImpl.setUserId(usedApplication.getUserId());

		return usedApplicationImpl;
	}

	/**
	 * Returns the used application with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the used application
	 * @return the used application
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUsedApplicationException, SystemException {
		UsedApplication usedApplication = fetchByPrimaryKey(primaryKey);

		if (usedApplication == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUsedApplicationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return usedApplication;
	}

	/**
	 * Returns the used application with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException} if it could not be found.
	 *
	 * @param usedApplicationPK the primary key of the used application
	 * @return the used application
	 * @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication findByPrimaryKey(UsedApplicationPK usedApplicationPK)
		throws NoSuchUsedApplicationException, SystemException {
		return findByPrimaryKey((Serializable)usedApplicationPK);
	}

	/**
	 * Returns the used application with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the used application
	 * @return the used application, or <code>null</code> if a used application with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UsedApplication usedApplication = (UsedApplication)EntityCacheUtil.getResult(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
				UsedApplicationImpl.class, primaryKey);

		if (usedApplication == _nullUsedApplication) {
			return null;
		}

		if (usedApplication == null) {
			Session session = null;

			try {
				session = openSession();

				usedApplication = (UsedApplication)session.get(UsedApplicationImpl.class,
						primaryKey);

				if (usedApplication != null) {
					cacheResult(usedApplication);
				}
				else {
					EntityCacheUtil.putResult(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
						UsedApplicationImpl.class, primaryKey,
						_nullUsedApplication);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UsedApplicationModelImpl.ENTITY_CACHE_ENABLED,
					UsedApplicationImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return usedApplication;
	}

	/**
	 * Returns the used application with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param usedApplicationPK the primary key of the used application
	 * @return the used application, or <code>null</code> if a used application with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsedApplication fetchByPrimaryKey(
		UsedApplicationPK usedApplicationPK) throws SystemException {
		return fetchByPrimaryKey((Serializable)usedApplicationPK);
	}

	/**
	 * Returns all the used applications.
	 *
	 * @return the used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the used applications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of used applications
	 * @param end the upper bound of the range of used applications (not inclusive)
	 * @return the range of used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the used applications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of used applications
	 * @param end the upper bound of the range of used applications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsedApplication> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UsedApplication> list = (List<UsedApplication>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_USEDAPPLICATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USEDAPPLICATION;

				if (pagination) {
					sql = sql.concat(UsedApplicationModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UsedApplication>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UsedApplication>(list);
				}
				else {
					list = (List<UsedApplication>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the used applications from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UsedApplication usedApplication : findAll()) {
			remove(usedApplication);
		}
	}

	/**
	 * Returns the number of used applications.
	 *
	 * @return the number of used applications
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USEDAPPLICATION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the used application persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.cdv.datamodel.model.UsedApplication")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UsedApplication>> listenersList = new ArrayList<ModelListener<UsedApplication>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UsedApplication>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UsedApplicationImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_USEDAPPLICATION = "SELECT usedApplication FROM UsedApplication usedApplication";
	private static final String _SQL_SELECT_USEDAPPLICATION_WHERE = "SELECT usedApplication FROM UsedApplication usedApplication WHERE ";
	private static final String _SQL_COUNT_USEDAPPLICATION = "SELECT COUNT(usedApplication) FROM UsedApplication usedApplication";
	private static final String _SQL_COUNT_USEDAPPLICATION_WHERE = "SELECT COUNT(usedApplication) FROM UsedApplication usedApplication WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "usedApplication.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UsedApplication exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UsedApplication exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UsedApplicationPersistenceImpl.class);
	private static UsedApplication _nullUsedApplication = new UsedApplicationImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UsedApplication> toCacheModel() {
				return _nullUsedApplicationCacheModel;
			}
		};

	private static CacheModel<UsedApplication> _nullUsedApplicationCacheModel = new CacheModel<UsedApplication>() {
			@Override
			public UsedApplication toEntityModel() {
				return _nullUsedApplication;
			}
		};
}