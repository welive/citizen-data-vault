/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.impl;

import java.util.List;

import it.eng.rspa.cdv.datamodel.model.SkillCdv;
import it.eng.rspa.cdv.datamodel.service.base.SkillCdvLocalServiceBaseImpl;

/**
 * The implementation of the skill cdv local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.cdv.datamodel.service.SkillCdvLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author SIRCHIA ANTONINO
 * @see it.eng.rspa.cdv.datamodel.service.base.SkillCdvLocalServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.SkillCdvLocalServiceUtil
 */
public class SkillCdvLocalServiceImpl extends SkillCdvLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.cdv.datamodel.service.SkillCdvLocalServiceUtil} to access the skill cdv local service.
	 */
	
	public SkillCdv getSkillByName(String skillname){
		
		try{
			List<SkillCdv> prevSkill = skillCdvPersistence.findByskillname(skillname);
			return prevSkill.get(0);
		}
		catch(Exception e){
			return null;
		}
		
	}
}