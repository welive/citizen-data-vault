/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.cdv.datamodel.model.UserPreference;
import it.eng.rspa.cdv.datamodel.model.UserPreferenceModel;
import it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the UserPreference service. Represents a row in the &quot;cdv_UserPreference&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.eng.rspa.cdv.datamodel.model.UserPreferenceModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link UserPreferenceImpl}.
 * </p>
 *
 * @author Engineering
 * @see UserPreferenceImpl
 * @see it.eng.rspa.cdv.datamodel.model.UserPreference
 * @see it.eng.rspa.cdv.datamodel.model.UserPreferenceModel
 * @generated
 */
public class UserPreferenceModelImpl extends BaseModelImpl<UserPreference>
	implements UserPreferenceModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a user preference model instance should use the {@link it.eng.rspa.cdv.datamodel.model.UserPreference} interface instead.
	 */
	public static final String TABLE_NAME = "cdv_UserPreference";
	public static final Object[][] TABLE_COLUMNS = {
			{ "preferenceId", Types.BIGINT },
			{ "userId", Types.BIGINT }
		};
	public static final String TABLE_SQL_CREATE = "create table cdv_UserPreference (preferenceId LONG not null,userId LONG not null,primary key (preferenceId, userId))";
	public static final String TABLE_SQL_DROP = "drop table cdv_UserPreference";
	public static final String ORDER_BY_JPQL = " ORDER BY userPreference.id.preferenceId ASC, userPreference.id.userId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY cdv_UserPreference.preferenceId ASC, cdv_UserPreference.userId ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.it.eng.rspa.cdv.datamodel.model.UserPreference"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.it.eng.rspa.cdv.datamodel.model.UserPreference"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.it.eng.rspa.cdv.datamodel.model.UserPreference"),
			true);
	public static long PREFERENCEID_COLUMN_BITMASK = 1L;
	public static long USERID_COLUMN_BITMASK = 2L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.it.eng.rspa.cdv.datamodel.model.UserPreference"));

	public UserPreferenceModelImpl() {
	}

	@Override
	public UserPreferencePK getPrimaryKey() {
		return new UserPreferencePK(_preferenceId, _userId);
	}

	@Override
	public void setPrimaryKey(UserPreferencePK primaryKey) {
		setPreferenceId(primaryKey.preferenceId);
		setUserId(primaryKey.userId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UserPreferencePK(_preferenceId, _userId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UserPreferencePK)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return UserPreference.class;
	}

	@Override
	public String getModelClassName() {
		return UserPreference.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("preferenceId", getPreferenceId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long preferenceId = (Long)attributes.get("preferenceId");

		if (preferenceId != null) {
			setPreferenceId(preferenceId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	@Override
	public long getPreferenceId() {
		return _preferenceId;
	}

	@Override
	public void setPreferenceId(long preferenceId) {
		_columnBitmask |= PREFERENCEID_COLUMN_BITMASK;

		if (!_setOriginalPreferenceId) {
			_setOriginalPreferenceId = true;

			_originalPreferenceId = _preferenceId;
		}

		_preferenceId = preferenceId;
	}

	public long getOriginalPreferenceId() {
		return _originalPreferenceId;
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_columnBitmask |= USERID_COLUMN_BITMASK;

		if (!_setOriginalUserId) {
			_setOriginalUserId = true;

			_originalUserId = _userId;
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getOriginalUserId() {
		return _originalUserId;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public UserPreference toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (UserPreference)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		UserPreferenceImpl userPreferenceImpl = new UserPreferenceImpl();

		userPreferenceImpl.setPreferenceId(getPreferenceId());
		userPreferenceImpl.setUserId(getUserId());

		userPreferenceImpl.resetOriginalValues();

		return userPreferenceImpl;
	}

	@Override
	public int compareTo(UserPreference userPreference) {
		UserPreferencePK primaryKey = userPreference.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserPreference)) {
			return false;
		}

		UserPreference userPreference = (UserPreference)obj;

		UserPreferencePK primaryKey = userPreference.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public void resetOriginalValues() {
		UserPreferenceModelImpl userPreferenceModelImpl = this;

		userPreferenceModelImpl._originalPreferenceId = userPreferenceModelImpl._preferenceId;

		userPreferenceModelImpl._setOriginalPreferenceId = false;

		userPreferenceModelImpl._originalUserId = userPreferenceModelImpl._userId;

		userPreferenceModelImpl._setOriginalUserId = false;

		userPreferenceModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<UserPreference> toCacheModel() {
		UserPreferenceCacheModel userPreferenceCacheModel = new UserPreferenceCacheModel();

		userPreferenceCacheModel.preferenceId = getPreferenceId();

		userPreferenceCacheModel.userId = getUserId();

		return userPreferenceCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{preferenceId=");
		sb.append(getPreferenceId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.UserPreference");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>preferenceId</column-name><column-value><![CDATA[");
		sb.append(getPreferenceId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = UserPreference.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			UserPreference.class
		};
	private long _preferenceId;
	private long _originalPreferenceId;
	private boolean _setOriginalPreferenceId;
	private long _userId;
	private String _userUuid;
	private long _originalUserId;
	private boolean _setOriginalUserId;
	private long _columnBitmask;
	private UserPreference _escapedModel;
}