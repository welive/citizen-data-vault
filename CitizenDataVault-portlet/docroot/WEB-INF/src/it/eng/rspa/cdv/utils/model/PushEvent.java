package it.eng.rspa.cdv.utils.model;

public enum PushEvent {

	IdeaCreated("IdeaCreated"),
	IdeaDeleted("IdeaDeleted"),
	NewCollaboration("NewCollaboration"),
	CollaborationRemoved("CollaborationRemoved"),
	SkillsUpdated("SkillsUpdated"),
	SkillsRemoved("SkillsRemoved"),
	NewAppInstalled("NewAppInstalled"),
	NewKnownLocation("NewKnownLocation"),
	ReputationUpdated("ReputationUpdated");
	
	private String text;
	
	private PushEvent(String s){
		this.text = s;
	}
	
	public String toString(){
		return this.text;
	}
	
}
