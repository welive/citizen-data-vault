/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.impl;

import java.util.List;

import it.eng.rspa.cdv.datamodel.model.Preference;
import it.eng.rspa.cdv.datamodel.service.base.PreferenceLocalServiceBaseImpl;

/**
 * The implementation of the preference local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.cdv.datamodel.service.PreferenceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author SIRCHIA ANTONINO
 * @see it.eng.rspa.cdv.datamodel.service.base.PreferenceLocalServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.PreferenceLocalServiceUtil
 */
public class PreferenceLocalServiceImpl extends PreferenceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.cdv.datamodel.service.PreferenceLocalServiceUtil} to access the preference local service.
	 */
	
	public Preference getPreferenceByName(String prefname){
		
		try{
			List<Preference> prevLang = preferencePersistence.findByname(prefname);
			return prevLang.get(0);
		}
		catch(Exception e){
			return null;
		}
		
	}
}