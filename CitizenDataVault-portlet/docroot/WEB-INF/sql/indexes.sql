create index IX_96F3A6AD on cdv_CustomDataEntry (clientid);
create index IX_BE6EB2C1 on cdv_CustomDataEntry (clientid, key_);
create index IX_D3E24667 on cdv_CustomDataEntry (key_);
create index IX_36D5CFED on cdv_CustomDataEntry (userid);

create index IX_B77CB0C2 on cdv_CustomDataTag (entryId);
create index IX_25652C8A on cdv_CustomDataTag (tagid);

create index IX_B92A40F2 on cdv_Endorsement (skillId);
create index IX_5D58872C on cdv_Endorsement (skillId, userId);
create index IX_8F252004 on cdv_Endorsement (userId);

create index IX_D12BD5E4 on cdv_IdeaCdv (need);

create index IX_34A39947 on cdv_Language (name);

create index IX_7C777D4A on cdv_Preference (name);

create index IX_DA390F38 on cdv_SkillCdv (skillname);

create index IX_CA98379F on cdv_Tag (tagname);

create index IX_6E17513D on cdv_UsedApplication (appId);
create index IX_8E13E9 on cdv_UsedApplication (userId);

create index IX_B94A51DA on cdv_UserCdv (ccuid);
create index IX_330D8686 on cdv_UserCdv (email);
create index IX_72BC8AA2 on cdv_UserCdv (pilot);
create index IX_F15EA440 on cdv_UserCdv (userId);
create index IX_535F28D0 on cdv_UserCdv (username);

create index IX_3D72E678 on cdv_UserIdea (ideaId);
create index IX_F14CB362 on cdv_UserIdea (ideaId, role);
create index IX_FD20547C on cdv_UserIdea (role);
create index IX_366E094C on cdv_UserIdea (userId);
create index IX_648F6736 on cdv_UserIdea (userId, role);

create index IX_6B2F85ED on cdv_UserLanguage (userId);

create index IX_C8172580 on cdv_UserPreference (preferenceId);
create index IX_BCE99670 on cdv_UserPreference (userId);

create index IX_55C4DF5A on cdv_UserSkill (skillId);
create index IX_EF09149C on cdv_UserSkill (userId);

create index IX_D4A88DD7 on cdv_UserThirdProfile (userId);