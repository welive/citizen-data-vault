create table cdv_Application (
	appId LONG not null primary key,
	appName VARCHAR(75) null
);

create table cdv_CustomDataEntry (
	entryId LONG not null primary key,
	key_ VARCHAR(75) null,
	value VARCHAR(75) null,
	type_ VARCHAR(75) null,
	userid LONG,
	clientid VARCHAR(75) null
);

create table cdv_CustomDataTag (
	entryId LONG not null,
	tagid LONG not null,
	primary key (entryId, tagid)
);

create table cdv_Endorsement (
	skillId LONG not null,
	userId LONG not null,
	endorserId LONG not null,
	date_ DATE null,
	primary key (skillId, userId, endorserId)
);

create table cdv_IdeaCdv (
	ideaId LONG not null primary key,
	ideaName VARCHAR(75) null,
	need BOOLEAN
);

create table cdv_Language (
	languageId LONG not null primary key,
	name VARCHAR(75) null
);

create table cdv_Preference (
	preferenceId LONG not null primary key,
	name VARCHAR(75) null
);

create table cdv_SkillCdv (
	skillId LONG not null primary key,
	skillname VARCHAR(75) null
);

create table cdv_Tag (
	tagid LONG not null primary key,
	tagname VARCHAR(75) null
);

create table cdv_ThirdPartiesProfile (
	thirdPartyId LONG not null,
	key_ VARCHAR(75) not null,
	value VARCHAR(75) null,
	primary key (thirdPartyId, key_)
);

create table cdv_ThirdParty (
	thirdPartyId LONG not null primary key,
	thirdPartyName VARCHAR(75) null
);

create table cdv_UsedApplication (
	appId LONG not null,
	userId LONG not null,
	primary key (appId, userId)
);

create table cdv_UserCdv (
	ccuid LONG not null,
	userId LONG not null,
	companyId LONG,
	username VARCHAR(75) null,
	password_ VARCHAR(75) null,
	name VARCHAR(75) null,
	surname VARCHAR(75) null,
	gender VARCHAR(75) null,
	birthdate DATE null,
	address VARCHAR(75) null,
	city VARCHAR(75) null,
	country VARCHAR(75) null,
	zipcode VARCHAR(75) null,
	email VARCHAR(75) null,
	isDeveloper BOOLEAN,
	lastKnownLatitude DOUBLE,
	lastKnownLongitude DOUBLE,
	pilot VARCHAR(75) null,
	reputation LONG,
	employement VARCHAR(75) null,
	primary key (ccuid, userId)
);

create table cdv_UserIdea (
	ideaId LONG not null,
	userId LONG not null,
	role VARCHAR(75) not null,
	primary key (ideaId, userId, role)
);

create table cdv_UserLanguage (
	languageId LONG not null,
	userId LONG not null,
	primary key (languageId, userId)
);

create table cdv_UserPreference (
	preferenceId LONG not null,
	userId LONG not null,
	primary key (preferenceId, userId)
);

create table cdv_UserSkill (
	skillId LONG not null,
	userId LONG not null,
	date_ DATE null,
	endorsmentCounter LONG,
	primary key (skillId, userId)
);

create table cdv_UserThirdProfile (
	thirdPartyId LONG not null,
	key_ VARCHAR(75) not null,
	userId LONG not null,
	primary key (thirdPartyId, key_, userId)
);