/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.cdv.datamodel.model.SkillCdv;

import java.util.List;

/**
 * The persistence utility for the skill cdv service. This utility wraps {@link SkillCdvPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see SkillCdvPersistence
 * @see SkillCdvPersistenceImpl
 * @generated
 */
public class SkillCdvUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(SkillCdv skillCdv) {
		getPersistence().clearCache(skillCdv);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SkillCdv> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SkillCdv> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SkillCdv> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static SkillCdv update(SkillCdv skillCdv) throws SystemException {
		return getPersistence().update(skillCdv);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static SkillCdv update(SkillCdv skillCdv,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(skillCdv, serviceContext);
	}

	/**
	* Returns all the skill cdvs where skillname = &#63;.
	*
	* @param skillname the skillname
	* @return the matching skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findByskillname(
		java.lang.String skillname)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByskillname(skillname);
	}

	/**
	* Returns a range of all the skill cdvs where skillname = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillname the skillname
	* @param start the lower bound of the range of skill cdvs
	* @param end the upper bound of the range of skill cdvs (not inclusive)
	* @return the range of matching skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findByskillname(
		java.lang.String skillname, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByskillname(skillname, start, end);
	}

	/**
	* Returns an ordered range of all the skill cdvs where skillname = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillname the skillname
	* @param start the lower bound of the range of skill cdvs
	* @param end the upper bound of the range of skill cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findByskillname(
		java.lang.String skillname, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByskillname(skillname, start, end, orderByComparator);
	}

	/**
	* Returns the first skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching skill cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a matching skill cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv findByskillname_First(
		java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException {
		return getPersistence()
				   .findByskillname_First(skillname, orderByComparator);
	}

	/**
	* Returns the first skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching skill cdv, or <code>null</code> if a matching skill cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv fetchByskillname_First(
		java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByskillname_First(skillname, orderByComparator);
	}

	/**
	* Returns the last skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching skill cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a matching skill cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv findByskillname_Last(
		java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException {
		return getPersistence()
				   .findByskillname_Last(skillname, orderByComparator);
	}

	/**
	* Returns the last skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching skill cdv, or <code>null</code> if a matching skill cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv fetchByskillname_Last(
		java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByskillname_Last(skillname, orderByComparator);
	}

	/**
	* Returns the skill cdvs before and after the current skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillId the primary key of the current skill cdv
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next skill cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv[] findByskillname_PrevAndNext(
		long skillId, java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException {
		return getPersistence()
				   .findByskillname_PrevAndNext(skillId, skillname,
			orderByComparator);
	}

	/**
	* Removes all the skill cdvs where skillname = &#63; from the database.
	*
	* @param skillname the skillname
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByskillname(java.lang.String skillname)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByskillname(skillname);
	}

	/**
	* Returns the number of skill cdvs where skillname = &#63;.
	*
	* @param skillname the skillname
	* @return the number of matching skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByskillname(java.lang.String skillname)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByskillname(skillname);
	}

	/**
	* Caches the skill cdv in the entity cache if it is enabled.
	*
	* @param skillCdv the skill cdv
	*/
	public static void cacheResult(
		it.eng.rspa.cdv.datamodel.model.SkillCdv skillCdv) {
		getPersistence().cacheResult(skillCdv);
	}

	/**
	* Caches the skill cdvs in the entity cache if it is enabled.
	*
	* @param skillCdvs the skill cdvs
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> skillCdvs) {
		getPersistence().cacheResult(skillCdvs);
	}

	/**
	* Creates a new skill cdv with the primary key. Does not add the skill cdv to the database.
	*
	* @param skillId the primary key for the new skill cdv
	* @return the new skill cdv
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv create(long skillId) {
		return getPersistence().create(skillId);
	}

	/**
	* Removes the skill cdv with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param skillId the primary key of the skill cdv
	* @return the skill cdv that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv remove(long skillId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException {
		return getPersistence().remove(skillId);
	}

	public static it.eng.rspa.cdv.datamodel.model.SkillCdv updateImpl(
		it.eng.rspa.cdv.datamodel.model.SkillCdv skillCdv)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(skillCdv);
	}

	/**
	* Returns the skill cdv with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException} if it could not be found.
	*
	* @param skillId the primary key of the skill cdv
	* @return the skill cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv findByPrimaryKey(
		long skillId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException {
		return getPersistence().findByPrimaryKey(skillId);
	}

	/**
	* Returns the skill cdv with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param skillId the primary key of the skill cdv
	* @return the skill cdv, or <code>null</code> if a skill cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.SkillCdv fetchByPrimaryKey(
		long skillId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(skillId);
	}

	/**
	* Returns all the skill cdvs.
	*
	* @return the skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the skill cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of skill cdvs
	* @param end the upper bound of the range of skill cdvs (not inclusive)
	* @return the range of skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the skill cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of skill cdvs
	* @param end the upper bound of the range of skill cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the skill cdvs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of skill cdvs.
	*
	* @return the number of skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static SkillCdvPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (SkillCdvPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.cdv.datamodel.service.ClpSerializer.getServletContextName(),
					SkillCdvPersistence.class.getName());

			ReferenceRegistry.registerReference(SkillCdvUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(SkillCdvPersistence persistence) {
	}

	private static SkillCdvPersistence _persistence;
}