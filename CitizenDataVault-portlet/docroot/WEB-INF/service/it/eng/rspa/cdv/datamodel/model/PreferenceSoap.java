/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class PreferenceSoap implements Serializable {
	public static PreferenceSoap toSoapModel(Preference model) {
		PreferenceSoap soapModel = new PreferenceSoap();

		soapModel.setPreferenceId(model.getPreferenceId());
		soapModel.setName(model.getName());

		return soapModel;
	}

	public static PreferenceSoap[] toSoapModels(Preference[] models) {
		PreferenceSoap[] soapModels = new PreferenceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PreferenceSoap[][] toSoapModels(Preference[][] models) {
		PreferenceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PreferenceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PreferenceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PreferenceSoap[] toSoapModels(List<Preference> models) {
		List<PreferenceSoap> soapModels = new ArrayList<PreferenceSoap>(models.size());

		for (Preference model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PreferenceSoap[soapModels.size()]);
	}

	public PreferenceSoap() {
	}

	public long getPrimaryKey() {
		return _preferenceId;
	}

	public void setPrimaryKey(long pk) {
		setPreferenceId(pk);
	}

	public long getPreferenceId() {
		return _preferenceId;
	}

	public void setPreferenceId(long preferenceId) {
		_preferenceId = preferenceId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	private long _preferenceId;
	private String _name;
}