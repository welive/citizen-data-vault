/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.cdv.datamodel.model.CustomDataEntry;

import java.util.List;

/**
 * The persistence utility for the custom data entry service. This utility wraps {@link CustomDataEntryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see CustomDataEntryPersistence
 * @see CustomDataEntryPersistenceImpl
 * @generated
 */
public class CustomDataEntryUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CustomDataEntry customDataEntry) {
		getPersistence().clearCache(customDataEntry);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CustomDataEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CustomDataEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CustomDataEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CustomDataEntry update(CustomDataEntry customDataEntry)
		throws SystemException {
		return getPersistence().update(customDataEntry);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CustomDataEntry update(CustomDataEntry customDataEntry,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(customDataEntry, serviceContext);
	}

	/**
	* Returns all the custom data entries where key = &#63;.
	*
	* @param key the key
	* @return the matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findBykey(
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBykey(key);
	}

	/**
	* Returns a range of all the custom data entries where key = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param key the key
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @return the range of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findBykey(
		java.lang.String key, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBykey(key, start, end);
	}

	/**
	* Returns an ordered range of all the custom data entries where key = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param key the key
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findBykey(
		java.lang.String key, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBykey(key, start, end, orderByComparator);
	}

	/**
	* Returns the first custom data entry in the ordered set where key = &#63;.
	*
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findBykey_First(
		java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence().findBykey_First(key, orderByComparator);
	}

	/**
	* Returns the first custom data entry in the ordered set where key = &#63;.
	*
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchBykey_First(
		java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBykey_First(key, orderByComparator);
	}

	/**
	* Returns the last custom data entry in the ordered set where key = &#63;.
	*
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findBykey_Last(
		java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence().findBykey_Last(key, orderByComparator);
	}

	/**
	* Returns the last custom data entry in the ordered set where key = &#63;.
	*
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchBykey_Last(
		java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBykey_Last(key, orderByComparator);
	}

	/**
	* Returns the custom data entries before and after the current custom data entry in the ordered set where key = &#63;.
	*
	* @param entryId the primary key of the current custom data entry
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry[] findBykey_PrevAndNext(
		long entryId, java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence()
				   .findBykey_PrevAndNext(entryId, key, orderByComparator);
	}

	/**
	* Removes all the custom data entries where key = &#63; from the database.
	*
	* @param key the key
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBykey(java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBykey(key);
	}

	/**
	* Returns the number of custom data entries where key = &#63;.
	*
	* @param key the key
	* @return the number of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static int countBykey(java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBykey(key);
	}

	/**
	* Returns all the custom data entries where userid = &#63;.
	*
	* @param userid the userid
	* @return the matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByuserid(
		long userid) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserid(userid);
	}

	/**
	* Returns a range of all the custom data entries where userid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userid the userid
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @return the range of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByuserid(
		long userid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserid(userid, start, end);
	}

	/**
	* Returns an ordered range of all the custom data entries where userid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userid the userid
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByuserid(
		long userid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByuserid(userid, start, end, orderByComparator);
	}

	/**
	* Returns the first custom data entry in the ordered set where userid = &#63;.
	*
	* @param userid the userid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findByuserid_First(
		long userid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence().findByuserid_First(userid, orderByComparator);
	}

	/**
	* Returns the first custom data entry in the ordered set where userid = &#63;.
	*
	* @param userid the userid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchByuserid_First(
		long userid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserid_First(userid, orderByComparator);
	}

	/**
	* Returns the last custom data entry in the ordered set where userid = &#63;.
	*
	* @param userid the userid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findByuserid_Last(
		long userid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence().findByuserid_Last(userid, orderByComparator);
	}

	/**
	* Returns the last custom data entry in the ordered set where userid = &#63;.
	*
	* @param userid the userid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchByuserid_Last(
		long userid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserid_Last(userid, orderByComparator);
	}

	/**
	* Returns the custom data entries before and after the current custom data entry in the ordered set where userid = &#63;.
	*
	* @param entryId the primary key of the current custom data entry
	* @param userid the userid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry[] findByuserid_PrevAndNext(
		long entryId, long userid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence()
				   .findByuserid_PrevAndNext(entryId, userid, orderByComparator);
	}

	/**
	* Removes all the custom data entries where userid = &#63; from the database.
	*
	* @param userid the userid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByuserid(long userid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByuserid(userid);
	}

	/**
	* Returns the number of custom data entries where userid = &#63;.
	*
	* @param userid the userid
	* @return the number of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static int countByuserid(long userid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByuserid(userid);
	}

	/**
	* Returns all the custom data entries where clientid = &#63;.
	*
	* @param clientid the clientid
	* @return the matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByclientid(
		java.lang.String clientid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByclientid(clientid);
	}

	/**
	* Returns a range of all the custom data entries where clientid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param clientid the clientid
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @return the range of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByclientid(
		java.lang.String clientid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByclientid(clientid, start, end);
	}

	/**
	* Returns an ordered range of all the custom data entries where clientid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param clientid the clientid
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByclientid(
		java.lang.String clientid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByclientid(clientid, start, end, orderByComparator);
	}

	/**
	* Returns the first custom data entry in the ordered set where clientid = &#63;.
	*
	* @param clientid the clientid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findByclientid_First(
		java.lang.String clientid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence().findByclientid_First(clientid, orderByComparator);
	}

	/**
	* Returns the first custom data entry in the ordered set where clientid = &#63;.
	*
	* @param clientid the clientid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchByclientid_First(
		java.lang.String clientid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByclientid_First(clientid, orderByComparator);
	}

	/**
	* Returns the last custom data entry in the ordered set where clientid = &#63;.
	*
	* @param clientid the clientid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findByclientid_Last(
		java.lang.String clientid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence().findByclientid_Last(clientid, orderByComparator);
	}

	/**
	* Returns the last custom data entry in the ordered set where clientid = &#63;.
	*
	* @param clientid the clientid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchByclientid_Last(
		java.lang.String clientid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByclientid_Last(clientid, orderByComparator);
	}

	/**
	* Returns the custom data entries before and after the current custom data entry in the ordered set where clientid = &#63;.
	*
	* @param entryId the primary key of the current custom data entry
	* @param clientid the clientid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry[] findByclientid_PrevAndNext(
		long entryId, java.lang.String clientid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence()
				   .findByclientid_PrevAndNext(entryId, clientid,
			orderByComparator);
	}

	/**
	* Removes all the custom data entries where clientid = &#63; from the database.
	*
	* @param clientid the clientid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByclientid(java.lang.String clientid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByclientid(clientid);
	}

	/**
	* Returns the number of custom data entries where clientid = &#63;.
	*
	* @param clientid the clientid
	* @return the number of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static int countByclientid(java.lang.String clientid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByclientid(clientid);
	}

	/**
	* Returns all the custom data entries where clientid = &#63; and key = &#63;.
	*
	* @param clientid the clientid
	* @param key the key
	* @return the matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByclientKey(
		java.lang.String clientid, java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByclientKey(clientid, key);
	}

	/**
	* Returns a range of all the custom data entries where clientid = &#63; and key = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param clientid the clientid
	* @param key the key
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @return the range of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByclientKey(
		java.lang.String clientid, java.lang.String key, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByclientKey(clientid, key, start, end);
	}

	/**
	* Returns an ordered range of all the custom data entries where clientid = &#63; and key = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param clientid the clientid
	* @param key the key
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findByclientKey(
		java.lang.String clientid, java.lang.String key, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByclientKey(clientid, key, start, end, orderByComparator);
	}

	/**
	* Returns the first custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	*
	* @param clientid the clientid
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findByclientKey_First(
		java.lang.String clientid, java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence()
				   .findByclientKey_First(clientid, key, orderByComparator);
	}

	/**
	* Returns the first custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	*
	* @param clientid the clientid
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchByclientKey_First(
		java.lang.String clientid, java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByclientKey_First(clientid, key, orderByComparator);
	}

	/**
	* Returns the last custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	*
	* @param clientid the clientid
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findByclientKey_Last(
		java.lang.String clientid, java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence()
				   .findByclientKey_Last(clientid, key, orderByComparator);
	}

	/**
	* Returns the last custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	*
	* @param clientid the clientid
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data entry, or <code>null</code> if a matching custom data entry could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchByclientKey_Last(
		java.lang.String clientid, java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByclientKey_Last(clientid, key, orderByComparator);
	}

	/**
	* Returns the custom data entries before and after the current custom data entry in the ordered set where clientid = &#63; and key = &#63;.
	*
	* @param entryId the primary key of the current custom data entry
	* @param clientid the clientid
	* @param key the key
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry[] findByclientKey_PrevAndNext(
		long entryId, java.lang.String clientid, java.lang.String key,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence()
				   .findByclientKey_PrevAndNext(entryId, clientid, key,
			orderByComparator);
	}

	/**
	* Removes all the custom data entries where clientid = &#63; and key = &#63; from the database.
	*
	* @param clientid the clientid
	* @param key the key
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByclientKey(java.lang.String clientid,
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByclientKey(clientid, key);
	}

	/**
	* Returns the number of custom data entries where clientid = &#63; and key = &#63;.
	*
	* @param clientid the clientid
	* @param key the key
	* @return the number of matching custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static int countByclientKey(java.lang.String clientid,
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByclientKey(clientid, key);
	}

	/**
	* Caches the custom data entry in the entity cache if it is enabled.
	*
	* @param customDataEntry the custom data entry
	*/
	public static void cacheResult(
		it.eng.rspa.cdv.datamodel.model.CustomDataEntry customDataEntry) {
		getPersistence().cacheResult(customDataEntry);
	}

	/**
	* Caches the custom data entries in the entity cache if it is enabled.
	*
	* @param customDataEntries the custom data entries
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> customDataEntries) {
		getPersistence().cacheResult(customDataEntries);
	}

	/**
	* Creates a new custom data entry with the primary key. Does not add the custom data entry to the database.
	*
	* @param entryId the primary key for the new custom data entry
	* @return the new custom data entry
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry create(
		long entryId) {
		return getPersistence().create(entryId);
	}

	/**
	* Removes the custom data entry with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param entryId the primary key of the custom data entry
	* @return the custom data entry that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry remove(
		long entryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence().remove(entryId);
	}

	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry updateImpl(
		it.eng.rspa.cdv.datamodel.model.CustomDataEntry customDataEntry)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(customDataEntry);
	}

	/**
	* Returns the custom data entry with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException} if it could not be found.
	*
	* @param entryId the primary key of the custom data entry
	* @return the custom data entry
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry findByPrimaryKey(
		long entryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException {
		return getPersistence().findByPrimaryKey(entryId);
	}

	/**
	* Returns the custom data entry with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param entryId the primary key of the custom data entry
	* @return the custom data entry, or <code>null</code> if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchByPrimaryKey(
		long entryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(entryId);
	}

	/**
	* Returns all the custom data entries.
	*
	* @return the custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the custom data entries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @return the range of custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the custom data entries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the custom data entries from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of custom data entries.
	*
	* @return the number of custom data entries
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CustomDataEntryPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CustomDataEntryPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.cdv.datamodel.service.ClpSerializer.getServletContextName(),
					CustomDataEntryPersistence.class.getName());

			ReferenceRegistry.registerReference(CustomDataEntryUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CustomDataEntryPersistence persistence) {
	}

	private static CustomDataEntryPersistence _persistence;
}