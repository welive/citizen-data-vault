/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UserThirdProfile}.
 * </p>
 *
 * @author Engineering
 * @see UserThirdProfile
 * @generated
 */
public class UserThirdProfileWrapper implements UserThirdProfile,
	ModelWrapper<UserThirdProfile> {
	public UserThirdProfileWrapper(UserThirdProfile userThirdProfile) {
		_userThirdProfile = userThirdProfile;
	}

	@Override
	public Class<?> getModelClass() {
		return UserThirdProfile.class;
	}

	@Override
	public String getModelClassName() {
		return UserThirdProfile.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("thirdPartyId", getThirdPartyId());
		attributes.put("key", getKey());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long thirdPartyId = (Long)attributes.get("thirdPartyId");

		if (thirdPartyId != null) {
			setThirdPartyId(thirdPartyId);
		}

		String key = (String)attributes.get("key");

		if (key != null) {
			setKey(key);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	/**
	* Returns the primary key of this user third profile.
	*
	* @return the primary key of this user third profile
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.UserThirdProfilePK getPrimaryKey() {
		return _userThirdProfile.getPrimaryKey();
	}

	/**
	* Sets the primary key of this user third profile.
	*
	* @param primaryKey the primary key of this user third profile
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserThirdProfilePK primaryKey) {
		_userThirdProfile.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the third party ID of this user third profile.
	*
	* @return the third party ID of this user third profile
	*/
	@Override
	public long getThirdPartyId() {
		return _userThirdProfile.getThirdPartyId();
	}

	/**
	* Sets the third party ID of this user third profile.
	*
	* @param thirdPartyId the third party ID of this user third profile
	*/
	@Override
	public void setThirdPartyId(long thirdPartyId) {
		_userThirdProfile.setThirdPartyId(thirdPartyId);
	}

	/**
	* Returns the key of this user third profile.
	*
	* @return the key of this user third profile
	*/
	@Override
	public java.lang.String getKey() {
		return _userThirdProfile.getKey();
	}

	/**
	* Sets the key of this user third profile.
	*
	* @param key the key of this user third profile
	*/
	@Override
	public void setKey(java.lang.String key) {
		_userThirdProfile.setKey(key);
	}

	/**
	* Returns the user ID of this user third profile.
	*
	* @return the user ID of this user third profile
	*/
	@Override
	public long getUserId() {
		return _userThirdProfile.getUserId();
	}

	/**
	* Sets the user ID of this user third profile.
	*
	* @param userId the user ID of this user third profile
	*/
	@Override
	public void setUserId(long userId) {
		_userThirdProfile.setUserId(userId);
	}

	/**
	* Returns the user uuid of this user third profile.
	*
	* @return the user uuid of this user third profile
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfile.getUserUuid();
	}

	/**
	* Sets the user uuid of this user third profile.
	*
	* @param userUuid the user uuid of this user third profile
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userThirdProfile.setUserUuid(userUuid);
	}

	@Override
	public boolean isNew() {
		return _userThirdProfile.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_userThirdProfile.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _userThirdProfile.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userThirdProfile.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _userThirdProfile.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _userThirdProfile.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_userThirdProfile.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _userThirdProfile.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_userThirdProfile.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_userThirdProfile.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_userThirdProfile.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UserThirdProfileWrapper((UserThirdProfile)_userThirdProfile.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.UserThirdProfile userThirdProfile) {
		return _userThirdProfile.compareTo(userThirdProfile);
	}

	@Override
	public int hashCode() {
		return _userThirdProfile.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.UserThirdProfile> toCacheModel() {
		return _userThirdProfile.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile toEscapedModel() {
		return new UserThirdProfileWrapper(_userThirdProfile.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile toUnescapedModel() {
		return new UserThirdProfileWrapper(_userThirdProfile.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _userThirdProfile.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userThirdProfile.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_userThirdProfile.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserThirdProfileWrapper)) {
			return false;
		}

		UserThirdProfileWrapper userThirdProfileWrapper = (UserThirdProfileWrapper)obj;

		if (Validator.equals(_userThirdProfile,
					userThirdProfileWrapper._userThirdProfile)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UserThirdProfile getWrappedUserThirdProfile() {
		return _userThirdProfile;
	}

	@Override
	public UserThirdProfile getWrappedModel() {
		return _userThirdProfile;
	}

	@Override
	public void resetOriginalValues() {
		_userThirdProfile.resetOriginalValues();
	}

	private UserThirdProfile _userThirdProfile;
}