/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class LanguageSoap implements Serializable {
	public static LanguageSoap toSoapModel(Language model) {
		LanguageSoap soapModel = new LanguageSoap();

		soapModel.setLanguageId(model.getLanguageId());
		soapModel.setName(model.getName());

		return soapModel;
	}

	public static LanguageSoap[] toSoapModels(Language[] models) {
		LanguageSoap[] soapModels = new LanguageSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LanguageSoap[][] toSoapModels(Language[][] models) {
		LanguageSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LanguageSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LanguageSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LanguageSoap[] toSoapModels(List<Language> models) {
		List<LanguageSoap> soapModels = new ArrayList<LanguageSoap>(models.size());

		for (Language model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LanguageSoap[soapModels.size()]);
	}

	public LanguageSoap() {
	}

	public long getPrimaryKey() {
		return _languageId;
	}

	public void setPrimaryKey(long pk) {
		setLanguageId(pk);
	}

	public long getLanguageId() {
		return _languageId;
	}

	public void setLanguageId(long languageId) {
		_languageId = languageId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	private long _languageId;
	private String _name;
}