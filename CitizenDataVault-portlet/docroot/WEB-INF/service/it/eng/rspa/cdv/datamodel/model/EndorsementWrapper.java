/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Endorsement}.
 * </p>
 *
 * @author Engineering
 * @see Endorsement
 * @generated
 */
public class EndorsementWrapper implements Endorsement,
	ModelWrapper<Endorsement> {
	public EndorsementWrapper(Endorsement endorsement) {
		_endorsement = endorsement;
	}

	@Override
	public Class<?> getModelClass() {
		return Endorsement.class;
	}

	@Override
	public String getModelClassName() {
		return Endorsement.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("skillId", getSkillId());
		attributes.put("userId", getUserId());
		attributes.put("endorserId", getEndorserId());
		attributes.put("date", getDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long skillId = (Long)attributes.get("skillId");

		if (skillId != null) {
			setSkillId(skillId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long endorserId = (Long)attributes.get("endorserId");

		if (endorserId != null) {
			setEndorserId(endorserId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}
	}

	/**
	* Returns the primary key of this endorsement.
	*
	* @return the primary key of this endorsement
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK getPrimaryKey() {
		return _endorsement.getPrimaryKey();
	}

	/**
	* Sets the primary key of this endorsement.
	*
	* @param primaryKey the primary key of this endorsement
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK primaryKey) {
		_endorsement.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the skill ID of this endorsement.
	*
	* @return the skill ID of this endorsement
	*/
	@Override
	public long getSkillId() {
		return _endorsement.getSkillId();
	}

	/**
	* Sets the skill ID of this endorsement.
	*
	* @param skillId the skill ID of this endorsement
	*/
	@Override
	public void setSkillId(long skillId) {
		_endorsement.setSkillId(skillId);
	}

	/**
	* Returns the user ID of this endorsement.
	*
	* @return the user ID of this endorsement
	*/
	@Override
	public long getUserId() {
		return _endorsement.getUserId();
	}

	/**
	* Sets the user ID of this endorsement.
	*
	* @param userId the user ID of this endorsement
	*/
	@Override
	public void setUserId(long userId) {
		_endorsement.setUserId(userId);
	}

	/**
	* Returns the user uuid of this endorsement.
	*
	* @return the user uuid of this endorsement
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _endorsement.getUserUuid();
	}

	/**
	* Sets the user uuid of this endorsement.
	*
	* @param userUuid the user uuid of this endorsement
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_endorsement.setUserUuid(userUuid);
	}

	/**
	* Returns the endorser ID of this endorsement.
	*
	* @return the endorser ID of this endorsement
	*/
	@Override
	public long getEndorserId() {
		return _endorsement.getEndorserId();
	}

	/**
	* Sets the endorser ID of this endorsement.
	*
	* @param endorserId the endorser ID of this endorsement
	*/
	@Override
	public void setEndorserId(long endorserId) {
		_endorsement.setEndorserId(endorserId);
	}

	/**
	* Returns the date of this endorsement.
	*
	* @return the date of this endorsement
	*/
	@Override
	public java.util.Date getDate() {
		return _endorsement.getDate();
	}

	/**
	* Sets the date of this endorsement.
	*
	* @param date the date of this endorsement
	*/
	@Override
	public void setDate(java.util.Date date) {
		_endorsement.setDate(date);
	}

	@Override
	public boolean isNew() {
		return _endorsement.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_endorsement.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _endorsement.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_endorsement.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _endorsement.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _endorsement.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_endorsement.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _endorsement.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_endorsement.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_endorsement.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_endorsement.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EndorsementWrapper((Endorsement)_endorsement.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.Endorsement endorsement) {
		return _endorsement.compareTo(endorsement);
	}

	@Override
	public int hashCode() {
		return _endorsement.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.Endorsement> toCacheModel() {
		return _endorsement.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Endorsement toEscapedModel() {
		return new EndorsementWrapper(_endorsement.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Endorsement toUnescapedModel() {
		return new EndorsementWrapper(_endorsement.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _endorsement.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _endorsement.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_endorsement.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EndorsementWrapper)) {
			return false;
		}

		EndorsementWrapper endorsementWrapper = (EndorsementWrapper)obj;

		if (Validator.equals(_endorsement, endorsementWrapper._endorsement)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Endorsement getWrappedEndorsement() {
		return _endorsement;
	}

	@Override
	public Endorsement getWrappedModel() {
		return _endorsement;
	}

	@Override
	public void resetOriginalValues() {
		_endorsement.resetOriginalValues();
	}

	private Endorsement _endorsement;
}