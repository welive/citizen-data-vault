/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.UserPreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class UserPreferenceClp extends BaseModelImpl<UserPreference>
	implements UserPreference {
	public UserPreferenceClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UserPreference.class;
	}

	@Override
	public String getModelClassName() {
		return UserPreference.class.getName();
	}

	@Override
	public UserPreferencePK getPrimaryKey() {
		return new UserPreferencePK(_preferenceId, _userId);
	}

	@Override
	public void setPrimaryKey(UserPreferencePK primaryKey) {
		setPreferenceId(primaryKey.preferenceId);
		setUserId(primaryKey.userId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UserPreferencePK(_preferenceId, _userId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UserPreferencePK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("preferenceId", getPreferenceId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long preferenceId = (Long)attributes.get("preferenceId");

		if (preferenceId != null) {
			setPreferenceId(preferenceId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	@Override
	public long getPreferenceId() {
		return _preferenceId;
	}

	@Override
	public void setPreferenceId(long preferenceId) {
		_preferenceId = preferenceId;

		if (_userPreferenceRemoteModel != null) {
			try {
				Class<?> clazz = _userPreferenceRemoteModel.getClass();

				Method method = clazz.getMethod("setPreferenceId", long.class);

				method.invoke(_userPreferenceRemoteModel, preferenceId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_userPreferenceRemoteModel != null) {
			try {
				Class<?> clazz = _userPreferenceRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_userPreferenceRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public BaseModel<?> getUserPreferenceRemoteModel() {
		return _userPreferenceRemoteModel;
	}

	public void setUserPreferenceRemoteModel(
		BaseModel<?> userPreferenceRemoteModel) {
		_userPreferenceRemoteModel = userPreferenceRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _userPreferenceRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_userPreferenceRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UserPreferenceLocalServiceUtil.addUserPreference(this);
		}
		else {
			UserPreferenceLocalServiceUtil.updateUserPreference(this);
		}
	}

	@Override
	public UserPreference toEscapedModel() {
		return (UserPreference)ProxyUtil.newProxyInstance(UserPreference.class.getClassLoader(),
			new Class[] { UserPreference.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UserPreferenceClp clone = new UserPreferenceClp();

		clone.setPreferenceId(getPreferenceId());
		clone.setUserId(getUserId());

		return clone;
	}

	@Override
	public int compareTo(UserPreference userPreference) {
		UserPreferencePK primaryKey = userPreference.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserPreferenceClp)) {
			return false;
		}

		UserPreferenceClp userPreference = (UserPreferenceClp)obj;

		UserPreferencePK primaryKey = userPreference.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{preferenceId=");
		sb.append(getPreferenceId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.UserPreference");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>preferenceId</column-name><column-value><![CDATA[");
		sb.append(getPreferenceId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _preferenceId;
	private long _userId;
	private String _userUuid;
	private BaseModel<?> _userPreferenceRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}