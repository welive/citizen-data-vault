/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UserLanguage}.
 * </p>
 *
 * @author Engineering
 * @see UserLanguage
 * @generated
 */
public class UserLanguageWrapper implements UserLanguage,
	ModelWrapper<UserLanguage> {
	public UserLanguageWrapper(UserLanguage userLanguage) {
		_userLanguage = userLanguage;
	}

	@Override
	public Class<?> getModelClass() {
		return UserLanguage.class;
	}

	@Override
	public String getModelClassName() {
		return UserLanguage.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("languageId", getLanguageId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long languageId = (Long)attributes.get("languageId");

		if (languageId != null) {
			setLanguageId(languageId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	/**
	* Returns the primary key of this user language.
	*
	* @return the primary key of this user language
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.UserLanguagePK getPrimaryKey() {
		return _userLanguage.getPrimaryKey();
	}

	/**
	* Sets the primary key of this user language.
	*
	* @param primaryKey the primary key of this user language
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserLanguagePK primaryKey) {
		_userLanguage.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the language ID of this user language.
	*
	* @return the language ID of this user language
	*/
	@Override
	public long getLanguageId() {
		return _userLanguage.getLanguageId();
	}

	/**
	* Sets the language ID of this user language.
	*
	* @param languageId the language ID of this user language
	*/
	@Override
	public void setLanguageId(long languageId) {
		_userLanguage.setLanguageId(languageId);
	}

	/**
	* Returns the user ID of this user language.
	*
	* @return the user ID of this user language
	*/
	@Override
	public long getUserId() {
		return _userLanguage.getUserId();
	}

	/**
	* Sets the user ID of this user language.
	*
	* @param userId the user ID of this user language
	*/
	@Override
	public void setUserId(long userId) {
		_userLanguage.setUserId(userId);
	}

	/**
	* Returns the user uuid of this user language.
	*
	* @return the user uuid of this user language
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userLanguage.getUserUuid();
	}

	/**
	* Sets the user uuid of this user language.
	*
	* @param userUuid the user uuid of this user language
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userLanguage.setUserUuid(userUuid);
	}

	@Override
	public boolean isNew() {
		return _userLanguage.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_userLanguage.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _userLanguage.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userLanguage.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _userLanguage.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _userLanguage.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_userLanguage.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _userLanguage.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_userLanguage.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_userLanguage.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_userLanguage.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UserLanguageWrapper((UserLanguage)_userLanguage.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.UserLanguage userLanguage) {
		return _userLanguage.compareTo(userLanguage);
	}

	@Override
	public int hashCode() {
		return _userLanguage.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.UserLanguage> toCacheModel() {
		return _userLanguage.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserLanguage toEscapedModel() {
		return new UserLanguageWrapper(_userLanguage.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserLanguage toUnescapedModel() {
		return new UserLanguageWrapper(_userLanguage.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _userLanguage.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userLanguage.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_userLanguage.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserLanguageWrapper)) {
			return false;
		}

		UserLanguageWrapper userLanguageWrapper = (UserLanguageWrapper)obj;

		if (Validator.equals(_userLanguage, userLanguageWrapper._userLanguage)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UserLanguage getWrappedUserLanguage() {
		return _userLanguage;
	}

	@Override
	public UserLanguage getWrappedModel() {
		return _userLanguage;
	}

	@Override
	public void resetOriginalValues() {
		_userLanguage.resetOriginalValues();
	}

	private UserLanguage _userLanguage;
}