/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class ThirdPartiesProfileSoap implements Serializable {
	public static ThirdPartiesProfileSoap toSoapModel(ThirdPartiesProfile model) {
		ThirdPartiesProfileSoap soapModel = new ThirdPartiesProfileSoap();

		soapModel.setThirdPartyId(model.getThirdPartyId());
		soapModel.setKey(model.getKey());
		soapModel.setValue(model.getValue());

		return soapModel;
	}

	public static ThirdPartiesProfileSoap[] toSoapModels(
		ThirdPartiesProfile[] models) {
		ThirdPartiesProfileSoap[] soapModels = new ThirdPartiesProfileSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ThirdPartiesProfileSoap[][] toSoapModels(
		ThirdPartiesProfile[][] models) {
		ThirdPartiesProfileSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ThirdPartiesProfileSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ThirdPartiesProfileSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ThirdPartiesProfileSoap[] toSoapModels(
		List<ThirdPartiesProfile> models) {
		List<ThirdPartiesProfileSoap> soapModels = new ArrayList<ThirdPartiesProfileSoap>(models.size());

		for (ThirdPartiesProfile model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ThirdPartiesProfileSoap[soapModels.size()]);
	}

	public ThirdPartiesProfileSoap() {
	}

	public ThirdPartiesProfilePK getPrimaryKey() {
		return new ThirdPartiesProfilePK(_thirdPartyId, _key);
	}

	public void setPrimaryKey(ThirdPartiesProfilePK pk) {
		setThirdPartyId(pk.thirdPartyId);
		setKey(pk.key);
	}

	public long getThirdPartyId() {
		return _thirdPartyId;
	}

	public void setThirdPartyId(long thirdPartyId) {
		_thirdPartyId = thirdPartyId;
	}

	public String getKey() {
		return _key;
	}

	public void setKey(String key) {
		_key = key;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	private long _thirdPartyId;
	private String _key;
	private String _value;
}