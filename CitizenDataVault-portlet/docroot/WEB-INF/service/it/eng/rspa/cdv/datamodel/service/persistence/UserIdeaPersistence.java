/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.UserIdea;

/**
 * The persistence interface for the user idea service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserIdeaPersistenceImpl
 * @see UserIdeaUtil
 * @generated
 */
public interface UserIdeaPersistence extends BasePersistence<UserIdea> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserIdeaUtil} to access the user idea persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the user ideas where role = &#63;.
	*
	* @param role the role
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByrole(
		java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user ideas where role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByrole(
		java.lang.String role, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user ideas where role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByrole(
		java.lang.String role, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user idea in the ordered set where role = &#63;.
	*
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByrole_First(
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the first user idea in the ordered set where role = &#63;.
	*
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByrole_First(
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user idea in the ordered set where role = &#63;.
	*
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByrole_Last(
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the last user idea in the ordered set where role = &#63;.
	*
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByrole_Last(
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where role = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea[] findByrole_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Removes all the user ideas where role = &#63; from the database.
	*
	* @param role the role
	* @throws SystemException if a system exception occurred
	*/
	public void removeByrole(java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user ideas where role = &#63;.
	*
	* @param role the role
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public int countByrole(java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user ideas where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user ideas where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuser(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user ideas where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuser(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByuser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the first user idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByuser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByuser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the last user idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByuser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where userId = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea[] findByuser_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Removes all the user ideas where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user ideas where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public int countByuser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user ideas where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuserRole(
		long userId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user ideas where userId = &#63; and role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuserRole(
		long userId, java.lang.String role, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user ideas where userId = &#63; and role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuserRole(
		long userId, java.lang.String role, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByuserRole_First(
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the first user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByuserRole_First(
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByuserRole_Last(
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the last user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByuserRole_Last(
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea[] findByuserRole_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Removes all the user ideas where userId = &#63; and role = &#63; from the database.
	*
	* @param userId the user ID
	* @param role the role
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuserRole(long userId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user ideas where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserRole(long userId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user ideas where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user ideas where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user ideas where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the first user idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the last user idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where ideaId = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea[] findByideaId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Removes all the user ideas where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user ideas where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public int countByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user ideas where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaRole(
		long ideaId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user ideas where ideaId = &#63; and role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaRole(
		long ideaId, java.lang.String role, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user ideas where ideaId = &#63; and role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaRole(
		long ideaId, java.lang.String role, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByideaRole_First(
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the first user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByideaRole_First(
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByideaRole_Last(
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the last user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByideaRole_Last(
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea[] findByideaRole_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Removes all the user ideas where ideaId = &#63; and role = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @throws SystemException if a system exception occurred
	*/
	public void removeByideaRole(long ideaId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user ideas where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public int countByideaRole(long ideaId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the user idea in the entity cache if it is enabled.
	*
	* @param userIdea the user idea
	*/
	public void cacheResult(it.eng.rspa.cdv.datamodel.model.UserIdea userIdea);

	/**
	* Caches the user ideas in the entity cache if it is enabled.
	*
	* @param userIdeas the user ideas
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> userIdeas);

	/**
	* Creates a new user idea with the primary key. Does not add the user idea to the database.
	*
	* @param userIdeaPK the primary key for the new user idea
	* @return the new user idea
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea create(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK);

	/**
	* Removes the user idea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userIdeaPK the primary key of the user idea
	* @return the user idea that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea remove(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	public it.eng.rspa.cdv.datamodel.model.UserIdea updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserIdea userIdea)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user idea with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException} if it could not be found.
	*
	* @param userIdeaPK the primary key of the user idea
	* @return the user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException;

	/**
	* Returns the user idea with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userIdeaPK the primary key of the user idea
	* @return the user idea, or <code>null</code> if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserIdea fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user ideas.
	*
	* @return the user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user ideas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user ideas from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user ideas.
	*
	* @return the number of user ideas
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}