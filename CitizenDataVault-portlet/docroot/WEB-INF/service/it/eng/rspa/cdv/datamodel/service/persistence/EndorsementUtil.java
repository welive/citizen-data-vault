/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.cdv.datamodel.model.Endorsement;

import java.util.List;

/**
 * The persistence utility for the endorsement service. This utility wraps {@link EndorsementPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see EndorsementPersistence
 * @see EndorsementPersistenceImpl
 * @generated
 */
public class EndorsementUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Endorsement endorsement) {
		getPersistence().clearCache(endorsement);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Endorsement> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Endorsement> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Endorsement> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Endorsement update(Endorsement endorsement)
		throws SystemException {
		return getPersistence().update(endorsement);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Endorsement update(Endorsement endorsement,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(endorsement, serviceContext);
	}

	/**
	* Returns all the endorsements where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @return the matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByskillId(
		long skillId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByskillId(skillId);
	}

	/**
	* Returns a range of all the endorsements where skillId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillId the skill ID
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @return the range of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByskillId(
		long skillId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByskillId(skillId, start, end);
	}

	/**
	* Returns an ordered range of all the endorsements where skillId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillId the skill ID
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByskillId(
		long skillId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByskillId(skillId, start, end, orderByComparator);
	}

	/**
	* Returns the first endorsement in the ordered set where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement findByskillId_First(
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence().findByskillId_First(skillId, orderByComparator);
	}

	/**
	* Returns the first endorsement in the ordered set where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching endorsement, or <code>null</code> if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement fetchByskillId_First(
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByskillId_First(skillId, orderByComparator);
	}

	/**
	* Returns the last endorsement in the ordered set where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement findByskillId_Last(
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence().findByskillId_Last(skillId, orderByComparator);
	}

	/**
	* Returns the last endorsement in the ordered set where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching endorsement, or <code>null</code> if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement fetchByskillId_Last(
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByskillId_Last(skillId, orderByComparator);
	}

	/**
	* Returns the endorsements before and after the current endorsement in the ordered set where skillId = &#63;.
	*
	* @param endorsementPK the primary key of the current endorsement
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement[] findByskillId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK,
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence()
				   .findByskillId_PrevAndNext(endorsementPK, skillId,
			orderByComparator);
	}

	/**
	* Removes all the endorsements where skillId = &#63; from the database.
	*
	* @param skillId the skill ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByskillId(long skillId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByskillId(skillId);
	}

	/**
	* Returns the number of endorsements where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @return the number of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static int countByskillId(long skillId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByskillId(skillId);
	}

	/**
	* Returns all the endorsements where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId(userId);
	}

	/**
	* Returns a range of all the endorsements where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @return the range of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the endorsements where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByuserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first endorsement in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence().findByuserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first endorsement in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching endorsement, or <code>null</code> if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last endorsement in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence().findByuserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last endorsement in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching endorsement, or <code>null</code> if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the endorsements before and after the current endorsement in the ordered set where userId = &#63;.
	*
	* @param endorsementPK the primary key of the current endorsement
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement[] findByuserId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence()
				   .findByuserId_PrevAndNext(endorsementPK, userId,
			orderByComparator);
	}

	/**
	* Removes all the endorsements where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByuserId(userId);
	}

	/**
	* Returns the number of endorsements where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByuserId(userId);
	}

	/**
	* Returns all the endorsements where skillId = &#63; and userId = &#63;.
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @return the matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByskillIDAndUserID(
		long skillId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByskillIDAndUserID(skillId, userId);
	}

	/**
	* Returns a range of all the endorsements where skillId = &#63; and userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @return the range of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByskillIDAndUserID(
		long skillId, long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByskillIDAndUserID(skillId, userId, start, end);
	}

	/**
	* Returns an ordered range of all the endorsements where skillId = &#63; and userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findByskillIDAndUserID(
		long skillId, long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByskillIDAndUserID(skillId, userId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement findByskillIDAndUserID_First(
		long skillId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence()
				   .findByskillIDAndUserID_First(skillId, userId,
			orderByComparator);
	}

	/**
	* Returns the first endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching endorsement, or <code>null</code> if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement fetchByskillIDAndUserID_First(
		long skillId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByskillIDAndUserID_First(skillId, userId,
			orderByComparator);
	}

	/**
	* Returns the last endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement findByskillIDAndUserID_Last(
		long skillId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence()
				   .findByskillIDAndUserID_Last(skillId, userId,
			orderByComparator);
	}

	/**
	* Returns the last endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching endorsement, or <code>null</code> if a matching endorsement could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement fetchByskillIDAndUserID_Last(
		long skillId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByskillIDAndUserID_Last(skillId, userId,
			orderByComparator);
	}

	/**
	* Returns the endorsements before and after the current endorsement in the ordered set where skillId = &#63; and userId = &#63;.
	*
	* @param endorsementPK the primary key of the current endorsement
	* @param skillId the skill ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement[] findByskillIDAndUserID_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK,
		long skillId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence()
				   .findByskillIDAndUserID_PrevAndNext(endorsementPK, skillId,
			userId, orderByComparator);
	}

	/**
	* Removes all the endorsements where skillId = &#63; and userId = &#63; from the database.
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByskillIDAndUserID(long skillId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByskillIDAndUserID(skillId, userId);
	}

	/**
	* Returns the number of endorsements where skillId = &#63; and userId = &#63;.
	*
	* @param skillId the skill ID
	* @param userId the user ID
	* @return the number of matching endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static int countByskillIDAndUserID(long skillId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByskillIDAndUserID(skillId, userId);
	}

	/**
	* Caches the endorsement in the entity cache if it is enabled.
	*
	* @param endorsement the endorsement
	*/
	public static void cacheResult(
		it.eng.rspa.cdv.datamodel.model.Endorsement endorsement) {
		getPersistence().cacheResult(endorsement);
	}

	/**
	* Caches the endorsements in the entity cache if it is enabled.
	*
	* @param endorsements the endorsements
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> endorsements) {
		getPersistence().cacheResult(endorsements);
	}

	/**
	* Creates a new endorsement with the primary key. Does not add the endorsement to the database.
	*
	* @param endorsementPK the primary key for the new endorsement
	* @return the new endorsement
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement create(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK) {
		return getPersistence().create(endorsementPK);
	}

	/**
	* Removes the endorsement with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param endorsementPK the primary key of the endorsement
	* @return the endorsement that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement remove(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence().remove(endorsementPK);
	}

	public static it.eng.rspa.cdv.datamodel.model.Endorsement updateImpl(
		it.eng.rspa.cdv.datamodel.model.Endorsement endorsement)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(endorsement);
	}

	/**
	* Returns the endorsement with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchEndorsementException} if it could not be found.
	*
	* @param endorsementPK the primary key of the endorsement
	* @return the endorsement
	* @throws it.eng.rspa.cdv.datamodel.NoSuchEndorsementException if a endorsement with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchEndorsementException {
		return getPersistence().findByPrimaryKey(endorsementPK);
	}

	/**
	* Returns the endorsement with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param endorsementPK the primary key of the endorsement
	* @return the endorsement, or <code>null</code> if a endorsement with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(endorsementPK);
	}

	/**
	* Returns all the endorsements.
	*
	* @return the endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the endorsements.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @return the range of endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the endorsements.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the endorsements from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of endorsements.
	*
	* @return the number of endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static EndorsementPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (EndorsementPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.cdv.datamodel.service.ClpSerializer.getServletContextName(),
					EndorsementPersistence.class.getName());

			ReferenceRegistry.registerReference(EndorsementUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(EndorsementPersistence persistence) {
	}

	private static EndorsementPersistence _persistence;
}