/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserThirdProfileLocalService}.
 *
 * @author Engineering
 * @see UserThirdProfileLocalService
 * @generated
 */
public class UserThirdProfileLocalServiceWrapper
	implements UserThirdProfileLocalService,
		ServiceWrapper<UserThirdProfileLocalService> {
	public UserThirdProfileLocalServiceWrapper(
		UserThirdProfileLocalService userThirdProfileLocalService) {
		_userThirdProfileLocalService = userThirdProfileLocalService;
	}

	/**
	* Adds the user third profile to the database. Also notifies the appropriate model listeners.
	*
	* @param userThirdProfile the user third profile
	* @return the user third profile that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile addUserThirdProfile(
		it.eng.rspa.cdv.datamodel.model.UserThirdProfile userThirdProfile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.addUserThirdProfile(userThirdProfile);
	}

	/**
	* Creates a new user third profile with the primary key. Does not add the user third profile to the database.
	*
	* @param userThirdProfilePK the primary key for the new user third profile
	* @return the new user third profile
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile createUserThirdProfile(
		it.eng.rspa.cdv.datamodel.service.persistence.UserThirdProfilePK userThirdProfilePK) {
		return _userThirdProfileLocalService.createUserThirdProfile(userThirdProfilePK);
	}

	/**
	* Deletes the user third profile with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userThirdProfilePK the primary key of the user third profile
	* @return the user third profile that was removed
	* @throws PortalException if a user third profile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile deleteUserThirdProfile(
		it.eng.rspa.cdv.datamodel.service.persistence.UserThirdProfilePK userThirdProfilePK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.deleteUserThirdProfile(userThirdProfilePK);
	}

	/**
	* Deletes the user third profile from the database. Also notifies the appropriate model listeners.
	*
	* @param userThirdProfile the user third profile
	* @return the user third profile that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile deleteUserThirdProfile(
		it.eng.rspa.cdv.datamodel.model.UserThirdProfile userThirdProfile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.deleteUserThirdProfile(userThirdProfile);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userThirdProfileLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile fetchUserThirdProfile(
		it.eng.rspa.cdv.datamodel.service.persistence.UserThirdProfilePK userThirdProfilePK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.fetchUserThirdProfile(userThirdProfilePK);
	}

	/**
	* Returns the user third profile with the primary key.
	*
	* @param userThirdProfilePK the primary key of the user third profile
	* @return the user third profile
	* @throws PortalException if a user third profile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile getUserThirdProfile(
		it.eng.rspa.cdv.datamodel.service.persistence.UserThirdProfilePK userThirdProfilePK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.getUserThirdProfile(userThirdProfilePK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the user third profiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user third profiles
	* @param end the upper bound of the range of user third profiles (not inclusive)
	* @return the range of user third profiles
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserThirdProfile> getUserThirdProfiles(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.getUserThirdProfiles(start, end);
	}

	/**
	* Returns the number of user third profiles.
	*
	* @return the number of user third profiles
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getUserThirdProfilesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.getUserThirdProfilesCount();
	}

	/**
	* Updates the user third profile in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userThirdProfile the user third profile
	* @return the user third profile that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserThirdProfile updateUserThirdProfile(
		it.eng.rspa.cdv.datamodel.model.UserThirdProfile userThirdProfile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userThirdProfileLocalService.updateUserThirdProfile(userThirdProfile);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _userThirdProfileLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_userThirdProfileLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _userThirdProfileLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public UserThirdProfileLocalService getWrappedUserThirdProfileLocalService() {
		return _userThirdProfileLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedUserThirdProfileLocalService(
		UserThirdProfileLocalService userThirdProfileLocalService) {
		_userThirdProfileLocalService = userThirdProfileLocalService;
	}

	@Override
	public UserThirdProfileLocalService getWrappedService() {
		return _userThirdProfileLocalService;
	}

	@Override
	public void setWrappedService(
		UserThirdProfileLocalService userThirdProfileLocalService) {
		_userThirdProfileLocalService = userThirdProfileLocalService;
	}

	private UserThirdProfileLocalService _userThirdProfileLocalService;
}