/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.UserThirdProfileLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.UserThirdProfilePK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class UserThirdProfileClp extends BaseModelImpl<UserThirdProfile>
	implements UserThirdProfile {
	public UserThirdProfileClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UserThirdProfile.class;
	}

	@Override
	public String getModelClassName() {
		return UserThirdProfile.class.getName();
	}

	@Override
	public UserThirdProfilePK getPrimaryKey() {
		return new UserThirdProfilePK(_thirdPartyId, _key, _userId);
	}

	@Override
	public void setPrimaryKey(UserThirdProfilePK primaryKey) {
		setThirdPartyId(primaryKey.thirdPartyId);
		setKey(primaryKey.key);
		setUserId(primaryKey.userId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UserThirdProfilePK(_thirdPartyId, _key, _userId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UserThirdProfilePK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("thirdPartyId", getThirdPartyId());
		attributes.put("key", getKey());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long thirdPartyId = (Long)attributes.get("thirdPartyId");

		if (thirdPartyId != null) {
			setThirdPartyId(thirdPartyId);
		}

		String key = (String)attributes.get("key");

		if (key != null) {
			setKey(key);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	@Override
	public long getThirdPartyId() {
		return _thirdPartyId;
	}

	@Override
	public void setThirdPartyId(long thirdPartyId) {
		_thirdPartyId = thirdPartyId;

		if (_userThirdProfileRemoteModel != null) {
			try {
				Class<?> clazz = _userThirdProfileRemoteModel.getClass();

				Method method = clazz.getMethod("setThirdPartyId", long.class);

				method.invoke(_userThirdProfileRemoteModel, thirdPartyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getKey() {
		return _key;
	}

	@Override
	public void setKey(String key) {
		_key = key;

		if (_userThirdProfileRemoteModel != null) {
			try {
				Class<?> clazz = _userThirdProfileRemoteModel.getClass();

				Method method = clazz.getMethod("setKey", String.class);

				method.invoke(_userThirdProfileRemoteModel, key);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_userThirdProfileRemoteModel != null) {
			try {
				Class<?> clazz = _userThirdProfileRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_userThirdProfileRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public BaseModel<?> getUserThirdProfileRemoteModel() {
		return _userThirdProfileRemoteModel;
	}

	public void setUserThirdProfileRemoteModel(
		BaseModel<?> userThirdProfileRemoteModel) {
		_userThirdProfileRemoteModel = userThirdProfileRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _userThirdProfileRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_userThirdProfileRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UserThirdProfileLocalServiceUtil.addUserThirdProfile(this);
		}
		else {
			UserThirdProfileLocalServiceUtil.updateUserThirdProfile(this);
		}
	}

	@Override
	public UserThirdProfile toEscapedModel() {
		return (UserThirdProfile)ProxyUtil.newProxyInstance(UserThirdProfile.class.getClassLoader(),
			new Class[] { UserThirdProfile.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UserThirdProfileClp clone = new UserThirdProfileClp();

		clone.setThirdPartyId(getThirdPartyId());
		clone.setKey(getKey());
		clone.setUserId(getUserId());

		return clone;
	}

	@Override
	public int compareTo(UserThirdProfile userThirdProfile) {
		UserThirdProfilePK primaryKey = userThirdProfile.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserThirdProfileClp)) {
			return false;
		}

		UserThirdProfileClp userThirdProfile = (UserThirdProfileClp)obj;

		UserThirdProfilePK primaryKey = userThirdProfile.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{thirdPartyId=");
		sb.append(getThirdPartyId());
		sb.append(", key=");
		sb.append(getKey());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.UserThirdProfile");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>thirdPartyId</column-name><column-value><![CDATA[");
		sb.append(getThirdPartyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>key</column-name><column-value><![CDATA[");
		sb.append(getKey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _thirdPartyId;
	private String _key;
	private long _userId;
	private String _userUuid;
	private BaseModel<?> _userThirdProfileRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}