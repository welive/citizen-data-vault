/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Endorsement. This utility wraps
 * {@link it.eng.rspa.cdv.datamodel.service.impl.EndorsementLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering
 * @see EndorsementLocalService
 * @see it.eng.rspa.cdv.datamodel.service.base.EndorsementLocalServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.impl.EndorsementLocalServiceImpl
 * @generated
 */
public class EndorsementLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.cdv.datamodel.service.impl.EndorsementLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the endorsement to the database. Also notifies the appropriate model listeners.
	*
	* @param endorsement the endorsement
	* @return the endorsement that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement addEndorsement(
		it.eng.rspa.cdv.datamodel.model.Endorsement endorsement)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addEndorsement(endorsement);
	}

	/**
	* Creates a new endorsement with the primary key. Does not add the endorsement to the database.
	*
	* @param endorsementPK the primary key for the new endorsement
	* @return the new endorsement
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement createEndorsement(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK) {
		return getService().createEndorsement(endorsementPK);
	}

	/**
	* Deletes the endorsement with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param endorsementPK the primary key of the endorsement
	* @return the endorsement that was removed
	* @throws PortalException if a endorsement with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement deleteEndorsement(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteEndorsement(endorsementPK);
	}

	/**
	* Deletes the endorsement from the database. Also notifies the appropriate model listeners.
	*
	* @param endorsement the endorsement
	* @return the endorsement that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement deleteEndorsement(
		it.eng.rspa.cdv.datamodel.model.Endorsement endorsement)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteEndorsement(endorsement);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.cdv.datamodel.model.Endorsement fetchEndorsement(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchEndorsement(endorsementPK);
	}

	/**
	* Returns the endorsement with the primary key.
	*
	* @param endorsementPK the primary key of the endorsement
	* @return the endorsement
	* @throws PortalException if a endorsement with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement getEndorsement(
		it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK endorsementPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getEndorsement(endorsementPK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the endorsements.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.EndorsementModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of endorsements
	* @param end the upper bound of the range of endorsements (not inclusive)
	* @return the range of endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> getEndorsements(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getEndorsements(start, end);
	}

	/**
	* Returns the number of endorsements.
	*
	* @return the number of endorsements
	* @throws SystemException if a system exception occurred
	*/
	public static int getEndorsementsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getEndorsementsCount();
	}

	/**
	* Updates the endorsement in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param endorsement the endorsement
	* @return the endorsement that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Endorsement updateEndorsement(
		it.eng.rspa.cdv.datamodel.model.Endorsement endorsement)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateEndorsement(endorsement);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* @param skillId
	* @param userId
	* @return
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> getEndorsementsBySkillIdAndUserId(
		long skillId, long userId) {
		return getService().getEndorsementsBySkillIdAndUserId(skillId, userId);
	}

	/**
	* @param skillId
	* @param userId
	* @return
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> getEndorsementsBySkillIdAndUserIdNoSelf(
		long skillId, long userId) {
		return getService()
				   .getEndorsementsBySkillIdAndUserIdNoSelf(skillId, userId);
	}

	/**
	* @param userId
	* @return
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Endorsement> getEndorsementsByUserId(
		long userId) {
		return getService().getEndorsementsByUserId(userId);
	}

	/**
	* @param skillId
	* @param userId
	*/
	public static void deleteAllEndorsementsBySkillAndUser(long skillId,
		long userId) {
		getService().deleteAllEndorsementsBySkillAndUser(skillId, userId);
	}

	/**
	* @param skillId
	* @param userId
	* @param endorserId
	*/
	public static void deleteEndorsementByPKs(long skillId, long userId,
		long endorserId) {
		getService().deleteEndorsementByPKs(skillId, userId, endorserId);
	}

	public static void clearService() {
		_service = null;
	}

	public static EndorsementLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					EndorsementLocalService.class.getName());

			if (invokableLocalService instanceof EndorsementLocalService) {
				_service = (EndorsementLocalService)invokableLocalService;
			}
			else {
				_service = new EndorsementLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(EndorsementLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(EndorsementLocalService service) {
	}

	private static EndorsementLocalService _service;
}