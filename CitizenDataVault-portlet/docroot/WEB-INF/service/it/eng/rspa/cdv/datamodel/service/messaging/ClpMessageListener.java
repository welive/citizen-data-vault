/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import it.eng.rspa.cdv.datamodel.service.ApplicationLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.CustomDataEntryLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.CustomDataTagLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.IdeaCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.LanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.PreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.RestApiServiceUtil;
import it.eng.rspa.cdv.datamodel.service.SkillCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.TagLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.ThirdPartiesProfileLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.ThirdPartyLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UsedApplicationLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserIdeaLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserLanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserPreferenceLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserSkillLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserThirdProfileLocalServiceUtil;

/**
 * @author Engineering
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			ApplicationLocalServiceUtil.clearService();

			CustomDataEntryLocalServiceUtil.clearService();

			CustomDataTagLocalServiceUtil.clearService();

			EndorsementLocalServiceUtil.clearService();

			IdeaCdvLocalServiceUtil.clearService();

			LanguageLocalServiceUtil.clearService();

			PreferenceLocalServiceUtil.clearService();

			RestApiServiceUtil.clearService();
			SkillCdvLocalServiceUtil.clearService();

			TagLocalServiceUtil.clearService();

			ThirdPartiesProfileLocalServiceUtil.clearService();

			ThirdPartyLocalServiceUtil.clearService();

			UsedApplicationLocalServiceUtil.clearService();

			UserCdvLocalServiceUtil.clearService();

			UserIdeaLocalServiceUtil.clearService();

			UserLanguageLocalServiceUtil.clearService();

			UserPreferenceLocalServiceUtil.clearService();

			UserSkillLocalServiceUtil.clearService();

			UserThirdProfileLocalServiceUtil.clearService();
		}
	}
}