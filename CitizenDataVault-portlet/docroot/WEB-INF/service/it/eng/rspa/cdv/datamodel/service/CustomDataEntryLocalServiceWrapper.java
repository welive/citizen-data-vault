/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CustomDataEntryLocalService}.
 *
 * @author Engineering
 * @see CustomDataEntryLocalService
 * @generated
 */
public class CustomDataEntryLocalServiceWrapper
	implements CustomDataEntryLocalService,
		ServiceWrapper<CustomDataEntryLocalService> {
	public CustomDataEntryLocalServiceWrapper(
		CustomDataEntryLocalService customDataEntryLocalService) {
		_customDataEntryLocalService = customDataEntryLocalService;
	}

	/**
	* Adds the custom data entry to the database. Also notifies the appropriate model listeners.
	*
	* @param customDataEntry the custom data entry
	* @return the custom data entry that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry addCustomDataEntry(
		it.eng.rspa.cdv.datamodel.model.CustomDataEntry customDataEntry)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.addCustomDataEntry(customDataEntry);
	}

	/**
	* Creates a new custom data entry with the primary key. Does not add the custom data entry to the database.
	*
	* @param entryId the primary key for the new custom data entry
	* @return the new custom data entry
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry createCustomDataEntry(
		long entryId) {
		return _customDataEntryLocalService.createCustomDataEntry(entryId);
	}

	/**
	* Deletes the custom data entry with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param entryId the primary key of the custom data entry
	* @return the custom data entry that was removed
	* @throws PortalException if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry deleteCustomDataEntry(
		long entryId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.deleteCustomDataEntry(entryId);
	}

	/**
	* Deletes the custom data entry from the database. Also notifies the appropriate model listeners.
	*
	* @param customDataEntry the custom data entry
	* @return the custom data entry that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry deleteCustomDataEntry(
		it.eng.rspa.cdv.datamodel.model.CustomDataEntry customDataEntry)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.deleteCustomDataEntry(customDataEntry);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _customDataEntryLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry fetchCustomDataEntry(
		long entryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.fetchCustomDataEntry(entryId);
	}

	/**
	* Returns the custom data entry with the primary key.
	*
	* @param entryId the primary key of the custom data entry
	* @return the custom data entry
	* @throws PortalException if a custom data entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry getCustomDataEntry(
		long entryId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.getCustomDataEntry(entryId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the custom data entries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of custom data entries
	* @param end the upper bound of the range of custom data entries (not inclusive)
	* @return the range of custom data entries
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> getCustomDataEntries(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.getCustomDataEntries(start, end);
	}

	/**
	* Returns the number of custom data entries.
	*
	* @return the number of custom data entries
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCustomDataEntriesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.getCustomDataEntriesCount();
	}

	/**
	* Updates the custom data entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param customDataEntry the custom data entry
	* @return the custom data entry that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry updateCustomDataEntry(
		it.eng.rspa.cdv.datamodel.model.CustomDataEntry customDataEntry)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _customDataEntryLocalService.updateCustomDataEntry(customDataEntry);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _customDataEntryLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_customDataEntryLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _customDataEntryLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CustomDataEntryLocalService getWrappedCustomDataEntryLocalService() {
		return _customDataEntryLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCustomDataEntryLocalService(
		CustomDataEntryLocalService customDataEntryLocalService) {
		_customDataEntryLocalService = customDataEntryLocalService;
	}

	@Override
	public CustomDataEntryLocalService getWrappedService() {
		return _customDataEntryLocalService;
	}

	@Override
	public void setWrappedService(
		CustomDataEntryLocalService customDataEntryLocalService) {
		_customDataEntryLocalService = customDataEntryLocalService;
	}

	private CustomDataEntryLocalService _customDataEntryLocalService;
}