/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.UsedApplication;

/**
 * The persistence interface for the used application service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UsedApplicationPersistenceImpl
 * @see UsedApplicationUtil
 * @generated
 */
public interface UsedApplicationPersistence extends BasePersistence<UsedApplication> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UsedApplicationUtil} to access the used application persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the used applications where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the used applications where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of used applications
	* @param end the upper bound of the range of used applications (not inclusive)
	* @return the range of matching used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the used applications where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of used applications
	* @param end the upper bound of the range of used applications (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first used application in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching used application
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a matching used application could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;

	/**
	* Returns the first used application in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching used application, or <code>null</code> if a matching used application could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last used application in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching used application
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a matching used application could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;

	/**
	* Returns the last used application in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching used application, or <code>null</code> if a matching used application could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the used applications before and after the current used application in the ordered set where userId = &#63;.
	*
	* @param usedApplicationPK the primary key of the current used application
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next used application
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication[] findByuserId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK usedApplicationPK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;

	/**
	* Removes all the used applications where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of used applications where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching used applications
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the used applications where appId = &#63;.
	*
	* @param appId the app ID
	* @return the matching used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findByappId(
		long appId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the used applications where appId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param appId the app ID
	* @param start the lower bound of the range of used applications
	* @param end the upper bound of the range of used applications (not inclusive)
	* @return the range of matching used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findByappId(
		long appId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the used applications where appId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param appId the app ID
	* @param start the lower bound of the range of used applications
	* @param end the upper bound of the range of used applications (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findByappId(
		long appId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first used application in the ordered set where appId = &#63;.
	*
	* @param appId the app ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching used application
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a matching used application could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication findByappId_First(
		long appId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;

	/**
	* Returns the first used application in the ordered set where appId = &#63;.
	*
	* @param appId the app ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching used application, or <code>null</code> if a matching used application could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication fetchByappId_First(
		long appId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last used application in the ordered set where appId = &#63;.
	*
	* @param appId the app ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching used application
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a matching used application could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication findByappId_Last(
		long appId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;

	/**
	* Returns the last used application in the ordered set where appId = &#63;.
	*
	* @param appId the app ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching used application, or <code>null</code> if a matching used application could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication fetchByappId_Last(
		long appId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the used applications before and after the current used application in the ordered set where appId = &#63;.
	*
	* @param usedApplicationPK the primary key of the current used application
	* @param appId the app ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next used application
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication[] findByappId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK usedApplicationPK,
		long appId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;

	/**
	* Removes all the used applications where appId = &#63; from the database.
	*
	* @param appId the app ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByappId(long appId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of used applications where appId = &#63;.
	*
	* @param appId the app ID
	* @return the number of matching used applications
	* @throws SystemException if a system exception occurred
	*/
	public int countByappId(long appId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the used application in the entity cache if it is enabled.
	*
	* @param usedApplication the used application
	*/
	public void cacheResult(
		it.eng.rspa.cdv.datamodel.model.UsedApplication usedApplication);

	/**
	* Caches the used applications in the entity cache if it is enabled.
	*
	* @param usedApplications the used applications
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> usedApplications);

	/**
	* Creates a new used application with the primary key. Does not add the used application to the database.
	*
	* @param usedApplicationPK the primary key for the new used application
	* @return the new used application
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication create(
		it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK usedApplicationPK);

	/**
	* Removes the used application with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param usedApplicationPK the primary key of the used application
	* @return the used application that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication remove(
		it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK usedApplicationPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;

	public it.eng.rspa.cdv.datamodel.model.UsedApplication updateImpl(
		it.eng.rspa.cdv.datamodel.model.UsedApplication usedApplication)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the used application with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException} if it could not be found.
	*
	* @param usedApplicationPK the primary key of the used application
	* @return the used application
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException if a used application with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK usedApplicationPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException;

	/**
	* Returns the used application with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param usedApplicationPK the primary key of the used application
	* @return the used application, or <code>null</code> if a used application with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UsedApplication fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK usedApplicationPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the used applications.
	*
	* @return the used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the used applications.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of used applications
	* @param end the upper bound of the range of used applications (not inclusive)
	* @return the range of used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the used applications.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of used applications
	* @param end the upper bound of the range of used applications (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of used applications
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UsedApplication> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the used applications from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of used applications.
	*
	* @return the number of used applications
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}