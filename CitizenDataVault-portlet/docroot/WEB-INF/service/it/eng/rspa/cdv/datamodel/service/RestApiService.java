/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.security.ac.AccessControlled;
import com.liferay.portal.service.BaseService;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service interface for RestApi. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Engineering
 * @see RestApiServiceUtil
 * @see it.eng.rspa.cdv.datamodel.service.base.RestApiServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.impl.RestApiServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface RestApiService extends BaseService, InvokableService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RestApiServiceUtil} to access the rest api remote service. Add custom service methods to {@link it.eng.rspa.cdv.datamodel.service.impl.RestApiServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier();

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier);

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable;

	/**
	* DataSource Oriented
	*/
	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/adduser")
	public com.liferay.portal.kernel.json.JSONObject addUser(
		java.lang.String body);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/updateuser")
	public com.liferay.portal.kernel.json.JSONObject updateUser(
		java.lang.String body);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/push")
	public com.liferay.portal.kernel.json.JSONObject push(java.lang.String body);

	/**
	* DataConsumer Oriented *
	*/
	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getusersskills")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONArray getUsersSkills();

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getusermetadata")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getUserMetadata(
		java.lang.String ccuserid);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getuserprofile")
	@com.liferay.portal.security.ac.AccessControlled(guestAccessEnabled = true)
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getUserProfile(
		java.lang.String uid);

	/**
	* Statistics Oriented *
	*/
	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getkpi11")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getKPI11(
		java.lang.String pilotid, java.lang.String from, java.lang.String to);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getkpi43")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getKPI43(
		java.lang.String pilotid, java.lang.String from, java.lang.String to);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getkpi111")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getKPI111(
		java.lang.String pilotid);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getkpi112")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getKPI112(
		java.lang.String pilotid);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getkpi113")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getKPI113(
		java.lang.String pilotid, java.lang.String from, java.lang.String to);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getkpi114")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getKPI114(
		java.lang.String pilotid, java.lang.String from, java.lang.String to);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/getkpi115")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getKPI115(
		java.lang.String pilotid, java.lang.String from, java.lang.String to);

	/**
	* Data Oriented *
	*/
	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/userdataupdate")
	@com.liferay.portal.security.ac.AccessControlled(guestAccessEnabled = true)
	public com.liferay.portal.kernel.json.JSONArray userDataUpdate(
		java.lang.String body, java.lang.String uid, java.lang.String client_id);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/userdataget")
	@com.liferay.portal.security.ac.AccessControlled(guestAccessEnabled = true)
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONArray getUserData(
		java.lang.String dataid, java.lang.String uid,
		java.lang.String client_id);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/userdatasearch")
	@com.liferay.portal.security.ac.AccessControlled(guestAccessEnabled = true)
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONArray getUserDataSearchResult(
		java.lang.String tags, java.lang.String uid, java.lang.String client_id);

	@com.liferay.portal.kernel.jsonwebservice.JSONWebService(value = "/cdv/userdatadelete", method = "DELETE")
	@com.liferay.portal.security.ac.AccessControlled(guestAccessEnabled = true)
	public com.liferay.portal.kernel.json.JSONArray deleteUserData(
		java.lang.String dataid, java.lang.String uid,
		java.lang.String client_id);
}