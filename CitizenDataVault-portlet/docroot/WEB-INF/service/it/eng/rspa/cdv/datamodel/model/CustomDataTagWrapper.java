/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CustomDataTag}.
 * </p>
 *
 * @author Engineering
 * @see CustomDataTag
 * @generated
 */
public class CustomDataTagWrapper implements CustomDataTag,
	ModelWrapper<CustomDataTag> {
	public CustomDataTagWrapper(CustomDataTag customDataTag) {
		_customDataTag = customDataTag;
	}

	@Override
	public Class<?> getModelClass() {
		return CustomDataTag.class;
	}

	@Override
	public String getModelClassName() {
		return CustomDataTag.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("entryId", getEntryId());
		attributes.put("tagid", getTagid());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long entryId = (Long)attributes.get("entryId");

		if (entryId != null) {
			setEntryId(entryId);
		}

		Long tagid = (Long)attributes.get("tagid");

		if (tagid != null) {
			setTagid(tagid);
		}
	}

	/**
	* Returns the primary key of this custom data tag.
	*
	* @return the primary key of this custom data tag
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK getPrimaryKey() {
		return _customDataTag.getPrimaryKey();
	}

	/**
	* Sets the primary key of this custom data tag.
	*
	* @param primaryKey the primary key of this custom data tag
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK primaryKey) {
		_customDataTag.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the entry ID of this custom data tag.
	*
	* @return the entry ID of this custom data tag
	*/
	@Override
	public long getEntryId() {
		return _customDataTag.getEntryId();
	}

	/**
	* Sets the entry ID of this custom data tag.
	*
	* @param entryId the entry ID of this custom data tag
	*/
	@Override
	public void setEntryId(long entryId) {
		_customDataTag.setEntryId(entryId);
	}

	/**
	* Returns the tagid of this custom data tag.
	*
	* @return the tagid of this custom data tag
	*/
	@Override
	public long getTagid() {
		return _customDataTag.getTagid();
	}

	/**
	* Sets the tagid of this custom data tag.
	*
	* @param tagid the tagid of this custom data tag
	*/
	@Override
	public void setTagid(long tagid) {
		_customDataTag.setTagid(tagid);
	}

	@Override
	public boolean isNew() {
		return _customDataTag.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_customDataTag.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _customDataTag.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_customDataTag.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _customDataTag.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _customDataTag.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_customDataTag.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _customDataTag.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_customDataTag.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_customDataTag.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_customDataTag.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CustomDataTagWrapper((CustomDataTag)_customDataTag.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.CustomDataTag customDataTag) {
		return _customDataTag.compareTo(customDataTag);
	}

	@Override
	public int hashCode() {
		return _customDataTag.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.CustomDataTag> toCacheModel() {
		return _customDataTag.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag toEscapedModel() {
		return new CustomDataTagWrapper(_customDataTag.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag toUnescapedModel() {
		return new CustomDataTagWrapper(_customDataTag.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _customDataTag.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _customDataTag.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_customDataTag.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CustomDataTagWrapper)) {
			return false;
		}

		CustomDataTagWrapper customDataTagWrapper = (CustomDataTagWrapper)obj;

		if (Validator.equals(_customDataTag, customDataTagWrapper._customDataTag)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CustomDataTag getWrappedCustomDataTag() {
		return _customDataTag;
	}

	@Override
	public CustomDataTag getWrappedModel() {
		return _customDataTag;
	}

	@Override
	public void resetOriginalValues() {
		_customDataTag.resetOriginalValues();
	}

	private CustomDataTag _customDataTag;
}