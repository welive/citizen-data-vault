/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Engineering
 * @generated
 */
public class ThirdPartiesProfilePK implements Comparable<ThirdPartiesProfilePK>,
	Serializable {
	public long thirdPartyId;
	public String key;

	public ThirdPartiesProfilePK() {
	}

	public ThirdPartiesProfilePK(long thirdPartyId, String key) {
		this.thirdPartyId = thirdPartyId;
		this.key = key;
	}

	public long getThirdPartyId() {
		return thirdPartyId;
	}

	public void setThirdPartyId(long thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public int compareTo(ThirdPartiesProfilePK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (thirdPartyId < pk.thirdPartyId) {
			value = -1;
		}
		else if (thirdPartyId > pk.thirdPartyId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = key.compareTo(pk.key);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ThirdPartiesProfilePK)) {
			return false;
		}

		ThirdPartiesProfilePK pk = (ThirdPartiesProfilePK)obj;

		if ((thirdPartyId == pk.thirdPartyId) && (key.equals(pk.key))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(thirdPartyId) + String.valueOf(key)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("thirdPartyId");
		sb.append(StringPool.EQUAL);
		sb.append(thirdPartyId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("key");
		sb.append(StringPool.EQUAL);
		sb.append(key);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}