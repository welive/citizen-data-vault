/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile;

/**
 * The persistence interface for the third parties profile service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see ThirdPartiesProfilePersistenceImpl
 * @see ThirdPartiesProfileUtil
 * @generated
 */
public interface ThirdPartiesProfilePersistence extends BasePersistence<ThirdPartiesProfile> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ThirdPartiesProfileUtil} to access the third parties profile persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the third parties profile in the entity cache if it is enabled.
	*
	* @param thirdPartiesProfile the third parties profile
	*/
	public void cacheResult(
		it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile thirdPartiesProfile);

	/**
	* Caches the third parties profiles in the entity cache if it is enabled.
	*
	* @param thirdPartiesProfiles the third parties profiles
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile> thirdPartiesProfiles);

	/**
	* Creates a new third parties profile with the primary key. Does not add the third parties profile to the database.
	*
	* @param thirdPartiesProfilePK the primary key for the new third parties profile
	* @return the new third parties profile
	*/
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile create(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK thirdPartiesProfilePK);

	/**
	* Removes the third parties profile with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param thirdPartiesProfilePK the primary key of the third parties profile
	* @return the third parties profile that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchThirdPartiesProfileException if a third parties profile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile remove(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK thirdPartiesProfilePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchThirdPartiesProfileException;

	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile updateImpl(
		it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile thirdPartiesProfile)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the third parties profile with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchThirdPartiesProfileException} if it could not be found.
	*
	* @param thirdPartiesProfilePK the primary key of the third parties profile
	* @return the third parties profile
	* @throws it.eng.rspa.cdv.datamodel.NoSuchThirdPartiesProfileException if a third parties profile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK thirdPartiesProfilePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchThirdPartiesProfileException;

	/**
	* Returns the third parties profile with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param thirdPartiesProfilePK the primary key of the third parties profile
	* @return the third parties profile, or <code>null</code> if a third parties profile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK thirdPartiesProfilePK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the third parties profiles.
	*
	* @return the third parties profiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the third parties profiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartiesProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of third parties profiles
	* @param end the upper bound of the range of third parties profiles (not inclusive)
	* @return the range of third parties profiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the third parties profiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartiesProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of third parties profiles
	* @param end the upper bound of the range of third parties profiles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of third parties profiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the third parties profiles from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of third parties profiles.
	*
	* @return the number of third parties profiles
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}