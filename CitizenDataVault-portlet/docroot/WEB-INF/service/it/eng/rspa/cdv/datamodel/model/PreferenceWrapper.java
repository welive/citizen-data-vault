/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Preference}.
 * </p>
 *
 * @author Engineering
 * @see Preference
 * @generated
 */
public class PreferenceWrapper implements Preference, ModelWrapper<Preference> {
	public PreferenceWrapper(Preference preference) {
		_preference = preference;
	}

	@Override
	public Class<?> getModelClass() {
		return Preference.class;
	}

	@Override
	public String getModelClassName() {
		return Preference.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("preferenceId", getPreferenceId());
		attributes.put("name", getName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long preferenceId = (Long)attributes.get("preferenceId");

		if (preferenceId != null) {
			setPreferenceId(preferenceId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}
	}

	/**
	* Returns the primary key of this preference.
	*
	* @return the primary key of this preference
	*/
	@Override
	public long getPrimaryKey() {
		return _preference.getPrimaryKey();
	}

	/**
	* Sets the primary key of this preference.
	*
	* @param primaryKey the primary key of this preference
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_preference.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the preference ID of this preference.
	*
	* @return the preference ID of this preference
	*/
	@Override
	public long getPreferenceId() {
		return _preference.getPreferenceId();
	}

	/**
	* Sets the preference ID of this preference.
	*
	* @param preferenceId the preference ID of this preference
	*/
	@Override
	public void setPreferenceId(long preferenceId) {
		_preference.setPreferenceId(preferenceId);
	}

	/**
	* Returns the name of this preference.
	*
	* @return the name of this preference
	*/
	@Override
	public java.lang.String getName() {
		return _preference.getName();
	}

	/**
	* Sets the name of this preference.
	*
	* @param name the name of this preference
	*/
	@Override
	public void setName(java.lang.String name) {
		_preference.setName(name);
	}

	@Override
	public boolean isNew() {
		return _preference.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_preference.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _preference.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_preference.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _preference.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _preference.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_preference.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _preference.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_preference.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_preference.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_preference.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PreferenceWrapper((Preference)_preference.clone());
	}

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.Preference preference) {
		return _preference.compareTo(preference);
	}

	@Override
	public int hashCode() {
		return _preference.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.Preference> toCacheModel() {
		return _preference.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Preference toEscapedModel() {
		return new PreferenceWrapper(_preference.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Preference toUnescapedModel() {
		return new PreferenceWrapper(_preference.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _preference.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _preference.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_preference.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PreferenceWrapper)) {
			return false;
		}

		PreferenceWrapper preferenceWrapper = (PreferenceWrapper)obj;

		if (Validator.equals(_preference, preferenceWrapper._preference)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Preference getWrappedPreference() {
		return _preference;
	}

	@Override
	public Preference getWrappedModel() {
		return _preference;
	}

	@Override
	public void resetOriginalValues() {
		_preference.resetOriginalValues();
	}

	private Preference _preference;
}