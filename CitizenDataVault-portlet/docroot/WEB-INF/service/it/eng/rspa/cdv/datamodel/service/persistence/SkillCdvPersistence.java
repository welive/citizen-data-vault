/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.SkillCdv;

/**
 * The persistence interface for the skill cdv service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see SkillCdvPersistenceImpl
 * @see SkillCdvUtil
 * @generated
 */
public interface SkillCdvPersistence extends BasePersistence<SkillCdv> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SkillCdvUtil} to access the skill cdv persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the skill cdvs where skillname = &#63;.
	*
	* @param skillname the skillname
	* @return the matching skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findByskillname(
		java.lang.String skillname)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the skill cdvs where skillname = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillname the skillname
	* @param start the lower bound of the range of skill cdvs
	* @param end the upper bound of the range of skill cdvs (not inclusive)
	* @return the range of matching skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findByskillname(
		java.lang.String skillname, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the skill cdvs where skillname = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillname the skillname
	* @param start the lower bound of the range of skill cdvs
	* @param end the upper bound of the range of skill cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findByskillname(
		java.lang.String skillname, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching skill cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a matching skill cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv findByskillname_First(
		java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException;

	/**
	* Returns the first skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching skill cdv, or <code>null</code> if a matching skill cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv fetchByskillname_First(
		java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching skill cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a matching skill cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv findByskillname_Last(
		java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException;

	/**
	* Returns the last skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching skill cdv, or <code>null</code> if a matching skill cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv fetchByskillname_Last(
		java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the skill cdvs before and after the current skill cdv in the ordered set where skillname = &#63;.
	*
	* @param skillId the primary key of the current skill cdv
	* @param skillname the skillname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next skill cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv[] findByskillname_PrevAndNext(
		long skillId, java.lang.String skillname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException;

	/**
	* Removes all the skill cdvs where skillname = &#63; from the database.
	*
	* @param skillname the skillname
	* @throws SystemException if a system exception occurred
	*/
	public void removeByskillname(java.lang.String skillname)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of skill cdvs where skillname = &#63;.
	*
	* @param skillname the skillname
	* @return the number of matching skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countByskillname(java.lang.String skillname)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the skill cdv in the entity cache if it is enabled.
	*
	* @param skillCdv the skill cdv
	*/
	public void cacheResult(it.eng.rspa.cdv.datamodel.model.SkillCdv skillCdv);

	/**
	* Caches the skill cdvs in the entity cache if it is enabled.
	*
	* @param skillCdvs the skill cdvs
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> skillCdvs);

	/**
	* Creates a new skill cdv with the primary key. Does not add the skill cdv to the database.
	*
	* @param skillId the primary key for the new skill cdv
	* @return the new skill cdv
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv create(long skillId);

	/**
	* Removes the skill cdv with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param skillId the primary key of the skill cdv
	* @return the skill cdv that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv remove(long skillId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException;

	public it.eng.rspa.cdv.datamodel.model.SkillCdv updateImpl(
		it.eng.rspa.cdv.datamodel.model.SkillCdv skillCdv)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the skill cdv with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException} if it could not be found.
	*
	* @param skillId the primary key of the skill cdv
	* @return the skill cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException if a skill cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv findByPrimaryKey(
		long skillId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException;

	/**
	* Returns the skill cdv with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param skillId the primary key of the skill cdv
	* @return the skill cdv, or <code>null</code> if a skill cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.SkillCdv fetchByPrimaryKey(
		long skillId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the skill cdvs.
	*
	* @return the skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the skill cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of skill cdvs
	* @param end the upper bound of the range of skill cdvs (not inclusive)
	* @return the range of skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the skill cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.SkillCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of skill cdvs
	* @param end the upper bound of the range of skill cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.SkillCdv> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the skill cdvs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of skill cdvs.
	*
	* @return the number of skill cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}