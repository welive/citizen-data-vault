/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Engineering
 * @generated
 */
public class UserIdeaPK implements Comparable<UserIdeaPK>, Serializable {
	public long ideaId;
	public long userId;
	public String role;

	public UserIdeaPK() {
	}

	public UserIdeaPK(long ideaId, long userId, String role) {
		this.ideaId = ideaId;
		this.userId = userId;
		this.role = role;
	}

	public long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(long ideaId) {
		this.ideaId = ideaId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public int compareTo(UserIdeaPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (ideaId < pk.ideaId) {
			value = -1;
		}
		else if (ideaId > pk.ideaId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (userId < pk.userId) {
			value = -1;
		}
		else if (userId > pk.userId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = role.compareTo(pk.role);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserIdeaPK)) {
			return false;
		}

		UserIdeaPK pk = (UserIdeaPK)obj;

		if ((ideaId == pk.ideaId) && (userId == pk.userId) &&
				(role.equals(pk.role))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(ideaId) + String.valueOf(userId) +
		String.valueOf(role)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("ideaId");
		sb.append(StringPool.EQUAL);
		sb.append(ideaId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("userId");
		sb.append(StringPool.EQUAL);
		sb.append(userId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("role");
		sb.append(StringPool.EQUAL);
		sb.append(role);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}