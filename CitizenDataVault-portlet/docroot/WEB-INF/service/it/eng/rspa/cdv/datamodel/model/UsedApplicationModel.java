/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK;

import java.io.Serializable;

/**
 * The base model interface for the UsedApplication service. Represents a row in the &quot;cdv_UsedApplication&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationImpl}.
 * </p>
 *
 * @author Engineering
 * @see UsedApplication
 * @see it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationImpl
 * @see it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationModelImpl
 * @generated
 */
public interface UsedApplicationModel extends BaseModel<UsedApplication> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a used application model instance should use the {@link UsedApplication} interface instead.
	 */

	/**
	 * Returns the primary key of this used application.
	 *
	 * @return the primary key of this used application
	 */
	public UsedApplicationPK getPrimaryKey();

	/**
	 * Sets the primary key of this used application.
	 *
	 * @param primaryKey the primary key of this used application
	 */
	public void setPrimaryKey(UsedApplicationPK primaryKey);

	/**
	 * Returns the app ID of this used application.
	 *
	 * @return the app ID of this used application
	 */
	public long getAppId();

	/**
	 * Sets the app ID of this used application.
	 *
	 * @param appId the app ID of this used application
	 */
	public void setAppId(long appId);

	/**
	 * Returns the user ID of this used application.
	 *
	 * @return the user ID of this used application
	 */
	public long getUserId();

	/**
	 * Sets the user ID of this used application.
	 *
	 * @param userId the user ID of this used application
	 */
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this used application.
	 *
	 * @return the user uuid of this used application
	 * @throws SystemException if a system exception occurred
	 */
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this used application.
	 *
	 * @param userUuid the user uuid of this used application
	 */
	public void setUserUuid(String userUuid);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.UsedApplication usedApplication);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.eng.rspa.cdv.datamodel.model.UsedApplication> toCacheModel();

	@Override
	public it.eng.rspa.cdv.datamodel.model.UsedApplication toEscapedModel();

	@Override
	public it.eng.rspa.cdv.datamodel.model.UsedApplication toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}