/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class UserSkillSoap implements Serializable {
	public static UserSkillSoap toSoapModel(UserSkill model) {
		UserSkillSoap soapModel = new UserSkillSoap();

		soapModel.setSkillId(model.getSkillId());
		soapModel.setUserId(model.getUserId());
		soapModel.setDate(model.getDate());
		soapModel.setEndorsmentCounter(model.getEndorsmentCounter());

		return soapModel;
	}

	public static UserSkillSoap[] toSoapModels(UserSkill[] models) {
		UserSkillSoap[] soapModels = new UserSkillSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserSkillSoap[][] toSoapModels(UserSkill[][] models) {
		UserSkillSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserSkillSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserSkillSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserSkillSoap[] toSoapModels(List<UserSkill> models) {
		List<UserSkillSoap> soapModels = new ArrayList<UserSkillSoap>(models.size());

		for (UserSkill model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserSkillSoap[soapModels.size()]);
	}

	public UserSkillSoap() {
	}

	public UserSkillPK getPrimaryKey() {
		return new UserSkillPK(_skillId, _userId);
	}

	public void setPrimaryKey(UserSkillPK pk) {
		setSkillId(pk.skillId);
		setUserId(pk.userId);
	}

	public long getSkillId() {
		return _skillId;
	}

	public void setSkillId(long skillId) {
		_skillId = skillId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public long getEndorsmentCounter() {
		return _endorsmentCounter;
	}

	public void setEndorsmentCounter(long endorsmentCounter) {
		_endorsmentCounter = endorsmentCounter;
	}

	private long _skillId;
	private long _userId;
	private Date _date;
	private long _endorsmentCounter;
}