/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ThirdParty}.
 * </p>
 *
 * @author Engineering
 * @see ThirdParty
 * @generated
 */
public class ThirdPartyWrapper implements ThirdParty, ModelWrapper<ThirdParty> {
	public ThirdPartyWrapper(ThirdParty thirdParty) {
		_thirdParty = thirdParty;
	}

	@Override
	public Class<?> getModelClass() {
		return ThirdParty.class;
	}

	@Override
	public String getModelClassName() {
		return ThirdParty.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("thirdPartyId", getThirdPartyId());
		attributes.put("thirdPartyName", getThirdPartyName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long thirdPartyId = (Long)attributes.get("thirdPartyId");

		if (thirdPartyId != null) {
			setThirdPartyId(thirdPartyId);
		}

		String thirdPartyName = (String)attributes.get("thirdPartyName");

		if (thirdPartyName != null) {
			setThirdPartyName(thirdPartyName);
		}
	}

	/**
	* Returns the primary key of this third party.
	*
	* @return the primary key of this third party
	*/
	@Override
	public long getPrimaryKey() {
		return _thirdParty.getPrimaryKey();
	}

	/**
	* Sets the primary key of this third party.
	*
	* @param primaryKey the primary key of this third party
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_thirdParty.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the third party ID of this third party.
	*
	* @return the third party ID of this third party
	*/
	@Override
	public long getThirdPartyId() {
		return _thirdParty.getThirdPartyId();
	}

	/**
	* Sets the third party ID of this third party.
	*
	* @param thirdPartyId the third party ID of this third party
	*/
	@Override
	public void setThirdPartyId(long thirdPartyId) {
		_thirdParty.setThirdPartyId(thirdPartyId);
	}

	/**
	* Returns the third party name of this third party.
	*
	* @return the third party name of this third party
	*/
	@Override
	public java.lang.String getThirdPartyName() {
		return _thirdParty.getThirdPartyName();
	}

	/**
	* Sets the third party name of this third party.
	*
	* @param thirdPartyName the third party name of this third party
	*/
	@Override
	public void setThirdPartyName(java.lang.String thirdPartyName) {
		_thirdParty.setThirdPartyName(thirdPartyName);
	}

	@Override
	public boolean isNew() {
		return _thirdParty.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_thirdParty.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _thirdParty.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_thirdParty.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _thirdParty.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _thirdParty.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_thirdParty.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _thirdParty.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_thirdParty.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_thirdParty.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_thirdParty.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ThirdPartyWrapper((ThirdParty)_thirdParty.clone());
	}

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.ThirdParty thirdParty) {
		return _thirdParty.compareTo(thirdParty);
	}

	@Override
	public int hashCode() {
		return _thirdParty.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.ThirdParty> toCacheModel() {
		return _thirdParty.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdParty toEscapedModel() {
		return new ThirdPartyWrapper(_thirdParty.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdParty toUnescapedModel() {
		return new ThirdPartyWrapper(_thirdParty.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _thirdParty.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _thirdParty.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_thirdParty.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ThirdPartyWrapper)) {
			return false;
		}

		ThirdPartyWrapper thirdPartyWrapper = (ThirdPartyWrapper)obj;

		if (Validator.equals(_thirdParty, thirdPartyWrapper._thirdParty)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ThirdParty getWrappedThirdParty() {
		return _thirdParty;
	}

	@Override
	public ThirdParty getWrappedModel() {
		return _thirdParty;
	}

	@Override
	public void resetOriginalValues() {
		_thirdParty.resetOriginalValues();
	}

	private ThirdParty _thirdParty;
}