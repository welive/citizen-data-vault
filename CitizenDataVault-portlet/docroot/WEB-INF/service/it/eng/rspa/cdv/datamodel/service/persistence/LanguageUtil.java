/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.cdv.datamodel.model.Language;

import java.util.List;

/**
 * The persistence utility for the language service. This utility wraps {@link LanguagePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see LanguagePersistence
 * @see LanguagePersistenceImpl
 * @generated
 */
public class LanguageUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Language language) {
		getPersistence().clearCache(language);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Language> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Language> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Language> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Language update(Language language) throws SystemException {
		return getPersistence().update(language);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Language update(Language language,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(language, serviceContext);
	}

	/**
	* Returns all the languages where name = &#63;.
	*
	* @param name the name
	* @return the matching languages
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Language> findByname(
		java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name);
	}

	/**
	* Returns a range of all the languages where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of languages
	* @param end the upper bound of the range of languages (not inclusive)
	* @return the range of matching languages
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Language> findByname(
		java.lang.String name, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name, start, end);
	}

	/**
	* Returns an ordered range of all the languages where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of languages
	* @param end the upper bound of the range of languages (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching languages
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Language> findByname(
		java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name, start, end, orderByComparator);
	}

	/**
	* Returns the first language in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching language
	* @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a matching language could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language findByname_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchLanguageException {
		return getPersistence().findByname_First(name, orderByComparator);
	}

	/**
	* Returns the first language in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching language, or <code>null</code> if a matching language could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language fetchByname_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByname_First(name, orderByComparator);
	}

	/**
	* Returns the last language in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching language
	* @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a matching language could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language findByname_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchLanguageException {
		return getPersistence().findByname_Last(name, orderByComparator);
	}

	/**
	* Returns the last language in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching language, or <code>null</code> if a matching language could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language fetchByname_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByname_Last(name, orderByComparator);
	}

	/**
	* Returns the languages before and after the current language in the ordered set where name = &#63;.
	*
	* @param languageId the primary key of the current language
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next language
	* @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a language with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language[] findByname_PrevAndNext(
		long languageId, java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchLanguageException {
		return getPersistence()
				   .findByname_PrevAndNext(languageId, name, orderByComparator);
	}

	/**
	* Removes all the languages where name = &#63; from the database.
	*
	* @param name the name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByname(name);
	}

	/**
	* Returns the number of languages where name = &#63;.
	*
	* @param name the name
	* @return the number of matching languages
	* @throws SystemException if a system exception occurred
	*/
	public static int countByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByname(name);
	}

	/**
	* Caches the language in the entity cache if it is enabled.
	*
	* @param language the language
	*/
	public static void cacheResult(
		it.eng.rspa.cdv.datamodel.model.Language language) {
		getPersistence().cacheResult(language);
	}

	/**
	* Caches the languages in the entity cache if it is enabled.
	*
	* @param languages the languages
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.Language> languages) {
		getPersistence().cacheResult(languages);
	}

	/**
	* Creates a new language with the primary key. Does not add the language to the database.
	*
	* @param languageId the primary key for the new language
	* @return the new language
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language create(
		long languageId) {
		return getPersistence().create(languageId);
	}

	/**
	* Removes the language with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param languageId the primary key of the language
	* @return the language that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a language with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language remove(
		long languageId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchLanguageException {
		return getPersistence().remove(languageId);
	}

	public static it.eng.rspa.cdv.datamodel.model.Language updateImpl(
		it.eng.rspa.cdv.datamodel.model.Language language)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(language);
	}

	/**
	* Returns the language with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchLanguageException} if it could not be found.
	*
	* @param languageId the primary key of the language
	* @return the language
	* @throws it.eng.rspa.cdv.datamodel.NoSuchLanguageException if a language with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language findByPrimaryKey(
		long languageId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchLanguageException {
		return getPersistence().findByPrimaryKey(languageId);
	}

	/**
	* Returns the language with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param languageId the primary key of the language
	* @return the language, or <code>null</code> if a language with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.Language fetchByPrimaryKey(
		long languageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(languageId);
	}

	/**
	* Returns all the languages.
	*
	* @return the languages
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Language> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the languages.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of languages
	* @param end the upper bound of the range of languages (not inclusive)
	* @return the range of languages
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Language> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the languages.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.LanguageModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of languages
	* @param end the upper bound of the range of languages (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of languages
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.Language> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the languages from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of languages.
	*
	* @return the number of languages
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LanguagePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LanguagePersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.cdv.datamodel.service.ClpSerializer.getServletContextName(),
					LanguagePersistence.class.getName());

			ReferenceRegistry.registerReference(LanguageUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(LanguagePersistence persistence) {
	}

	private static LanguagePersistence _persistence;
}