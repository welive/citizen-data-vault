/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class SkillCdvSoap implements Serializable {
	public static SkillCdvSoap toSoapModel(SkillCdv model) {
		SkillCdvSoap soapModel = new SkillCdvSoap();

		soapModel.setSkillId(model.getSkillId());
		soapModel.setSkillname(model.getSkillname());

		return soapModel;
	}

	public static SkillCdvSoap[] toSoapModels(SkillCdv[] models) {
		SkillCdvSoap[] soapModels = new SkillCdvSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SkillCdvSoap[][] toSoapModels(SkillCdv[][] models) {
		SkillCdvSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SkillCdvSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SkillCdvSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SkillCdvSoap[] toSoapModels(List<SkillCdv> models) {
		List<SkillCdvSoap> soapModels = new ArrayList<SkillCdvSoap>(models.size());

		for (SkillCdv model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SkillCdvSoap[soapModels.size()]);
	}

	public SkillCdvSoap() {
	}

	public long getPrimaryKey() {
		return _skillId;
	}

	public void setPrimaryKey(long pk) {
		setSkillId(pk);
	}

	public long getSkillId() {
		return _skillId;
	}

	public void setSkillId(long skillId) {
		_skillId = skillId;
	}

	public String getSkillname() {
		return _skillname;
	}

	public void setSkillname(String skillname) {
		_skillname = skillname;
	}

	private long _skillId;
	private String _skillname;
}