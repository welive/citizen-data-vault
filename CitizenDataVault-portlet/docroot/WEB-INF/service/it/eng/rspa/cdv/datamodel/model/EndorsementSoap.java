/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class EndorsementSoap implements Serializable {
	public static EndorsementSoap toSoapModel(Endorsement model) {
		EndorsementSoap soapModel = new EndorsementSoap();

		soapModel.setSkillId(model.getSkillId());
		soapModel.setUserId(model.getUserId());
		soapModel.setEndorserId(model.getEndorserId());
		soapModel.setDate(model.getDate());

		return soapModel;
	}

	public static EndorsementSoap[] toSoapModels(Endorsement[] models) {
		EndorsementSoap[] soapModels = new EndorsementSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EndorsementSoap[][] toSoapModels(Endorsement[][] models) {
		EndorsementSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EndorsementSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EndorsementSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EndorsementSoap[] toSoapModels(List<Endorsement> models) {
		List<EndorsementSoap> soapModels = new ArrayList<EndorsementSoap>(models.size());

		for (Endorsement model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EndorsementSoap[soapModels.size()]);
	}

	public EndorsementSoap() {
	}

	public EndorsementPK getPrimaryKey() {
		return new EndorsementPK(_skillId, _userId, _endorserId);
	}

	public void setPrimaryKey(EndorsementPK pk) {
		setSkillId(pk.skillId);
		setUserId(pk.userId);
		setEndorserId(pk.endorserId);
	}

	public long getSkillId() {
		return _skillId;
	}

	public void setSkillId(long skillId) {
		_skillId = skillId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getEndorserId() {
		return _endorserId;
	}

	public void setEndorserId(long endorserId) {
		_endorserId = endorserId;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	private long _skillId;
	private long _userId;
	private long _endorserId;
	private Date _date;
}