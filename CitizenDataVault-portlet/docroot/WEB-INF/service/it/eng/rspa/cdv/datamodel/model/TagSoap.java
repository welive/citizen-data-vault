/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class TagSoap implements Serializable {
	public static TagSoap toSoapModel(Tag model) {
		TagSoap soapModel = new TagSoap();

		soapModel.setTagid(model.getTagid());
		soapModel.setTagname(model.getTagname());

		return soapModel;
	}

	public static TagSoap[] toSoapModels(Tag[] models) {
		TagSoap[] soapModels = new TagSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TagSoap[][] toSoapModels(Tag[][] models) {
		TagSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TagSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TagSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TagSoap[] toSoapModels(List<Tag> models) {
		List<TagSoap> soapModels = new ArrayList<TagSoap>(models.size());

		for (Tag model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TagSoap[soapModels.size()]);
	}

	public TagSoap() {
	}

	public long getPrimaryKey() {
		return _tagid;
	}

	public void setPrimaryKey(long pk) {
		setTagid(pk);
	}

	public long getTagid() {
		return _tagid;
	}

	public void setTagid(long tagid) {
		_tagid = tagid;
	}

	public String getTagname() {
		return _tagname;
	}

	public void setTagname(String tagname) {
		_tagname = tagname;
	}

	private long _tagid;
	private String _tagname;
}