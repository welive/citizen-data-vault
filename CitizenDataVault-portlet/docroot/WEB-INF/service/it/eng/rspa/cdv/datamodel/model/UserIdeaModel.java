/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK;

import java.io.Serializable;

/**
 * The base model interface for the UserIdea service. Represents a row in the &quot;cdv_UserIdea&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaImpl}.
 * </p>
 *
 * @author Engineering
 * @see UserIdea
 * @see it.eng.rspa.cdv.datamodel.model.impl.UserIdeaImpl
 * @see it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl
 * @generated
 */
public interface UserIdeaModel extends BaseModel<UserIdea> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a user idea model instance should use the {@link UserIdea} interface instead.
	 */

	/**
	 * Returns the primary key of this user idea.
	 *
	 * @return the primary key of this user idea
	 */
	public UserIdeaPK getPrimaryKey();

	/**
	 * Sets the primary key of this user idea.
	 *
	 * @param primaryKey the primary key of this user idea
	 */
	public void setPrimaryKey(UserIdeaPK primaryKey);

	/**
	 * Returns the idea ID of this user idea.
	 *
	 * @return the idea ID of this user idea
	 */
	public long getIdeaId();

	/**
	 * Sets the idea ID of this user idea.
	 *
	 * @param ideaId the idea ID of this user idea
	 */
	public void setIdeaId(long ideaId);

	/**
	 * Returns the user ID of this user idea.
	 *
	 * @return the user ID of this user idea
	 */
	public long getUserId();

	/**
	 * Sets the user ID of this user idea.
	 *
	 * @param userId the user ID of this user idea
	 */
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this user idea.
	 *
	 * @return the user uuid of this user idea
	 * @throws SystemException if a system exception occurred
	 */
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this user idea.
	 *
	 * @param userUuid the user uuid of this user idea
	 */
	public void setUserUuid(String userUuid);

	/**
	 * Returns the role of this user idea.
	 *
	 * @return the role of this user idea
	 */
	@AutoEscape
	public String getRole();

	/**
	 * Sets the role of this user idea.
	 *
	 * @param role the role of this user idea
	 */
	public void setRole(String role);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.UserIdea userIdea);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.eng.rspa.cdv.datamodel.model.UserIdea> toCacheModel();

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserIdea toEscapedModel();

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserIdea toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}