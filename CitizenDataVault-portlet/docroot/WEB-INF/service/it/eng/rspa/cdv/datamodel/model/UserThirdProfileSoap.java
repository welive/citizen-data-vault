/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.UserThirdProfilePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class UserThirdProfileSoap implements Serializable {
	public static UserThirdProfileSoap toSoapModel(UserThirdProfile model) {
		UserThirdProfileSoap soapModel = new UserThirdProfileSoap();

		soapModel.setThirdPartyId(model.getThirdPartyId());
		soapModel.setKey(model.getKey());
		soapModel.setUserId(model.getUserId());

		return soapModel;
	}

	public static UserThirdProfileSoap[] toSoapModels(UserThirdProfile[] models) {
		UserThirdProfileSoap[] soapModels = new UserThirdProfileSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserThirdProfileSoap[][] toSoapModels(
		UserThirdProfile[][] models) {
		UserThirdProfileSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserThirdProfileSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserThirdProfileSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserThirdProfileSoap[] toSoapModels(
		List<UserThirdProfile> models) {
		List<UserThirdProfileSoap> soapModels = new ArrayList<UserThirdProfileSoap>(models.size());

		for (UserThirdProfile model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserThirdProfileSoap[soapModels.size()]);
	}

	public UserThirdProfileSoap() {
	}

	public UserThirdProfilePK getPrimaryKey() {
		return new UserThirdProfilePK(_thirdPartyId, _key, _userId);
	}

	public void setPrimaryKey(UserThirdProfilePK pk) {
		setThirdPartyId(pk.thirdPartyId);
		setKey(pk.key);
		setUserId(pk.userId);
	}

	public long getThirdPartyId() {
		return _thirdPartyId;
	}

	public void setThirdPartyId(long thirdPartyId) {
		_thirdPartyId = thirdPartyId;
	}

	public String getKey() {
		return _key;
	}

	public void setKey(String key) {
		_key = key;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	private long _thirdPartyId;
	private String _key;
	private long _userId;
}