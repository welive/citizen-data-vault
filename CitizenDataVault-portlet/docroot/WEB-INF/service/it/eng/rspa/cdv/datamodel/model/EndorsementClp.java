/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.EndorsementPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class EndorsementClp extends BaseModelImpl<Endorsement>
	implements Endorsement {
	public EndorsementClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Endorsement.class;
	}

	@Override
	public String getModelClassName() {
		return Endorsement.class.getName();
	}

	@Override
	public EndorsementPK getPrimaryKey() {
		return new EndorsementPK(_skillId, _userId, _endorserId);
	}

	@Override
	public void setPrimaryKey(EndorsementPK primaryKey) {
		setSkillId(primaryKey.skillId);
		setUserId(primaryKey.userId);
		setEndorserId(primaryKey.endorserId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new EndorsementPK(_skillId, _userId, _endorserId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((EndorsementPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("skillId", getSkillId());
		attributes.put("userId", getUserId());
		attributes.put("endorserId", getEndorserId());
		attributes.put("date", getDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long skillId = (Long)attributes.get("skillId");

		if (skillId != null) {
			setSkillId(skillId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long endorserId = (Long)attributes.get("endorserId");

		if (endorserId != null) {
			setEndorserId(endorserId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}
	}

	@Override
	public long getSkillId() {
		return _skillId;
	}

	@Override
	public void setSkillId(long skillId) {
		_skillId = skillId;

		if (_endorsementRemoteModel != null) {
			try {
				Class<?> clazz = _endorsementRemoteModel.getClass();

				Method method = clazz.getMethod("setSkillId", long.class);

				method.invoke(_endorsementRemoteModel, skillId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_endorsementRemoteModel != null) {
			try {
				Class<?> clazz = _endorsementRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_endorsementRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public long getEndorserId() {
		return _endorserId;
	}

	@Override
	public void setEndorserId(long endorserId) {
		_endorserId = endorserId;

		if (_endorsementRemoteModel != null) {
			try {
				Class<?> clazz = _endorsementRemoteModel.getClass();

				Method method = clazz.getMethod("setEndorserId", long.class);

				method.invoke(_endorsementRemoteModel, endorserId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;

		if (_endorsementRemoteModel != null) {
			try {
				Class<?> clazz = _endorsementRemoteModel.getClass();

				Method method = clazz.getMethod("setDate", Date.class);

				method.invoke(_endorsementRemoteModel, date);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getEndorsementRemoteModel() {
		return _endorsementRemoteModel;
	}

	public void setEndorsementRemoteModel(BaseModel<?> endorsementRemoteModel) {
		_endorsementRemoteModel = endorsementRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _endorsementRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_endorsementRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			EndorsementLocalServiceUtil.addEndorsement(this);
		}
		else {
			EndorsementLocalServiceUtil.updateEndorsement(this);
		}
	}

	@Override
	public Endorsement toEscapedModel() {
		return (Endorsement)ProxyUtil.newProxyInstance(Endorsement.class.getClassLoader(),
			new Class[] { Endorsement.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		EndorsementClp clone = new EndorsementClp();

		clone.setSkillId(getSkillId());
		clone.setUserId(getUserId());
		clone.setEndorserId(getEndorserId());
		clone.setDate(getDate());

		return clone;
	}

	@Override
	public int compareTo(Endorsement endorsement) {
		EndorsementPK primaryKey = endorsement.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EndorsementClp)) {
			return false;
		}

		EndorsementClp endorsement = (EndorsementClp)obj;

		EndorsementPK primaryKey = endorsement.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{skillId=");
		sb.append(getSkillId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", endorserId=");
		sb.append(getEndorserId());
		sb.append(", date=");
		sb.append(getDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.Endorsement");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>skillId</column-name><column-value><![CDATA[");
		sb.append(getSkillId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>endorserId</column-name><column-value><![CDATA[");
		sb.append(getEndorserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _skillId;
	private long _userId;
	private String _userUuid;
	private long _endorserId;
	private Date _date;
	private BaseModel<?> _endorsementRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}