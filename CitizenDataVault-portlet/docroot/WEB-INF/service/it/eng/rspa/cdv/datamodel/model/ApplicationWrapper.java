/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Application}.
 * </p>
 *
 * @author Engineering
 * @see Application
 * @generated
 */
public class ApplicationWrapper implements Application,
	ModelWrapper<Application> {
	public ApplicationWrapper(Application application) {
		_application = application;
	}

	@Override
	public Class<?> getModelClass() {
		return Application.class;
	}

	@Override
	public String getModelClassName() {
		return Application.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("appId", getAppId());
		attributes.put("appName", getAppName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long appId = (Long)attributes.get("appId");

		if (appId != null) {
			setAppId(appId);
		}

		String appName = (String)attributes.get("appName");

		if (appName != null) {
			setAppName(appName);
		}
	}

	/**
	* Returns the primary key of this application.
	*
	* @return the primary key of this application
	*/
	@Override
	public long getPrimaryKey() {
		return _application.getPrimaryKey();
	}

	/**
	* Sets the primary key of this application.
	*
	* @param primaryKey the primary key of this application
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_application.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the app ID of this application.
	*
	* @return the app ID of this application
	*/
	@Override
	public long getAppId() {
		return _application.getAppId();
	}

	/**
	* Sets the app ID of this application.
	*
	* @param appId the app ID of this application
	*/
	@Override
	public void setAppId(long appId) {
		_application.setAppId(appId);
	}

	/**
	* Returns the app name of this application.
	*
	* @return the app name of this application
	*/
	@Override
	public java.lang.String getAppName() {
		return _application.getAppName();
	}

	/**
	* Sets the app name of this application.
	*
	* @param appName the app name of this application
	*/
	@Override
	public void setAppName(java.lang.String appName) {
		_application.setAppName(appName);
	}

	@Override
	public boolean isNew() {
		return _application.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_application.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _application.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_application.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _application.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _application.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_application.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _application.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_application.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_application.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_application.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ApplicationWrapper((Application)_application.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.Application application) {
		return _application.compareTo(application);
	}

	@Override
	public int hashCode() {
		return _application.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.Application> toCacheModel() {
		return _application.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Application toEscapedModel() {
		return new ApplicationWrapper(_application.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Application toUnescapedModel() {
		return new ApplicationWrapper(_application.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _application.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _application.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_application.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ApplicationWrapper)) {
			return false;
		}

		ApplicationWrapper applicationWrapper = (ApplicationWrapper)obj;

		if (Validator.equals(_application, applicationWrapper._application)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Application getWrappedApplication() {
		return _application;
	}

	@Override
	public Application getWrappedModel() {
		return _application;
	}

	@Override
	public void resetOriginalValues() {
		_application.resetOriginalValues();
	}

	private Application _application;
}