/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class UserCdvClp extends BaseModelImpl<UserCdv> implements UserCdv {
	public UserCdvClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UserCdv.class;
	}

	@Override
	public String getModelClassName() {
		return UserCdv.class.getName();
	}

	@Override
	public UserCdvPK getPrimaryKey() {
		return new UserCdvPK(_ccuid, _userId);
	}

	@Override
	public void setPrimaryKey(UserCdvPK primaryKey) {
		setCcuid(primaryKey.ccuid);
		setUserId(primaryKey.userId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UserCdvPK(_ccuid, _userId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UserCdvPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ccuid", getCcuid());
		attributes.put("userId", getUserId());
		attributes.put("companyId", getCompanyId());
		attributes.put("username", getUsername());
		attributes.put("password", getPassword());
		attributes.put("name", getName());
		attributes.put("surname", getSurname());
		attributes.put("gender", getGender());
		attributes.put("birthdate", getBirthdate());
		attributes.put("address", getAddress());
		attributes.put("city", getCity());
		attributes.put("country", getCountry());
		attributes.put("zipcode", getZipcode());
		attributes.put("email", getEmail());
		attributes.put("isDeveloper", getIsDeveloper());
		attributes.put("lastKnownLatitude", getLastKnownLatitude());
		attributes.put("lastKnownLongitude", getLastKnownLongitude());
		attributes.put("pilot", getPilot());
		attributes.put("reputation", getReputation());
		attributes.put("employement", getEmployement());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ccuid = (Long)attributes.get("ccuid");

		if (ccuid != null) {
			setCcuid(ccuid);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String username = (String)attributes.get("username");

		if (username != null) {
			setUsername(username);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String surname = (String)attributes.get("surname");

		if (surname != null) {
			setSurname(surname);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		Date birthdate = (Date)attributes.get("birthdate");

		if (birthdate != null) {
			setBirthdate(birthdate);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		String city = (String)attributes.get("city");

		if (city != null) {
			setCity(city);
		}

		String country = (String)attributes.get("country");

		if (country != null) {
			setCountry(country);
		}

		String zipcode = (String)attributes.get("zipcode");

		if (zipcode != null) {
			setZipcode(zipcode);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		Boolean isDeveloper = (Boolean)attributes.get("isDeveloper");

		if (isDeveloper != null) {
			setIsDeveloper(isDeveloper);
		}

		Double lastKnownLatitude = (Double)attributes.get("lastKnownLatitude");

		if (lastKnownLatitude != null) {
			setLastKnownLatitude(lastKnownLatitude);
		}

		Double lastKnownLongitude = (Double)attributes.get("lastKnownLongitude");

		if (lastKnownLongitude != null) {
			setLastKnownLongitude(lastKnownLongitude);
		}

		String pilot = (String)attributes.get("pilot");

		if (pilot != null) {
			setPilot(pilot);
		}

		Long reputation = (Long)attributes.get("reputation");

		if (reputation != null) {
			setReputation(reputation);
		}

		String employement = (String)attributes.get("employement");

		if (employement != null) {
			setEmployement(employement);
		}
	}

	@Override
	public long getCcuid() {
		return _ccuid;
	}

	@Override
	public void setCcuid(long ccuid) {
		_ccuid = ccuid;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setCcuid", long.class);

				method.invoke(_userCdvRemoteModel, ccuid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_userCdvRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setCompanyId", long.class);

				method.invoke(_userCdvRemoteModel, companyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUsername() {
		return _username;
	}

	@Override
	public void setUsername(String username) {
		_username = username;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setUsername", String.class);

				method.invoke(_userCdvRemoteModel, username);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPassword() {
		return _password;
	}

	@Override
	public void setPassword(String password) {
		_password = password;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setPassword", String.class);

				method.invoke(_userCdvRemoteModel, password);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public void setName(String name) {
		_name = name;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setName", String.class);

				method.invoke(_userCdvRemoteModel, name);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSurname() {
		return _surname;
	}

	@Override
	public void setSurname(String surname) {
		_surname = surname;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setSurname", String.class);

				method.invoke(_userCdvRemoteModel, surname);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getGender() {
		return _gender;
	}

	@Override
	public void setGender(String gender) {
		_gender = gender;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setGender", String.class);

				method.invoke(_userCdvRemoteModel, gender);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getBirthdate() {
		return _birthdate;
	}

	@Override
	public void setBirthdate(Date birthdate) {
		_birthdate = birthdate;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setBirthdate", Date.class);

				method.invoke(_userCdvRemoteModel, birthdate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAddress() {
		return _address;
	}

	@Override
	public void setAddress(String address) {
		_address = address;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setAddress", String.class);

				method.invoke(_userCdvRemoteModel, address);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCity() {
		return _city;
	}

	@Override
	public void setCity(String city) {
		_city = city;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setCity", String.class);

				method.invoke(_userCdvRemoteModel, city);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCountry() {
		return _country;
	}

	@Override
	public void setCountry(String country) {
		_country = country;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setCountry", String.class);

				method.invoke(_userCdvRemoteModel, country);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getZipcode() {
		return _zipcode;
	}

	@Override
	public void setZipcode(String zipcode) {
		_zipcode = zipcode;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setZipcode", String.class);

				method.invoke(_userCdvRemoteModel, zipcode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEmail() {
		return _email;
	}

	@Override
	public void setEmail(String email) {
		_email = email;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setEmail", String.class);

				method.invoke(_userCdvRemoteModel, email);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getIsDeveloper() {
		return _isDeveloper;
	}

	@Override
	public boolean isIsDeveloper() {
		return _isDeveloper;
	}

	@Override
	public void setIsDeveloper(boolean isDeveloper) {
		_isDeveloper = isDeveloper;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setIsDeveloper", boolean.class);

				method.invoke(_userCdvRemoteModel, isDeveloper);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLastKnownLatitude() {
		return _lastKnownLatitude;
	}

	@Override
	public void setLastKnownLatitude(double lastKnownLatitude) {
		_lastKnownLatitude = lastKnownLatitude;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setLastKnownLatitude",
						double.class);

				method.invoke(_userCdvRemoteModel, lastKnownLatitude);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLastKnownLongitude() {
		return _lastKnownLongitude;
	}

	@Override
	public void setLastKnownLongitude(double lastKnownLongitude) {
		_lastKnownLongitude = lastKnownLongitude;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setLastKnownLongitude",
						double.class);

				method.invoke(_userCdvRemoteModel, lastKnownLongitude);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPilot() {
		return _pilot;
	}

	@Override
	public void setPilot(String pilot) {
		_pilot = pilot;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setPilot", String.class);

				method.invoke(_userCdvRemoteModel, pilot);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getReputation() {
		return _reputation;
	}

	@Override
	public void setReputation(long reputation) {
		_reputation = reputation;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setReputation", long.class);

				method.invoke(_userCdvRemoteModel, reputation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEmployement() {
		return _employement;
	}

	@Override
	public void setEmployement(String employement) {
		_employement = employement;

		if (_userCdvRemoteModel != null) {
			try {
				Class<?> clazz = _userCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setEmployement", String.class);

				method.invoke(_userCdvRemoteModel, employement);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUserCdvRemoteModel() {
		return _userCdvRemoteModel;
	}

	public void setUserCdvRemoteModel(BaseModel<?> userCdvRemoteModel) {
		_userCdvRemoteModel = userCdvRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _userCdvRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_userCdvRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UserCdvLocalServiceUtil.addUserCdv(this);
		}
		else {
			UserCdvLocalServiceUtil.updateUserCdv(this);
		}
	}

	@Override
	public UserCdv toEscapedModel() {
		return (UserCdv)ProxyUtil.newProxyInstance(UserCdv.class.getClassLoader(),
			new Class[] { UserCdv.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UserCdvClp clone = new UserCdvClp();

		clone.setCcuid(getCcuid());
		clone.setUserId(getUserId());
		clone.setCompanyId(getCompanyId());
		clone.setUsername(getUsername());
		clone.setPassword(getPassword());
		clone.setName(getName());
		clone.setSurname(getSurname());
		clone.setGender(getGender());
		clone.setBirthdate(getBirthdate());
		clone.setAddress(getAddress());
		clone.setCity(getCity());
		clone.setCountry(getCountry());
		clone.setZipcode(getZipcode());
		clone.setEmail(getEmail());
		clone.setIsDeveloper(getIsDeveloper());
		clone.setLastKnownLatitude(getLastKnownLatitude());
		clone.setLastKnownLongitude(getLastKnownLongitude());
		clone.setPilot(getPilot());
		clone.setReputation(getReputation());
		clone.setEmployement(getEmployement());

		return clone;
	}

	@Override
	public int compareTo(UserCdv userCdv) {
		UserCdvPK primaryKey = userCdv.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserCdvClp)) {
			return false;
		}

		UserCdvClp userCdv = (UserCdvClp)obj;

		UserCdvPK primaryKey = userCdv.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{ccuid=");
		sb.append(getCcuid());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", username=");
		sb.append(getUsername());
		sb.append(", password=");
		sb.append(getPassword());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", surname=");
		sb.append(getSurname());
		sb.append(", gender=");
		sb.append(getGender());
		sb.append(", birthdate=");
		sb.append(getBirthdate());
		sb.append(", address=");
		sb.append(getAddress());
		sb.append(", city=");
		sb.append(getCity());
		sb.append(", country=");
		sb.append(getCountry());
		sb.append(", zipcode=");
		sb.append(getZipcode());
		sb.append(", email=");
		sb.append(getEmail());
		sb.append(", isDeveloper=");
		sb.append(getIsDeveloper());
		sb.append(", lastKnownLatitude=");
		sb.append(getLastKnownLatitude());
		sb.append(", lastKnownLongitude=");
		sb.append(getLastKnownLongitude());
		sb.append(", pilot=");
		sb.append(getPilot());
		sb.append(", reputation=");
		sb.append(getReputation());
		sb.append(", employement=");
		sb.append(getEmployement());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(64);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.UserCdv");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ccuid</column-name><column-value><![CDATA[");
		sb.append(getCcuid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>username</column-name><column-value><![CDATA[");
		sb.append(getUsername());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>password</column-name><column-value><![CDATA[");
		sb.append(getPassword());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>surname</column-name><column-value><![CDATA[");
		sb.append(getSurname());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gender</column-name><column-value><![CDATA[");
		sb.append(getGender());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>birthdate</column-name><column-value><![CDATA[");
		sb.append(getBirthdate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>address</column-name><column-value><![CDATA[");
		sb.append(getAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>city</column-name><column-value><![CDATA[");
		sb.append(getCity());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>country</column-name><column-value><![CDATA[");
		sb.append(getCountry());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>zipcode</column-name><column-value><![CDATA[");
		sb.append(getZipcode());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>email</column-name><column-value><![CDATA[");
		sb.append(getEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isDeveloper</column-name><column-value><![CDATA[");
		sb.append(getIsDeveloper());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastKnownLatitude</column-name><column-value><![CDATA[");
		sb.append(getLastKnownLatitude());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastKnownLongitude</column-name><column-value><![CDATA[");
		sb.append(getLastKnownLongitude());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pilot</column-name><column-value><![CDATA[");
		sb.append(getPilot());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>reputation</column-name><column-value><![CDATA[");
		sb.append(getReputation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>employement</column-name><column-value><![CDATA[");
		sb.append(getEmployement());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _ccuid;
	private long _userId;
	private String _userUuid;
	private long _companyId;
	private String _username;
	private String _password;
	private String _name;
	private String _surname;
	private String _gender;
	private Date _birthdate;
	private String _address;
	private String _city;
	private String _country;
	private String _zipcode;
	private String _email;
	private boolean _isDeveloper;
	private double _lastKnownLatitude;
	private double _lastKnownLongitude;
	private String _pilot;
	private long _reputation;
	private String _employement;
	private BaseModel<?> _userCdvRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}