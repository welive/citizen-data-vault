/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserCdvLocalService}.
 *
 * @author Engineering
 * @see UserCdvLocalService
 * @generated
 */
public class UserCdvLocalServiceWrapper implements UserCdvLocalService,
	ServiceWrapper<UserCdvLocalService> {
	public UserCdvLocalServiceWrapper(UserCdvLocalService userCdvLocalService) {
		_userCdvLocalService = userCdvLocalService;
	}

	/**
	* Adds the user cdv to the database. Also notifies the appropriate model listeners.
	*
	* @param userCdv the user cdv
	* @return the user cdv that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv addUserCdv(
		it.eng.rspa.cdv.datamodel.model.UserCdv userCdv)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.addUserCdv(userCdv);
	}

	/**
	* Creates a new user cdv with the primary key. Does not add the user cdv to the database.
	*
	* @param userCdvPK the primary key for the new user cdv
	* @return the new user cdv
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv createUserCdv(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK) {
		return _userCdvLocalService.createUserCdv(userCdvPK);
	}

	/**
	* Deletes the user cdv with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userCdvPK the primary key of the user cdv
	* @return the user cdv that was removed
	* @throws PortalException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv deleteUserCdv(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.deleteUserCdv(userCdvPK);
	}

	/**
	* Deletes the user cdv from the database. Also notifies the appropriate model listeners.
	*
	* @param userCdv the user cdv
	* @return the user cdv that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv deleteUserCdv(
		it.eng.rspa.cdv.datamodel.model.UserCdv userCdv)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.deleteUserCdv(userCdv);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userCdvLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchUserCdv(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.fetchUserCdv(userCdvPK);
	}

	/**
	* Returns the user cdv with the primary key.
	*
	* @param userCdvPK the primary key of the user cdv
	* @return the user cdv
	* @throws PortalException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv getUserCdv(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.getUserCdv(userCdvPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the user cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @return the range of user cdvs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> getUserCdvs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.getUserCdvs(start, end);
	}

	/**
	* Returns the number of user cdvs.
	*
	* @return the number of user cdvs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getUserCdvsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.getUserCdvsCount();
	}

	/**
	* Updates the user cdv in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userCdv the user cdv
	* @return the user cdv that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv updateUserCdv(
		it.eng.rspa.cdv.datamodel.model.UserCdv userCdv)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdvLocalService.updateUserCdv(userCdv);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _userCdvLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_userCdvLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _userCdvLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> getUserCdvByPilot(
		java.lang.String pilot) {
		return _userCdvLocalService.getUserCdvByPilot(pilot);
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv getUserCdvByPK(
		long ccuserid, long userid) {
		return _userCdvLocalService.getUserCdvByPK(ccuserid, userid);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject forget(long liferayUserID) {
		return _userCdvLocalService.forget(liferayUserID);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public UserCdvLocalService getWrappedUserCdvLocalService() {
		return _userCdvLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedUserCdvLocalService(
		UserCdvLocalService userCdvLocalService) {
		_userCdvLocalService = userCdvLocalService;
	}

	@Override
	public UserCdvLocalService getWrappedService() {
		return _userCdvLocalService;
	}

	@Override
	public void setWrappedService(UserCdvLocalService userCdvLocalService) {
		_userCdvLocalService = userCdvLocalService;
	}

	private UserCdvLocalService _userCdvLocalService;
}