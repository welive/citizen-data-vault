/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.ThirdPartiesProfileLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class ThirdPartiesProfileClp extends BaseModelImpl<ThirdPartiesProfile>
	implements ThirdPartiesProfile {
	public ThirdPartiesProfileClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ThirdPartiesProfile.class;
	}

	@Override
	public String getModelClassName() {
		return ThirdPartiesProfile.class.getName();
	}

	@Override
	public ThirdPartiesProfilePK getPrimaryKey() {
		return new ThirdPartiesProfilePK(_thirdPartyId, _key);
	}

	@Override
	public void setPrimaryKey(ThirdPartiesProfilePK primaryKey) {
		setThirdPartyId(primaryKey.thirdPartyId);
		setKey(primaryKey.key);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new ThirdPartiesProfilePK(_thirdPartyId, _key);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((ThirdPartiesProfilePK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("thirdPartyId", getThirdPartyId());
		attributes.put("key", getKey());
		attributes.put("value", getValue());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long thirdPartyId = (Long)attributes.get("thirdPartyId");

		if (thirdPartyId != null) {
			setThirdPartyId(thirdPartyId);
		}

		String key = (String)attributes.get("key");

		if (key != null) {
			setKey(key);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}
	}

	@Override
	public long getThirdPartyId() {
		return _thirdPartyId;
	}

	@Override
	public void setThirdPartyId(long thirdPartyId) {
		_thirdPartyId = thirdPartyId;

		if (_thirdPartiesProfileRemoteModel != null) {
			try {
				Class<?> clazz = _thirdPartiesProfileRemoteModel.getClass();

				Method method = clazz.getMethod("setThirdPartyId", long.class);

				method.invoke(_thirdPartiesProfileRemoteModel, thirdPartyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getKey() {
		return _key;
	}

	@Override
	public void setKey(String key) {
		_key = key;

		if (_thirdPartiesProfileRemoteModel != null) {
			try {
				Class<?> clazz = _thirdPartiesProfileRemoteModel.getClass();

				Method method = clazz.getMethod("setKey", String.class);

				method.invoke(_thirdPartiesProfileRemoteModel, key);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getValue() {
		return _value;
	}

	@Override
	public void setValue(String value) {
		_value = value;

		if (_thirdPartiesProfileRemoteModel != null) {
			try {
				Class<?> clazz = _thirdPartiesProfileRemoteModel.getClass();

				Method method = clazz.getMethod("setValue", String.class);

				method.invoke(_thirdPartiesProfileRemoteModel, value);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getThirdPartiesProfileRemoteModel() {
		return _thirdPartiesProfileRemoteModel;
	}

	public void setThirdPartiesProfileRemoteModel(
		BaseModel<?> thirdPartiesProfileRemoteModel) {
		_thirdPartiesProfileRemoteModel = thirdPartiesProfileRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _thirdPartiesProfileRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_thirdPartiesProfileRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ThirdPartiesProfileLocalServiceUtil.addThirdPartiesProfile(this);
		}
		else {
			ThirdPartiesProfileLocalServiceUtil.updateThirdPartiesProfile(this);
		}
	}

	@Override
	public ThirdPartiesProfile toEscapedModel() {
		return (ThirdPartiesProfile)ProxyUtil.newProxyInstance(ThirdPartiesProfile.class.getClassLoader(),
			new Class[] { ThirdPartiesProfile.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ThirdPartiesProfileClp clone = new ThirdPartiesProfileClp();

		clone.setThirdPartyId(getThirdPartyId());
		clone.setKey(getKey());
		clone.setValue(getValue());

		return clone;
	}

	@Override
	public int compareTo(ThirdPartiesProfile thirdPartiesProfile) {
		ThirdPartiesProfilePK primaryKey = thirdPartiesProfile.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ThirdPartiesProfileClp)) {
			return false;
		}

		ThirdPartiesProfileClp thirdPartiesProfile = (ThirdPartiesProfileClp)obj;

		ThirdPartiesProfilePK primaryKey = thirdPartiesProfile.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{thirdPartyId=");
		sb.append(getThirdPartyId());
		sb.append(", key=");
		sb.append(getKey());
		sb.append(", value=");
		sb.append(getValue());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>thirdPartyId</column-name><column-value><![CDATA[");
		sb.append(getThirdPartyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>key</column-name><column-value><![CDATA[");
		sb.append(getKey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>value</column-name><column-value><![CDATA[");
		sb.append(getValue());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _thirdPartyId;
	private String _key;
	private String _value;
	private BaseModel<?> _thirdPartiesProfileRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}