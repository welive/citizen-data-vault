/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.UserLanguagePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class UserLanguageSoap implements Serializable {
	public static UserLanguageSoap toSoapModel(UserLanguage model) {
		UserLanguageSoap soapModel = new UserLanguageSoap();

		soapModel.setLanguageId(model.getLanguageId());
		soapModel.setUserId(model.getUserId());

		return soapModel;
	}

	public static UserLanguageSoap[] toSoapModels(UserLanguage[] models) {
		UserLanguageSoap[] soapModels = new UserLanguageSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserLanguageSoap[][] toSoapModels(UserLanguage[][] models) {
		UserLanguageSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserLanguageSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserLanguageSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserLanguageSoap[] toSoapModels(List<UserLanguage> models) {
		List<UserLanguageSoap> soapModels = new ArrayList<UserLanguageSoap>(models.size());

		for (UserLanguage model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserLanguageSoap[soapModels.size()]);
	}

	public UserLanguageSoap() {
	}

	public UserLanguagePK getPrimaryKey() {
		return new UserLanguagePK(_languageId, _userId);
	}

	public void setPrimaryKey(UserLanguagePK pk) {
		setLanguageId(pk.languageId);
		setUserId(pk.userId);
	}

	public long getLanguageId() {
		return _languageId;
	}

	public void setLanguageId(long languageId) {
		_languageId = languageId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	private long _languageId;
	private long _userId;
}