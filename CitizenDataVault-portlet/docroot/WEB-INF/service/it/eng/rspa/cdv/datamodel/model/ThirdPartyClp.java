/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.ThirdPartyLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class ThirdPartyClp extends BaseModelImpl<ThirdParty>
	implements ThirdParty {
	public ThirdPartyClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ThirdParty.class;
	}

	@Override
	public String getModelClassName() {
		return ThirdParty.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _thirdPartyId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setThirdPartyId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _thirdPartyId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("thirdPartyId", getThirdPartyId());
		attributes.put("thirdPartyName", getThirdPartyName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long thirdPartyId = (Long)attributes.get("thirdPartyId");

		if (thirdPartyId != null) {
			setThirdPartyId(thirdPartyId);
		}

		String thirdPartyName = (String)attributes.get("thirdPartyName");

		if (thirdPartyName != null) {
			setThirdPartyName(thirdPartyName);
		}
	}

	@Override
	public long getThirdPartyId() {
		return _thirdPartyId;
	}

	@Override
	public void setThirdPartyId(long thirdPartyId) {
		_thirdPartyId = thirdPartyId;

		if (_thirdPartyRemoteModel != null) {
			try {
				Class<?> clazz = _thirdPartyRemoteModel.getClass();

				Method method = clazz.getMethod("setThirdPartyId", long.class);

				method.invoke(_thirdPartyRemoteModel, thirdPartyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getThirdPartyName() {
		return _thirdPartyName;
	}

	@Override
	public void setThirdPartyName(String thirdPartyName) {
		_thirdPartyName = thirdPartyName;

		if (_thirdPartyRemoteModel != null) {
			try {
				Class<?> clazz = _thirdPartyRemoteModel.getClass();

				Method method = clazz.getMethod("setThirdPartyName",
						String.class);

				method.invoke(_thirdPartyRemoteModel, thirdPartyName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getThirdPartyRemoteModel() {
		return _thirdPartyRemoteModel;
	}

	public void setThirdPartyRemoteModel(BaseModel<?> thirdPartyRemoteModel) {
		_thirdPartyRemoteModel = thirdPartyRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _thirdPartyRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_thirdPartyRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ThirdPartyLocalServiceUtil.addThirdParty(this);
		}
		else {
			ThirdPartyLocalServiceUtil.updateThirdParty(this);
		}
	}

	@Override
	public ThirdParty toEscapedModel() {
		return (ThirdParty)ProxyUtil.newProxyInstance(ThirdParty.class.getClassLoader(),
			new Class[] { ThirdParty.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ThirdPartyClp clone = new ThirdPartyClp();

		clone.setThirdPartyId(getThirdPartyId());
		clone.setThirdPartyName(getThirdPartyName());

		return clone;
	}

	@Override
	public int compareTo(ThirdParty thirdParty) {
		long primaryKey = thirdParty.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ThirdPartyClp)) {
			return false;
		}

		ThirdPartyClp thirdParty = (ThirdPartyClp)obj;

		long primaryKey = thirdParty.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{thirdPartyId=");
		sb.append(getThirdPartyId());
		sb.append(", thirdPartyName=");
		sb.append(getThirdPartyName());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.ThirdParty");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>thirdPartyId</column-name><column-value><![CDATA[");
		sb.append(getThirdPartyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>thirdPartyName</column-name><column-value><![CDATA[");
		sb.append(getThirdPartyName());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _thirdPartyId;
	private String _thirdPartyName;
	private BaseModel<?> _thirdPartyRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}