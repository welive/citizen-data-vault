/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class ApplicationSoap implements Serializable {
	public static ApplicationSoap toSoapModel(Application model) {
		ApplicationSoap soapModel = new ApplicationSoap();

		soapModel.setAppId(model.getAppId());
		soapModel.setAppName(model.getAppName());

		return soapModel;
	}

	public static ApplicationSoap[] toSoapModels(Application[] models) {
		ApplicationSoap[] soapModels = new ApplicationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ApplicationSoap[][] toSoapModels(Application[][] models) {
		ApplicationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ApplicationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ApplicationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ApplicationSoap[] toSoapModels(List<Application> models) {
		List<ApplicationSoap> soapModels = new ArrayList<ApplicationSoap>(models.size());

		for (Application model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ApplicationSoap[soapModels.size()]);
	}

	public ApplicationSoap() {
	}

	public long getPrimaryKey() {
		return _appId;
	}

	public void setPrimaryKey(long pk) {
		setAppId(pk);
	}

	public long getAppId() {
		return _appId;
	}

	public void setAppId(long appId) {
		_appId = appId;
	}

	public String getAppName() {
		return _appName;
	}

	public void setAppName(String appName) {
		_appName = appName;
	}

	private long _appId;
	private String _appName;
}