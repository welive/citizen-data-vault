/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class UserCdvSoap implements Serializable {
	public static UserCdvSoap toSoapModel(UserCdv model) {
		UserCdvSoap soapModel = new UserCdvSoap();

		soapModel.setCcuid(model.getCcuid());
		soapModel.setUserId(model.getUserId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUsername(model.getUsername());
		soapModel.setPassword(model.getPassword());
		soapModel.setName(model.getName());
		soapModel.setSurname(model.getSurname());
		soapModel.setGender(model.getGender());
		soapModel.setBirthdate(model.getBirthdate());
		soapModel.setAddress(model.getAddress());
		soapModel.setCity(model.getCity());
		soapModel.setCountry(model.getCountry());
		soapModel.setZipcode(model.getZipcode());
		soapModel.setEmail(model.getEmail());
		soapModel.setIsDeveloper(model.getIsDeveloper());
		soapModel.setLastKnownLatitude(model.getLastKnownLatitude());
		soapModel.setLastKnownLongitude(model.getLastKnownLongitude());
		soapModel.setPilot(model.getPilot());
		soapModel.setReputation(model.getReputation());
		soapModel.setEmployement(model.getEmployement());

		return soapModel;
	}

	public static UserCdvSoap[] toSoapModels(UserCdv[] models) {
		UserCdvSoap[] soapModels = new UserCdvSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserCdvSoap[][] toSoapModels(UserCdv[][] models) {
		UserCdvSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserCdvSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserCdvSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserCdvSoap[] toSoapModels(List<UserCdv> models) {
		List<UserCdvSoap> soapModels = new ArrayList<UserCdvSoap>(models.size());

		for (UserCdv model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserCdvSoap[soapModels.size()]);
	}

	public UserCdvSoap() {
	}

	public UserCdvPK getPrimaryKey() {
		return new UserCdvPK(_ccuid, _userId);
	}

	public void setPrimaryKey(UserCdvPK pk) {
		setCcuid(pk.ccuid);
		setUserId(pk.userId);
	}

	public long getCcuid() {
		return _ccuid;
	}

	public void setCcuid(long ccuid) {
		_ccuid = ccuid;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getUsername() {
		return _username;
	}

	public void setUsername(String username) {
		_username = username;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		_password = password;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getSurname() {
		return _surname;
	}

	public void setSurname(String surname) {
		_surname = surname;
	}

	public String getGender() {
		return _gender;
	}

	public void setGender(String gender) {
		_gender = gender;
	}

	public Date getBirthdate() {
		return _birthdate;
	}

	public void setBirthdate(Date birthdate) {
		_birthdate = birthdate;
	}

	public String getAddress() {
		return _address;
	}

	public void setAddress(String address) {
		_address = address;
	}

	public String getCity() {
		return _city;
	}

	public void setCity(String city) {
		_city = city;
	}

	public String getCountry() {
		return _country;
	}

	public void setCountry(String country) {
		_country = country;
	}

	public String getZipcode() {
		return _zipcode;
	}

	public void setZipcode(String zipcode) {
		_zipcode = zipcode;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public boolean getIsDeveloper() {
		return _isDeveloper;
	}

	public boolean isIsDeveloper() {
		return _isDeveloper;
	}

	public void setIsDeveloper(boolean isDeveloper) {
		_isDeveloper = isDeveloper;
	}

	public double getLastKnownLatitude() {
		return _lastKnownLatitude;
	}

	public void setLastKnownLatitude(double lastKnownLatitude) {
		_lastKnownLatitude = lastKnownLatitude;
	}

	public double getLastKnownLongitude() {
		return _lastKnownLongitude;
	}

	public void setLastKnownLongitude(double lastKnownLongitude) {
		_lastKnownLongitude = lastKnownLongitude;
	}

	public String getPilot() {
		return _pilot;
	}

	public void setPilot(String pilot) {
		_pilot = pilot;
	}

	public long getReputation() {
		return _reputation;
	}

	public void setReputation(long reputation) {
		_reputation = reputation;
	}

	public String getEmployement() {
		return _employement;
	}

	public void setEmployement(String employement) {
		_employement = employement;
	}

	private long _ccuid;
	private long _userId;
	private long _companyId;
	private String _username;
	private String _password;
	private String _name;
	private String _surname;
	private String _gender;
	private Date _birthdate;
	private String _address;
	private String _city;
	private String _country;
	private String _zipcode;
	private String _email;
	private boolean _isDeveloper;
	private double _lastKnownLatitude;
	private double _lastKnownLongitude;
	private String _pilot;
	private long _reputation;
	private String _employement;
}