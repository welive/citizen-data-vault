/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Tag}.
 * </p>
 *
 * @author Engineering
 * @see Tag
 * @generated
 */
public class TagWrapper implements Tag, ModelWrapper<Tag> {
	public TagWrapper(Tag tag) {
		_tag = tag;
	}

	@Override
	public Class<?> getModelClass() {
		return Tag.class;
	}

	@Override
	public String getModelClassName() {
		return Tag.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tagid", getTagid());
		attributes.put("tagname", getTagname());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long tagid = (Long)attributes.get("tagid");

		if (tagid != null) {
			setTagid(tagid);
		}

		String tagname = (String)attributes.get("tagname");

		if (tagname != null) {
			setTagname(tagname);
		}
	}

	/**
	* Returns the primary key of this tag.
	*
	* @return the primary key of this tag
	*/
	@Override
	public long getPrimaryKey() {
		return _tag.getPrimaryKey();
	}

	/**
	* Sets the primary key of this tag.
	*
	* @param primaryKey the primary key of this tag
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_tag.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the tagid of this tag.
	*
	* @return the tagid of this tag
	*/
	@Override
	public long getTagid() {
		return _tag.getTagid();
	}

	/**
	* Sets the tagid of this tag.
	*
	* @param tagid the tagid of this tag
	*/
	@Override
	public void setTagid(long tagid) {
		_tag.setTagid(tagid);
	}

	/**
	* Returns the tagname of this tag.
	*
	* @return the tagname of this tag
	*/
	@Override
	public java.lang.String getTagname() {
		return _tag.getTagname();
	}

	/**
	* Sets the tagname of this tag.
	*
	* @param tagname the tagname of this tag
	*/
	@Override
	public void setTagname(java.lang.String tagname) {
		_tag.setTagname(tagname);
	}

	@Override
	public boolean isNew() {
		return _tag.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_tag.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _tag.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_tag.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _tag.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _tag.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_tag.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _tag.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_tag.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_tag.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_tag.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TagWrapper((Tag)_tag.clone());
	}

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.Tag tag) {
		return _tag.compareTo(tag);
	}

	@Override
	public int hashCode() {
		return _tag.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.Tag> toCacheModel() {
		return _tag.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag toEscapedModel() {
		return new TagWrapper(_tag.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag toUnescapedModel() {
		return new TagWrapper(_tag.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _tag.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _tag.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_tag.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TagWrapper)) {
			return false;
		}

		TagWrapper tagWrapper = (TagWrapper)obj;

		if (Validator.equals(_tag, tagWrapper._tag)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Tag getWrappedTag() {
		return _tag;
	}

	@Override
	public Tag getWrappedModel() {
		return _tag;
	}

	@Override
	public void resetOriginalValues() {
		_tag.resetOriginalValues();
	}

	private Tag _tag;
}