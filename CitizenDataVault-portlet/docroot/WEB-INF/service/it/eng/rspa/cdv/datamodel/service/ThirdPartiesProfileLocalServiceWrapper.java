/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ThirdPartiesProfileLocalService}.
 *
 * @author Engineering
 * @see ThirdPartiesProfileLocalService
 * @generated
 */
public class ThirdPartiesProfileLocalServiceWrapper
	implements ThirdPartiesProfileLocalService,
		ServiceWrapper<ThirdPartiesProfileLocalService> {
	public ThirdPartiesProfileLocalServiceWrapper(
		ThirdPartiesProfileLocalService thirdPartiesProfileLocalService) {
		_thirdPartiesProfileLocalService = thirdPartiesProfileLocalService;
	}

	/**
	* Adds the third parties profile to the database. Also notifies the appropriate model listeners.
	*
	* @param thirdPartiesProfile the third parties profile
	* @return the third parties profile that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile addThirdPartiesProfile(
		it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile thirdPartiesProfile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.addThirdPartiesProfile(thirdPartiesProfile);
	}

	/**
	* Creates a new third parties profile with the primary key. Does not add the third parties profile to the database.
	*
	* @param thirdPartiesProfilePK the primary key for the new third parties profile
	* @return the new third parties profile
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile createThirdPartiesProfile(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK thirdPartiesProfilePK) {
		return _thirdPartiesProfileLocalService.createThirdPartiesProfile(thirdPartiesProfilePK);
	}

	/**
	* Deletes the third parties profile with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param thirdPartiesProfilePK the primary key of the third parties profile
	* @return the third parties profile that was removed
	* @throws PortalException if a third parties profile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile deleteThirdPartiesProfile(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK thirdPartiesProfilePK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.deleteThirdPartiesProfile(thirdPartiesProfilePK);
	}

	/**
	* Deletes the third parties profile from the database. Also notifies the appropriate model listeners.
	*
	* @param thirdPartiesProfile the third parties profile
	* @return the third parties profile that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile deleteThirdPartiesProfile(
		it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile thirdPartiesProfile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.deleteThirdPartiesProfile(thirdPartiesProfile);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _thirdPartiesProfileLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartiesProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartiesProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile fetchThirdPartiesProfile(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK thirdPartiesProfilePK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.fetchThirdPartiesProfile(thirdPartiesProfilePK);
	}

	/**
	* Returns the third parties profile with the primary key.
	*
	* @param thirdPartiesProfilePK the primary key of the third parties profile
	* @return the third parties profile
	* @throws PortalException if a third parties profile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile getThirdPartiesProfile(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK thirdPartiesProfilePK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.getThirdPartiesProfile(thirdPartiesProfilePK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the third parties profiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartiesProfileModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of third parties profiles
	* @param end the upper bound of the range of third parties profiles (not inclusive)
	* @return the range of third parties profiles
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile> getThirdPartiesProfiles(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.getThirdPartiesProfiles(start,
			end);
	}

	/**
	* Returns the number of third parties profiles.
	*
	* @return the number of third parties profiles
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getThirdPartiesProfilesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.getThirdPartiesProfilesCount();
	}

	/**
	* Updates the third parties profile in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param thirdPartiesProfile the third parties profile
	* @return the third parties profile that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile updateThirdPartiesProfile(
		it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile thirdPartiesProfile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _thirdPartiesProfileLocalService.updateThirdPartiesProfile(thirdPartiesProfile);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _thirdPartiesProfileLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_thirdPartiesProfileLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _thirdPartiesProfileLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ThirdPartiesProfileLocalService getWrappedThirdPartiesProfileLocalService() {
		return _thirdPartiesProfileLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedThirdPartiesProfileLocalService(
		ThirdPartiesProfileLocalService thirdPartiesProfileLocalService) {
		_thirdPartiesProfileLocalService = thirdPartiesProfileLocalService;
	}

	@Override
	public ThirdPartiesProfileLocalService getWrappedService() {
		return _thirdPartiesProfileLocalService;
	}

	@Override
	public void setWrappedService(
		ThirdPartiesProfileLocalService thirdPartiesProfileLocalService) {
		_thirdPartiesProfileLocalService = thirdPartiesProfileLocalService;
	}

	private ThirdPartiesProfileLocalService _thirdPartiesProfileLocalService;
}