/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UserSkill}.
 * </p>
 *
 * @author Engineering
 * @see UserSkill
 * @generated
 */
public class UserSkillWrapper implements UserSkill, ModelWrapper<UserSkill> {
	public UserSkillWrapper(UserSkill userSkill) {
		_userSkill = userSkill;
	}

	@Override
	public Class<?> getModelClass() {
		return UserSkill.class;
	}

	@Override
	public String getModelClassName() {
		return UserSkill.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("skillId", getSkillId());
		attributes.put("userId", getUserId());
		attributes.put("date", getDate());
		attributes.put("endorsmentCounter", getEndorsmentCounter());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long skillId = (Long)attributes.get("skillId");

		if (skillId != null) {
			setSkillId(skillId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long endorsmentCounter = (Long)attributes.get("endorsmentCounter");

		if (endorsmentCounter != null) {
			setEndorsmentCounter(endorsmentCounter);
		}
	}

	/**
	* Returns the primary key of this user skill.
	*
	* @return the primary key of this user skill
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK getPrimaryKey() {
		return _userSkill.getPrimaryKey();
	}

	/**
	* Sets the primary key of this user skill.
	*
	* @param primaryKey the primary key of this user skill
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK primaryKey) {
		_userSkill.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the skill ID of this user skill.
	*
	* @return the skill ID of this user skill
	*/
	@Override
	public long getSkillId() {
		return _userSkill.getSkillId();
	}

	/**
	* Sets the skill ID of this user skill.
	*
	* @param skillId the skill ID of this user skill
	*/
	@Override
	public void setSkillId(long skillId) {
		_userSkill.setSkillId(skillId);
	}

	/**
	* Returns the user ID of this user skill.
	*
	* @return the user ID of this user skill
	*/
	@Override
	public long getUserId() {
		return _userSkill.getUserId();
	}

	/**
	* Sets the user ID of this user skill.
	*
	* @param userId the user ID of this user skill
	*/
	@Override
	public void setUserId(long userId) {
		_userSkill.setUserId(userId);
	}

	/**
	* Returns the user uuid of this user skill.
	*
	* @return the user uuid of this user skill
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userSkill.getUserUuid();
	}

	/**
	* Sets the user uuid of this user skill.
	*
	* @param userUuid the user uuid of this user skill
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userSkill.setUserUuid(userUuid);
	}

	/**
	* Returns the date of this user skill.
	*
	* @return the date of this user skill
	*/
	@Override
	public java.util.Date getDate() {
		return _userSkill.getDate();
	}

	/**
	* Sets the date of this user skill.
	*
	* @param date the date of this user skill
	*/
	@Override
	public void setDate(java.util.Date date) {
		_userSkill.setDate(date);
	}

	/**
	* Returns the endorsment counter of this user skill.
	*
	* @return the endorsment counter of this user skill
	*/
	@Override
	public long getEndorsmentCounter() {
		return _userSkill.getEndorsmentCounter();
	}

	/**
	* Sets the endorsment counter of this user skill.
	*
	* @param endorsmentCounter the endorsment counter of this user skill
	*/
	@Override
	public void setEndorsmentCounter(long endorsmentCounter) {
		_userSkill.setEndorsmentCounter(endorsmentCounter);
	}

	@Override
	public boolean isNew() {
		return _userSkill.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_userSkill.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _userSkill.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userSkill.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _userSkill.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _userSkill.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_userSkill.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _userSkill.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_userSkill.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_userSkill.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_userSkill.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UserSkillWrapper((UserSkill)_userSkill.clone());
	}

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.UserSkill userSkill) {
		return _userSkill.compareTo(userSkill);
	}

	@Override
	public int hashCode() {
		return _userSkill.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.UserSkill> toCacheModel() {
		return _userSkill.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserSkill toEscapedModel() {
		return new UserSkillWrapper(_userSkill.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserSkill toUnescapedModel() {
		return new UserSkillWrapper(_userSkill.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _userSkill.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userSkill.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_userSkill.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserSkillWrapper)) {
			return false;
		}

		UserSkillWrapper userSkillWrapper = (UserSkillWrapper)obj;

		if (Validator.equals(_userSkill, userSkillWrapper._userSkill)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UserSkill getWrappedUserSkill() {
		return _userSkill;
	}

	@Override
	public UserSkill getWrappedModel() {
		return _userSkill;
	}

	@Override
	public void resetOriginalValues() {
		_userSkill.resetOriginalValues();
	}

	private UserSkill _userSkill;
}