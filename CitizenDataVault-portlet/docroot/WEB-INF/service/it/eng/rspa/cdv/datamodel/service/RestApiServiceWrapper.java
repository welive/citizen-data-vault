/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RestApiService}.
 *
 * @author Engineering
 * @see RestApiService
 * @generated
 */
public class RestApiServiceWrapper implements RestApiService,
	ServiceWrapper<RestApiService> {
	public RestApiServiceWrapper(RestApiService restApiService) {
		_restApiService = restApiService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _restApiService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_restApiService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _restApiService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* DataSource Oriented
	*/
	@Override
	public com.liferay.portal.kernel.json.JSONObject addUser(
		java.lang.String body) {
		return _restApiService.addUser(body);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateUser(
		java.lang.String body) {
		return _restApiService.updateUser(body);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject push(java.lang.String body) {
		return _restApiService.push(body);
	}

	/**
	* DataConsumer Oriented *
	*/
	@Override
	public com.liferay.portal.kernel.json.JSONArray getUsersSkills() {
		return _restApiService.getUsersSkills();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getUserMetadata(
		java.lang.String ccuserid) {
		return _restApiService.getUserMetadata(ccuserid);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getUserProfile(
		java.lang.String uid) {
		return _restApiService.getUserProfile(uid);
	}

	/**
	* Statistics Oriented *
	*/
	@Override
	public com.liferay.portal.kernel.json.JSONObject getKPI11(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return _restApiService.getKPI11(pilotid, from, to);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getKPI43(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return _restApiService.getKPI43(pilotid, from, to);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getKPI111(
		java.lang.String pilotid) {
		return _restApiService.getKPI111(pilotid);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getKPI112(
		java.lang.String pilotid) {
		return _restApiService.getKPI112(pilotid);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getKPI113(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return _restApiService.getKPI113(pilotid, from, to);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getKPI114(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return _restApiService.getKPI114(pilotid, from, to);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getKPI115(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return _restApiService.getKPI115(pilotid, from, to);
	}

	/**
	* Data Oriented *
	*/
	@Override
	public com.liferay.portal.kernel.json.JSONArray userDataUpdate(
		java.lang.String body, java.lang.String uid, java.lang.String client_id) {
		return _restApiService.userDataUpdate(body, uid, client_id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getUserData(
		java.lang.String dataid, java.lang.String uid,
		java.lang.String client_id) {
		return _restApiService.getUserData(dataid, uid, client_id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getUserDataSearchResult(
		java.lang.String tags, java.lang.String uid, java.lang.String client_id) {
		return _restApiService.getUserDataSearchResult(tags, uid, client_id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray deleteUserData(
		java.lang.String dataid, java.lang.String uid,
		java.lang.String client_id) {
		return _restApiService.deleteUserData(dataid, uid, client_id);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public RestApiService getWrappedRestApiService() {
		return _restApiService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedRestApiService(RestApiService restApiService) {
		_restApiService = restApiService;
	}

	@Override
	public RestApiService getWrappedService() {
		return _restApiService;
	}

	@Override
	public void setWrappedService(RestApiService restApiService) {
		_restApiService = restApiService;
	}

	private RestApiService _restApiService;
}