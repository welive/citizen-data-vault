/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.cdv.datamodel.model.UserIdea;

import java.util.List;

/**
 * The persistence utility for the user idea service. This utility wraps {@link UserIdeaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserIdeaPersistence
 * @see UserIdeaPersistenceImpl
 * @generated
 */
public class UserIdeaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(UserIdea userIdea) {
		getPersistence().clearCache(userIdea);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserIdea> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserIdea> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserIdea> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static UserIdea update(UserIdea userIdea) throws SystemException {
		return getPersistence().update(userIdea);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static UserIdea update(UserIdea userIdea,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(userIdea, serviceContext);
	}

	/**
	* Returns all the user ideas where role = &#63;.
	*
	* @param role the role
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByrole(
		java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByrole(role);
	}

	/**
	* Returns a range of all the user ideas where role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByrole(
		java.lang.String role, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByrole(role, start, end);
	}

	/**
	* Returns an ordered range of all the user ideas where role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByrole(
		java.lang.String role, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByrole(role, start, end, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where role = &#63;.
	*
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByrole_First(
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence().findByrole_First(role, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where role = &#63;.
	*
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByrole_First(
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByrole_First(role, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where role = &#63;.
	*
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByrole_Last(
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence().findByrole_Last(role, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where role = &#63;.
	*
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByrole_Last(
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByrole_Last(role, orderByComparator);
	}

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where role = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea[] findByrole_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByrole_PrevAndNext(userIdeaPK, role, orderByComparator);
	}

	/**
	* Removes all the user ideas where role = &#63; from the database.
	*
	* @param role the role
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByrole(java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByrole(role);
	}

	/**
	* Returns the number of user ideas where role = &#63;.
	*
	* @param role the role
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByrole(java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByrole(role);
	}

	/**
	* Returns all the user ideas where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuser(userId);
	}

	/**
	* Returns a range of all the user ideas where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuser(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuser(userId, start, end);
	}

	/**
	* Returns an ordered range of all the user ideas where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuser(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuser(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByuser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence().findByuser_First(userId, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByuser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuser_First(userId, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByuser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence().findByuser_Last(userId, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByuser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuser_Last(userId, orderByComparator);
	}

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where userId = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea[] findByuser_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByuser_PrevAndNext(userIdeaPK, userId, orderByComparator);
	}

	/**
	* Removes all the user ideas where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByuser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByuser(userId);
	}

	/**
	* Returns the number of user ideas where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByuser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByuser(userId);
	}

	/**
	* Returns all the user ideas where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuserRole(
		long userId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserRole(userId, role);
	}

	/**
	* Returns a range of all the user ideas where userId = &#63; and role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuserRole(
		long userId, java.lang.String role, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserRole(userId, role, start, end);
	}

	/**
	* Returns an ordered range of all the user ideas where userId = &#63; and role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByuserRole(
		long userId, java.lang.String role, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByuserRole(userId, role, start, end, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByuserRole_First(
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByuserRole_First(userId, role, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByuserRole_First(
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByuserRole_First(userId, role, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByuserRole_Last(
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByuserRole_Last(userId, role, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByuserRole_Last(
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByuserRole_Last(userId, role, orderByComparator);
	}

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where userId = &#63; and role = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param userId the user ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea[] findByuserRole_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		long userId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByuserRole_PrevAndNext(userIdeaPK, userId, role,
			orderByComparator);
	}

	/**
	* Removes all the user ideas where userId = &#63; and role = &#63; from the database.
	*
	* @param userId the user ID
	* @param role the role
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByuserRole(long userId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByuserRole(userId, role);
	}

	/**
	* Returns the number of user ideas where userId = &#63; and role = &#63;.
	*
	* @param userId the user ID
	* @param role the role
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByuserRole(long userId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByuserRole(userId, role);
	}

	/**
	* Returns all the user ideas where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByideaId(ideaId);
	}

	/**
	* Returns a range of all the user ideas where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByideaId(ideaId, start, end);
	}

	/**
	* Returns an ordered range of all the user ideas where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByideaId(ideaId, start, end, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence().findByideaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByideaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence().findByideaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByideaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where ideaId = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea[] findByideaId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByideaId_PrevAndNext(userIdeaPK, ideaId,
			orderByComparator);
	}

	/**
	* Removes all the user ideas where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByideaId(ideaId);
	}

	/**
	* Returns the number of user ideas where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByideaId(ideaId);
	}

	/**
	* Returns all the user ideas where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @return the matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaRole(
		long ideaId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByideaRole(ideaId, role);
	}

	/**
	* Returns a range of all the user ideas where ideaId = &#63; and role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaRole(
		long ideaId, java.lang.String role, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByideaRole(ideaId, role, start, end);
	}

	/**
	* Returns an ordered range of all the user ideas where ideaId = &#63; and role = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findByideaRole(
		long ideaId, java.lang.String role, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByideaRole(ideaId, role, start, end, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByideaRole_First(
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByideaRole_First(ideaId, role, orderByComparator);
	}

	/**
	* Returns the first user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByideaRole_First(
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByideaRole_First(ideaId, role, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByideaRole_Last(
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByideaRole_Last(ideaId, role, orderByComparator);
	}

	/**
	* Returns the last user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user idea, or <code>null</code> if a matching user idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByideaRole_Last(
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByideaRole_Last(ideaId, role, orderByComparator);
	}

	/**
	* Returns the user ideas before and after the current user idea in the ordered set where ideaId = &#63; and role = &#63;.
	*
	* @param userIdeaPK the primary key of the current user idea
	* @param ideaId the idea ID
	* @param role the role
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea[] findByideaRole_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK,
		long ideaId, java.lang.String role,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence()
				   .findByideaRole_PrevAndNext(userIdeaPK, ideaId, role,
			orderByComparator);
	}

	/**
	* Removes all the user ideas where ideaId = &#63; and role = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByideaRole(long ideaId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByideaRole(ideaId, role);
	}

	/**
	* Returns the number of user ideas where ideaId = &#63; and role = &#63;.
	*
	* @param ideaId the idea ID
	* @param role the role
	* @return the number of matching user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByideaRole(long ideaId, java.lang.String role)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByideaRole(ideaId, role);
	}

	/**
	* Caches the user idea in the entity cache if it is enabled.
	*
	* @param userIdea the user idea
	*/
	public static void cacheResult(
		it.eng.rspa.cdv.datamodel.model.UserIdea userIdea) {
		getPersistence().cacheResult(userIdea);
	}

	/**
	* Caches the user ideas in the entity cache if it is enabled.
	*
	* @param userIdeas the user ideas
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> userIdeas) {
		getPersistence().cacheResult(userIdeas);
	}

	/**
	* Creates a new user idea with the primary key. Does not add the user idea to the database.
	*
	* @param userIdeaPK the primary key for the new user idea
	* @return the new user idea
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea create(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK) {
		return getPersistence().create(userIdeaPK);
	}

	/**
	* Removes the user idea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userIdeaPK the primary key of the user idea
	* @return the user idea that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea remove(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence().remove(userIdeaPK);
	}

	public static it.eng.rspa.cdv.datamodel.model.UserIdea updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserIdea userIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(userIdea);
	}

	/**
	* Returns the user idea with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException} if it could not be found.
	*
	* @param userIdeaPK the primary key of the user idea
	* @return the user idea
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException {
		return getPersistence().findByPrimaryKey(userIdeaPK);
	}

	/**
	* Returns the user idea with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userIdeaPK the primary key of the user idea
	* @return the user idea, or <code>null</code> if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(userIdeaPK);
	}

	/**
	* Returns all the user ideas.
	*
	* @return the user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the user ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the user ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the user ideas from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of user ideas.
	*
	* @return the number of user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static UserIdeaPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (UserIdeaPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.cdv.datamodel.service.ClpSerializer.getServletContextName(),
					UserIdeaPersistence.class.getName());

			ReferenceRegistry.registerReference(UserIdeaUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(UserIdeaPersistence persistence) {
	}

	private static UserIdeaPersistence _persistence;
}