/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the UserCdv service. Represents a row in the &quot;cdv_UserCdv&quot; database table, with each column mapped to a property of this class.
 *
 * @author Engineering
 * @see UserCdvModel
 * @see it.eng.rspa.cdv.datamodel.model.impl.UserCdvImpl
 * @see it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl
 * @generated
 */
public interface UserCdv extends UserCdvModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
}