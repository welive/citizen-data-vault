/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.cdv.datamodel.model.IdeaCdv;

import java.util.List;

/**
 * The persistence utility for the idea cdv service. This utility wraps {@link IdeaCdvPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see IdeaCdvPersistence
 * @see IdeaCdvPersistenceImpl
 * @generated
 */
public class IdeaCdvUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(IdeaCdv ideaCdv) {
		getPersistence().clearCache(ideaCdv);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<IdeaCdv> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<IdeaCdv> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<IdeaCdv> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static IdeaCdv update(IdeaCdv ideaCdv) throws SystemException {
		return getPersistence().update(ideaCdv);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static IdeaCdv update(IdeaCdv ideaCdv, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(ideaCdv, serviceContext);
	}

	/**
	* Returns all the idea cdvs where need = &#63;.
	*
	* @param need the need
	* @return the matching idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findByneed(
		boolean need)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByneed(need);
	}

	/**
	* Returns a range of all the idea cdvs where need = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param need the need
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @return the range of matching idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findByneed(
		boolean need, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByneed(need, start, end);
	}

	/**
	* Returns an ordered range of all the idea cdvs where need = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param need the need
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findByneed(
		boolean need, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByneed(need, start, end, orderByComparator);
	}

	/**
	* Returns the first idea cdv in the ordered set where need = &#63;.
	*
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a matching idea cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv findByneed_First(
		boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException {
		return getPersistence().findByneed_First(need, orderByComparator);
	}

	/**
	* Returns the first idea cdv in the ordered set where need = &#63;.
	*
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea cdv, or <code>null</code> if a matching idea cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv fetchByneed_First(
		boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByneed_First(need, orderByComparator);
	}

	/**
	* Returns the last idea cdv in the ordered set where need = &#63;.
	*
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a matching idea cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv findByneed_Last(
		boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException {
		return getPersistence().findByneed_Last(need, orderByComparator);
	}

	/**
	* Returns the last idea cdv in the ordered set where need = &#63;.
	*
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea cdv, or <code>null</code> if a matching idea cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv fetchByneed_Last(
		boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByneed_Last(need, orderByComparator);
	}

	/**
	* Returns the idea cdvs before and after the current idea cdv in the ordered set where need = &#63;.
	*
	* @param ideaId the primary key of the current idea cdv
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next idea cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv[] findByneed_PrevAndNext(
		long ideaId, boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException {
		return getPersistence()
				   .findByneed_PrevAndNext(ideaId, need, orderByComparator);
	}

	/**
	* Removes all the idea cdvs where need = &#63; from the database.
	*
	* @param need the need
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByneed(boolean need)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByneed(need);
	}

	/**
	* Returns the number of idea cdvs where need = &#63;.
	*
	* @param need the need
	* @return the number of matching idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByneed(boolean need)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByneed(need);
	}

	/**
	* Caches the idea cdv in the entity cache if it is enabled.
	*
	* @param ideaCdv the idea cdv
	*/
	public static void cacheResult(
		it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv) {
		getPersistence().cacheResult(ideaCdv);
	}

	/**
	* Caches the idea cdvs in the entity cache if it is enabled.
	*
	* @param ideaCdvs the idea cdvs
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> ideaCdvs) {
		getPersistence().cacheResult(ideaCdvs);
	}

	/**
	* Creates a new idea cdv with the primary key. Does not add the idea cdv to the database.
	*
	* @param ideaId the primary key for the new idea cdv
	* @return the new idea cdv
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv create(long ideaId) {
		return getPersistence().create(ideaId);
	}

	/**
	* Removes the idea cdv with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaId the primary key of the idea cdv
	* @return the idea cdv that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv remove(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException {
		return getPersistence().remove(ideaId);
	}

	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv updateImpl(
		it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(ideaCdv);
	}

	/**
	* Returns the idea cdv with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException} if it could not be found.
	*
	* @param ideaId the primary key of the idea cdv
	* @return the idea cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv findByPrimaryKey(
		long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException {
		return getPersistence().findByPrimaryKey(ideaId);
	}

	/**
	* Returns the idea cdv with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ideaId the primary key of the idea cdv
	* @return the idea cdv, or <code>null</code> if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.IdeaCdv fetchByPrimaryKey(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(ideaId);
	}

	/**
	* Returns all the idea cdvs.
	*
	* @return the idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the idea cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @return the range of idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the idea cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the idea cdvs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of idea cdvs.
	*
	* @return the number of idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static IdeaCdvPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (IdeaCdvPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.cdv.datamodel.service.ClpSerializer.getServletContextName(),
					IdeaCdvPersistence.class.getName());

			ReferenceRegistry.registerReference(IdeaCdvUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(IdeaCdvPersistence persistence) {
	}

	private static IdeaCdvPersistence _persistence;
}