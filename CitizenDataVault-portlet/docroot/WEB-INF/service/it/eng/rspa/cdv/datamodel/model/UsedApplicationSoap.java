/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class UsedApplicationSoap implements Serializable {
	public static UsedApplicationSoap toSoapModel(UsedApplication model) {
		UsedApplicationSoap soapModel = new UsedApplicationSoap();

		soapModel.setAppId(model.getAppId());
		soapModel.setUserId(model.getUserId());

		return soapModel;
	}

	public static UsedApplicationSoap[] toSoapModels(UsedApplication[] models) {
		UsedApplicationSoap[] soapModels = new UsedApplicationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UsedApplicationSoap[][] toSoapModels(
		UsedApplication[][] models) {
		UsedApplicationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UsedApplicationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UsedApplicationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UsedApplicationSoap[] toSoapModels(
		List<UsedApplication> models) {
		List<UsedApplicationSoap> soapModels = new ArrayList<UsedApplicationSoap>(models.size());

		for (UsedApplication model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UsedApplicationSoap[soapModels.size()]);
	}

	public UsedApplicationSoap() {
	}

	public UsedApplicationPK getPrimaryKey() {
		return new UsedApplicationPK(_appId, _userId);
	}

	public void setPrimaryKey(UsedApplicationPK pk) {
		setAppId(pk.appId);
		setUserId(pk.userId);
	}

	public long getAppId() {
		return _appId;
	}

	public void setAppId(long appId) {
		_appId = appId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	private long _appId;
	private long _userId;
}