/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link SkillCdv}.
 * </p>
 *
 * @author Engineering
 * @see SkillCdv
 * @generated
 */
public class SkillCdvWrapper implements SkillCdv, ModelWrapper<SkillCdv> {
	public SkillCdvWrapper(SkillCdv skillCdv) {
		_skillCdv = skillCdv;
	}

	@Override
	public Class<?> getModelClass() {
		return SkillCdv.class;
	}

	@Override
	public String getModelClassName() {
		return SkillCdv.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("skillId", getSkillId());
		attributes.put("skillname", getSkillname());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long skillId = (Long)attributes.get("skillId");

		if (skillId != null) {
			setSkillId(skillId);
		}

		String skillname = (String)attributes.get("skillname");

		if (skillname != null) {
			setSkillname(skillname);
		}
	}

	/**
	* Returns the primary key of this skill cdv.
	*
	* @return the primary key of this skill cdv
	*/
	@Override
	public long getPrimaryKey() {
		return _skillCdv.getPrimaryKey();
	}

	/**
	* Sets the primary key of this skill cdv.
	*
	* @param primaryKey the primary key of this skill cdv
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_skillCdv.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the skill ID of this skill cdv.
	*
	* @return the skill ID of this skill cdv
	*/
	@Override
	public long getSkillId() {
		return _skillCdv.getSkillId();
	}

	/**
	* Sets the skill ID of this skill cdv.
	*
	* @param skillId the skill ID of this skill cdv
	*/
	@Override
	public void setSkillId(long skillId) {
		_skillCdv.setSkillId(skillId);
	}

	/**
	* Returns the skillname of this skill cdv.
	*
	* @return the skillname of this skill cdv
	*/
	@Override
	public java.lang.String getSkillname() {
		return _skillCdv.getSkillname();
	}

	/**
	* Sets the skillname of this skill cdv.
	*
	* @param skillname the skillname of this skill cdv
	*/
	@Override
	public void setSkillname(java.lang.String skillname) {
		_skillCdv.setSkillname(skillname);
	}

	@Override
	public boolean isNew() {
		return _skillCdv.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_skillCdv.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _skillCdv.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_skillCdv.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _skillCdv.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _skillCdv.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_skillCdv.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _skillCdv.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_skillCdv.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_skillCdv.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_skillCdv.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new SkillCdvWrapper((SkillCdv)_skillCdv.clone());
	}

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.SkillCdv skillCdv) {
		return _skillCdv.compareTo(skillCdv);
	}

	@Override
	public int hashCode() {
		return _skillCdv.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.SkillCdv> toCacheModel() {
		return _skillCdv.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.SkillCdv toEscapedModel() {
		return new SkillCdvWrapper(_skillCdv.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.SkillCdv toUnescapedModel() {
		return new SkillCdvWrapper(_skillCdv.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _skillCdv.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _skillCdv.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_skillCdv.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SkillCdvWrapper)) {
			return false;
		}

		SkillCdvWrapper skillCdvWrapper = (SkillCdvWrapper)obj;

		if (Validator.equals(_skillCdv, skillCdvWrapper._skillCdv)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public SkillCdv getWrappedSkillCdv() {
		return _skillCdv;
	}

	@Override
	public SkillCdv getWrappedModel() {
		return _skillCdv;
	}

	@Override
	public void resetOriginalValues() {
		_skillCdv.resetOriginalValues();
	}

	private SkillCdv _skillCdv;
}