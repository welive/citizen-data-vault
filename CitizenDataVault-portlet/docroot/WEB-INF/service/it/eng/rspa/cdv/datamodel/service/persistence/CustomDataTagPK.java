/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Engineering
 * @generated
 */
public class CustomDataTagPK implements Comparable<CustomDataTagPK>,
	Serializable {
	public long entryId;
	public long tagid;

	public CustomDataTagPK() {
	}

	public CustomDataTagPK(long entryId, long tagid) {
		this.entryId = entryId;
		this.tagid = tagid;
	}

	public long getEntryId() {
		return entryId;
	}

	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}

	public long getTagid() {
		return tagid;
	}

	public void setTagid(long tagid) {
		this.tagid = tagid;
	}

	@Override
	public int compareTo(CustomDataTagPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (entryId < pk.entryId) {
			value = -1;
		}
		else if (entryId > pk.entryId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (tagid < pk.tagid) {
			value = -1;
		}
		else if (tagid > pk.tagid) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CustomDataTagPK)) {
			return false;
		}

		CustomDataTagPK pk = (CustomDataTagPK)obj;

		if ((entryId == pk.entryId) && (tagid == pk.tagid)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(entryId) + String.valueOf(tagid)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("entryId");
		sb.append(StringPool.EQUAL);
		sb.append(entryId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("tagid");
		sb.append(StringPool.EQUAL);
		sb.append(tagid);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}