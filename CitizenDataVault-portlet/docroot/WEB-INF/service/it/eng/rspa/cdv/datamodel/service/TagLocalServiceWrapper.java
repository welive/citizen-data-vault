/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TagLocalService}.
 *
 * @author Engineering
 * @see TagLocalService
 * @generated
 */
public class TagLocalServiceWrapper implements TagLocalService,
	ServiceWrapper<TagLocalService> {
	public TagLocalServiceWrapper(TagLocalService tagLocalService) {
		_tagLocalService = tagLocalService;
	}

	/**
	* Adds the tag to the database. Also notifies the appropriate model listeners.
	*
	* @param tag the tag
	* @return the tag that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag addTag(
		it.eng.rspa.cdv.datamodel.model.Tag tag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.addTag(tag);
	}

	/**
	* Creates a new tag with the primary key. Does not add the tag to the database.
	*
	* @param tagid the primary key for the new tag
	* @return the new tag
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag createTag(long tagid) {
		return _tagLocalService.createTag(tagid);
	}

	/**
	* Deletes the tag with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param tagid the primary key of the tag
	* @return the tag that was removed
	* @throws PortalException if a tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag deleteTag(long tagid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.deleteTag(tagid);
	}

	/**
	* Deletes the tag from the database. Also notifies the appropriate model listeners.
	*
	* @param tag the tag
	* @return the tag that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag deleteTag(
		it.eng.rspa.cdv.datamodel.model.Tag tag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.deleteTag(tag);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _tagLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag fetchTag(long tagid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.fetchTag(tagid);
	}

	/**
	* Returns the tag with the primary key.
	*
	* @param tagid the primary key of the tag
	* @return the tag
	* @throws PortalException if a tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag getTag(long tagid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.getTag(tagid);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the tags.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tags
	* @param end the upper bound of the range of tags (not inclusive)
	* @return the range of tags
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.cdv.datamodel.model.Tag> getTags(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.getTags(start, end);
	}

	/**
	* Returns the number of tags.
	*
	* @return the number of tags
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getTagsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.getTagsCount();
	}

	/**
	* Updates the tag in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param tag the tag
	* @return the tag that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag updateTag(
		it.eng.rspa.cdv.datamodel.model.Tag tag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tagLocalService.updateTag(tag);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _tagLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_tagLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _tagLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Tag getTagByName(
		java.lang.String tagname) {
		return _tagLocalService.getTagByName(tagname);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public TagLocalService getWrappedTagLocalService() {
		return _tagLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedTagLocalService(TagLocalService tagLocalService) {
		_tagLocalService = tagLocalService;
	}

	@Override
	public TagLocalService getWrappedService() {
		return _tagLocalService;
	}

	@Override
	public void setWrappedService(TagLocalService tagLocalService) {
		_tagLocalService = tagLocalService;
	}

	private TagLocalService _tagLocalService;
}