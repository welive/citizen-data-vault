/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UsedApplication}.
 * </p>
 *
 * @author Engineering
 * @see UsedApplication
 * @generated
 */
public class UsedApplicationWrapper implements UsedApplication,
	ModelWrapper<UsedApplication> {
	public UsedApplicationWrapper(UsedApplication usedApplication) {
		_usedApplication = usedApplication;
	}

	@Override
	public Class<?> getModelClass() {
		return UsedApplication.class;
	}

	@Override
	public String getModelClassName() {
		return UsedApplication.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("appId", getAppId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long appId = (Long)attributes.get("appId");

		if (appId != null) {
			setAppId(appId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	/**
	* Returns the primary key of this used application.
	*
	* @return the primary key of this used application
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK getPrimaryKey() {
		return _usedApplication.getPrimaryKey();
	}

	/**
	* Sets the primary key of this used application.
	*
	* @param primaryKey the primary key of this used application
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UsedApplicationPK primaryKey) {
		_usedApplication.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the app ID of this used application.
	*
	* @return the app ID of this used application
	*/
	@Override
	public long getAppId() {
		return _usedApplication.getAppId();
	}

	/**
	* Sets the app ID of this used application.
	*
	* @param appId the app ID of this used application
	*/
	@Override
	public void setAppId(long appId) {
		_usedApplication.setAppId(appId);
	}

	/**
	* Returns the user ID of this used application.
	*
	* @return the user ID of this used application
	*/
	@Override
	public long getUserId() {
		return _usedApplication.getUserId();
	}

	/**
	* Sets the user ID of this used application.
	*
	* @param userId the user ID of this used application
	*/
	@Override
	public void setUserId(long userId) {
		_usedApplication.setUserId(userId);
	}

	/**
	* Returns the user uuid of this used application.
	*
	* @return the user uuid of this used application
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usedApplication.getUserUuid();
	}

	/**
	* Sets the user uuid of this used application.
	*
	* @param userUuid the user uuid of this used application
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_usedApplication.setUserUuid(userUuid);
	}

	@Override
	public boolean isNew() {
		return _usedApplication.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_usedApplication.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _usedApplication.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_usedApplication.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _usedApplication.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _usedApplication.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_usedApplication.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _usedApplication.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_usedApplication.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_usedApplication.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_usedApplication.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UsedApplicationWrapper((UsedApplication)_usedApplication.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.UsedApplication usedApplication) {
		return _usedApplication.compareTo(usedApplication);
	}

	@Override
	public int hashCode() {
		return _usedApplication.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.UsedApplication> toCacheModel() {
		return _usedApplication.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UsedApplication toEscapedModel() {
		return new UsedApplicationWrapper(_usedApplication.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UsedApplication toUnescapedModel() {
		return new UsedApplicationWrapper(_usedApplication.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _usedApplication.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _usedApplication.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_usedApplication.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UsedApplicationWrapper)) {
			return false;
		}

		UsedApplicationWrapper usedApplicationWrapper = (UsedApplicationWrapper)obj;

		if (Validator.equals(_usedApplication,
					usedApplicationWrapper._usedApplication)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UsedApplication getWrappedUsedApplication() {
		return _usedApplication;
	}

	@Override
	public UsedApplication getWrappedModel() {
		return _usedApplication;
	}

	@Override
	public void resetOriginalValues() {
		_usedApplication.resetOriginalValues();
	}

	private UsedApplication _usedApplication;
}