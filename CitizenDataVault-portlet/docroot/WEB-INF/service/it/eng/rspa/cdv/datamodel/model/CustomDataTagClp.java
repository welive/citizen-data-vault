/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.CustomDataTagLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class CustomDataTagClp extends BaseModelImpl<CustomDataTag>
	implements CustomDataTag {
	public CustomDataTagClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CustomDataTag.class;
	}

	@Override
	public String getModelClassName() {
		return CustomDataTag.class.getName();
	}

	@Override
	public CustomDataTagPK getPrimaryKey() {
		return new CustomDataTagPK(_entryId, _tagid);
	}

	@Override
	public void setPrimaryKey(CustomDataTagPK primaryKey) {
		setEntryId(primaryKey.entryId);
		setTagid(primaryKey.tagid);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new CustomDataTagPK(_entryId, _tagid);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((CustomDataTagPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("entryId", getEntryId());
		attributes.put("tagid", getTagid());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long entryId = (Long)attributes.get("entryId");

		if (entryId != null) {
			setEntryId(entryId);
		}

		Long tagid = (Long)attributes.get("tagid");

		if (tagid != null) {
			setTagid(tagid);
		}
	}

	@Override
	public long getEntryId() {
		return _entryId;
	}

	@Override
	public void setEntryId(long entryId) {
		_entryId = entryId;

		if (_customDataTagRemoteModel != null) {
			try {
				Class<?> clazz = _customDataTagRemoteModel.getClass();

				Method method = clazz.getMethod("setEntryId", long.class);

				method.invoke(_customDataTagRemoteModel, entryId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTagid() {
		return _tagid;
	}

	@Override
	public void setTagid(long tagid) {
		_tagid = tagid;

		if (_customDataTagRemoteModel != null) {
			try {
				Class<?> clazz = _customDataTagRemoteModel.getClass();

				Method method = clazz.getMethod("setTagid", long.class);

				method.invoke(_customDataTagRemoteModel, tagid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCustomDataTagRemoteModel() {
		return _customDataTagRemoteModel;
	}

	public void setCustomDataTagRemoteModel(
		BaseModel<?> customDataTagRemoteModel) {
		_customDataTagRemoteModel = customDataTagRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _customDataTagRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_customDataTagRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CustomDataTagLocalServiceUtil.addCustomDataTag(this);
		}
		else {
			CustomDataTagLocalServiceUtil.updateCustomDataTag(this);
		}
	}

	@Override
	public CustomDataTag toEscapedModel() {
		return (CustomDataTag)ProxyUtil.newProxyInstance(CustomDataTag.class.getClassLoader(),
			new Class[] { CustomDataTag.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CustomDataTagClp clone = new CustomDataTagClp();

		clone.setEntryId(getEntryId());
		clone.setTagid(getTagid());

		return clone;
	}

	@Override
	public int compareTo(CustomDataTag customDataTag) {
		CustomDataTagPK primaryKey = customDataTag.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CustomDataTagClp)) {
			return false;
		}

		CustomDataTagClp customDataTag = (CustomDataTagClp)obj;

		CustomDataTagPK primaryKey = customDataTag.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{entryId=");
		sb.append(getEntryId());
		sb.append(", tagid=");
		sb.append(getTagid());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.CustomDataTag");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>entryId</column-name><column-value><![CDATA[");
		sb.append(getEntryId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tagid</column-name><column-value><![CDATA[");
		sb.append(getTagid());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _entryId;
	private long _tagid;
	private BaseModel<?> _customDataTagRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}