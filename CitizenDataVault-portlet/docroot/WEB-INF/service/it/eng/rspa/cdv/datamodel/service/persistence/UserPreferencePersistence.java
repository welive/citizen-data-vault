/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.UserPreference;

/**
 * The persistence interface for the user preference service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserPreferencePersistenceImpl
 * @see UserPreferenceUtil
 * @generated
 */
public interface UserPreferencePersistence extends BasePersistence<UserPreference> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserPreferenceUtil} to access the user preference persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the user preferences where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user preferences where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user preferences
	* @param end the upper bound of the range of user preferences (not inclusive)
	* @return the range of matching user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user preferences where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user preferences
	* @param end the upper bound of the range of user preferences (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user preference in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user preference
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a matching user preference could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;

	/**
	* Returns the first user preference in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user preference, or <code>null</code> if a matching user preference could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user preference in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user preference
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a matching user preference could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;

	/**
	* Returns the last user preference in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user preference, or <code>null</code> if a matching user preference could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user preferences before and after the current user preference in the ordered set where userId = &#63;.
	*
	* @param userPreferencePK the primary key of the current user preference
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user preference
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference[] findByuserId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK userPreferencePK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;

	/**
	* Removes all the user preferences where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user preferences where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user preferences
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user preferences where preferenceId = &#63;.
	*
	* @param preferenceId the preference ID
	* @return the matching user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findBypreferenceId(
		long preferenceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user preferences where preferenceId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param preferenceId the preference ID
	* @param start the lower bound of the range of user preferences
	* @param end the upper bound of the range of user preferences (not inclusive)
	* @return the range of matching user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findBypreferenceId(
		long preferenceId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user preferences where preferenceId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param preferenceId the preference ID
	* @param start the lower bound of the range of user preferences
	* @param end the upper bound of the range of user preferences (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findBypreferenceId(
		long preferenceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user preference in the ordered set where preferenceId = &#63;.
	*
	* @param preferenceId the preference ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user preference
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a matching user preference could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference findBypreferenceId_First(
		long preferenceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;

	/**
	* Returns the first user preference in the ordered set where preferenceId = &#63;.
	*
	* @param preferenceId the preference ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user preference, or <code>null</code> if a matching user preference could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference fetchBypreferenceId_First(
		long preferenceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user preference in the ordered set where preferenceId = &#63;.
	*
	* @param preferenceId the preference ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user preference
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a matching user preference could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference findBypreferenceId_Last(
		long preferenceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;

	/**
	* Returns the last user preference in the ordered set where preferenceId = &#63;.
	*
	* @param preferenceId the preference ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user preference, or <code>null</code> if a matching user preference could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference fetchBypreferenceId_Last(
		long preferenceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user preferences before and after the current user preference in the ordered set where preferenceId = &#63;.
	*
	* @param userPreferencePK the primary key of the current user preference
	* @param preferenceId the preference ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user preference
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference[] findBypreferenceId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK userPreferencePK,
		long preferenceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;

	/**
	* Removes all the user preferences where preferenceId = &#63; from the database.
	*
	* @param preferenceId the preference ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBypreferenceId(long preferenceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user preferences where preferenceId = &#63;.
	*
	* @param preferenceId the preference ID
	* @return the number of matching user preferences
	* @throws SystemException if a system exception occurred
	*/
	public int countBypreferenceId(long preferenceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the user preference in the entity cache if it is enabled.
	*
	* @param userPreference the user preference
	*/
	public void cacheResult(
		it.eng.rspa.cdv.datamodel.model.UserPreference userPreference);

	/**
	* Caches the user preferences in the entity cache if it is enabled.
	*
	* @param userPreferences the user preferences
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> userPreferences);

	/**
	* Creates a new user preference with the primary key. Does not add the user preference to the database.
	*
	* @param userPreferencePK the primary key for the new user preference
	* @return the new user preference
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference create(
		it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK userPreferencePK);

	/**
	* Removes the user preference with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userPreferencePK the primary key of the user preference
	* @return the user preference that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference remove(
		it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK userPreferencePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;

	public it.eng.rspa.cdv.datamodel.model.UserPreference updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserPreference userPreference)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user preference with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException} if it could not be found.
	*
	* @param userPreferencePK the primary key of the user preference
	* @return the user preference
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException if a user preference with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK userPreferencePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException;

	/**
	* Returns the user preference with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userPreferencePK the primary key of the user preference
	* @return the user preference, or <code>null</code> if a user preference with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserPreference fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK userPreferencePK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user preferences.
	*
	* @return the user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user preferences.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user preferences
	* @param end the upper bound of the range of user preferences (not inclusive)
	* @return the range of user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user preferences.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user preferences
	* @param end the upper bound of the range of user preferences (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user preferences
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserPreference> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user preferences from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user preferences.
	*
	* @return the number of user preferences
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}