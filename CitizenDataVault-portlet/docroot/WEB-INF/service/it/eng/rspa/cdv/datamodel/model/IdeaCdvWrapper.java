/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link IdeaCdv}.
 * </p>
 *
 * @author Engineering
 * @see IdeaCdv
 * @generated
 */
public class IdeaCdvWrapper implements IdeaCdv, ModelWrapper<IdeaCdv> {
	public IdeaCdvWrapper(IdeaCdv ideaCdv) {
		_ideaCdv = ideaCdv;
	}

	@Override
	public Class<?> getModelClass() {
		return IdeaCdv.class;
	}

	@Override
	public String getModelClassName() {
		return IdeaCdv.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("ideaName", getIdeaName());
		attributes.put("need", getNeed());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String ideaName = (String)attributes.get("ideaName");

		if (ideaName != null) {
			setIdeaName(ideaName);
		}

		Boolean need = (Boolean)attributes.get("need");

		if (need != null) {
			setNeed(need);
		}
	}

	/**
	* Returns the primary key of this idea cdv.
	*
	* @return the primary key of this idea cdv
	*/
	@Override
	public long getPrimaryKey() {
		return _ideaCdv.getPrimaryKey();
	}

	/**
	* Sets the primary key of this idea cdv.
	*
	* @param primaryKey the primary key of this idea cdv
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_ideaCdv.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the idea ID of this idea cdv.
	*
	* @return the idea ID of this idea cdv
	*/
	@Override
	public long getIdeaId() {
		return _ideaCdv.getIdeaId();
	}

	/**
	* Sets the idea ID of this idea cdv.
	*
	* @param ideaId the idea ID of this idea cdv
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_ideaCdv.setIdeaId(ideaId);
	}

	/**
	* Returns the idea name of this idea cdv.
	*
	* @return the idea name of this idea cdv
	*/
	@Override
	public java.lang.String getIdeaName() {
		return _ideaCdv.getIdeaName();
	}

	/**
	* Sets the idea name of this idea cdv.
	*
	* @param ideaName the idea name of this idea cdv
	*/
	@Override
	public void setIdeaName(java.lang.String ideaName) {
		_ideaCdv.setIdeaName(ideaName);
	}

	/**
	* Returns the need of this idea cdv.
	*
	* @return the need of this idea cdv
	*/
	@Override
	public boolean getNeed() {
		return _ideaCdv.getNeed();
	}

	/**
	* Returns <code>true</code> if this idea cdv is need.
	*
	* @return <code>true</code> if this idea cdv is need; <code>false</code> otherwise
	*/
	@Override
	public boolean isNeed() {
		return _ideaCdv.isNeed();
	}

	/**
	* Sets whether this idea cdv is need.
	*
	* @param need the need of this idea cdv
	*/
	@Override
	public void setNeed(boolean need) {
		_ideaCdv.setNeed(need);
	}

	@Override
	public boolean isNew() {
		return _ideaCdv.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_ideaCdv.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _ideaCdv.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ideaCdv.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _ideaCdv.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _ideaCdv.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_ideaCdv.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _ideaCdv.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_ideaCdv.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_ideaCdv.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_ideaCdv.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new IdeaCdvWrapper((IdeaCdv)_ideaCdv.clone());
	}

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv) {
		return _ideaCdv.compareTo(ideaCdv);
	}

	@Override
	public int hashCode() {
		return _ideaCdv.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.IdeaCdv> toCacheModel() {
		return _ideaCdv.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv toEscapedModel() {
		return new IdeaCdvWrapper(_ideaCdv.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv toUnescapedModel() {
		return new IdeaCdvWrapper(_ideaCdv.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _ideaCdv.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ideaCdv.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_ideaCdv.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeaCdvWrapper)) {
			return false;
		}

		IdeaCdvWrapper ideaCdvWrapper = (IdeaCdvWrapper)obj;

		if (Validator.equals(_ideaCdv, ideaCdvWrapper._ideaCdv)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public IdeaCdv getWrappedIdeaCdv() {
		return _ideaCdv;
	}

	@Override
	public IdeaCdv getWrappedModel() {
		return _ideaCdv;
	}

	@Override
	public void resetOriginalValues() {
		_ideaCdv.resetOriginalValues();
	}

	private IdeaCdv _ideaCdv;
}