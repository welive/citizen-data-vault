/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for UserIdea. This utility wraps
 * {@link it.eng.rspa.cdv.datamodel.service.impl.UserIdeaLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering
 * @see UserIdeaLocalService
 * @see it.eng.rspa.cdv.datamodel.service.base.UserIdeaLocalServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.impl.UserIdeaLocalServiceImpl
 * @generated
 */
public class UserIdeaLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.cdv.datamodel.service.impl.UserIdeaLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the user idea to the database. Also notifies the appropriate model listeners.
	*
	* @param userIdea the user idea
	* @return the user idea that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea addUserIdea(
		it.eng.rspa.cdv.datamodel.model.UserIdea userIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addUserIdea(userIdea);
	}

	/**
	* Creates a new user idea with the primary key. Does not add the user idea to the database.
	*
	* @param userIdeaPK the primary key for the new user idea
	* @return the new user idea
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea createUserIdea(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK) {
		return getService().createUserIdea(userIdeaPK);
	}

	/**
	* Deletes the user idea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userIdeaPK the primary key of the user idea
	* @return the user idea that was removed
	* @throws PortalException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea deleteUserIdea(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteUserIdea(userIdeaPK);
	}

	/**
	* Deletes the user idea from the database. Also notifies the appropriate model listeners.
	*
	* @param userIdea the user idea
	* @return the user idea that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea deleteUserIdea(
		it.eng.rspa.cdv.datamodel.model.UserIdea userIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteUserIdea(userIdea);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.cdv.datamodel.model.UserIdea fetchUserIdea(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchUserIdea(userIdeaPK);
	}

	/**
	* Returns the user idea with the primary key.
	*
	* @param userIdeaPK the primary key of the user idea
	* @return the user idea
	* @throws PortalException if a user idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea getUserIdea(
		it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK userIdeaPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getUserIdea(userIdeaPK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the user ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user ideas
	* @param end the upper bound of the range of user ideas (not inclusive)
	* @return the range of user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> getUserIdeas(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getUserIdeas(start, end);
	}

	/**
	* Returns the number of user ideas.
	*
	* @return the number of user ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int getUserIdeasCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getUserIdeasCount();
	}

	/**
	* Updates the user idea in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userIdea the user idea
	* @return the user idea that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.UserIdea updateUserIdea(
		it.eng.rspa.cdv.datamodel.model.UserIdea userIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateUserIdea(userIdea);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> getUserIdeaByUserId(
		long userid) {
		return getService().getUserIdeaByUserId(userid);
	}

	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> getUserIdeaByUserIdAndRole(
		long userid, java.lang.String role) {
		return getService().getUserIdeaByUserIdAndRole(userid, role);
	}

	public static java.util.List<it.eng.rspa.cdv.datamodel.model.UserIdea> getUserIdeaByIdeaId(
		long ideaid) {
		return getService().getUserIdeaByIdeaId(ideaid);
	}

	public static void clearService() {
		_service = null;
	}

	public static UserIdeaLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					UserIdeaLocalService.class.getName());

			if (invokableLocalService instanceof UserIdeaLocalService) {
				_service = (UserIdeaLocalService)invokableLocalService;
			}
			else {
				_service = new UserIdeaLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(UserIdeaLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(UserIdeaLocalService service) {
	}

	private static UserIdeaLocalService _service;
}