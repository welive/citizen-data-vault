/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IdeaCdvLocalService}.
 *
 * @author Engineering
 * @see IdeaCdvLocalService
 * @generated
 */
public class IdeaCdvLocalServiceWrapper implements IdeaCdvLocalService,
	ServiceWrapper<IdeaCdvLocalService> {
	public IdeaCdvLocalServiceWrapper(IdeaCdvLocalService ideaCdvLocalService) {
		_ideaCdvLocalService = ideaCdvLocalService;
	}

	/**
	* Adds the idea cdv to the database. Also notifies the appropriate model listeners.
	*
	* @param ideaCdv the idea cdv
	* @return the idea cdv that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv addIdeaCdv(
		it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.addIdeaCdv(ideaCdv);
	}

	/**
	* Creates a new idea cdv with the primary key. Does not add the idea cdv to the database.
	*
	* @param ideaId the primary key for the new idea cdv
	* @return the new idea cdv
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv createIdeaCdv(long ideaId) {
		return _ideaCdvLocalService.createIdeaCdv(ideaId);
	}

	/**
	* Deletes the idea cdv with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaId the primary key of the idea cdv
	* @return the idea cdv that was removed
	* @throws PortalException if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv deleteIdeaCdv(long ideaId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.deleteIdeaCdv(ideaId);
	}

	/**
	* Deletes the idea cdv from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaCdv the idea cdv
	* @return the idea cdv that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv deleteIdeaCdv(
		it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.deleteIdeaCdv(ideaCdv);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ideaCdvLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv fetchIdeaCdv(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.fetchIdeaCdv(ideaId);
	}

	/**
	* Returns the idea cdv with the primary key.
	*
	* @param ideaId the primary key of the idea cdv
	* @return the idea cdv
	* @throws PortalException if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv getIdeaCdv(long ideaId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.getIdeaCdv(ideaId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the idea cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @return the range of idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> getIdeaCdvs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.getIdeaCdvs(start, end);
	}

	/**
	* Returns the number of idea cdvs.
	*
	* @return the number of idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getIdeaCdvsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.getIdeaCdvsCount();
	}

	/**
	* Updates the idea cdv in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ideaCdv the idea cdv
	* @return the idea cdv that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv updateIdeaCdv(
		it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaCdvLocalService.updateIdeaCdv(ideaCdv);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _ideaCdvLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_ideaCdvLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _ideaCdvLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public IdeaCdvLocalService getWrappedIdeaCdvLocalService() {
		return _ideaCdvLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedIdeaCdvLocalService(
		IdeaCdvLocalService ideaCdvLocalService) {
		_ideaCdvLocalService = ideaCdvLocalService;
	}

	@Override
	public IdeaCdvLocalService getWrappedService() {
		return _ideaCdvLocalService;
	}

	@Override
	public void setWrappedService(IdeaCdvLocalService ideaCdvLocalService) {
		_ideaCdvLocalService = ideaCdvLocalService;
	}

	private IdeaCdvLocalService _ideaCdvLocalService;
}