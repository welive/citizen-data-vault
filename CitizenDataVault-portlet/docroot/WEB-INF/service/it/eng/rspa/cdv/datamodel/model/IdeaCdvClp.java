/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.IdeaCdvLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class IdeaCdvClp extends BaseModelImpl<IdeaCdv> implements IdeaCdv {
	public IdeaCdvClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return IdeaCdv.class;
	}

	@Override
	public String getModelClassName() {
		return IdeaCdv.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _ideaId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdeaId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ideaId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("ideaName", getIdeaName());
		attributes.put("need", getNeed());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String ideaName = (String)attributes.get("ideaName");

		if (ideaName != null) {
			setIdeaName(ideaName);
		}

		Boolean need = (Boolean)attributes.get("need");

		if (need != null) {
			setNeed(need);
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_ideaCdvRemoteModel != null) {
			try {
				Class<?> clazz = _ideaCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_ideaCdvRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdeaName() {
		return _ideaName;
	}

	@Override
	public void setIdeaName(String ideaName) {
		_ideaName = ideaName;

		if (_ideaCdvRemoteModel != null) {
			try {
				Class<?> clazz = _ideaCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaName", String.class);

				method.invoke(_ideaCdvRemoteModel, ideaName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getNeed() {
		return _need;
	}

	@Override
	public boolean isNeed() {
		return _need;
	}

	@Override
	public void setNeed(boolean need) {
		_need = need;

		if (_ideaCdvRemoteModel != null) {
			try {
				Class<?> clazz = _ideaCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setNeed", boolean.class);

				method.invoke(_ideaCdvRemoteModel, need);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getIdeaCdvRemoteModel() {
		return _ideaCdvRemoteModel;
	}

	public void setIdeaCdvRemoteModel(BaseModel<?> ideaCdvRemoteModel) {
		_ideaCdvRemoteModel = ideaCdvRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _ideaCdvRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_ideaCdvRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			IdeaCdvLocalServiceUtil.addIdeaCdv(this);
		}
		else {
			IdeaCdvLocalServiceUtil.updateIdeaCdv(this);
		}
	}

	@Override
	public IdeaCdv toEscapedModel() {
		return (IdeaCdv)ProxyUtil.newProxyInstance(IdeaCdv.class.getClassLoader(),
			new Class[] { IdeaCdv.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		IdeaCdvClp clone = new IdeaCdvClp();

		clone.setIdeaId(getIdeaId());
		clone.setIdeaName(getIdeaName());
		clone.setNeed(getNeed());

		return clone;
	}

	@Override
	public int compareTo(IdeaCdv ideaCdv) {
		long primaryKey = ideaCdv.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeaCdvClp)) {
			return false;
		}

		IdeaCdvClp ideaCdv = (IdeaCdvClp)obj;

		long primaryKey = ideaCdv.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{ideaId=");
		sb.append(getIdeaId());
		sb.append(", ideaName=");
		sb.append(getIdeaName());
		sb.append(", need=");
		sb.append(getNeed());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.IdeaCdv");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaName</column-name><column-value><![CDATA[");
		sb.append(getIdeaName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>need</column-name><column-value><![CDATA[");
		sb.append(getNeed());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _ideaId;
	private String _ideaName;
	private boolean _need;
	private BaseModel<?> _ideaCdvRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}