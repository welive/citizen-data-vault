/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class CustomDataTagSoap implements Serializable {
	public static CustomDataTagSoap toSoapModel(CustomDataTag model) {
		CustomDataTagSoap soapModel = new CustomDataTagSoap();

		soapModel.setEntryId(model.getEntryId());
		soapModel.setTagid(model.getTagid());

		return soapModel;
	}

	public static CustomDataTagSoap[] toSoapModels(CustomDataTag[] models) {
		CustomDataTagSoap[] soapModels = new CustomDataTagSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CustomDataTagSoap[][] toSoapModels(CustomDataTag[][] models) {
		CustomDataTagSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CustomDataTagSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CustomDataTagSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CustomDataTagSoap[] toSoapModels(List<CustomDataTag> models) {
		List<CustomDataTagSoap> soapModels = new ArrayList<CustomDataTagSoap>(models.size());

		for (CustomDataTag model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CustomDataTagSoap[soapModels.size()]);
	}

	public CustomDataTagSoap() {
	}

	public CustomDataTagPK getPrimaryKey() {
		return new CustomDataTagPK(_entryId, _tagid);
	}

	public void setPrimaryKey(CustomDataTagPK pk) {
		setEntryId(pk.entryId);
		setTagid(pk.tagid);
	}

	public long getEntryId() {
		return _entryId;
	}

	public void setEntryId(long entryId) {
		_entryId = entryId;
	}

	public long getTagid() {
		return _tagid;
	}

	public void setTagid(long tagid) {
		_tagid = tagid;
	}

	private long _entryId;
	private long _tagid;
}