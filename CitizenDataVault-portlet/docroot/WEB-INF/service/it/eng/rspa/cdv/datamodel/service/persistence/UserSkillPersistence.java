/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.UserSkill;

/**
 * The persistence interface for the user skill service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserSkillPersistenceImpl
 * @see UserSkillUtil
 * @generated
 */
public interface UserSkillPersistence extends BasePersistence<UserSkill> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserSkillUtil} to access the user skill persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the user skills where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @return the matching user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findByskillId(
		long skillId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user skills where skillId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillId the skill ID
	* @param start the lower bound of the range of user skills
	* @param end the upper bound of the range of user skills (not inclusive)
	* @return the range of matching user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findByskillId(
		long skillId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user skills where skillId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param skillId the skill ID
	* @param start the lower bound of the range of user skills
	* @param end the upper bound of the range of user skills (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findByskillId(
		long skillId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user skill in the ordered set where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user skill
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a matching user skill could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill findByskillId_First(
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;

	/**
	* Returns the first user skill in the ordered set where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user skill, or <code>null</code> if a matching user skill could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill fetchByskillId_First(
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user skill in the ordered set where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user skill
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a matching user skill could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill findByskillId_Last(
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;

	/**
	* Returns the last user skill in the ordered set where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user skill, or <code>null</code> if a matching user skill could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill fetchByskillId_Last(
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user skills before and after the current user skill in the ordered set where skillId = &#63;.
	*
	* @param userSkillPK the primary key of the current user skill
	* @param skillId the skill ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user skill
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill[] findByskillId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK userSkillPK,
		long skillId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;

	/**
	* Removes all the user skills where skillId = &#63; from the database.
	*
	* @param skillId the skill ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByskillId(long skillId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user skills where skillId = &#63;.
	*
	* @param skillId the skill ID
	* @return the number of matching user skills
	* @throws SystemException if a system exception occurred
	*/
	public int countByskillId(long skillId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user skills where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user skills where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user skills
	* @param end the upper bound of the range of user skills (not inclusive)
	* @return the range of matching user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user skills where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user skills
	* @param end the upper bound of the range of user skills (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user skill in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user skill
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a matching user skill could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;

	/**
	* Returns the first user skill in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user skill, or <code>null</code> if a matching user skill could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user skill in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user skill
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a matching user skill could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;

	/**
	* Returns the last user skill in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user skill, or <code>null</code> if a matching user skill could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user skills before and after the current user skill in the ordered set where userId = &#63;.
	*
	* @param userSkillPK the primary key of the current user skill
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user skill
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill[] findByuserId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK userSkillPK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;

	/**
	* Removes all the user skills where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user skills where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user skills
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the user skill in the entity cache if it is enabled.
	*
	* @param userSkill the user skill
	*/
	public void cacheResult(it.eng.rspa.cdv.datamodel.model.UserSkill userSkill);

	/**
	* Caches the user skills in the entity cache if it is enabled.
	*
	* @param userSkills the user skills
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> userSkills);

	/**
	* Creates a new user skill with the primary key. Does not add the user skill to the database.
	*
	* @param userSkillPK the primary key for the new user skill
	* @return the new user skill
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill create(
		it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK userSkillPK);

	/**
	* Removes the user skill with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userSkillPK the primary key of the user skill
	* @return the user skill that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill remove(
		it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK userSkillPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;

	public it.eng.rspa.cdv.datamodel.model.UserSkill updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserSkill userSkill)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user skill with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserSkillException} if it could not be found.
	*
	* @param userSkillPK the primary key of the user skill
	* @return the user skill
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserSkillException if a user skill with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK userSkillPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserSkillException;

	/**
	* Returns the user skill with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userSkillPK the primary key of the user skill
	* @return the user skill, or <code>null</code> if a user skill with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserSkill fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK userSkillPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user skills.
	*
	* @return the user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user skills.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user skills
	* @param end the upper bound of the range of user skills (not inclusive)
	* @return the range of user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user skills.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserSkillModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user skills
	* @param end the upper bound of the range of user skills (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user skills
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserSkill> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user skills from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user skills.
	*
	* @return the number of user skills
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}