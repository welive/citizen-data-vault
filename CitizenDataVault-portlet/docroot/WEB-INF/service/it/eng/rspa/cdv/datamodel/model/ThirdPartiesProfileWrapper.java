/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ThirdPartiesProfile}.
 * </p>
 *
 * @author Engineering
 * @see ThirdPartiesProfile
 * @generated
 */
public class ThirdPartiesProfileWrapper implements ThirdPartiesProfile,
	ModelWrapper<ThirdPartiesProfile> {
	public ThirdPartiesProfileWrapper(ThirdPartiesProfile thirdPartiesProfile) {
		_thirdPartiesProfile = thirdPartiesProfile;
	}

	@Override
	public Class<?> getModelClass() {
		return ThirdPartiesProfile.class;
	}

	@Override
	public String getModelClassName() {
		return ThirdPartiesProfile.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("thirdPartyId", getThirdPartyId());
		attributes.put("key", getKey());
		attributes.put("value", getValue());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long thirdPartyId = (Long)attributes.get("thirdPartyId");

		if (thirdPartyId != null) {
			setThirdPartyId(thirdPartyId);
		}

		String key = (String)attributes.get("key");

		if (key != null) {
			setKey(key);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}
	}

	/**
	* Returns the primary key of this third parties profile.
	*
	* @return the primary key of this third parties profile
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK getPrimaryKey() {
		return _thirdPartiesProfile.getPrimaryKey();
	}

	/**
	* Sets the primary key of this third parties profile.
	*
	* @param primaryKey the primary key of this third parties profile
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.ThirdPartiesProfilePK primaryKey) {
		_thirdPartiesProfile.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the third party ID of this third parties profile.
	*
	* @return the third party ID of this third parties profile
	*/
	@Override
	public long getThirdPartyId() {
		return _thirdPartiesProfile.getThirdPartyId();
	}

	/**
	* Sets the third party ID of this third parties profile.
	*
	* @param thirdPartyId the third party ID of this third parties profile
	*/
	@Override
	public void setThirdPartyId(long thirdPartyId) {
		_thirdPartiesProfile.setThirdPartyId(thirdPartyId);
	}

	/**
	* Returns the key of this third parties profile.
	*
	* @return the key of this third parties profile
	*/
	@Override
	public java.lang.String getKey() {
		return _thirdPartiesProfile.getKey();
	}

	/**
	* Sets the key of this third parties profile.
	*
	* @param key the key of this third parties profile
	*/
	@Override
	public void setKey(java.lang.String key) {
		_thirdPartiesProfile.setKey(key);
	}

	/**
	* Returns the value of this third parties profile.
	*
	* @return the value of this third parties profile
	*/
	@Override
	public java.lang.String getValue() {
		return _thirdPartiesProfile.getValue();
	}

	/**
	* Sets the value of this third parties profile.
	*
	* @param value the value of this third parties profile
	*/
	@Override
	public void setValue(java.lang.String value) {
		_thirdPartiesProfile.setValue(value);
	}

	@Override
	public boolean isNew() {
		return _thirdPartiesProfile.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_thirdPartiesProfile.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _thirdPartiesProfile.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_thirdPartiesProfile.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _thirdPartiesProfile.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _thirdPartiesProfile.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_thirdPartiesProfile.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _thirdPartiesProfile.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_thirdPartiesProfile.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_thirdPartiesProfile.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_thirdPartiesProfile.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ThirdPartiesProfileWrapper((ThirdPartiesProfile)_thirdPartiesProfile.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile thirdPartiesProfile) {
		return _thirdPartiesProfile.compareTo(thirdPartiesProfile);
	}

	@Override
	public int hashCode() {
		return _thirdPartiesProfile.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile> toCacheModel() {
		return _thirdPartiesProfile.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile toEscapedModel() {
		return new ThirdPartiesProfileWrapper(_thirdPartiesProfile.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfile toUnescapedModel() {
		return new ThirdPartiesProfileWrapper(_thirdPartiesProfile.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _thirdPartiesProfile.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _thirdPartiesProfile.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_thirdPartiesProfile.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ThirdPartiesProfileWrapper)) {
			return false;
		}

		ThirdPartiesProfileWrapper thirdPartiesProfileWrapper = (ThirdPartiesProfileWrapper)obj;

		if (Validator.equals(_thirdPartiesProfile,
					thirdPartiesProfileWrapper._thirdPartiesProfile)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ThirdPartiesProfile getWrappedThirdPartiesProfile() {
		return _thirdPartiesProfile;
	}

	@Override
	public ThirdPartiesProfile getWrappedModel() {
		return _thirdPartiesProfile;
	}

	@Override
	public void resetOriginalValues() {
		_thirdPartiesProfile.resetOriginalValues();
	}

	private ThirdPartiesProfile _thirdPartiesProfile;
}