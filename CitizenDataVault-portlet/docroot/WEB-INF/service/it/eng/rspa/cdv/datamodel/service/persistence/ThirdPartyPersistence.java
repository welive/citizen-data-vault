/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.ThirdParty;

/**
 * The persistence interface for the third party service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see ThirdPartyPersistenceImpl
 * @see ThirdPartyUtil
 * @generated
 */
public interface ThirdPartyPersistence extends BasePersistence<ThirdParty> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ThirdPartyUtil} to access the third party persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the third party in the entity cache if it is enabled.
	*
	* @param thirdParty the third party
	*/
	public void cacheResult(
		it.eng.rspa.cdv.datamodel.model.ThirdParty thirdParty);

	/**
	* Caches the third parties in the entity cache if it is enabled.
	*
	* @param thirdParties the third parties
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdParty> thirdParties);

	/**
	* Creates a new third party with the primary key. Does not add the third party to the database.
	*
	* @param thirdPartyId the primary key for the new third party
	* @return the new third party
	*/
	public it.eng.rspa.cdv.datamodel.model.ThirdParty create(long thirdPartyId);

	/**
	* Removes the third party with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param thirdPartyId the primary key of the third party
	* @return the third party that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException if a third party with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.ThirdParty remove(long thirdPartyId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException;

	public it.eng.rspa.cdv.datamodel.model.ThirdParty updateImpl(
		it.eng.rspa.cdv.datamodel.model.ThirdParty thirdParty)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the third party with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException} if it could not be found.
	*
	* @param thirdPartyId the primary key of the third party
	* @return the third party
	* @throws it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException if a third party with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.ThirdParty findByPrimaryKey(
		long thirdPartyId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException;

	/**
	* Returns the third party with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param thirdPartyId the primary key of the third party
	* @return the third party, or <code>null</code> if a third party with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.ThirdParty fetchByPrimaryKey(
		long thirdPartyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the third parties.
	*
	* @return the third parties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdParty> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the third parties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of third parties
	* @param end the upper bound of the range of third parties (not inclusive)
	* @return the range of third parties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdParty> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the third parties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.ThirdPartyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of third parties
	* @param end the upper bound of the range of third parties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of third parties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.ThirdParty> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the third parties from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of third parties.
	*
	* @return the number of third parties
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}