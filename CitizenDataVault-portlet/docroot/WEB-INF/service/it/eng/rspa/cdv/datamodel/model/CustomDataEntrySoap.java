/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class CustomDataEntrySoap implements Serializable {
	public static CustomDataEntrySoap toSoapModel(CustomDataEntry model) {
		CustomDataEntrySoap soapModel = new CustomDataEntrySoap();

		soapModel.setEntryId(model.getEntryId());
		soapModel.setKey(model.getKey());
		soapModel.setValue(model.getValue());
		soapModel.setType(model.getType());
		soapModel.setUserid(model.getUserid());
		soapModel.setClientid(model.getClientid());

		return soapModel;
	}

	public static CustomDataEntrySoap[] toSoapModels(CustomDataEntry[] models) {
		CustomDataEntrySoap[] soapModels = new CustomDataEntrySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CustomDataEntrySoap[][] toSoapModels(
		CustomDataEntry[][] models) {
		CustomDataEntrySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CustomDataEntrySoap[models.length][models[0].length];
		}
		else {
			soapModels = new CustomDataEntrySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CustomDataEntrySoap[] toSoapModels(
		List<CustomDataEntry> models) {
		List<CustomDataEntrySoap> soapModels = new ArrayList<CustomDataEntrySoap>(models.size());

		for (CustomDataEntry model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CustomDataEntrySoap[soapModels.size()]);
	}

	public CustomDataEntrySoap() {
	}

	public long getPrimaryKey() {
		return _entryId;
	}

	public void setPrimaryKey(long pk) {
		setEntryId(pk);
	}

	public long getEntryId() {
		return _entryId;
	}

	public void setEntryId(long entryId) {
		_entryId = entryId;
	}

	public String getKey() {
		return _key;
	}

	public void setKey(String key) {
		_key = key;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	public long getUserid() {
		return _userid;
	}

	public void setUserid(long userid) {
		_userid = userid;
	}

	public String getClientid() {
		return _clientid;
	}

	public void setClientid(String clientid) {
		_clientid = clientid;
	}

	private long _entryId;
	private String _key;
	private String _value;
	private String _type;
	private long _userid;
	private String _clientid;
}