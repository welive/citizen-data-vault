/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for RestApi. This utility wraps
 * {@link it.eng.rspa.cdv.datamodel.service.impl.RestApiServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Engineering
 * @see RestApiService
 * @see it.eng.rspa.cdv.datamodel.service.base.RestApiServiceBaseImpl
 * @see it.eng.rspa.cdv.datamodel.service.impl.RestApiServiceImpl
 * @generated
 */
public class RestApiServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.cdv.datamodel.service.impl.RestApiServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* DataSource Oriented
	*/
	public static com.liferay.portal.kernel.json.JSONObject addUser(
		java.lang.String body) {
		return getService().addUser(body);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateUser(
		java.lang.String body) {
		return getService().updateUser(body);
	}

	public static com.liferay.portal.kernel.json.JSONObject push(
		java.lang.String body) {
		return getService().push(body);
	}

	/**
	* DataConsumer Oriented *
	*/
	public static com.liferay.portal.kernel.json.JSONArray getUsersSkills() {
		return getService().getUsersSkills();
	}

	public static com.liferay.portal.kernel.json.JSONObject getUserMetadata(
		java.lang.String ccuserid) {
		return getService().getUserMetadata(ccuserid);
	}

	public static com.liferay.portal.kernel.json.JSONObject getUserProfile(
		java.lang.String uid) {
		return getService().getUserProfile(uid);
	}

	/**
	* Statistics Oriented *
	*/
	public static com.liferay.portal.kernel.json.JSONObject getKPI11(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return getService().getKPI11(pilotid, from, to);
	}

	public static com.liferay.portal.kernel.json.JSONObject getKPI43(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return getService().getKPI43(pilotid, from, to);
	}

	public static com.liferay.portal.kernel.json.JSONObject getKPI111(
		java.lang.String pilotid) {
		return getService().getKPI111(pilotid);
	}

	public static com.liferay.portal.kernel.json.JSONObject getKPI112(
		java.lang.String pilotid) {
		return getService().getKPI112(pilotid);
	}

	public static com.liferay.portal.kernel.json.JSONObject getKPI113(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return getService().getKPI113(pilotid, from, to);
	}

	public static com.liferay.portal.kernel.json.JSONObject getKPI114(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return getService().getKPI114(pilotid, from, to);
	}

	public static com.liferay.portal.kernel.json.JSONObject getKPI115(
		java.lang.String pilotid, java.lang.String from, java.lang.String to) {
		return getService().getKPI115(pilotid, from, to);
	}

	/**
	* Data Oriented *
	*/
	public static com.liferay.portal.kernel.json.JSONArray userDataUpdate(
		java.lang.String body, java.lang.String uid, java.lang.String client_id) {
		return getService().userDataUpdate(body, uid, client_id);
	}

	public static com.liferay.portal.kernel.json.JSONArray getUserData(
		java.lang.String dataid, java.lang.String uid,
		java.lang.String client_id) {
		return getService().getUserData(dataid, uid, client_id);
	}

	public static com.liferay.portal.kernel.json.JSONArray getUserDataSearchResult(
		java.lang.String tags, java.lang.String uid, java.lang.String client_id) {
		return getService().getUserDataSearchResult(tags, uid, client_id);
	}

	public static com.liferay.portal.kernel.json.JSONArray deleteUserData(
		java.lang.String dataid, java.lang.String uid,
		java.lang.String client_id) {
		return getService().deleteUserData(dataid, uid, client_id);
	}

	public static void clearService() {
		_service = null;
	}

	public static RestApiService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					RestApiService.class.getName());

			if (invokableService instanceof RestApiService) {
				_service = (RestApiService)invokableService;
			}
			else {
				_service = new RestApiServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(RestApiServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(RestApiService service) {
	}

	private static RestApiService _service;
}