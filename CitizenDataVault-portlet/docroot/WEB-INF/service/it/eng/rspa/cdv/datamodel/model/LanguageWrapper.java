/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Language}.
 * </p>
 *
 * @author Engineering
 * @see Language
 * @generated
 */
public class LanguageWrapper implements Language, ModelWrapper<Language> {
	public LanguageWrapper(Language language) {
		_language = language;
	}

	@Override
	public Class<?> getModelClass() {
		return Language.class;
	}

	@Override
	public String getModelClassName() {
		return Language.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("languageId", getLanguageId());
		attributes.put("name", getName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long languageId = (Long)attributes.get("languageId");

		if (languageId != null) {
			setLanguageId(languageId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}
	}

	/**
	* Returns the primary key of this language.
	*
	* @return the primary key of this language
	*/
	@Override
	public long getPrimaryKey() {
		return _language.getPrimaryKey();
	}

	/**
	* Sets the primary key of this language.
	*
	* @param primaryKey the primary key of this language
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_language.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the language ID of this language.
	*
	* @return the language ID of this language
	*/
	@Override
	public long getLanguageId() {
		return _language.getLanguageId();
	}

	/**
	* Sets the language ID of this language.
	*
	* @param languageId the language ID of this language
	*/
	@Override
	public void setLanguageId(long languageId) {
		_language.setLanguageId(languageId);
	}

	/**
	* Returns the name of this language.
	*
	* @return the name of this language
	*/
	@Override
	public java.lang.String getName() {
		return _language.getName();
	}

	/**
	* Sets the name of this language.
	*
	* @param name the name of this language
	*/
	@Override
	public void setName(java.lang.String name) {
		_language.setName(name);
	}

	@Override
	public boolean isNew() {
		return _language.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_language.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _language.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_language.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _language.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _language.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_language.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _language.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_language.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_language.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_language.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LanguageWrapper((Language)_language.clone());
	}

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.Language language) {
		return _language.compareTo(language);
	}

	@Override
	public int hashCode() {
		return _language.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.Language> toCacheModel() {
		return _language.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Language toEscapedModel() {
		return new LanguageWrapper(_language.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.Language toUnescapedModel() {
		return new LanguageWrapper(_language.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _language.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _language.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_language.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LanguageWrapper)) {
			return false;
		}

		LanguageWrapper languageWrapper = (LanguageWrapper)obj;

		if (Validator.equals(_language, languageWrapper._language)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Language getWrappedLanguage() {
		return _language;
	}

	@Override
	public Language getWrappedModel() {
		return _language;
	}

	@Override
	public void resetOriginalValues() {
		_language.resetOriginalValues();
	}

	private Language _language;
}