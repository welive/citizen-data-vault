/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.UserIdeaLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class UserIdeaClp extends BaseModelImpl<UserIdea> implements UserIdea {
	public UserIdeaClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UserIdea.class;
	}

	@Override
	public String getModelClassName() {
		return UserIdea.class.getName();
	}

	@Override
	public UserIdeaPK getPrimaryKey() {
		return new UserIdeaPK(_ideaId, _userId, _role);
	}

	@Override
	public void setPrimaryKey(UserIdeaPK primaryKey) {
		setIdeaId(primaryKey.ideaId);
		setUserId(primaryKey.userId);
		setRole(primaryKey.role);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UserIdeaPK(_ideaId, _userId, _role);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UserIdeaPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("userId", getUserId());
		attributes.put("role", getRole());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String role = (String)attributes.get("role");

		if (role != null) {
			setRole(role);
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_userIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _userIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_userIdeaRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_userIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _userIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_userIdeaRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public String getRole() {
		return _role;
	}

	@Override
	public void setRole(String role) {
		_role = role;

		if (_userIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _userIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setRole", String.class);

				method.invoke(_userIdeaRemoteModel, role);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUserIdeaRemoteModel() {
		return _userIdeaRemoteModel;
	}

	public void setUserIdeaRemoteModel(BaseModel<?> userIdeaRemoteModel) {
		_userIdeaRemoteModel = userIdeaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _userIdeaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_userIdeaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UserIdeaLocalServiceUtil.addUserIdea(this);
		}
		else {
			UserIdeaLocalServiceUtil.updateUserIdea(this);
		}
	}

	@Override
	public UserIdea toEscapedModel() {
		return (UserIdea)ProxyUtil.newProxyInstance(UserIdea.class.getClassLoader(),
			new Class[] { UserIdea.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UserIdeaClp clone = new UserIdeaClp();

		clone.setIdeaId(getIdeaId());
		clone.setUserId(getUserId());
		clone.setRole(getRole());

		return clone;
	}

	@Override
	public int compareTo(UserIdea userIdea) {
		UserIdeaPK primaryKey = userIdea.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserIdeaClp)) {
			return false;
		}

		UserIdeaClp userIdea = (UserIdeaClp)obj;

		UserIdeaPK primaryKey = userIdea.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{ideaId=");
		sb.append(getIdeaId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", role=");
		sb.append(getRole());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.UserIdea");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>role</column-name><column-value><![CDATA[");
		sb.append(getRole());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _ideaId;
	private long _userId;
	private String _userUuid;
	private String _role;
	private BaseModel<?> _userIdeaRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}