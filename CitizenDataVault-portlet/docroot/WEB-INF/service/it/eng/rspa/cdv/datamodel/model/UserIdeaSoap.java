/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.UserIdeaPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class UserIdeaSoap implements Serializable {
	public static UserIdeaSoap toSoapModel(UserIdea model) {
		UserIdeaSoap soapModel = new UserIdeaSoap();

		soapModel.setIdeaId(model.getIdeaId());
		soapModel.setUserId(model.getUserId());
		soapModel.setRole(model.getRole());

		return soapModel;
	}

	public static UserIdeaSoap[] toSoapModels(UserIdea[] models) {
		UserIdeaSoap[] soapModels = new UserIdeaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserIdeaSoap[][] toSoapModels(UserIdea[][] models) {
		UserIdeaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserIdeaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserIdeaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserIdeaSoap[] toSoapModels(List<UserIdea> models) {
		List<UserIdeaSoap> soapModels = new ArrayList<UserIdeaSoap>(models.size());

		for (UserIdea model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserIdeaSoap[soapModels.size()]);
	}

	public UserIdeaSoap() {
	}

	public UserIdeaPK getPrimaryKey() {
		return new UserIdeaPK(_ideaId, _userId, _role);
	}

	public void setPrimaryKey(UserIdeaPK pk) {
		setIdeaId(pk.ideaId);
		setUserId(pk.userId);
		setRole(pk.role);
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getRole() {
		return _role;
	}

	public void setRole(String role) {
		_role = role;
	}

	private long _ideaId;
	private long _userId;
	private String _role;
}