/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class UserPreferenceSoap implements Serializable {
	public static UserPreferenceSoap toSoapModel(UserPreference model) {
		UserPreferenceSoap soapModel = new UserPreferenceSoap();

		soapModel.setPreferenceId(model.getPreferenceId());
		soapModel.setUserId(model.getUserId());

		return soapModel;
	}

	public static UserPreferenceSoap[] toSoapModels(UserPreference[] models) {
		UserPreferenceSoap[] soapModels = new UserPreferenceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserPreferenceSoap[][] toSoapModels(UserPreference[][] models) {
		UserPreferenceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserPreferenceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserPreferenceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserPreferenceSoap[] toSoapModels(List<UserPreference> models) {
		List<UserPreferenceSoap> soapModels = new ArrayList<UserPreferenceSoap>(models.size());

		for (UserPreference model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserPreferenceSoap[soapModels.size()]);
	}

	public UserPreferenceSoap() {
	}

	public UserPreferencePK getPrimaryKey() {
		return new UserPreferencePK(_preferenceId, _userId);
	}

	public void setPrimaryKey(UserPreferencePK pk) {
		setPreferenceId(pk.preferenceId);
		setUserId(pk.userId);
	}

	public long getPreferenceId() {
		return _preferenceId;
	}

	public void setPreferenceId(long preferenceId) {
		_preferenceId = preferenceId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	private long _preferenceId;
	private long _userId;
}