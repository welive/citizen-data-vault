/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.UserSkillLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.persistence.UserSkillPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class UserSkillClp extends BaseModelImpl<UserSkill> implements UserSkill {
	public UserSkillClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UserSkill.class;
	}

	@Override
	public String getModelClassName() {
		return UserSkill.class.getName();
	}

	@Override
	public UserSkillPK getPrimaryKey() {
		return new UserSkillPK(_skillId, _userId);
	}

	@Override
	public void setPrimaryKey(UserSkillPK primaryKey) {
		setSkillId(primaryKey.skillId);
		setUserId(primaryKey.userId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UserSkillPK(_skillId, _userId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UserSkillPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("skillId", getSkillId());
		attributes.put("userId", getUserId());
		attributes.put("date", getDate());
		attributes.put("endorsmentCounter", getEndorsmentCounter());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long skillId = (Long)attributes.get("skillId");

		if (skillId != null) {
			setSkillId(skillId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long endorsmentCounter = (Long)attributes.get("endorsmentCounter");

		if (endorsmentCounter != null) {
			setEndorsmentCounter(endorsmentCounter);
		}
	}

	@Override
	public long getSkillId() {
		return _skillId;
	}

	@Override
	public void setSkillId(long skillId) {
		_skillId = skillId;

		if (_userSkillRemoteModel != null) {
			try {
				Class<?> clazz = _userSkillRemoteModel.getClass();

				Method method = clazz.getMethod("setSkillId", long.class);

				method.invoke(_userSkillRemoteModel, skillId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_userSkillRemoteModel != null) {
			try {
				Class<?> clazz = _userSkillRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_userSkillRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;

		if (_userSkillRemoteModel != null) {
			try {
				Class<?> clazz = _userSkillRemoteModel.getClass();

				Method method = clazz.getMethod("setDate", Date.class);

				method.invoke(_userSkillRemoteModel, date);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getEndorsmentCounter() {
		return _endorsmentCounter;
	}

	@Override
	public void setEndorsmentCounter(long endorsmentCounter) {
		_endorsmentCounter = endorsmentCounter;

		if (_userSkillRemoteModel != null) {
			try {
				Class<?> clazz = _userSkillRemoteModel.getClass();

				Method method = clazz.getMethod("setEndorsmentCounter",
						long.class);

				method.invoke(_userSkillRemoteModel, endorsmentCounter);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUserSkillRemoteModel() {
		return _userSkillRemoteModel;
	}

	public void setUserSkillRemoteModel(BaseModel<?> userSkillRemoteModel) {
		_userSkillRemoteModel = userSkillRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _userSkillRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_userSkillRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UserSkillLocalServiceUtil.addUserSkill(this);
		}
		else {
			UserSkillLocalServiceUtil.updateUserSkill(this);
		}
	}

	@Override
	public UserSkill toEscapedModel() {
		return (UserSkill)ProxyUtil.newProxyInstance(UserSkill.class.getClassLoader(),
			new Class[] { UserSkill.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UserSkillClp clone = new UserSkillClp();

		clone.setSkillId(getSkillId());
		clone.setUserId(getUserId());
		clone.setDate(getDate());
		clone.setEndorsmentCounter(getEndorsmentCounter());

		return clone;
	}

	@Override
	public int compareTo(UserSkill userSkill) {
		int value = 0;

		if (getEndorsmentCounter() < userSkill.getEndorsmentCounter()) {
			value = -1;
		}
		else if (getEndorsmentCounter() > userSkill.getEndorsmentCounter()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserSkillClp)) {
			return false;
		}

		UserSkillClp userSkill = (UserSkillClp)obj;

		UserSkillPK primaryKey = userSkill.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{skillId=");
		sb.append(getSkillId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", endorsmentCounter=");
		sb.append(getEndorsmentCounter());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.UserSkill");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>skillId</column-name><column-value><![CDATA[");
		sb.append(getSkillId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>endorsmentCounter</column-name><column-value><![CDATA[");
		sb.append(getEndorsmentCounter());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _skillId;
	private long _userId;
	private String _userUuid;
	private Date _date;
	private long _endorsmentCounter;
	private BaseModel<?> _userSkillRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}