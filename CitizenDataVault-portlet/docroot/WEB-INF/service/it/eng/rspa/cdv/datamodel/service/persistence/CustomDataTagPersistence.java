/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.CustomDataTag;

/**
 * The persistence interface for the custom data tag service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see CustomDataTagPersistenceImpl
 * @see CustomDataTagUtil
 * @generated
 */
public interface CustomDataTagPersistence extends BasePersistence<CustomDataTag> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CustomDataTagUtil} to access the custom data tag persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the custom data tags where tagid = &#63;.
	*
	* @param tagid the tagid
	* @return the matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findBytagid(
		long tagid) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the custom data tags where tagid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tagid the tagid
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @return the range of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findBytagid(
		long tagid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the custom data tags where tagid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tagid the tagid
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findBytagid(
		long tagid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first custom data tag in the ordered set where tagid = &#63;.
	*
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag findBytagid_First(
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException;

	/**
	* Returns the first custom data tag in the ordered set where tagid = &#63;.
	*
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data tag, or <code>null</code> if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchBytagid_First(
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last custom data tag in the ordered set where tagid = &#63;.
	*
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag findBytagid_Last(
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException;

	/**
	* Returns the last custom data tag in the ordered set where tagid = &#63;.
	*
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data tag, or <code>null</code> if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchBytagid_Last(
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the custom data tags before and after the current custom data tag in the ordered set where tagid = &#63;.
	*
	* @param customDataTagPK the primary key of the current custom data tag
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag[] findBytagid_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK,
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException;

	/**
	* Removes all the custom data tags where tagid = &#63; from the database.
	*
	* @param tagid the tagid
	* @throws SystemException if a system exception occurred
	*/
	public void removeBytagid(long tagid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of custom data tags where tagid = &#63;.
	*
	* @param tagid the tagid
	* @return the number of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public int countBytagid(long tagid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the custom data tags where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @return the matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findByentryId(
		long entryId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the custom data tags where entryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param entryId the entry ID
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @return the range of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findByentryId(
		long entryId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the custom data tags where entryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param entryId the entry ID
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findByentryId(
		long entryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first custom data tag in the ordered set where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag findByentryId_First(
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException;

	/**
	* Returns the first custom data tag in the ordered set where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data tag, or <code>null</code> if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchByentryId_First(
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last custom data tag in the ordered set where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag findByentryId_Last(
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException;

	/**
	* Returns the last custom data tag in the ordered set where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data tag, or <code>null</code> if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchByentryId_Last(
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the custom data tags before and after the current custom data tag in the ordered set where entryId = &#63;.
	*
	* @param customDataTagPK the primary key of the current custom data tag
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag[] findByentryId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK,
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException;

	/**
	* Removes all the custom data tags where entryId = &#63; from the database.
	*
	* @param entryId the entry ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByentryId(long entryId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of custom data tags where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @return the number of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public int countByentryId(long entryId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the custom data tag in the entity cache if it is enabled.
	*
	* @param customDataTag the custom data tag
	*/
	public void cacheResult(
		it.eng.rspa.cdv.datamodel.model.CustomDataTag customDataTag);

	/**
	* Caches the custom data tags in the entity cache if it is enabled.
	*
	* @param customDataTags the custom data tags
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> customDataTags);

	/**
	* Creates a new custom data tag with the primary key. Does not add the custom data tag to the database.
	*
	* @param customDataTagPK the primary key for the new custom data tag
	* @return the new custom data tag
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag create(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK);

	/**
	* Removes the custom data tag with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param customDataTagPK the primary key of the custom data tag
	* @return the custom data tag that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag remove(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException;

	public it.eng.rspa.cdv.datamodel.model.CustomDataTag updateImpl(
		it.eng.rspa.cdv.datamodel.model.CustomDataTag customDataTag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the custom data tag with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException} if it could not be found.
	*
	* @param customDataTagPK the primary key of the custom data tag
	* @return the custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException;

	/**
	* Returns the custom data tag with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param customDataTagPK the primary key of the custom data tag
	* @return the custom data tag, or <code>null</code> if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the custom data tags.
	*
	* @return the custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the custom data tags.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @return the range of custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the custom data tags.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the custom data tags from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of custom data tags.
	*
	* @return the number of custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}