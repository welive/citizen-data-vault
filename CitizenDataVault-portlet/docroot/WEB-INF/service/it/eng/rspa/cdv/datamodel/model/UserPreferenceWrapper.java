/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UserPreference}.
 * </p>
 *
 * @author Engineering
 * @see UserPreference
 * @generated
 */
public class UserPreferenceWrapper implements UserPreference,
	ModelWrapper<UserPreference> {
	public UserPreferenceWrapper(UserPreference userPreference) {
		_userPreference = userPreference;
	}

	@Override
	public Class<?> getModelClass() {
		return UserPreference.class;
	}

	@Override
	public String getModelClassName() {
		return UserPreference.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("preferenceId", getPreferenceId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long preferenceId = (Long)attributes.get("preferenceId");

		if (preferenceId != null) {
			setPreferenceId(preferenceId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	/**
	* Returns the primary key of this user preference.
	*
	* @return the primary key of this user preference
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK getPrimaryKey() {
		return _userPreference.getPrimaryKey();
	}

	/**
	* Sets the primary key of this user preference.
	*
	* @param primaryKey the primary key of this user preference
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserPreferencePK primaryKey) {
		_userPreference.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the preference ID of this user preference.
	*
	* @return the preference ID of this user preference
	*/
	@Override
	public long getPreferenceId() {
		return _userPreference.getPreferenceId();
	}

	/**
	* Sets the preference ID of this user preference.
	*
	* @param preferenceId the preference ID of this user preference
	*/
	@Override
	public void setPreferenceId(long preferenceId) {
		_userPreference.setPreferenceId(preferenceId);
	}

	/**
	* Returns the user ID of this user preference.
	*
	* @return the user ID of this user preference
	*/
	@Override
	public long getUserId() {
		return _userPreference.getUserId();
	}

	/**
	* Sets the user ID of this user preference.
	*
	* @param userId the user ID of this user preference
	*/
	@Override
	public void setUserId(long userId) {
		_userPreference.setUserId(userId);
	}

	/**
	* Returns the user uuid of this user preference.
	*
	* @return the user uuid of this user preference
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userPreference.getUserUuid();
	}

	/**
	* Sets the user uuid of this user preference.
	*
	* @param userUuid the user uuid of this user preference
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userPreference.setUserUuid(userUuid);
	}

	@Override
	public boolean isNew() {
		return _userPreference.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_userPreference.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _userPreference.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userPreference.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _userPreference.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _userPreference.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_userPreference.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _userPreference.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_userPreference.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_userPreference.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_userPreference.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UserPreferenceWrapper((UserPreference)_userPreference.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.UserPreference userPreference) {
		return _userPreference.compareTo(userPreference);
	}

	@Override
	public int hashCode() {
		return _userPreference.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.UserPreference> toCacheModel() {
		return _userPreference.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserPreference toEscapedModel() {
		return new UserPreferenceWrapper(_userPreference.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserPreference toUnescapedModel() {
		return new UserPreferenceWrapper(_userPreference.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _userPreference.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userPreference.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_userPreference.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserPreferenceWrapper)) {
			return false;
		}

		UserPreferenceWrapper userPreferenceWrapper = (UserPreferenceWrapper)obj;

		if (Validator.equals(_userPreference,
					userPreferenceWrapper._userPreference)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UserPreference getWrappedUserPreference() {
		return _userPreference;
	}

	@Override
	public UserPreference getWrappedModel() {
		return _userPreference;
	}

	@Override
	public void resetOriginalValues() {
		_userPreference.resetOriginalValues();
	}

	private UserPreference _userPreference;
}