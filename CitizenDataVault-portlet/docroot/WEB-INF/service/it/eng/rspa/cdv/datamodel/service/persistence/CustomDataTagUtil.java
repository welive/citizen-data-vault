/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.cdv.datamodel.model.CustomDataTag;

import java.util.List;

/**
 * The persistence utility for the custom data tag service. This utility wraps {@link CustomDataTagPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see CustomDataTagPersistence
 * @see CustomDataTagPersistenceImpl
 * @generated
 */
public class CustomDataTagUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CustomDataTag customDataTag) {
		getPersistence().clearCache(customDataTag);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CustomDataTag> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CustomDataTag> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CustomDataTag> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CustomDataTag update(CustomDataTag customDataTag)
		throws SystemException {
		return getPersistence().update(customDataTag);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CustomDataTag update(CustomDataTag customDataTag,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(customDataTag, serviceContext);
	}

	/**
	* Returns all the custom data tags where tagid = &#63;.
	*
	* @param tagid the tagid
	* @return the matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findBytagid(
		long tagid) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBytagid(tagid);
	}

	/**
	* Returns a range of all the custom data tags where tagid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tagid the tagid
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @return the range of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findBytagid(
		long tagid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBytagid(tagid, start, end);
	}

	/**
	* Returns an ordered range of all the custom data tags where tagid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tagid the tagid
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findBytagid(
		long tagid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBytagid(tagid, start, end, orderByComparator);
	}

	/**
	* Returns the first custom data tag in the ordered set where tagid = &#63;.
	*
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag findBytagid_First(
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException {
		return getPersistence().findBytagid_First(tagid, orderByComparator);
	}

	/**
	* Returns the first custom data tag in the ordered set where tagid = &#63;.
	*
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data tag, or <code>null</code> if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchBytagid_First(
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBytagid_First(tagid, orderByComparator);
	}

	/**
	* Returns the last custom data tag in the ordered set where tagid = &#63;.
	*
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag findBytagid_Last(
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException {
		return getPersistence().findBytagid_Last(tagid, orderByComparator);
	}

	/**
	* Returns the last custom data tag in the ordered set where tagid = &#63;.
	*
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data tag, or <code>null</code> if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchBytagid_Last(
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBytagid_Last(tagid, orderByComparator);
	}

	/**
	* Returns the custom data tags before and after the current custom data tag in the ordered set where tagid = &#63;.
	*
	* @param customDataTagPK the primary key of the current custom data tag
	* @param tagid the tagid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag[] findBytagid_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK,
		long tagid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException {
		return getPersistence()
				   .findBytagid_PrevAndNext(customDataTagPK, tagid,
			orderByComparator);
	}

	/**
	* Removes all the custom data tags where tagid = &#63; from the database.
	*
	* @param tagid the tagid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBytagid(long tagid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBytagid(tagid);
	}

	/**
	* Returns the number of custom data tags where tagid = &#63;.
	*
	* @param tagid the tagid
	* @return the number of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static int countBytagid(long tagid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBytagid(tagid);
	}

	/**
	* Returns all the custom data tags where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @return the matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findByentryId(
		long entryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByentryId(entryId);
	}

	/**
	* Returns a range of all the custom data tags where entryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param entryId the entry ID
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @return the range of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findByentryId(
		long entryId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByentryId(entryId, start, end);
	}

	/**
	* Returns an ordered range of all the custom data tags where entryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param entryId the entry ID
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findByentryId(
		long entryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByentryId(entryId, start, end, orderByComparator);
	}

	/**
	* Returns the first custom data tag in the ordered set where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag findByentryId_First(
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException {
		return getPersistence().findByentryId_First(entryId, orderByComparator);
	}

	/**
	* Returns the first custom data tag in the ordered set where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching custom data tag, or <code>null</code> if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchByentryId_First(
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByentryId_First(entryId, orderByComparator);
	}

	/**
	* Returns the last custom data tag in the ordered set where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag findByentryId_Last(
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException {
		return getPersistence().findByentryId_Last(entryId, orderByComparator);
	}

	/**
	* Returns the last custom data tag in the ordered set where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching custom data tag, or <code>null</code> if a matching custom data tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchByentryId_Last(
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByentryId_Last(entryId, orderByComparator);
	}

	/**
	* Returns the custom data tags before and after the current custom data tag in the ordered set where entryId = &#63;.
	*
	* @param customDataTagPK the primary key of the current custom data tag
	* @param entryId the entry ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag[] findByentryId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK,
		long entryId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException {
		return getPersistence()
				   .findByentryId_PrevAndNext(customDataTagPK, entryId,
			orderByComparator);
	}

	/**
	* Removes all the custom data tags where entryId = &#63; from the database.
	*
	* @param entryId the entry ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByentryId(long entryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByentryId(entryId);
	}

	/**
	* Returns the number of custom data tags where entryId = &#63;.
	*
	* @param entryId the entry ID
	* @return the number of matching custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static int countByentryId(long entryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByentryId(entryId);
	}

	/**
	* Caches the custom data tag in the entity cache if it is enabled.
	*
	* @param customDataTag the custom data tag
	*/
	public static void cacheResult(
		it.eng.rspa.cdv.datamodel.model.CustomDataTag customDataTag) {
		getPersistence().cacheResult(customDataTag);
	}

	/**
	* Caches the custom data tags in the entity cache if it is enabled.
	*
	* @param customDataTags the custom data tags
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> customDataTags) {
		getPersistence().cacheResult(customDataTags);
	}

	/**
	* Creates a new custom data tag with the primary key. Does not add the custom data tag to the database.
	*
	* @param customDataTagPK the primary key for the new custom data tag
	* @return the new custom data tag
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag create(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK) {
		return getPersistence().create(customDataTagPK);
	}

	/**
	* Removes the custom data tag with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param customDataTagPK the primary key of the custom data tag
	* @return the custom data tag that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag remove(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException {
		return getPersistence().remove(customDataTagPK);
	}

	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag updateImpl(
		it.eng.rspa.cdv.datamodel.model.CustomDataTag customDataTag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(customDataTag);
	}

	/**
	* Returns the custom data tag with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException} if it could not be found.
	*
	* @param customDataTagPK the primary key of the custom data tag
	* @return the custom data tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException {
		return getPersistence().findByPrimaryKey(customDataTagPK);
	}

	/**
	* Returns the custom data tag with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param customDataTagPK the primary key of the custom data tag
	* @return the custom data tag, or <code>null</code> if a custom data tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.cdv.datamodel.model.CustomDataTag fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.CustomDataTagPK customDataTagPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(customDataTagPK);
	}

	/**
	* Returns all the custom data tags.
	*
	* @return the custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the custom data tags.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @return the range of custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the custom data tags.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of custom data tags
	* @param end the upper bound of the range of custom data tags (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.cdv.datamodel.model.CustomDataTag> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the custom data tags from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of custom data tags.
	*
	* @return the number of custom data tags
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CustomDataTagPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CustomDataTagPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.cdv.datamodel.service.ClpSerializer.getServletContextName(),
					CustomDataTagPersistence.class.getName());

			ReferenceRegistry.registerReference(CustomDataTagUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CustomDataTagPersistence persistence) {
	}

	private static CustomDataTagPersistence _persistence;
}