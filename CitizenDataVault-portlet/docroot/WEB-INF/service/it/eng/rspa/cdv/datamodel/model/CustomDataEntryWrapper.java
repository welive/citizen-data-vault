/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CustomDataEntry}.
 * </p>
 *
 * @author Engineering
 * @see CustomDataEntry
 * @generated
 */
public class CustomDataEntryWrapper implements CustomDataEntry,
	ModelWrapper<CustomDataEntry> {
	public CustomDataEntryWrapper(CustomDataEntry customDataEntry) {
		_customDataEntry = customDataEntry;
	}

	@Override
	public Class<?> getModelClass() {
		return CustomDataEntry.class;
	}

	@Override
	public String getModelClassName() {
		return CustomDataEntry.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("entryId", getEntryId());
		attributes.put("key", getKey());
		attributes.put("value", getValue());
		attributes.put("type", getType());
		attributes.put("userid", getUserid());
		attributes.put("clientid", getClientid());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long entryId = (Long)attributes.get("entryId");

		if (entryId != null) {
			setEntryId(entryId);
		}

		String key = (String)attributes.get("key");

		if (key != null) {
			setKey(key);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Long userid = (Long)attributes.get("userid");

		if (userid != null) {
			setUserid(userid);
		}

		String clientid = (String)attributes.get("clientid");

		if (clientid != null) {
			setClientid(clientid);
		}
	}

	/**
	* Returns the primary key of this custom data entry.
	*
	* @return the primary key of this custom data entry
	*/
	@Override
	public long getPrimaryKey() {
		return _customDataEntry.getPrimaryKey();
	}

	/**
	* Sets the primary key of this custom data entry.
	*
	* @param primaryKey the primary key of this custom data entry
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_customDataEntry.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the entry ID of this custom data entry.
	*
	* @return the entry ID of this custom data entry
	*/
	@Override
	public long getEntryId() {
		return _customDataEntry.getEntryId();
	}

	/**
	* Sets the entry ID of this custom data entry.
	*
	* @param entryId the entry ID of this custom data entry
	*/
	@Override
	public void setEntryId(long entryId) {
		_customDataEntry.setEntryId(entryId);
	}

	/**
	* Returns the key of this custom data entry.
	*
	* @return the key of this custom data entry
	*/
	@Override
	public java.lang.String getKey() {
		return _customDataEntry.getKey();
	}

	/**
	* Sets the key of this custom data entry.
	*
	* @param key the key of this custom data entry
	*/
	@Override
	public void setKey(java.lang.String key) {
		_customDataEntry.setKey(key);
	}

	/**
	* Returns the value of this custom data entry.
	*
	* @return the value of this custom data entry
	*/
	@Override
	public java.lang.String getValue() {
		return _customDataEntry.getValue();
	}

	/**
	* Sets the value of this custom data entry.
	*
	* @param value the value of this custom data entry
	*/
	@Override
	public void setValue(java.lang.String value) {
		_customDataEntry.setValue(value);
	}

	/**
	* Returns the type of this custom data entry.
	*
	* @return the type of this custom data entry
	*/
	@Override
	public java.lang.String getType() {
		return _customDataEntry.getType();
	}

	/**
	* Sets the type of this custom data entry.
	*
	* @param type the type of this custom data entry
	*/
	@Override
	public void setType(java.lang.String type) {
		_customDataEntry.setType(type);
	}

	/**
	* Returns the userid of this custom data entry.
	*
	* @return the userid of this custom data entry
	*/
	@Override
	public long getUserid() {
		return _customDataEntry.getUserid();
	}

	/**
	* Sets the userid of this custom data entry.
	*
	* @param userid the userid of this custom data entry
	*/
	@Override
	public void setUserid(long userid) {
		_customDataEntry.setUserid(userid);
	}

	/**
	* Returns the clientid of this custom data entry.
	*
	* @return the clientid of this custom data entry
	*/
	@Override
	public java.lang.String getClientid() {
		return _customDataEntry.getClientid();
	}

	/**
	* Sets the clientid of this custom data entry.
	*
	* @param clientid the clientid of this custom data entry
	*/
	@Override
	public void setClientid(java.lang.String clientid) {
		_customDataEntry.setClientid(clientid);
	}

	@Override
	public boolean isNew() {
		return _customDataEntry.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_customDataEntry.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _customDataEntry.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_customDataEntry.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _customDataEntry.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _customDataEntry.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_customDataEntry.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _customDataEntry.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_customDataEntry.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_customDataEntry.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_customDataEntry.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CustomDataEntryWrapper((CustomDataEntry)_customDataEntry.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.cdv.datamodel.model.CustomDataEntry customDataEntry) {
		return _customDataEntry.compareTo(customDataEntry);
	}

	@Override
	public int hashCode() {
		return _customDataEntry.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.CustomDataEntry> toCacheModel() {
		return _customDataEntry.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry toEscapedModel() {
		return new CustomDataEntryWrapper(_customDataEntry.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.CustomDataEntry toUnescapedModel() {
		return new CustomDataEntryWrapper(_customDataEntry.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _customDataEntry.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _customDataEntry.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_customDataEntry.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CustomDataEntryWrapper)) {
			return false;
		}

		CustomDataEntryWrapper customDataEntryWrapper = (CustomDataEntryWrapper)obj;

		if (Validator.equals(_customDataEntry,
					customDataEntryWrapper._customDataEntry)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CustomDataEntry getWrappedCustomDataEntry() {
		return _customDataEntry;
	}

	@Override
	public CustomDataEntry getWrappedModel() {
		return _customDataEntry;
	}

	@Override
	public void resetOriginalValues() {
		_customDataEntry.resetOriginalValues();
	}

	private CustomDataEntry _customDataEntry;
}