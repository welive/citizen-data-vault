/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.SkillCdvLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class SkillCdvClp extends BaseModelImpl<SkillCdv> implements SkillCdv {
	public SkillCdvClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return SkillCdv.class;
	}

	@Override
	public String getModelClassName() {
		return SkillCdv.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _skillId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setSkillId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _skillId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("skillId", getSkillId());
		attributes.put("skillname", getSkillname());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long skillId = (Long)attributes.get("skillId");

		if (skillId != null) {
			setSkillId(skillId);
		}

		String skillname = (String)attributes.get("skillname");

		if (skillname != null) {
			setSkillname(skillname);
		}
	}

	@Override
	public long getSkillId() {
		return _skillId;
	}

	@Override
	public void setSkillId(long skillId) {
		_skillId = skillId;

		if (_skillCdvRemoteModel != null) {
			try {
				Class<?> clazz = _skillCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setSkillId", long.class);

				method.invoke(_skillCdvRemoteModel, skillId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSkillname() {
		return _skillname;
	}

	@Override
	public void setSkillname(String skillname) {
		_skillname = skillname;

		if (_skillCdvRemoteModel != null) {
			try {
				Class<?> clazz = _skillCdvRemoteModel.getClass();

				Method method = clazz.getMethod("setSkillname", String.class);

				method.invoke(_skillCdvRemoteModel, skillname);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getSkillCdvRemoteModel() {
		return _skillCdvRemoteModel;
	}

	public void setSkillCdvRemoteModel(BaseModel<?> skillCdvRemoteModel) {
		_skillCdvRemoteModel = skillCdvRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _skillCdvRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_skillCdvRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			SkillCdvLocalServiceUtil.addSkillCdv(this);
		}
		else {
			SkillCdvLocalServiceUtil.updateSkillCdv(this);
		}
	}

	@Override
	public SkillCdv toEscapedModel() {
		return (SkillCdv)ProxyUtil.newProxyInstance(SkillCdv.class.getClassLoader(),
			new Class[] { SkillCdv.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		SkillCdvClp clone = new SkillCdvClp();

		clone.setSkillId(getSkillId());
		clone.setSkillname(getSkillname());

		return clone;
	}

	@Override
	public int compareTo(SkillCdv skillCdv) {
		long primaryKey = skillCdv.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SkillCdvClp)) {
			return false;
		}

		SkillCdvClp skillCdv = (SkillCdvClp)obj;

		long primaryKey = skillCdv.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{skillId=");
		sb.append(getSkillId());
		sb.append(", skillname=");
		sb.append(getSkillname());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.SkillCdv");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>skillId</column-name><column-value><![CDATA[");
		sb.append(getSkillId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>skillname</column-name><column-value><![CDATA[");
		sb.append(getSkillname());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _skillId;
	private String _skillname;
	private BaseModel<?> _skillCdvRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}