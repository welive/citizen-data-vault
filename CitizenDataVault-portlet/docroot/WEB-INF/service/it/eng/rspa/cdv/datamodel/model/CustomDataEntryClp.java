/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.CustomDataEntryLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class CustomDataEntryClp extends BaseModelImpl<CustomDataEntry>
	implements CustomDataEntry {
	public CustomDataEntryClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CustomDataEntry.class;
	}

	@Override
	public String getModelClassName() {
		return CustomDataEntry.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _entryId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setEntryId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _entryId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("entryId", getEntryId());
		attributes.put("key", getKey());
		attributes.put("value", getValue());
		attributes.put("type", getType());
		attributes.put("userid", getUserid());
		attributes.put("clientid", getClientid());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long entryId = (Long)attributes.get("entryId");

		if (entryId != null) {
			setEntryId(entryId);
		}

		String key = (String)attributes.get("key");

		if (key != null) {
			setKey(key);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Long userid = (Long)attributes.get("userid");

		if (userid != null) {
			setUserid(userid);
		}

		String clientid = (String)attributes.get("clientid");

		if (clientid != null) {
			setClientid(clientid);
		}
	}

	@Override
	public long getEntryId() {
		return _entryId;
	}

	@Override
	public void setEntryId(long entryId) {
		_entryId = entryId;

		if (_customDataEntryRemoteModel != null) {
			try {
				Class<?> clazz = _customDataEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setEntryId", long.class);

				method.invoke(_customDataEntryRemoteModel, entryId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getKey() {
		return _key;
	}

	@Override
	public void setKey(String key) {
		_key = key;

		if (_customDataEntryRemoteModel != null) {
			try {
				Class<?> clazz = _customDataEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setKey", String.class);

				method.invoke(_customDataEntryRemoteModel, key);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getValue() {
		return _value;
	}

	@Override
	public void setValue(String value) {
		_value = value;

		if (_customDataEntryRemoteModel != null) {
			try {
				Class<?> clazz = _customDataEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setValue", String.class);

				method.invoke(_customDataEntryRemoteModel, value);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getType() {
		return _type;
	}

	@Override
	public void setType(String type) {
		_type = type;

		if (_customDataEntryRemoteModel != null) {
			try {
				Class<?> clazz = _customDataEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setType", String.class);

				method.invoke(_customDataEntryRemoteModel, type);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserid() {
		return _userid;
	}

	@Override
	public void setUserid(long userid) {
		_userid = userid;

		if (_customDataEntryRemoteModel != null) {
			try {
				Class<?> clazz = _customDataEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setUserid", long.class);

				method.invoke(_customDataEntryRemoteModel, userid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getClientid() {
		return _clientid;
	}

	@Override
	public void setClientid(String clientid) {
		_clientid = clientid;

		if (_customDataEntryRemoteModel != null) {
			try {
				Class<?> clazz = _customDataEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setClientid", String.class);

				method.invoke(_customDataEntryRemoteModel, clientid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCustomDataEntryRemoteModel() {
		return _customDataEntryRemoteModel;
	}

	public void setCustomDataEntryRemoteModel(
		BaseModel<?> customDataEntryRemoteModel) {
		_customDataEntryRemoteModel = customDataEntryRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _customDataEntryRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_customDataEntryRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CustomDataEntryLocalServiceUtil.addCustomDataEntry(this);
		}
		else {
			CustomDataEntryLocalServiceUtil.updateCustomDataEntry(this);
		}
	}

	@Override
	public CustomDataEntry toEscapedModel() {
		return (CustomDataEntry)ProxyUtil.newProxyInstance(CustomDataEntry.class.getClassLoader(),
			new Class[] { CustomDataEntry.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CustomDataEntryClp clone = new CustomDataEntryClp();

		clone.setEntryId(getEntryId());
		clone.setKey(getKey());
		clone.setValue(getValue());
		clone.setType(getType());
		clone.setUserid(getUserid());
		clone.setClientid(getClientid());

		return clone;
	}

	@Override
	public int compareTo(CustomDataEntry customDataEntry) {
		long primaryKey = customDataEntry.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CustomDataEntryClp)) {
			return false;
		}

		CustomDataEntryClp customDataEntry = (CustomDataEntryClp)obj;

		long primaryKey = customDataEntry.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{entryId=");
		sb.append(getEntryId());
		sb.append(", key=");
		sb.append(getKey());
		sb.append(", value=");
		sb.append(getValue());
		sb.append(", type=");
		sb.append(getType());
		sb.append(", userid=");
		sb.append(getUserid());
		sb.append(", clientid=");
		sb.append(getClientid());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.CustomDataEntry");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>entryId</column-name><column-value><![CDATA[");
		sb.append(getEntryId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>key</column-name><column-value><![CDATA[");
		sb.append(getKey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>value</column-name><column-value><![CDATA[");
		sb.append(getValue());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userid</column-name><column-value><![CDATA[");
		sb.append(getUserid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>clientid</column-name><column-value><![CDATA[");
		sb.append(getClientid());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _entryId;
	private String _key;
	private String _value;
	private String _type;
	private long _userid;
	private String _clientid;
	private BaseModel<?> _customDataEntryRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}