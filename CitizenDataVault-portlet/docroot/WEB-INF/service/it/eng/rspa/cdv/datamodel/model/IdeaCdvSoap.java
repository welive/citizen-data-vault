/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class IdeaCdvSoap implements Serializable {
	public static IdeaCdvSoap toSoapModel(IdeaCdv model) {
		IdeaCdvSoap soapModel = new IdeaCdvSoap();

		soapModel.setIdeaId(model.getIdeaId());
		soapModel.setIdeaName(model.getIdeaName());
		soapModel.setNeed(model.getNeed());

		return soapModel;
	}

	public static IdeaCdvSoap[] toSoapModels(IdeaCdv[] models) {
		IdeaCdvSoap[] soapModels = new IdeaCdvSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static IdeaCdvSoap[][] toSoapModels(IdeaCdv[][] models) {
		IdeaCdvSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new IdeaCdvSoap[models.length][models[0].length];
		}
		else {
			soapModels = new IdeaCdvSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static IdeaCdvSoap[] toSoapModels(List<IdeaCdv> models) {
		List<IdeaCdvSoap> soapModels = new ArrayList<IdeaCdvSoap>(models.size());

		for (IdeaCdv model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new IdeaCdvSoap[soapModels.size()]);
	}

	public IdeaCdvSoap() {
	}

	public long getPrimaryKey() {
		return _ideaId;
	}

	public void setPrimaryKey(long pk) {
		setIdeaId(pk);
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	public String getIdeaName() {
		return _ideaName;
	}

	public void setIdeaName(String ideaName) {
		_ideaName = ideaName;
	}

	public boolean getNeed() {
		return _need;
	}

	public boolean isNeed() {
		return _need;
	}

	public void setNeed(boolean need) {
		_need = need;
	}

	private long _ideaId;
	private String _ideaName;
	private boolean _need;
}