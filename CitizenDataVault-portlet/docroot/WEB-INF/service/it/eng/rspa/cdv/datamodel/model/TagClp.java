/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.cdv.datamodel.service.ClpSerializer;
import it.eng.rspa.cdv.datamodel.service.TagLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering
 */
public class TagClp extends BaseModelImpl<Tag> implements Tag {
	public TagClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Tag.class;
	}

	@Override
	public String getModelClassName() {
		return Tag.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _tagid;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setTagid(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _tagid;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tagid", getTagid());
		attributes.put("tagname", getTagname());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long tagid = (Long)attributes.get("tagid");

		if (tagid != null) {
			setTagid(tagid);
		}

		String tagname = (String)attributes.get("tagname");

		if (tagname != null) {
			setTagname(tagname);
		}
	}

	@Override
	public long getTagid() {
		return _tagid;
	}

	@Override
	public void setTagid(long tagid) {
		_tagid = tagid;

		if (_tagRemoteModel != null) {
			try {
				Class<?> clazz = _tagRemoteModel.getClass();

				Method method = clazz.getMethod("setTagid", long.class);

				method.invoke(_tagRemoteModel, tagid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTagname() {
		return _tagname;
	}

	@Override
	public void setTagname(String tagname) {
		_tagname = tagname;

		if (_tagRemoteModel != null) {
			try {
				Class<?> clazz = _tagRemoteModel.getClass();

				Method method = clazz.getMethod("setTagname", String.class);

				method.invoke(_tagRemoteModel, tagname);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getTagRemoteModel() {
		return _tagRemoteModel;
	}

	public void setTagRemoteModel(BaseModel<?> tagRemoteModel) {
		_tagRemoteModel = tagRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _tagRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_tagRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			TagLocalServiceUtil.addTag(this);
		}
		else {
			TagLocalServiceUtil.updateTag(this);
		}
	}

	@Override
	public Tag toEscapedModel() {
		return (Tag)ProxyUtil.newProxyInstance(Tag.class.getClassLoader(),
			new Class[] { Tag.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		TagClp clone = new TagClp();

		clone.setTagid(getTagid());
		clone.setTagname(getTagname());

		return clone;
	}

	@Override
	public int compareTo(Tag tag) {
		long primaryKey = tag.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TagClp)) {
			return false;
		}

		TagClp tag = (TagClp)obj;

		long primaryKey = tag.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{tagid=");
		sb.append(getTagid());
		sb.append(", tagname=");
		sb.append(getTagname());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.cdv.datamodel.model.Tag");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>tagid</column-name><column-value><![CDATA[");
		sb.append(getTagid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tagname</column-name><column-value><![CDATA[");
		sb.append(getTagname());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _tagid;
	private String _tagname;
	private BaseModel<?> _tagRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.cdv.datamodel.service.ClpSerializer.class;
}