/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering
 * @generated
 */
public class ThirdPartySoap implements Serializable {
	public static ThirdPartySoap toSoapModel(ThirdParty model) {
		ThirdPartySoap soapModel = new ThirdPartySoap();

		soapModel.setThirdPartyId(model.getThirdPartyId());
		soapModel.setThirdPartyName(model.getThirdPartyName());

		return soapModel;
	}

	public static ThirdPartySoap[] toSoapModels(ThirdParty[] models) {
		ThirdPartySoap[] soapModels = new ThirdPartySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ThirdPartySoap[][] toSoapModels(ThirdParty[][] models) {
		ThirdPartySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ThirdPartySoap[models.length][models[0].length];
		}
		else {
			soapModels = new ThirdPartySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ThirdPartySoap[] toSoapModels(List<ThirdParty> models) {
		List<ThirdPartySoap> soapModels = new ArrayList<ThirdPartySoap>(models.size());

		for (ThirdParty model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ThirdPartySoap[soapModels.size()]);
	}

	public ThirdPartySoap() {
	}

	public long getPrimaryKey() {
		return _thirdPartyId;
	}

	public void setPrimaryKey(long pk) {
		setThirdPartyId(pk);
	}

	public long getThirdPartyId() {
		return _thirdPartyId;
	}

	public void setThirdPartyId(long thirdPartyId) {
		_thirdPartyId = thirdPartyId;
	}

	public String getThirdPartyName() {
		return _thirdPartyName;
	}

	public void setThirdPartyName(String thirdPartyName) {
		_thirdPartyName = thirdPartyName;
	}

	private long _thirdPartyId;
	private String _thirdPartyName;
}