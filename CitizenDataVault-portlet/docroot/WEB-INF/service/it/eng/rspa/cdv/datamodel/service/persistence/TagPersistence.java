/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.Tag;

/**
 * The persistence interface for the tag service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see TagPersistenceImpl
 * @see TagUtil
 * @generated
 */
public interface TagPersistence extends BasePersistence<Tag> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TagUtil} to access the tag persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the tags where tagname = &#63;.
	*
	* @param tagname the tagname
	* @return the matching tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.Tag> findBytagname(
		java.lang.String tagname)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tags where tagname = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tagname the tagname
	* @param start the lower bound of the range of tags
	* @param end the upper bound of the range of tags (not inclusive)
	* @return the range of matching tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.Tag> findBytagname(
		java.lang.String tagname, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tags where tagname = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tagname the tagname
	* @param start the lower bound of the range of tags
	* @param end the upper bound of the range of tags (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.Tag> findBytagname(
		java.lang.String tagname, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first tag in the ordered set where tagname = &#63;.
	*
	* @param tagname the tagname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a matching tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag findBytagname_First(
		java.lang.String tagname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchTagException;

	/**
	* Returns the first tag in the ordered set where tagname = &#63;.
	*
	* @param tagname the tagname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tag, or <code>null</code> if a matching tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag fetchBytagname_First(
		java.lang.String tagname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last tag in the ordered set where tagname = &#63;.
	*
	* @param tagname the tagname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a matching tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag findBytagname_Last(
		java.lang.String tagname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchTagException;

	/**
	* Returns the last tag in the ordered set where tagname = &#63;.
	*
	* @param tagname the tagname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tag, or <code>null</code> if a matching tag could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag fetchBytagname_Last(
		java.lang.String tagname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tags before and after the current tag in the ordered set where tagname = &#63;.
	*
	* @param tagid the primary key of the current tag
	* @param tagname the tagname
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag[] findBytagname_PrevAndNext(
		long tagid, java.lang.String tagname,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchTagException;

	/**
	* Removes all the tags where tagname = &#63; from the database.
	*
	* @param tagname the tagname
	* @throws SystemException if a system exception occurred
	*/
	public void removeBytagname(java.lang.String tagname)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tags where tagname = &#63;.
	*
	* @param tagname the tagname
	* @return the number of matching tags
	* @throws SystemException if a system exception occurred
	*/
	public int countBytagname(java.lang.String tagname)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the tag in the entity cache if it is enabled.
	*
	* @param tag the tag
	*/
	public void cacheResult(it.eng.rspa.cdv.datamodel.model.Tag tag);

	/**
	* Caches the tags in the entity cache if it is enabled.
	*
	* @param tags the tags
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.Tag> tags);

	/**
	* Creates a new tag with the primary key. Does not add the tag to the database.
	*
	* @param tagid the primary key for the new tag
	* @return the new tag
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag create(long tagid);

	/**
	* Removes the tag with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param tagid the primary key of the tag
	* @return the tag that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag remove(long tagid)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchTagException;

	public it.eng.rspa.cdv.datamodel.model.Tag updateImpl(
		it.eng.rspa.cdv.datamodel.model.Tag tag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tag with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchTagException} if it could not be found.
	*
	* @param tagid the primary key of the tag
	* @return the tag
	* @throws it.eng.rspa.cdv.datamodel.NoSuchTagException if a tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag findByPrimaryKey(long tagid)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchTagException;

	/**
	* Returns the tag with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param tagid the primary key of the tag
	* @return the tag, or <code>null</code> if a tag with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.Tag fetchByPrimaryKey(long tagid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the tags.
	*
	* @return the tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.Tag> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tags.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tags
	* @param end the upper bound of the range of tags (not inclusive)
	* @return the range of tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.Tag> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tags.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.TagModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tags
	* @param end the upper bound of the range of tags (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tags
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.Tag> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the tags from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tags.
	*
	* @return the number of tags
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}