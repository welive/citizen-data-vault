/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.UserCdv;

/**
 * The persistence interface for the user cdv service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see UserCdvPersistenceImpl
 * @see UserCdvUtil
 * @generated
 */
public interface UserCdvPersistence extends BasePersistence<UserCdv> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserCdvUtil} to access the user cdv persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the user cdvs where ccuid = &#63;.
	*
	* @param ccuid the ccuid
	* @return the matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByccuid(
		long ccuid) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user cdvs where ccuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ccuid the ccuid
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @return the range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByccuid(
		long ccuid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user cdvs where ccuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ccuid the ccuid
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByccuid(
		long ccuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user cdv in the ordered set where ccuid = &#63;.
	*
	* @param ccuid the ccuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByccuid_First(
		long ccuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the first user cdv in the ordered set where ccuid = &#63;.
	*
	* @param ccuid the ccuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByccuid_First(
		long ccuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user cdv in the ordered set where ccuid = &#63;.
	*
	* @param ccuid the ccuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByccuid_Last(
		long ccuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the last user cdv in the ordered set where ccuid = &#63;.
	*
	* @param ccuid the ccuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByccuid_Last(
		long ccuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user cdvs before and after the current user cdv in the ordered set where ccuid = &#63;.
	*
	* @param userCdvPK the primary key of the current user cdv
	* @param ccuid the ccuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv[] findByccuid_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK,
		long ccuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Removes all the user cdvs where ccuid = &#63; from the database.
	*
	* @param ccuid the ccuid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByccuid(long ccuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user cdvs where ccuid = &#63;.
	*
	* @param ccuid the ccuid
	* @return the number of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countByccuid(long ccuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user cdvs where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user cdvs where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @return the range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user cdvs where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user cdv in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the first user cdv in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user cdv in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the last user cdv in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user cdvs before and after the current user cdv in the ordered set where userId = &#63;.
	*
	* @param userCdvPK the primary key of the current user cdv
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv[] findByuserId_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Removes all the user cdvs where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user cdvs where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user cdvs where username = &#63;.
	*
	* @param username the username
	* @return the matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByusername(
		java.lang.String username)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user cdvs where username = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param username the username
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @return the range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByusername(
		java.lang.String username, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user cdvs where username = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param username the username
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByusername(
		java.lang.String username, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user cdv in the ordered set where username = &#63;.
	*
	* @param username the username
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByusername_First(
		java.lang.String username,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the first user cdv in the ordered set where username = &#63;.
	*
	* @param username the username
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByusername_First(
		java.lang.String username,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user cdv in the ordered set where username = &#63;.
	*
	* @param username the username
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByusername_Last(
		java.lang.String username,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the last user cdv in the ordered set where username = &#63;.
	*
	* @param username the username
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByusername_Last(
		java.lang.String username,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user cdvs before and after the current user cdv in the ordered set where username = &#63;.
	*
	* @param userCdvPK the primary key of the current user cdv
	* @param username the username
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv[] findByusername_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK,
		java.lang.String username,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Removes all the user cdvs where username = &#63; from the database.
	*
	* @param username the username
	* @throws SystemException if a system exception occurred
	*/
	public void removeByusername(java.lang.String username)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user cdvs where username = &#63;.
	*
	* @param username the username
	* @return the number of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countByusername(java.lang.String username)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user cdvs where email = &#63;.
	*
	* @param email the email
	* @return the matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByemail(
		java.lang.String email)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user cdvs where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @return the range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByemail(
		java.lang.String email, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user cdvs where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findByemail(
		java.lang.String email, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user cdv in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByemail_First(
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the first user cdv in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByemail_First(
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user cdv in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByemail_Last(
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the last user cdv in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByemail_Last(
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user cdvs before and after the current user cdv in the ordered set where email = &#63;.
	*
	* @param userCdvPK the primary key of the current user cdv
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv[] findByemail_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK,
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Removes all the user cdvs where email = &#63; from the database.
	*
	* @param email the email
	* @throws SystemException if a system exception occurred
	*/
	public void removeByemail(java.lang.String email)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user cdvs where email = &#63;.
	*
	* @param email the email
	* @return the number of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countByemail(java.lang.String email)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user cdvs where pilot = &#63;.
	*
	* @param pilot the pilot
	* @return the matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findBypilot(
		java.lang.String pilot)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user cdvs where pilot = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilot the pilot
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @return the range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findBypilot(
		java.lang.String pilot, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user cdvs where pilot = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilot the pilot
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findBypilot(
		java.lang.String pilot, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user cdv in the ordered set where pilot = &#63;.
	*
	* @param pilot the pilot
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findBypilot_First(
		java.lang.String pilot,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the first user cdv in the ordered set where pilot = &#63;.
	*
	* @param pilot the pilot
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchBypilot_First(
		java.lang.String pilot,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user cdv in the ordered set where pilot = &#63;.
	*
	* @param pilot the pilot
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findBypilot_Last(
		java.lang.String pilot,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the last user cdv in the ordered set where pilot = &#63;.
	*
	* @param pilot the pilot
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user cdv, or <code>null</code> if a matching user cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchBypilot_Last(
		java.lang.String pilot,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user cdvs before and after the current user cdv in the ordered set where pilot = &#63;.
	*
	* @param userCdvPK the primary key of the current user cdv
	* @param pilot the pilot
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv[] findBypilot_PrevAndNext(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK,
		java.lang.String pilot,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Removes all the user cdvs where pilot = &#63; from the database.
	*
	* @param pilot the pilot
	* @throws SystemException if a system exception occurred
	*/
	public void removeBypilot(java.lang.String pilot)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user cdvs where pilot = &#63;.
	*
	* @param pilot the pilot
	* @return the number of matching user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countBypilot(java.lang.String pilot)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the user cdv in the entity cache if it is enabled.
	*
	* @param userCdv the user cdv
	*/
	public void cacheResult(it.eng.rspa.cdv.datamodel.model.UserCdv userCdv);

	/**
	* Caches the user cdvs in the entity cache if it is enabled.
	*
	* @param userCdvs the user cdvs
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> userCdvs);

	/**
	* Creates a new user cdv with the primary key. Does not add the user cdv to the database.
	*
	* @param userCdvPK the primary key for the new user cdv
	* @return the new user cdv
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv create(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK);

	/**
	* Removes the user cdv with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userCdvPK the primary key of the user cdv
	* @return the user cdv that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv remove(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	public it.eng.rspa.cdv.datamodel.model.UserCdv updateImpl(
		it.eng.rspa.cdv.datamodel.model.UserCdv userCdv)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user cdv with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchUserCdvException} if it could not be found.
	*
	* @param userCdvPK the primary key of the user cdv
	* @return the user cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchUserCdvException if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv findByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchUserCdvException;

	/**
	* Returns the user cdv with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userCdvPK the primary key of the user cdv
	* @return the user cdv, or <code>null</code> if a user cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.UserCdv fetchByPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK userCdvPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user cdvs.
	*
	* @return the user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @return the range of user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.UserCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user cdvs
	* @param end the upper bound of the range of user cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.UserCdv> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user cdvs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user cdvs.
	*
	* @return the number of user cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}