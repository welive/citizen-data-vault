/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.cdv.datamodel.model.IdeaCdv;

/**
 * The persistence interface for the idea cdv service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering
 * @see IdeaCdvPersistenceImpl
 * @see IdeaCdvUtil
 * @generated
 */
public interface IdeaCdvPersistence extends BasePersistence<IdeaCdv> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IdeaCdvUtil} to access the idea cdv persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the idea cdvs where need = &#63;.
	*
	* @param need the need
	* @return the matching idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findByneed(
		boolean need)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the idea cdvs where need = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param need the need
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @return the range of matching idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findByneed(
		boolean need, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the idea cdvs where need = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param need the need
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findByneed(
		boolean need, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first idea cdv in the ordered set where need = &#63;.
	*
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a matching idea cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv findByneed_First(
		boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException;

	/**
	* Returns the first idea cdv in the ordered set where need = &#63;.
	*
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea cdv, or <code>null</code> if a matching idea cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv fetchByneed_First(
		boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last idea cdv in the ordered set where need = &#63;.
	*
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a matching idea cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv findByneed_Last(
		boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException;

	/**
	* Returns the last idea cdv in the ordered set where need = &#63;.
	*
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea cdv, or <code>null</code> if a matching idea cdv could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv fetchByneed_Last(
		boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the idea cdvs before and after the current idea cdv in the ordered set where need = &#63;.
	*
	* @param ideaId the primary key of the current idea cdv
	* @param need the need
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next idea cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv[] findByneed_PrevAndNext(
		long ideaId, boolean need,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException;

	/**
	* Removes all the idea cdvs where need = &#63; from the database.
	*
	* @param need the need
	* @throws SystemException if a system exception occurred
	*/
	public void removeByneed(boolean need)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of idea cdvs where need = &#63;.
	*
	* @param need the need
	* @return the number of matching idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countByneed(boolean need)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the idea cdv in the entity cache if it is enabled.
	*
	* @param ideaCdv the idea cdv
	*/
	public void cacheResult(it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv);

	/**
	* Caches the idea cdvs in the entity cache if it is enabled.
	*
	* @param ideaCdvs the idea cdvs
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> ideaCdvs);

	/**
	* Creates a new idea cdv with the primary key. Does not add the idea cdv to the database.
	*
	* @param ideaId the primary key for the new idea cdv
	* @return the new idea cdv
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv create(long ideaId);

	/**
	* Removes the idea cdv with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaId the primary key of the idea cdv
	* @return the idea cdv that was removed
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv remove(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException;

	public it.eng.rspa.cdv.datamodel.model.IdeaCdv updateImpl(
		it.eng.rspa.cdv.datamodel.model.IdeaCdv ideaCdv)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the idea cdv with the primary key or throws a {@link it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException} if it could not be found.
	*
	* @param ideaId the primary key of the idea cdv
	* @return the idea cdv
	* @throws it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv findByPrimaryKey(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException;

	/**
	* Returns the idea cdv with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ideaId the primary key of the idea cdv
	* @return the idea cdv, or <code>null</code> if a idea cdv with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.cdv.datamodel.model.IdeaCdv fetchByPrimaryKey(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the idea cdvs.
	*
	* @return the idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the idea cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @return the range of idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the idea cdvs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea cdvs
	* @param end the upper bound of the range of idea cdvs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.cdv.datamodel.model.IdeaCdv> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the idea cdvs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of idea cdvs.
	*
	* @return the number of idea cdvs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}