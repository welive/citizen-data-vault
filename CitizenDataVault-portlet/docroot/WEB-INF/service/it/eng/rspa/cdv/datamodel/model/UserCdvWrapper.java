/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UserCdv}.
 * </p>
 *
 * @author Engineering
 * @see UserCdv
 * @generated
 */
public class UserCdvWrapper implements UserCdv, ModelWrapper<UserCdv> {
	public UserCdvWrapper(UserCdv userCdv) {
		_userCdv = userCdv;
	}

	@Override
	public Class<?> getModelClass() {
		return UserCdv.class;
	}

	@Override
	public String getModelClassName() {
		return UserCdv.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ccuid", getCcuid());
		attributes.put("userId", getUserId());
		attributes.put("companyId", getCompanyId());
		attributes.put("username", getUsername());
		attributes.put("password", getPassword());
		attributes.put("name", getName());
		attributes.put("surname", getSurname());
		attributes.put("gender", getGender());
		attributes.put("birthdate", getBirthdate());
		attributes.put("address", getAddress());
		attributes.put("city", getCity());
		attributes.put("country", getCountry());
		attributes.put("zipcode", getZipcode());
		attributes.put("email", getEmail());
		attributes.put("isDeveloper", getIsDeveloper());
		attributes.put("lastKnownLatitude", getLastKnownLatitude());
		attributes.put("lastKnownLongitude", getLastKnownLongitude());
		attributes.put("pilot", getPilot());
		attributes.put("reputation", getReputation());
		attributes.put("employement", getEmployement());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ccuid = (Long)attributes.get("ccuid");

		if (ccuid != null) {
			setCcuid(ccuid);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String username = (String)attributes.get("username");

		if (username != null) {
			setUsername(username);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String surname = (String)attributes.get("surname");

		if (surname != null) {
			setSurname(surname);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		Date birthdate = (Date)attributes.get("birthdate");

		if (birthdate != null) {
			setBirthdate(birthdate);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		String city = (String)attributes.get("city");

		if (city != null) {
			setCity(city);
		}

		String country = (String)attributes.get("country");

		if (country != null) {
			setCountry(country);
		}

		String zipcode = (String)attributes.get("zipcode");

		if (zipcode != null) {
			setZipcode(zipcode);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		Boolean isDeveloper = (Boolean)attributes.get("isDeveloper");

		if (isDeveloper != null) {
			setIsDeveloper(isDeveloper);
		}

		Double lastKnownLatitude = (Double)attributes.get("lastKnownLatitude");

		if (lastKnownLatitude != null) {
			setLastKnownLatitude(lastKnownLatitude);
		}

		Double lastKnownLongitude = (Double)attributes.get("lastKnownLongitude");

		if (lastKnownLongitude != null) {
			setLastKnownLongitude(lastKnownLongitude);
		}

		String pilot = (String)attributes.get("pilot");

		if (pilot != null) {
			setPilot(pilot);
		}

		Long reputation = (Long)attributes.get("reputation");

		if (reputation != null) {
			setReputation(reputation);
		}

		String employement = (String)attributes.get("employement");

		if (employement != null) {
			setEmployement(employement);
		}
	}

	/**
	* Returns the primary key of this user cdv.
	*
	* @return the primary key of this user cdv
	*/
	@Override
	public it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK getPrimaryKey() {
		return _userCdv.getPrimaryKey();
	}

	/**
	* Sets the primary key of this user cdv.
	*
	* @param primaryKey the primary key of this user cdv
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK primaryKey) {
		_userCdv.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ccuid of this user cdv.
	*
	* @return the ccuid of this user cdv
	*/
	@Override
	public long getCcuid() {
		return _userCdv.getCcuid();
	}

	/**
	* Sets the ccuid of this user cdv.
	*
	* @param ccuid the ccuid of this user cdv
	*/
	@Override
	public void setCcuid(long ccuid) {
		_userCdv.setCcuid(ccuid);
	}

	/**
	* Returns the user ID of this user cdv.
	*
	* @return the user ID of this user cdv
	*/
	@Override
	public long getUserId() {
		return _userCdv.getUserId();
	}

	/**
	* Sets the user ID of this user cdv.
	*
	* @param userId the user ID of this user cdv
	*/
	@Override
	public void setUserId(long userId) {
		_userCdv.setUserId(userId);
	}

	/**
	* Returns the user uuid of this user cdv.
	*
	* @return the user uuid of this user cdv
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userCdv.getUserUuid();
	}

	/**
	* Sets the user uuid of this user cdv.
	*
	* @param userUuid the user uuid of this user cdv
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userCdv.setUserUuid(userUuid);
	}

	/**
	* Returns the company ID of this user cdv.
	*
	* @return the company ID of this user cdv
	*/
	@Override
	public long getCompanyId() {
		return _userCdv.getCompanyId();
	}

	/**
	* Sets the company ID of this user cdv.
	*
	* @param companyId the company ID of this user cdv
	*/
	@Override
	public void setCompanyId(long companyId) {
		_userCdv.setCompanyId(companyId);
	}

	/**
	* Returns the username of this user cdv.
	*
	* @return the username of this user cdv
	*/
	@Override
	public java.lang.String getUsername() {
		return _userCdv.getUsername();
	}

	/**
	* Sets the username of this user cdv.
	*
	* @param username the username of this user cdv
	*/
	@Override
	public void setUsername(java.lang.String username) {
		_userCdv.setUsername(username);
	}

	/**
	* Returns the password of this user cdv.
	*
	* @return the password of this user cdv
	*/
	@Override
	public java.lang.String getPassword() {
		return _userCdv.getPassword();
	}

	/**
	* Sets the password of this user cdv.
	*
	* @param password the password of this user cdv
	*/
	@Override
	public void setPassword(java.lang.String password) {
		_userCdv.setPassword(password);
	}

	/**
	* Returns the name of this user cdv.
	*
	* @return the name of this user cdv
	*/
	@Override
	public java.lang.String getName() {
		return _userCdv.getName();
	}

	/**
	* Sets the name of this user cdv.
	*
	* @param name the name of this user cdv
	*/
	@Override
	public void setName(java.lang.String name) {
		_userCdv.setName(name);
	}

	/**
	* Returns the surname of this user cdv.
	*
	* @return the surname of this user cdv
	*/
	@Override
	public java.lang.String getSurname() {
		return _userCdv.getSurname();
	}

	/**
	* Sets the surname of this user cdv.
	*
	* @param surname the surname of this user cdv
	*/
	@Override
	public void setSurname(java.lang.String surname) {
		_userCdv.setSurname(surname);
	}

	/**
	* Returns the gender of this user cdv.
	*
	* @return the gender of this user cdv
	*/
	@Override
	public java.lang.String getGender() {
		return _userCdv.getGender();
	}

	/**
	* Sets the gender of this user cdv.
	*
	* @param gender the gender of this user cdv
	*/
	@Override
	public void setGender(java.lang.String gender) {
		_userCdv.setGender(gender);
	}

	/**
	* Returns the birthdate of this user cdv.
	*
	* @return the birthdate of this user cdv
	*/
	@Override
	public java.util.Date getBirthdate() {
		return _userCdv.getBirthdate();
	}

	/**
	* Sets the birthdate of this user cdv.
	*
	* @param birthdate the birthdate of this user cdv
	*/
	@Override
	public void setBirthdate(java.util.Date birthdate) {
		_userCdv.setBirthdate(birthdate);
	}

	/**
	* Returns the address of this user cdv.
	*
	* @return the address of this user cdv
	*/
	@Override
	public java.lang.String getAddress() {
		return _userCdv.getAddress();
	}

	/**
	* Sets the address of this user cdv.
	*
	* @param address the address of this user cdv
	*/
	@Override
	public void setAddress(java.lang.String address) {
		_userCdv.setAddress(address);
	}

	/**
	* Returns the city of this user cdv.
	*
	* @return the city of this user cdv
	*/
	@Override
	public java.lang.String getCity() {
		return _userCdv.getCity();
	}

	/**
	* Sets the city of this user cdv.
	*
	* @param city the city of this user cdv
	*/
	@Override
	public void setCity(java.lang.String city) {
		_userCdv.setCity(city);
	}

	/**
	* Returns the country of this user cdv.
	*
	* @return the country of this user cdv
	*/
	@Override
	public java.lang.String getCountry() {
		return _userCdv.getCountry();
	}

	/**
	* Sets the country of this user cdv.
	*
	* @param country the country of this user cdv
	*/
	@Override
	public void setCountry(java.lang.String country) {
		_userCdv.setCountry(country);
	}

	/**
	* Returns the zipcode of this user cdv.
	*
	* @return the zipcode of this user cdv
	*/
	@Override
	public java.lang.String getZipcode() {
		return _userCdv.getZipcode();
	}

	/**
	* Sets the zipcode of this user cdv.
	*
	* @param zipcode the zipcode of this user cdv
	*/
	@Override
	public void setZipcode(java.lang.String zipcode) {
		_userCdv.setZipcode(zipcode);
	}

	/**
	* Returns the email of this user cdv.
	*
	* @return the email of this user cdv
	*/
	@Override
	public java.lang.String getEmail() {
		return _userCdv.getEmail();
	}

	/**
	* Sets the email of this user cdv.
	*
	* @param email the email of this user cdv
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_userCdv.setEmail(email);
	}

	/**
	* Returns the is developer of this user cdv.
	*
	* @return the is developer of this user cdv
	*/
	@Override
	public boolean getIsDeveloper() {
		return _userCdv.getIsDeveloper();
	}

	/**
	* Returns <code>true</code> if this user cdv is is developer.
	*
	* @return <code>true</code> if this user cdv is is developer; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsDeveloper() {
		return _userCdv.isIsDeveloper();
	}

	/**
	* Sets whether this user cdv is is developer.
	*
	* @param isDeveloper the is developer of this user cdv
	*/
	@Override
	public void setIsDeveloper(boolean isDeveloper) {
		_userCdv.setIsDeveloper(isDeveloper);
	}

	/**
	* Returns the last known latitude of this user cdv.
	*
	* @return the last known latitude of this user cdv
	*/
	@Override
	public double getLastKnownLatitude() {
		return _userCdv.getLastKnownLatitude();
	}

	/**
	* Sets the last known latitude of this user cdv.
	*
	* @param lastKnownLatitude the last known latitude of this user cdv
	*/
	@Override
	public void setLastKnownLatitude(double lastKnownLatitude) {
		_userCdv.setLastKnownLatitude(lastKnownLatitude);
	}

	/**
	* Returns the last known longitude of this user cdv.
	*
	* @return the last known longitude of this user cdv
	*/
	@Override
	public double getLastKnownLongitude() {
		return _userCdv.getLastKnownLongitude();
	}

	/**
	* Sets the last known longitude of this user cdv.
	*
	* @param lastKnownLongitude the last known longitude of this user cdv
	*/
	@Override
	public void setLastKnownLongitude(double lastKnownLongitude) {
		_userCdv.setLastKnownLongitude(lastKnownLongitude);
	}

	/**
	* Returns the pilot of this user cdv.
	*
	* @return the pilot of this user cdv
	*/
	@Override
	public java.lang.String getPilot() {
		return _userCdv.getPilot();
	}

	/**
	* Sets the pilot of this user cdv.
	*
	* @param pilot the pilot of this user cdv
	*/
	@Override
	public void setPilot(java.lang.String pilot) {
		_userCdv.setPilot(pilot);
	}

	/**
	* Returns the reputation of this user cdv.
	*
	* @return the reputation of this user cdv
	*/
	@Override
	public long getReputation() {
		return _userCdv.getReputation();
	}

	/**
	* Sets the reputation of this user cdv.
	*
	* @param reputation the reputation of this user cdv
	*/
	@Override
	public void setReputation(long reputation) {
		_userCdv.setReputation(reputation);
	}

	/**
	* Returns the employement of this user cdv.
	*
	* @return the employement of this user cdv
	*/
	@Override
	public java.lang.String getEmployement() {
		return _userCdv.getEmployement();
	}

	/**
	* Sets the employement of this user cdv.
	*
	* @param employement the employement of this user cdv
	*/
	@Override
	public void setEmployement(java.lang.String employement) {
		_userCdv.setEmployement(employement);
	}

	@Override
	public boolean isNew() {
		return _userCdv.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_userCdv.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _userCdv.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userCdv.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _userCdv.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _userCdv.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_userCdv.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _userCdv.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_userCdv.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_userCdv.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_userCdv.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UserCdvWrapper((UserCdv)_userCdv.clone());
	}

	@Override
	public int compareTo(it.eng.rspa.cdv.datamodel.model.UserCdv userCdv) {
		return _userCdv.compareTo(userCdv);
	}

	@Override
	public int hashCode() {
		return _userCdv.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.cdv.datamodel.model.UserCdv> toCacheModel() {
		return _userCdv.toCacheModel();
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv toEscapedModel() {
		return new UserCdvWrapper(_userCdv.toEscapedModel());
	}

	@Override
	public it.eng.rspa.cdv.datamodel.model.UserCdv toUnescapedModel() {
		return new UserCdvWrapper(_userCdv.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _userCdv.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userCdv.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_userCdv.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserCdvWrapper)) {
			return false;
		}

		UserCdvWrapper userCdvWrapper = (UserCdvWrapper)obj;

		if (Validator.equals(_userCdv, userCdvWrapper._userCdv)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UserCdv getWrappedUserCdv() {
		return _userCdv;
	}

	@Override
	public UserCdv getWrappedModel() {
		return _userCdv;
	}

	@Override
	public void resetOriginalValues() {
		_userCdv.resetOriginalValues();
	}

	private UserCdv _userCdv;
}