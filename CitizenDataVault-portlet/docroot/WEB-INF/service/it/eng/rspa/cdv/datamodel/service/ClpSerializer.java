/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.cdv.datamodel.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import it.eng.rspa.cdv.datamodel.model.ApplicationClp;
import it.eng.rspa.cdv.datamodel.model.CustomDataEntryClp;
import it.eng.rspa.cdv.datamodel.model.CustomDataTagClp;
import it.eng.rspa.cdv.datamodel.model.EndorsementClp;
import it.eng.rspa.cdv.datamodel.model.IdeaCdvClp;
import it.eng.rspa.cdv.datamodel.model.LanguageClp;
import it.eng.rspa.cdv.datamodel.model.PreferenceClp;
import it.eng.rspa.cdv.datamodel.model.SkillCdvClp;
import it.eng.rspa.cdv.datamodel.model.TagClp;
import it.eng.rspa.cdv.datamodel.model.ThirdPartiesProfileClp;
import it.eng.rspa.cdv.datamodel.model.ThirdPartyClp;
import it.eng.rspa.cdv.datamodel.model.UsedApplicationClp;
import it.eng.rspa.cdv.datamodel.model.UserCdvClp;
import it.eng.rspa.cdv.datamodel.model.UserIdeaClp;
import it.eng.rspa.cdv.datamodel.model.UserLanguageClp;
import it.eng.rspa.cdv.datamodel.model.UserPreferenceClp;
import it.eng.rspa.cdv.datamodel.model.UserSkillClp;
import it.eng.rspa.cdv.datamodel.model.UserThirdProfileClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Engineering
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"CitizenDataVault-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"CitizenDataVault-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "CitizenDataVault-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(ApplicationClp.class.getName())) {
			return translateInputApplication(oldModel);
		}

		if (oldModelClassName.equals(CustomDataEntryClp.class.getName())) {
			return translateInputCustomDataEntry(oldModel);
		}

		if (oldModelClassName.equals(CustomDataTagClp.class.getName())) {
			return translateInputCustomDataTag(oldModel);
		}

		if (oldModelClassName.equals(EndorsementClp.class.getName())) {
			return translateInputEndorsement(oldModel);
		}

		if (oldModelClassName.equals(IdeaCdvClp.class.getName())) {
			return translateInputIdeaCdv(oldModel);
		}

		if (oldModelClassName.equals(LanguageClp.class.getName())) {
			return translateInputLanguage(oldModel);
		}

		if (oldModelClassName.equals(PreferenceClp.class.getName())) {
			return translateInputPreference(oldModel);
		}

		if (oldModelClassName.equals(SkillCdvClp.class.getName())) {
			return translateInputSkillCdv(oldModel);
		}

		if (oldModelClassName.equals(TagClp.class.getName())) {
			return translateInputTag(oldModel);
		}

		if (oldModelClassName.equals(ThirdPartiesProfileClp.class.getName())) {
			return translateInputThirdPartiesProfile(oldModel);
		}

		if (oldModelClassName.equals(ThirdPartyClp.class.getName())) {
			return translateInputThirdParty(oldModel);
		}

		if (oldModelClassName.equals(UsedApplicationClp.class.getName())) {
			return translateInputUsedApplication(oldModel);
		}

		if (oldModelClassName.equals(UserCdvClp.class.getName())) {
			return translateInputUserCdv(oldModel);
		}

		if (oldModelClassName.equals(UserIdeaClp.class.getName())) {
			return translateInputUserIdea(oldModel);
		}

		if (oldModelClassName.equals(UserLanguageClp.class.getName())) {
			return translateInputUserLanguage(oldModel);
		}

		if (oldModelClassName.equals(UserPreferenceClp.class.getName())) {
			return translateInputUserPreference(oldModel);
		}

		if (oldModelClassName.equals(UserSkillClp.class.getName())) {
			return translateInputUserSkill(oldModel);
		}

		if (oldModelClassName.equals(UserThirdProfileClp.class.getName())) {
			return translateInputUserThirdProfile(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputApplication(BaseModel<?> oldModel) {
		ApplicationClp oldClpModel = (ApplicationClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getApplicationRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCustomDataEntry(BaseModel<?> oldModel) {
		CustomDataEntryClp oldClpModel = (CustomDataEntryClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCustomDataEntryRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCustomDataTag(BaseModel<?> oldModel) {
		CustomDataTagClp oldClpModel = (CustomDataTagClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCustomDataTagRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEndorsement(BaseModel<?> oldModel) {
		EndorsementClp oldClpModel = (EndorsementClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEndorsementRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputIdeaCdv(BaseModel<?> oldModel) {
		IdeaCdvClp oldClpModel = (IdeaCdvClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getIdeaCdvRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLanguage(BaseModel<?> oldModel) {
		LanguageClp oldClpModel = (LanguageClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLanguageRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPreference(BaseModel<?> oldModel) {
		PreferenceClp oldClpModel = (PreferenceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPreferenceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputSkillCdv(BaseModel<?> oldModel) {
		SkillCdvClp oldClpModel = (SkillCdvClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getSkillCdvRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTag(BaseModel<?> oldModel) {
		TagClp oldClpModel = (TagClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTagRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputThirdPartiesProfile(
		BaseModel<?> oldModel) {
		ThirdPartiesProfileClp oldClpModel = (ThirdPartiesProfileClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getThirdPartiesProfileRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputThirdParty(BaseModel<?> oldModel) {
		ThirdPartyClp oldClpModel = (ThirdPartyClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getThirdPartyRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUsedApplication(BaseModel<?> oldModel) {
		UsedApplicationClp oldClpModel = (UsedApplicationClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUsedApplicationRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUserCdv(BaseModel<?> oldModel) {
		UserCdvClp oldClpModel = (UserCdvClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUserCdvRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUserIdea(BaseModel<?> oldModel) {
		UserIdeaClp oldClpModel = (UserIdeaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUserIdeaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUserLanguage(BaseModel<?> oldModel) {
		UserLanguageClp oldClpModel = (UserLanguageClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUserLanguageRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUserPreference(BaseModel<?> oldModel) {
		UserPreferenceClp oldClpModel = (UserPreferenceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUserPreferenceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUserSkill(BaseModel<?> oldModel) {
		UserSkillClp oldClpModel = (UserSkillClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUserSkillRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUserThirdProfile(BaseModel<?> oldModel) {
		UserThirdProfileClp oldClpModel = (UserThirdProfileClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUserThirdProfileRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.ApplicationImpl")) {
			return translateOutputApplication(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.CustomDataEntryImpl")) {
			return translateOutputCustomDataEntry(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.CustomDataTagImpl")) {
			return translateOutputCustomDataTag(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.EndorsementImpl")) {
			return translateOutputEndorsement(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.IdeaCdvImpl")) {
			return translateOutputIdeaCdv(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.LanguageImpl")) {
			return translateOutputLanguage(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.PreferenceImpl")) {
			return translateOutputPreference(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.SkillCdvImpl")) {
			return translateOutputSkillCdv(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.TagImpl")) {
			return translateOutputTag(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.ThirdPartiesProfileImpl")) {
			return translateOutputThirdPartiesProfile(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.ThirdPartyImpl")) {
			return translateOutputThirdParty(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.UsedApplicationImpl")) {
			return translateOutputUsedApplication(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.UserCdvImpl")) {
			return translateOutputUserCdv(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.UserIdeaImpl")) {
			return translateOutputUserIdea(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.UserLanguageImpl")) {
			return translateOutputUserLanguage(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.UserPreferenceImpl")) {
			return translateOutputUserPreference(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.UserSkillImpl")) {
			return translateOutputUserSkill(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.cdv.datamodel.model.impl.UserThirdProfileImpl")) {
			return translateOutputUserThirdProfile(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchApplicationException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchApplicationException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchCustomDataEntryException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchCustomDataTagException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchEndorsementException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchEndorsementException();
		}

		if (className.equals("it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchIdeaCdvException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchLanguageException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchLanguageException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchPreferenceException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchPreferenceException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchSkillCdvException();
		}

		if (className.equals("it.eng.rspa.cdv.datamodel.NoSuchTagException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchTagException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchThirdPartiesProfileException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchThirdPartiesProfileException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchThirdPartyException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchUsedApplicationException();
		}

		if (className.equals("it.eng.rspa.cdv.datamodel.NoSuchUserCdvException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchUserCdvException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchUserIdeaException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchUserLanguageException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchUserPreferenceException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchUserSkillException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchUserSkillException();
		}

		if (className.equals(
					"it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException")) {
			return new it.eng.rspa.cdv.datamodel.NoSuchUserThirdProfileException();
		}

		return throwable;
	}

	public static Object translateOutputApplication(BaseModel<?> oldModel) {
		ApplicationClp newModel = new ApplicationClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setApplicationRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCustomDataEntry(BaseModel<?> oldModel) {
		CustomDataEntryClp newModel = new CustomDataEntryClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCustomDataEntryRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCustomDataTag(BaseModel<?> oldModel) {
		CustomDataTagClp newModel = new CustomDataTagClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCustomDataTagRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEndorsement(BaseModel<?> oldModel) {
		EndorsementClp newModel = new EndorsementClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEndorsementRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputIdeaCdv(BaseModel<?> oldModel) {
		IdeaCdvClp newModel = new IdeaCdvClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setIdeaCdvRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLanguage(BaseModel<?> oldModel) {
		LanguageClp newModel = new LanguageClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLanguageRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPreference(BaseModel<?> oldModel) {
		PreferenceClp newModel = new PreferenceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPreferenceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputSkillCdv(BaseModel<?> oldModel) {
		SkillCdvClp newModel = new SkillCdvClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setSkillCdvRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTag(BaseModel<?> oldModel) {
		TagClp newModel = new TagClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTagRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputThirdPartiesProfile(
		BaseModel<?> oldModel) {
		ThirdPartiesProfileClp newModel = new ThirdPartiesProfileClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setThirdPartiesProfileRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputThirdParty(BaseModel<?> oldModel) {
		ThirdPartyClp newModel = new ThirdPartyClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setThirdPartyRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUsedApplication(BaseModel<?> oldModel) {
		UsedApplicationClp newModel = new UsedApplicationClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUsedApplicationRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUserCdv(BaseModel<?> oldModel) {
		UserCdvClp newModel = new UserCdvClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUserCdvRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUserIdea(BaseModel<?> oldModel) {
		UserIdeaClp newModel = new UserIdeaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUserIdeaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUserLanguage(BaseModel<?> oldModel) {
		UserLanguageClp newModel = new UserLanguageClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUserLanguageRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUserPreference(BaseModel<?> oldModel) {
		UserPreferenceClp newModel = new UserPreferenceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUserPreferenceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUserSkill(BaseModel<?> oldModel) {
		UserSkillClp newModel = new UserSkillClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUserSkillRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUserThirdProfile(BaseModel<?> oldModel) {
		UserThirdProfileClp newModel = new UserThirdProfileClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUserThirdProfileRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}