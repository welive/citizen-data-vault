<%@ include file="/html/portlet/users_admin/init.jsp" %>

<h3><liferay-ui:message key="moreinfo"/></h3>
<portlet:resourceURL var="resurl"/>

<script>
$(document).on('change', '#checkisdeveloper' ,function(){
	console.log("checking");
	var self = $(this);
	var input = {};
		input['<portlet:namespace/>action']='updateuser';
		input['<portlet:namespace/>ccuid']='${ccuid}';
		input['<portlet:namespace/>uid']='${selUser.userId}';
		input['<portlet:namespace/>isdeveloper']=self.prop('checked');

	jQuery.post('<%=resurl%>', input)
		  .done(function(data){
			  try{
				  console.log(data);
				  var err = JSON.parse(data);
				  if(err.error != 0){
					  var errorDomNode = $("<div class='alert alert-error' style=''display:none;'>"+err.error+"</div>");
					  self.parent().append(errorDomNode);
					  errorDomNode.slideDown();
					  setTimeout(function(){
						  errorDomNode.slideUp();
					  }, 5000);
					  self.focus();  
				  }
				  else{
					  console.log("update complete");
				  }
			  }
			  catch(error){
				  console.log("update complete");
			  }
		  });
});
</script>
<div class="materialize">
	<div class="row">
		<div class="col s6">
			<label><liferay-ui:message key="ccuserid"/>:&nbsp;</label>${ccuid}
		</div>
		<div class="col s6">
			<label><liferay-ui:message key="userid"/>:&nbsp;</label>${userid}
		</div>
	</div>
	<div class="row">
		<div class="col s6">
			<label><liferay-ui:message key="developer"/>:&nbsp;</label>
			<div class="switch">
			    <label>
			      <liferay-ui:message key="no"/>
			      <c:choose>
					<c:when test="${developer}">
						<input checked id="checkisdeveloper" type="checkbox">
					</c:when>
					<c:otherwise>
						<input type="checkbox" id="checkisdeveloper">
					</c:otherwise>
				</c:choose>
			      <span class="lever"></span>
			      <liferay-ui:message key="yes"/> 
			    </label>
			</div>
		</div>
		<div class="col s6">
			<label><liferay-ui:message key="pilot"/>:&nbsp;</label>${pilot}
		</div>
	</div>
	<div class="row">
		<div class="col s6">
			<label><liferay-ui:message key="reputation"/>:&nbsp;</label>${reputation}
		</div>
		<c:if test="${not empty organization }">
			<div class="col s6">
				<label><liferay-ui:message key="organization"/>:&nbsp;</label>
				${organization}
				<c:if test="${isLeader}">
					<em>(<liferay-ui:message key="leader"/>)</em>
				</c:if>
			</div>
		</c:if>
	</div>
</div>