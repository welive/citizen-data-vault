<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@ include file="/html/portlet/users_admin/init.jsp" %>
<!-- jQuery -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Font awesome -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"/>

<!-- MATERIALIZE -->
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>

<portlet:resourceURL var="resurl"/>
	
<h3><liferay-ui:message key="authorizations"/>&nbsp;<liferay-ui:icon-help message="cdv-authorization-tooltip"/></h3>

<div class="materialize">
	<div id="noauth-msg" class="hide alert alert-info"><liferay-ui:message key="no-approved-authorization"/></div>
	<ul class="collection auths">
		<li class="collection-item avatar template hide">
	      <i class="fa fa-play circle red" aria-hidden="true"></i>
	      <span class="title"></span>
	      <a href="javascript:void(0);" class="secondary-content">
	      		<i class="removeauth"><liferay-ui:message key="revoke"/></i>
		  </a>
	    </li>
	</ul>
	
	<script>
		jQuery(document).ready(function(){
			
			var input = {};
				input['<portlet:namespace/>action']='getauth';
				input['<portlet:namespace/>ccuid']='${ccuid}';
		
			jQuery.post('<%=resurl%>', input)
				  .done(function(data){
					  try{
						var arr = JSON.parse(data);
							
						var appender = $(".auths").first();
						var template = appender.find('.template');
						
						if(arr.length == 0){
							$('#noauth-msg').removeClass('hide');
						}
						else{
							for(var i in arr){
								var item = template.clone();
								item.removeClass('template hide');
								
								item.find('.title').text(arr[i].clientName);
								item.find('.secondary-content').attr("data-clientid", arr[i].clientId);
								
								
								appender.append(item);
							}
						}
					  }
					  catch(error){
						  console.log(error);
					  }
					
				}).
				error(function(resp){
					console.log(resp);
				});
			
			});

			jQuery(document).on('click', '.collection-item .secondary-content', function(){
				console.log("removing authorization");
				var self = $(this);
				
				var clientid = self.attr('data-clientid');
				
				var input = {};
					input['<portlet:namespace/>action']='revokeauth';
					input['<portlet:namespace/>ccuid']='${ccuid}';
					input['<portlet:namespace/>clientid']=clientid;
			
				jQuery.post('<%=resurl%>', input)
					  .done(function(response){
						  self.closest('.collection-item').remove();
					  });
				return false;
			});
			
	</script>
</div>