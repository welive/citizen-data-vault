<%@ include file="/html/portlet/users_admin/init.jsp" %>

<h3><liferay-ui:message key="ims.ideas"/></h3>

<div class="materialize">
	<div class="row">
		<div class="col s12">
			<h4><liferay-ui:message key="authorships"/>&nbsp;<liferay-ui:icon-help message="cdv-ideas-authorships"/></h4>
			<c:choose>
				<c:when test="${empty authorships}">
					<div class="alert alert-info"><liferay-ui:message key="no-authorship"/></div>
				</c:when>
				<c:otherwise>
					<ul class="collection">
						<c:forEach items="${authorships}" var="entry">
						   <li class="collection-item avatar">
									<img src="/documents/10181/0/idea-logo/78f80153-0844-4e82-8aa1-2e893bec6c36?t=1470388941769" alt="" class="circle">
									<a class="title" target="_blank" href="/ideas_explorer/-/ideas_explorer_contest/${entry.key}/view">
										${entry.value}
									</a>
									<p><liferay-ui:message key="idea-id"/>:&nbsp;${entry.key}</p>
							</li>
						</c:forEach>
					</ul>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	
	<div class="row">
		<div class="col s12">
			<h4><liferay-ui:message key="collaborations"/>&nbsp;<liferay-ui:icon-help message="cdv-ideas-collaborations"/></h4>
			<c:choose>
				<c:when test="${empty collaborations}">
					<div class="alert alert-info"><liferay-ui:message key="no-collaboration"/></div>
				</c:when>
				<c:otherwise>
					<ul class="collection">
						<c:forEach items="${collaborations}" var="entry">
						   <li class="collection-item avatar">
									<img src="/documents/10181/0/idea-logo/78f80153-0844-4e82-8aa1-2e893bec6c36?t=1470388941769" alt="" class="circle">
									<a class="title" target="_blank" href="/ideas_explorer/-/ideas_explorer_contest/${entry.key}/view">
										${entry.value}
									</a>
									<p><liferay-ui:message key="idea-id"/>:&nbsp;${entry.key}</p>
							</li>
						</c:forEach>
					</ul>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>