<%@ include file="/html/portlet/users_admin/init.jsp" %>

<h3><liferay-ui:message key="used-apps"/>&nbsp;<liferay-ui:icon-help message="cdv-usedapps-tooltip"/></h3>

<div class="materialize">
	<c:choose>
		<c:when test="${empty apps}">
			<div class="alert alert-info"><liferay-ui:message key="no-app-installed"/></div>
		</c:when>
		<c:otherwise>
			<ul class="collection">
				<c:forEach items="${apps}" var="entry">
				   <li class="collection-item avatar">
						<i class="fa fa-mobile circle" aria-hidden="true"></i>
						<a class="title" target="_blank" href="/marketplace/-/marketplace/view/${entry.key}">${entry.value}</a>
						<p><liferay-ui:message key="app-id"/>:&nbsp;${entry.key}</p>
					</li>
				</c:forEach>
			</ul>
		</c:otherwise>
	</c:choose>
</div>