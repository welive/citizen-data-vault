<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/html/portlet/users_admin/init.jsp" %>

<liferay-ui:error-marker key="errorSection" value="details" />

<h3><liferay-ui:message key="details" /></h3>
<style>
	.materialize .row{
		margin-left: 0px;
	}
	.materialize .row .col label{
		font-weight:bold;
	}
	#return-message{
		margin-left: 10px;
	}
	#add-plus{
		cursor:pointer;
	}
</style>
<!-- Nino -->
	<div class="materialize" id="details_section">
		<%@ include file="/html/portlet/users_admin/user/cdv/general.jspf" %>
	</div>
<!-- End Nino -->