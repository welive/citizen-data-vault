<%@ include file="/html/portlet/users_admin/init.jsp" %>
<% User selUser = (User)renderRequest.getAttribute("selUser"); %>
<portlet:resourceURL var="resurl"/>

<style>
	.row .col.skillchip {
		    border-radius: 2px;
		    background-color: #fff;
		    border: 1px solid #ddd;
	    }
		.endorsementCounter{
			border-radius:2px;
			padding:3px;
		}
		.skillchip .skillname{
			padding: 3px 5px;
		}
		.badge.endorsementCounter:not(.new){
			border-radius:2px;
		}
</style>
<script>
var skillsnr = ${skills.size()};
var skills_list = new Array();
</script>

<div class="materialize" id="skill_section">

	<h3><liferay-ui:message key="known-languages"/></h3>
	<div class="row">
		<div id="langs" class="col s12">
			
			<c:forEach items="${availablelangs}" var="l">
				
				<c:set var="contains" value="false" />
				<c:forEach var="item" items="${langs}">
					<c:if test="${item eq l}">
						<c:set var="contains" value="true" />
					</c:if>
				</c:forEach>
				
				<p class="col s12 m4">
			      	<c:choose>
				      	<c:when test="${contains}">
				      		<input type="checkbox" value="${l}" class="filled-in checklang" id="filled-in-box-${l}" checked="checked"/>
				      	</c:when>
				      	<c:otherwise>
				      		<input type="checkbox" value="${l}" id="filled-in-box-${l}" class="filled-in checklang"/>
				      	</c:otherwise>
	      			</c:choose>
			      	<label for="filled-in-box-${l}"><liferay-ui:message key="${l}"/></label>
			    </p>
			    <c:if test="${contains}">
			    	<script>
				    	userlangs.push("${l}");
				    </script>
			    </c:if>
			    
		    </c:forEach>
    	</div>
    	<script>
    		jQuery(document).on('change', '.checklang', function(){
    			var self = $(this);
    			var lang = self.val();
    			
    			console.log(lang);
    			console.log(self.prop('checked'));
    			
    			if(self.prop('checked')){
    				userlangs.push(lang);
    			}
    			else{
    				for (var i=userlangs.length-1; i>=0; i--) {
    				    if (userlangs[i] === lang) {
    				    	userlangs.splice(i, 1);
    				    }
    				}
    			}
    			
    			var data = new Object();
    			data['<portlet:namespace/>action'] = 'updateuser';
    			data['<portlet:namespace/>ccuid']='${ccuid}';
    			data['<portlet:namespace/>uid']='${selUser.userId}';
    			data['<portlet:namespace/>languages'] = userlangs;
    			
				jQuery.post('<%=resurl%>', data)
						.done(function(response){
							console.log(response);
				});
    			
    		});
    	</script>
    	
	</div>

	<h3><liferay-ui:message key="skills"/></h3>
	<c:choose>
		<c:when test="${empty skills}">
			<div class="empty-msg alert alert-info"><liferay-ui:message key="no-skill"/></div>
		</c:when>
		<c:otherwise>
			<div class="empty-msg hide alert alert-info"><liferay-ui:message key="no-skill"/></div>
		</c:otherwise>
	</c:choose>
	
	<div class="collection" id="skills-list">
		<div class="template hide collection-item skillitem"> 
		  <span class="welive-blue white-text endorsementCounter"></span> 
		  <span class="skillname"></span> 
		  <span class="badge closeicon"><i class=" fa fa-times" aria-hidden="true"></i> </span>
		</div>
		<c:forEach items="${skills}" var="entry" end="9">
    		<div class="collection-item skillitem"> 
			  <span class="welive-blue white-text endorsementCounter">${entry.value}</span> 
			  <span class="skillname">${entry.key.skillname}</span> 
			  <span class="badge closeicon"><i class="fa fa-times" aria-hidden="true"></i> </span>
			</div>
			<script>
				skills_list.push("${entry.key}");
			</script>
    	</c:forEach>
   	</div>
	
	<div class="row" id="skills-chiplist">
		<div class="col template hide chip skillchip skillitem">
			<span class="welive-blue white-text endorsementCounter "></span>
			<span class="skillname"></span>
			<i class="closeicon fa fa-times" aria-hidden="true"></i>
		</div>
    	<c:forEach items="${skills}" var="entry" begin="10">
    		<div class="col chip skillchip skillitem">
				<span class="welive-blue white-text endorsementCounter">${entry.value}</span>
				<span class="skillname">${entry.key.skillname}</span>
				<i class="closeicon fa fa-times" aria-hidden="true"></i>
			</div>
			<script>
				skills_list.push("${entry.key}");
			</script>
    	</c:forEach>
	</div>
	
    <div class="row">
		<div class="input-field col s6">
			<i class="fa fa-plus-circle prefix" id="add-plus-skill" aria-hidden="true"></i>
			<input id="addskill" type="text" class="validate" length="75">
			<label for="addskill"><liferay-ui:message key="add-skill"/></label>
		</div>
	</div>

	<script>
		function addTag(){
			
			var skillContainer = jQuery('#addskill');
			
			var currskill = skillContainer.val().trim();
			var index = jQuery.inArray(currskill, skills_list);
			if(index>-1)
				return false;
			
			var appender = jQuery('#skills-list'); 
			if(skillsnr >= 10){
				appender = jQuery('#skills-chiplist');
			}
			
			var input = new Object();
				input['<portlet:namespace/>action'] = 'updateuser';
				input['<portlet:namespace/>ccuid']='${ccuid}';
				input['<portlet:namespace/>uid']='${selUser.userId}';
				input['<portlet:namespace/>newskill'] = currskill;

			jQuery.post('<%=resurl%>', input)
			.done(function(respo){
				var resp = JSON.parse(respo);
				console.log(resp);
				if(resp.error != 0)
					return;
				
				var template = appender.find('.template').first().clone();
				template.removeClass("hide").removeClass("template");
				template.find('.endorsementCounter').text('0');
				template.find('.skillname').text(currskill);
				
				appender.append(template);
				jQuery('.empty-msg').addClass('hide');
				
				skills_list.push(currskill);
				skillsnr++;
			});
			
			skillContainer.val("");
		}
		
		jQuery(document).on('click', '#add-plus-skill', function(){
			var self = $(this);
			var val = self.parent().find('#addskill').val().trim();
			
			if(val.length == 0 || val.length>=75)
				return false;
			
			addTag();
			return false;
			
		});
		
		jQuery(document).on('keypress', '#addskill', function(event){
			
			var self = $(this);
			var key = event.which || event.keyCode;
			
			var index = jQuery.inArray(String.fromCharCode(key), unsupportedChars);
			if(index>-1)
				return false;
			
			if(key == 44 || key == 13){
				self.parent().find('#add-plus-skill').click();
				return false;
			}
		});
		
		jQuery(document).on('click', '#skill_section .closeicon', function(){
			console.log(skills_list);
			var item = $(this).closest('.skillitem');
			var skillname = item.find('.skillname').text();
			
			var pos = skills_list.indexOf(skillname);
			if(pos>-1){
				skills_list.splice(pos, 1);
			}
			item.remove();
			
			var input = new Object();
				input['<portlet:namespace/>action'] = 'updateuser';
				input['<portlet:namespace/>ccuid']='${ccuid}';
				input['<portlet:namespace/>uid']='${selUser.userId}';
				input['<portlet:namespace/>removedskill'] = skillname;

			jQuery.post('<%=resurl%>', input)
			.done(function(respo){
				var resp = JSON.parse(respo);
			});

		})
	</script>	
	
</div>


<div class="materialize" id="preference-section">
	<hr/>
	<div class="row">
		<h3><liferay-ui:message key="preferences"/></h3>
		
		<c:choose>
			<c:when test="${empty preferences}">
				<div class="empty-msg alert alert-info"><liferay-ui:message key="no-preference"/></div>
			</c:when>
			<c:otherwise>
				<div class="empty-msg hide alert alert-info"><liferay-ui:message key="no-preference"/></div>
			</c:otherwise>
		</c:choose>
		
		<div id="prefcontainer" class="row">
			
			<div class="template hide chip prefitem">
				<span class="prefname"></span>
				<i class="closeicon fa fa-times" aria-hidden="true"></i>
			</div>
			
			<c:forEach items="${preferences}" var="entry">
				
				<div class="chip prefitem">
					<span class="prefname">${entry.value}</span>
					<i class="closeicon fa fa-times" aria-hidden="true"></i>
				</div>
				
				<script>
					prefs.push('${entry.value}');
				</script>
				
			</c:forEach>
		</div>
	
		<div class="row">
			<div class="input-field col s6">
				<i class="fa fa-plus-circle prefix" id="add-plus" aria-hidden="true"></i>
				<input id="addpref" type="text" class="validate" length="75">
				<label for="addpref"><liferay-ui:message key="add-preference"/></label>
			</div>
		</div>
	</div>
	
</div>

<script>
	function addPref(){
		var prefContainer = jQuery('#addpref');
		
		var v = prefContainer.val().trim();
		var index = jQuery.inArray(v, prefs);
		if(index>-1)
			return false;
		
		var tmp = prefs.slice();
		tmp.push(v);
		
		var data = new Object();
		data['<portlet:namespace/>action'] = 'updateuser';
		data['<portlet:namespace/>ccuid']='${ccuid}';
		data['<portlet:namespace/>uid']='${selUser.userId}';
		data['<portlet:namespace/>userTags'] = tmp;
		
		jQuery.post('<%=resurl%>', data).done(function(respo){
			var resp = JSON.parse(respo);
			if(resp.error != 0)
				return;
			
			var appender = jQuery('#prefcontainer'); 
			var template = appender.find('.template').first().clone();
			
			template.removeClass("hide").removeClass("template");
			template.find('.prefname').text(prefContainer.val());
			
			appender.append(template);
			jQuery('.empty-msg').addClass('hide');
			
			prefs.push(prefContainer.val());
			prefContainer.val("");		
		});
		data = new Object();
		
	}
	
	jQuery(document).on('click', '#add-plus', function(){
		var self = $(this);
		var val = self.parent().find('#addpref').val().trim();
		
		if(val.length == 0 || val.length>=75)
			return false;
		
		addPref();
		return false;
		
	});
	
	jQuery(document).on('keypress', '#addpref', function(event){
		
		var self = $(this);
		var key = event.which || event.keyCode;
		
		var index = jQuery.inArray(String.fromCharCode(key), unsupportedChars);
		if(index>-1)
			return false;
		
		if(key == 44 || key == 13){
			self.parent().find('#add-plus').click();
			return false;
		}
	});
	
	jQuery(document).on('click', '#preference-section .closeicon', function(){
		
		var wrapperitem = $(this).closest('.prefitem');
		var pref_item = wrapperitem.find('.prefname');
		var prefname = pref_item.text();
		
		var pos = prefs.indexOf(prefname);
		if(pos>-1){
			prefs.splice(pos, 1);
		}
		
		var data = new Object();
			data['<portlet:namespace/>action'] = 'updateuser';
			data['<portlet:namespace/>ccuid']='${ccuid}';
			data['<portlet:namespace/>uid']='${selUser.userId}';
			data['<portlet:namespace/>userTags'] = prefs;
			
		jQuery.post('<%=resurl%>', data)
			.done(function(respo){
				var resp = JSON.parse(respo);
				if(resp.error != 0){
					console.log(resp);
				}
				else{
					wrapperitem.remove();
					if(prefs.length==0)
						jQuery('#details_section .empty-msg').removeClass('hide');
				}
				
		});
	});
</script>