<%@ include file="/html/portlet/users_admin/init.jsp" %>
<!-- MATERIALIZE -->
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
	
	<!-- Google icons  -->
	<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
<!-- URLs -->
	<portlet:resourceURL var="resurl"/>
	
<!-- DOM -->
	<div class="materialize">
		<div class="row">
			<h3 class="top-margin"><liferay-ui:message key="export-profile"/></h3>
			<div class="divider"></div>
			<p style="font-size:0.9rem;"><em><liferay-ui:message key="export-profile-tooltip"/></em></p>
			
			<a id="exportprofile" href='#!' class="btn">
				<i class="material-icons">insert_drive_file</i>
				<liferay-ui:message key="generate-downloadable-file"/>
			</a>
			<br/>
			<a href="#!" target="_blank" class="hide" id="dltrigger"><liferay-ui:message key="download"/></a>
			
		</div>
		
		<c:if test="${not isLeader}">
			<br/>
			<br/>
			
			<div class="row">
				<h3 class="top-margin"><liferay-ui:message key="cdv.delete-account"/></h3>
				<div class="divider"></div>
				<p style="font-size:0.9rem;"><em><liferay-ui:message key="cdv.delete-account.tooltip"/></em></p>
				 <p>
			      <input type="checkbox" class="filled-in" id="deletecontentcheckbox" onchange="javascript:oncheckboxchange(this)"/>
			      <label for="deletecontentcheckbox"><liferay-ui:message key="cdv.delete-all-my-contents"/></label>
			    </p>
				<a href="javascript:void();" id="delete_trigger_btn" class="btn btn-primary welive-red">
					<liferay-ui:message key="cdv.delete-account"/>
				</a>
			</div>
			
			<script>
				$(document).on('click', '#delete_trigger_btn', function(){
					$('#delete_account_form').submit();
					return false;
				});
				
				function oncheckboxchange(e){
					console.log("click: "+$(e).prop('checked'));
					$('#delete_account_form').find('input[type="hidden"]').val($(e).prop('checked'));
				}
			
				function promptconfirm(){
					
					return confirm("<liferay-ui:message key='are-you-sure-you-want-to-delete-this'/>");
				}
			</script>
		
		</c:if>
	</div>
	
<!-- JS -->
	<script>
	
		$(document).on('click', '#exportprofile', function(){
			var self = $(this);
			var getuserprofileactionurl = $(this).attr('href');
			
			var input = {};
				input['<portlet:namespace/>action']='downloadprofile';
				input['<portlet:namespace/>uid']='${ccuid}';
			
			jQuery.post('<%=resurl%>', input)
					.done(function(response){
						console.log(response);
						
						//Create a file with the response text inside
						var blob;
						try { blob = new Blob([response], {type: 'text/octet-stream'}); } 
						catch (e) {
					    	var Bbuilder = window.BlobBuilder || window.WebKitBlobBuilder ||
	                    					window.MozBlobBuilder || window.MSBlobBuilder;
						
							var bb = new Bbuilder();
							bb.append(response);
							blob = bb.getBlob('text/octet-stream');
					    }
						
						//Update the trigger to download the generated file
						var a = $('a#dltrigger');
						
						window.URL = window.URL || window.webkitURL;
						a.attr('href', window.URL.createObjectURL(blob));
						a.attr('download', 'profile.json');
						a.removeClass("hide");
						
					});
			
			return false;
			
		});
		
	</script>