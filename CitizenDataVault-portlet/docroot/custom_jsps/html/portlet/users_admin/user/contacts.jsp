<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%-- <%@ include file="/html/portlet/users_admin/common/addresses.jsp" %> --%>
<%@ include file="/html/portlet/users_admin/init.jsp" %>

<style>
	.materialize .row{
		margin-left: 0px;
	}
	.materialize .row .col label{
		font-weight:bold;
	}
	#return-message{
		margin-left: 10px;
	}
</style>

<h3><liferay-ui:message key="contacts" /></h3>

<div class="materialize">
	<div class="row">
		<div class="col s12">
			<label><liferay-ui:message key="email"/>:&nbsp;</label>${email}
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<label><liferay-ui:message key="address"/>:&nbsp;</label>
			<input type="text"
				class="tobeedited"
				name="address"
				value="${address}"/>
		</div>
		<div class="col s4">
			<label><liferay-ui:message key="zipcode"/>:&nbsp;</label>
			<input type="text"
				class="tobeedited"
				name="zipCode"
				value="${zipcode}"/>
		</div>
		<div class="col s4">
			<label><liferay-ui:message key="city"/>:&nbsp;</label>
			<input type="text"
				class="tobeedited"
				name="city"
				value="${city}"/>
		</div>
		<div class="col s4">
			<label><liferay-ui:message key="country"/>:&nbsp;</label>
			<input type="text"
				class="tobeedited"
				name="country"
				value="${country}"/>
		</div>
	</div>
	
	<%@ include file="/html/portlet/users_admin/user/cdv/lkl.jspf" %>
	
</div>
<portlet:resourceURL var="resurl"/>
<script>

	$(document).on('change', '.tobeedited', function(){
		
		var input = {};
			input['<portlet:namespace/>action']='updateuser';
			input['<portlet:namespace/>ccuid']='${ccuid}';
			input['<portlet:namespace/>uid']='${selUser.userId}';
			
		$('.tobeedited').each(function(index){
			var e = $(this);
			input['<portlet:namespace/>'+e.attr('name')]=e.val();
		});
		
		jQuery.post('<%=resurl%>', input)
		.done(function(respo){
			var resp = JSON.parse(respo);
			console.log(resp);
		});
	});
</script>