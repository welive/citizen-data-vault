<%@page import="it.eng.rspa.cdv.utils.MyConstants"%>
<%@page import="it.eng.rspa.cdv.datamodel.model.Endorsement"%>
<%@page import="java.util.List"%>
<%@page import="it.eng.rspa.cdv.datamodel.service.EndorsementLocalServiceUtil"%>
<%@page import="it.eng.rspa.cdv.datamodel.model.SkillCdv"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="java.util.Set"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="it.eng.rspa.cdv.datamodel.service.persistence.UserCdvPK"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.model.Group"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="it.eng.rspa.cdv.utils.Utils"%>
<%@page import="java.util.Map"%>
<%@page import="it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil"%>
<%@page import="it.eng.rspa.cdv.datamodel.model.UserCdv"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<portlet:defineObjects />
<liferay-theme:defineObjects/>

<portlet:resourceURL var="resurl"/>

<style>
	.row .col.skillchip {
	    border-radius: 2px;
	    background-color: #fff;
	    border: 1px solid #ddd;
    }
	.endorsementCounter{
		border-radius:2px;
		padding:3px;
	}
	.skillchip .skillname{
		padding: 3px 5px;
	}
	.badge.endorsementCounter:not(.new){
		border-radius:2px;
	}
	#user_avatar{
		border-radius:3px;
		max-width:100%;
		max-height:200px;
		box-shadow: 0px 0px 5px;
	}
	.welive-green{
		background-color:#B6BD00 !important;
	}
	.welive-blue{
		background-color:#67B2E8 !important;
	}
	.box-area{
		box-shadow:0px 0px 6px #afafaf;
		border-radius:3px;
		padding: 5px 40px;
		min-height:485px;
	}
</style>

<%
User currentUser = PortalUtil.getUser(request);
UserCdv ucdv = null;
Group group = themeDisplay.getScopeGroup();
User user2 = UserLocalServiceUtil.getUserById(group.getClassPK());

String currUserURL = themeDisplay.getPortalURL()+"/web/"+currentUser.getScreenName()+"/so/profile";
String currUserFullName = HtmlUtil.escape(currentUser.getFullName());
String currUserAvatarUt = HtmlUtil.escape(currentUser.getPortraitURL(themeDisplay)).replace("amp;","");
long currUserId = currentUser.getUserId();

try{

	UserCdvPK upk = new UserCdvPK();
	upk.setUserId(user2.getUserId());
	upk.setCcuid(Long.parseLong(user2.getExpandoBridge().getAttribute("CCUserID").toString()));
	
	ucdv = UserCdvLocalServiceUtil.getUserCdv(upk);

	//User identifiers
	renderRequest.setAttribute("ccuid", ucdv.getCcuid());
	renderRequest.setAttribute("userid", ucdv.getUserId());//user of the page
	renderRequest.setAttribute("endorserLiferayUserId", currentUser.getUserId());//logged user
	
	//User main data
	renderRequest.setAttribute("username", ucdv.getUsername());
	renderRequest.setAttribute("name", ucdv.getName());
	renderRequest.setAttribute("surname", ucdv.getSurname());
	renderRequest.setAttribute("email", ucdv.getEmail());
	
	//Address
	renderRequest.setAttribute("address", ucdv.getAddress());
	renderRequest.setAttribute("zipcode", ucdv.getZipcode());
	renderRequest.setAttribute("city", ucdv.getCity());
	renderRequest.setAttribute("country", ucdv.getCountry());
	
	//Other metadata
	renderRequest.setAttribute("birthdate", ucdv.getBirthdate());
	renderRequest.setAttribute("gender", ucdv.getGender());
	
	renderRequest.setAttribute("isDeveloper", ucdv.getIsDeveloper());
	renderRequest.setAttribute("pilot", ucdv.getPilot());
	renderRequest.setAttribute("reputation", ucdv.getReputation());
	
	renderRequest.setAttribute("lklat", ucdv.getLastKnownLatitude());
	renderRequest.setAttribute("lklng", ucdv.getLastKnownLongitude());
	
	//Idea Authorship
	Map<Long, String> authorshipsMap = Utils.getAuthorshipsMap(ucdv.getUserId());
	renderRequest.setAttribute("authorships", authorshipsMap);
	
	//Idea collaborations
	Map<Long, String> collaborationsMap = Utils.getCollaboratiosMap(ucdv.getUserId());
	renderRequest.setAttribute("collaborations", collaborationsMap);
	
	//Used Applications
	Map<Long, String> appsMap = Utils.getAppsMap(ucdv.getUserId());
	renderRequest.setAttribute("apps", appsMap);
	
	//Skills
	Map<SkillCdv, Long> skillsMap = Utils.getSkillsMap(ucdv.getUserId());
	renderRequest.setAttribute("skills", skillsMap);
	
	//Languages
	Set<String> langs = Utils.getLanguages(ucdv.getUserId());
	renderRequest.setAttribute("langs", langs);
	
	//Preferences
	Map<Long,String> preferencesMap = Utils.getPreferencesMap(ucdv.getUserId());
	renderRequest.setAttribute("preferences", preferencesMap);
	
	renderRequest.setAttribute("citizen", Utils.hasRole(user2, PortletProps.get("role.citizen")));
	renderRequest.setAttribute("business", Utils.hasRole(user2, PortletProps.get("role.business")));
	renderRequest.setAttribute("developer", Utils.hasRole(user2, PortletProps.get("role.developer")));
	renderRequest.setAttribute("authority", Utils.hasRole(user2, PortletProps.get("role.authority")));
	renderRequest.setAttribute("academy", Utils.hasRole(user2, PortletProps.get("role.academy")));
	renderRequest.setAttribute("entrepreneur", Utils.hasRole(user2, PortletProps.get("role.entrepreneur")));
	renderRequest.setAttribute("isFriend", Utils.isFriend(currentUser.getUserId(), ucdv.getUserId()));
	renderRequest.setAttribute("isDifferentUser", Utils.isDifferentUser(currentUser.getUserId(), ucdv.getUserId()));
	
	
}
catch(Exception e){%>
	<div class="alert alert-error"><liferay-ui:message key="user-not-registered-with-cdv"/></div>
	<%return;
}
%>
<script>
var skillsnr = ${skills.size()};
var skills_list = new Array();
</script>

<div class="materialize">

<%@ include file="sections/endorsers_modal.jspf" %>

	<section id="general">
		<div class="row">
			<div class="col s12 m3 center-align">
				<img id="user_avatar" src="<%=user2.getPortraitURL(themeDisplay) %>"/>
			</div>
			<div class="col s12 m9" id="general_form" >
				<div class="row">
					<div class="col s6">
						<label><liferay-ui:message key="name"/>:&nbsp;</label>${name}&nbsp;${surname}&nbsp;(${username})
					</div>
					<div class="col s6">
						<label><liferay-ui:message key="reputation"/>:&nbsp;</label>${reputation}
					</div>
				</div>
				<div class="row">	
					<div class="col s6">
						<label><liferay-ui:message key="languages"/>:&nbsp;</label>
							<c:forEach items="${langs}" var="lang">
								${lang}&nbsp;
							</c:forEach>
					</div>
				</div>
				
				<div class="row" id="preferences">
					<div class="col s12">
						<label><liferay-ui:message key="preferences"/>:&nbsp;</label>
						<c:choose>
							<c:when test="${empty preferences}">
								<div class="empty-msg alert alert-info"><liferay-ui:message key="no-preference"/></div>
							</c:when>
							<c:otherwise>
								<div class="empty-msg hide alert alert-info"><liferay-ui:message key="no-preference"/></div>
							</c:otherwise>
						</c:choose>
						
						<div id="prefcontainer" class="row">
							<c:forEach items="${preferences}" var="entry">
								<div class="chip prefitem">
									<span class="prefname"><i class="fa fa-tag left" aria-hidden="true"></i><span class="right">${entry.value}</span></span>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<div class="row">
		<section id="ideas" class="col s12 m6">
			<div class="box-area">
				<c:set var="authorships_size" value="${fn:length(authorships)}"/>
				<c:set var="collaborations_size" value="${fn:length(collaborations)}"/>
				<h4><liferay-ui:message key="ims.ideas"/>&nbsp;(${authorships_size + collaborations_size})</h4>
				<hr/>
				<div class="row">
					<div class="col s12">
						<h6><liferay-ui:message key="authorships"/>&nbsp;(${authorships_size})</h6>
						<c:choose>
							<c:when test="${empty authorships}">
								<div class="alert alert-info"><liferay-ui:message key="no-authorship"/></div>
							</c:when>
							<c:otherwise>
								<ul class="collection">
									<c:forEach items="${authorships}" var="entry">
									   <li class="collection-item avatar">
											<img src="/documents/10181/0/idea-logo/78f80153-0844-4e82-8aa1-2e893bec6c36?t=1470388941769" alt="" class="circle">
											<span class="title">${entry.value}</span>
											<p><liferay-ui:message key="idea-id"/>:&nbsp;${entry.key}</p>
											
											<a target="_blank" href="/ideas_explorer/-/ideas_explorer_contest/${entry.key}/view" class="secondary-content">
												<i class="fa fa-info-circle" aria-hidden="true"></i>
											</a>
										</li>
									</c:forEach>
								</ul>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				
				<div class="row">
					<div class="col s12">
						<h6><liferay-ui:message key="collaborations"/>&nbsp;(${collaborations_size})</h6>
						<c:choose>
							<c:when test="${empty collaborations}">
								<div class="alert alert-info"><liferay-ui:message key="no-collaboration"/></div>
							</c:when>
							<c:otherwise>
								<ul class="collection">
									<c:forEach items="${collaborations}" var="entry">
									   <li class="collection-item avatar">
											<img src="/documents/10181/0/idea-logo/78f80153-0844-4e82-8aa1-2e893bec6c36?t=1470388941769" alt="" class="circle">
											<span class="title">${entry.value}</span>
											<p><liferay-ui:message key="idea-id"/>:&nbsp;${entry.key}</p>
											
											<a target="_blank" href="/ideas_explorer/-/ideas_explorer_contest/${entry.key}/view" class="secondary-content">
												<i class="fa fa-info-circle" aria-hidden="true"></i>
											</a>
										</li>
									</c:forEach>
								</ul>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</section>
		
		<section id="apps" class="col s12 m6">
			<div class="box-area"> 
				<h4><liferay-ui:message key="used-apps"/>&nbsp;(${fn:length(apps)})</h4>
				<hr/>
				<c:choose>
					<c:when test="${empty apps}">
						<div class="alert alert-info"><liferay-ui:message key="no-app-installed"/></div>
					</c:when>
					<c:otherwise>
						<ul class="collection">
							<c:forEach items="${apps}" var="entry">
							   <li class="collection-item avatar">
									<i class="fa fa-mobile circle" aria-hidden="true"></i>
									<span class="title">${entry.value}</span>
									<p><liferay-ui:message key="app-id"/>:&nbsp;${entry.key}</p>
									
									<a target="_blank" href="/marketplace/-/marketplace/view/${entry.key}" class="secondary-content">
										<i class="fa fa-info-circle" aria-hidden="true"></i>
									</a>
								</li>
							</c:forEach>
						</ul>
					</c:otherwise>
				</c:choose>
			</div>
		</section>
	</div>
		
	<div class="row">
		<section id="skills" class="col s12">
			<div class="box-area">
				<h4><liferay-ui:message key="skills"/>&nbsp; (<span id="totalSkills">${fn:length(skills)}</span>) <liferay-ui:icon-help message="endorsement.tip-friends"/></h4>
				<hr/>
				<div id="skill_section">
				
					<c:choose>
						<c:when test="${empty skills}">
							<div class="empty-msg alert alert-info"><liferay-ui:message key="cdv.no-skill"/></div>
						</c:when>
						<c:otherwise>
							<div class="empty-msg hide alert alert-info"><liferay-ui:message key="cdv.no-skill"/></div>
						</c:otherwise>
					</c:choose>
					

					
						<ul class="collection competenze" id="skills-list">
						
						
						  	<li class="template hide collection-item skillitem skillwrapper avatar" data-hasselfendorsement="false">
						      <span class="welive-blue white-text endorsementCounter endorsementModal pointer">0</span>
						      <span class="skillname"></span>
						      <div  class="secondary-content">
								<a class="linkAvatar" href="" target="_blank" >
											<img class="imgAvatar" alt="" title=""  src="" />
								</a>
						      	<i class="material-icons closeicon tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="cdv.click-to-remove-your-endorsement"/>">close</i>
						      </div>
						      
						      
						      
						      
						    </li>
						
							<c:forEach items="${skills}" var="entry" end="9">
							
								<%
								long skillId = ((Map.Entry<SkillCdv, Long>)pageContext.getAttribute("entry")).getKey().getSkillId();
								
				    			%>
							
								<li class="collection-item skillitem skillwrapper avatar" data-skillid="<%=skillId %>" data-hasselfendorsement="<%=Utils.hasSelfEndorsement(skillId, ucdv.getUserId()) %>" >
							      <span class="welive-blue white-text endorsementCounter pointer endorsementModal">${entry.value}</span>
							      <span class="skillname">${entry.key.skillname}</span>
							      <div class="secondary-content">
							       <%
									
									List<Endorsement> allendors  = EndorsementLocalServiceUtil.getEndorsementsBySkillIdAndUserIdNoSelf(skillId, ucdv.getUserId());
									
									int z = 0;
									String hide = "";
									for (Endorsement endorse : allendors){
										User endorser = UserLocalServiceUtil.getUser(endorse.getEndorserId());
										String userURL = themeDisplay.getPortalURL()+"/web/"+endorser.getScreenName()+"/so/profile";
										String avatarUt = HtmlUtil.escape(endorser.getPortraitURL(themeDisplay)).replace("amp;","");
										
										if (z>=MyConstants.MAX_N_OF_AVATAR_TO_SHOW)
											hide = "hide";
										 
									%>
									
										<a id="<%=skillId+"_"+endorse.getEndorserId() %>" class="linkAvatar <%=hide %>" href="<%=userURL %>" target="_blank" >
											<img class="imgAvatar" alt="<%=HtmlUtil.escape(endorser.getFullName())%>" title="<%=endorser.getFullName() %>"  src="<%=avatarUt %>" />
										</a>
										 
									<%
									z++;
									}
									
									if (z>MyConstants.MAX_N_OF_AVATAR_TO_SHOW){
									%>
										<i class="material-icons endorsementModal tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="view-all"/>">more</i>
									
									<%	}if (!Utils.isAlreadyEndorsed(skillId, ucdv.getUserId(), currentUser.getUserId() )){ %>
											<i class="material-icons endorseicon tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="cdv.click-to-add-your-endorsement"/>">plus_one</i>
										<%}if (Utils.isEndorsementRemovible(skillId, ucdv.getUserId(), currentUser.getUserId() )){ %>
											<i class="material-icons closeicon tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="cdv.click-to-remove-your-endorsement"/>">close</i>
										<%} %>
									
									</div>
							   </li>
							   <script>
								skills_list.push("${entry.key}");
							   </script>
							</c:forEach>
					    </ul>
				
					
					<div class="row" id="skills-chiplist">
						<div class="col template hide chip skillchip skillitem skillwrapper" data-hasselfendorsement="false">
							<span class="welive-blue white-text endorsementCounter endorsementModal pointer">0</span>
							<span class="skillname"></span>
							<div  class="secondary-content">
								<a class="linkAvatar hide" href="" target="_blank" >
											<img class="imgAvatar" alt="" title=""  src="" />
								</a>
						      	<i class="material-icons closeicon tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="cdv.click-to-remove-your-endorsement"/>">close</i>
						      </div>
							
							
						</div>
				    	<c:forEach items="${skills}" var="entry" begin="10">
				    	
				    	<%
				    	long currSkillId = ((Map.Entry<SkillCdv, Long>)pageContext.getAttribute("entry")).getKey().getSkillId();
				    	%>
				    	
				    		<div class="col chip skillchip skillitem skillwrapper"  data-hasselfendorsement="<%=Utils.hasSelfEndorsement(currSkillId, ucdv.getUserId()) %>" >
								<span class="welive-blue white-text endorsementCounter endorsementModal pointer">${entry.value}</span>
								<span class="skillname">${entry.key.skillname}</span>
								
						      <div class="secondary-content">
								       <%
										
										List<Endorsement> allendors  = EndorsementLocalServiceUtil.getEndorsementsBySkillIdAndUserIdNoSelf(currSkillId, ucdv.getUserId());
										for (Endorsement endorse : allendors){
											User endorser = UserLocalServiceUtil.getUser(endorse.getEndorserId());
											String userURL = themeDisplay.getPortalURL()+"/web/"+endorser.getScreenName()+"/so/profile";
											String avatarUt = HtmlUtil.escape(endorser.getPortraitURL(themeDisplay)).replace("amp;","");
										%>
											<a id="<%=currSkillId+"_"+endorse.getEndorserId() %>" class="linkAvatar hide" href="<%=userURL %>" target="_blank" >
												<img class="imgAvatar" alt="<%=HtmlUtil.escape(endorser.getFullName())%>" title="<%=endorser.getFullName() %>"  src="<%=avatarUt %>" />
											</a>
											 
										<%}   if (!Utils.isAlreadyEndorsed(currSkillId, ucdv.getUserId(), currentUser.getUserId() )){ %>
												<i class="material-icons endorseicon tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="cdv.click-to-add-your-endorsement"/>">plus_one</i>
											<%}if (Utils.isEndorsementRemovible(currSkillId, ucdv.getUserId(), currentUser.getUserId() )){ %>
												<i class="material-icons closeicon tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="cdv.click-to-remove-your-endorsement"/>">close</i>
											<%} %>
										
								</div>  		
								
							</div>
							<script>
								skills_list.push("${entry.key}");
							</script>
				    	</c:forEach>
					</div>
					
					<c:if test="${isFriend || !isDifferentUser }">
					
						<div class="row">
							<div class="input-field col s6">
								<i class="fa fa-plus-circle prefix" id="add-plus-skill" aria-hidden="true"></i>
								<input id="addskill" type="text" class="validate" length="75">
								<label for="addskill"><liferay-ui:message key="add-skill"/></label>
							</div>
						</div>
					
					
					</c:if>
					
					
				</div>
<script>
var unsupportedChars = "&,',@,\\,],},:,=,>,/,<,[,{,%,|,+,#,`,?,\",;,/,*,~".split(',');

function addTag(){
	
	var skillContainer = jQuery('#addskill');
	
	var currskill = skillContainer.val().trim();
	var index = jQuery.inArray(currskill, skills_list);
	
	/*avoid duplicates */
	if(index>-1) 
		return false;
	
	var appender = jQuery('#skills-list'); 
	if(skillsnr >= 10){
		appender = jQuery('#skills-chiplist');
	}
	
	var data = new PushDataItem('SkillsUpdated');
	data.entries.push(new PushEntry(currskill, 0))
	
	
	var tmp = {};
	tmp['<portlet:namespace/>action'] = 'SkillsUpdated';
	tmp['<portlet:namespace/>data'] = JSON.stringify(data);
	 
	 
	jQuery.post('<%=resurl%>', tmp)
	.done(function(resp){
		
		var respo = JSON.parse(resp);
		if(respo.error != 0)
			return;
		
		var template = appender.find('.template').first().clone();
		template.removeClass("hide").removeClass("template");
		
		template.find('.skillname').text(currskill);
		
		<% if (Utils.isDifferentUser(currentUser.getUserId(), ucdv.getUserId())){ %>
		
			template.find('.linkAvatar').attr("id","<%=currUserId%>");
			template.find('.linkAvatar').attr("href","<%=currUserURL%>");
			template.find('.imgAvatar').attr("alt","<%=currUserFullName%>");
			template.find('.imgAvatar').attr("title","<%=currUserFullName%>");
			template.find('.imgAvatar').attr("src","<%=currUserAvatarUt%>");
			template.find('.endorsementCounter').text('1');
		<%} %>
		
		
		
		appender.append(template);
		jQuery('.empty-msg').addClass('hide');
		skills_list.push(currskill);
		skillsnr++;
		
		$("#totalSkills").html(skillsnr);
		
		$('.tooltipped').tooltip({delay: 50}); /*initialize the tooltip */
		
		
	});
	
	skillContainer.val("");
}

/********************************************************************
* 						ADD PLUS SKILL
********************************************************************/
jQuery(document).on('click', '#add-plus-skill', function(){
	var self = $(this);
	var val = self.parent().find('#addskill').val().trim();
	
	if(val.length == 0 || val.length>=75)
		return false;
	
	addTag();
	return false;
	
});
/********************************************************************
* 						ADD SKILL
********************************************************************/
jQuery(document).on('keypress', '#addskill', function(event){
	
	var self = $(this);
	var key = event.which || event.keyCode;
	
	var index = jQuery.inArray(String.fromCharCode(key), unsupportedChars);
	if(index>-1)
		return false;
	
	if(key == 44 || key == 13){
		self.parent().find('#add-plus-skill').click();
		return false;
	}
});

/********************************************************************
* 						ENDORSEMENT MODAL
********************************************************************/
$(document).on('click', ".endorsementModal", function(el){
 	
	var item = $(this).closest('.skillitem');
	var skillname = item.find('.skillname').text();
	var skillCounter = item.find('.endorsementCounter').text();
	var skillnameTitle = "<liferay-ui:message key="endorsement.skill"/>: "+ skillname;
	var skillnameCounter = "<liferay-ui:message key="cdv.number-of-endorsement"/>: "+ skillCounter;
	
	var appender = $('#skillListFull');
	appender.html("");//clean
	
	$('.skillModalUser').text ("<%=ucdv.getName()%> <%=ucdv.getSurname()%>");/* title of the modal, user name */
	$('.skillModalTitle').text (skillnameTitle);/* title of the modal, skill name */
	$('.skillModalNumber').text (skillnameCounter);/* title of the modal, skill counter */
 	
	var secondaryContent = item.find('.secondary-content');
	var avatars = secondaryContent.find('.linkAvatar');
	

	jQuery.each(avatars, function(index, avatar) {
		
		var template =$('#avatarListTemplate').clone();
		template.removeClass("hide");
		template.removeAttr('id');
		
 		var currentHref = avatar.href;
 		var imgAvatar = avatar.firstElementChild;
 		
 		template.find('.hrefAvatar').attr('href',currentHref);
 		
		var currentName = imgAvatar.title;
		var currentSrc = imgAvatar.src;
		
		template.find('.avatarImg').attr('src',currentSrc);
		template.find('.avatarImg').attr('alt',currentName);
		template.find('.nameSurname').text(currentName);

		appender.append(template);
	});
	
	
	
	/* OPEN THE MODAL */
 	$('#modalEndorsers').openModal({
  				starting_top: '5%', /* Starting top style attribute*/
			    ending_top: '5%' /* Ending top style attribute*/
	});
 		
});





/********************************************************************
* 							REMOVE ENDORSEMENT
********************************************************************/
jQuery(document).on('click', '#skill_section .closeicon', function(){
	console.log(skills_list);
	var item = $(this).closest('.skillitem');
	var skillname = item.find('.skillname').text();
	var skillCounter = item.find('.endorsementCounter').text();
	
	var hasselfendorsement = item.data('hasselfendorsement');
	var skillid = item.data('skillid');
	
	var secondaryContent = item.find('.secondary-content');

	
	
	if ((skillCounter == 1 && hasselfendorsement == false)
		|| 	<%= !Utils.isDifferentUser(currentUser.getUserId(), ucdv.getUserId()) %>	
		){ //remove full row
	
		$('.tooltipped').tooltip('remove');
		item.remove();
		skillsnr--;
		$("#totalSkills").html(skillsnr);
		
		if (skillsnr == 0)
			$('.empty-msg').removeClass('hide');
		
	
	}else {

		$("#"+skillid+"_<%=currUserId%>" ).remove();
		var newSkillCounter = parseInt(skillCounter,10)-parseInt(1,10);
		item.find('.endorsementCounter').text(newSkillCounter);
		$('.tooltipped').tooltip('remove');
		item.find('.closeicon').remove();
		
		var plus = $(' <i class="material-icons endorseicon tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="cdv.click-to-add-your-endorsement"/>">plus_one</i>');
		secondaryContent.append(plus);
		
		$('.tooltipped').tooltip({delay: 50}); /*initialize the tooltip */
	}
		
	
	var pos = skills_list.indexOf(skillname);
	if(pos>-1){
		skills_list.splice(pos, 1);
	}
	
	console.log(skills_list);
	
	var datawrapper = new Array();
	var data = new PushDataItem('SkillsRemoved');
	data.entries.push(new PushEntry("skill_name", skillname));
	
	datawrapper.push(data);
	
	
	
	var tmp = {};
	tmp['<portlet:namespace/>action'] = 'SkillsRemoved';
	tmp['<portlet:namespace/>data'] = JSON.stringify(data);
	 
	jQuery.post('<%=resurl%>', tmp)
	.done(function(resp){
		console.log(resp);
	});
	
// 	postdata('POST', '/api/jsonws/CitizenDataVault-portlet.cdv/push', datawrapper, function(resp){
// 		console.log(resp);
// 	});
	
})

/********************************************************************
* 							ADD ENDORSEMENT
********************************************************************/
jQuery(document).on('click', '.endorseicon', function(){
						var self = $(this);
						var item = self.closest('.skillwrapper');
						var skillname = item.find('.skillname').text();
						var endorsements = item.find('.endorsementCounter').text();
						var newEndorse = parseInt(1,10)+parseInt(endorsements,10);
						var secondaryContent = item.find('.secondary-content');
						var skillid = item.data('skillid');
						
						var isChip = $( item ).hasClass( "skillchip" );
						
						if (isChip == false){
							var avatarEndorser = $('<a id="'+skillid+'<%="_"+currUserId %>" class="linkAvatar" href="<%=currUserURL %>" target="_blank" ><img class="imgAvatar" alt="<%=currUserFullName%>" title="<%=currUserFullName %>"  src="<%=currUserAvatarUt %>" /></a>');
						}else{
							var avatarEndorser = $('<a id="'+skillid+'<%="_"+currUserId %>" class="linkAvatar hide" href="<%=currUserURL %>" target="_blank" ><img class="imgAvatar" alt="<%=currUserFullName%>" title="<%=currUserFullName %>"  src="<%=currUserAvatarUt %>" /></a>');
						}
						
						
						secondaryContent.append(avatarEndorser);
						
						var datawrapper = new Array();
						var data = new PushDataItem('SkillsUpdated');
						data.entries.push(new PushEntry(skillname, newEndorse));
						
						datawrapper.push(data);
						
						var tmp = {};
						tmp['<portlet:namespace/>action'] = 'SkillsUpdated';
						tmp['<portlet:namespace/>data'] = JSON.stringify(data);
						 
						jQuery.post('<%=resurl%>', tmp)
						.done(function(resp){
							
							var respo = JSON.parse(resp);
							if(respo.error != 0)
								return;
						
							item.find('.endorsementCounter').text(newEndorse);
							$('.tooltipped').tooltip('remove');
							self.remove();
							var plus = $('<i class="material-icons closeicon tooltipped pointer" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="cdv.click-to-remove-your-endorsement"/>">close</i>');
							secondaryContent.append(plus);
							 $('.tooltipped').tooltip({delay: 50}); /*initialize the tooltip */
						});
});

/********************************************************************
* 							PUSH DATA
********************************************************************/
function PushDataItem(eventname) {
    this.ccUserID = ${ccuid};
    this.eventName = eventname;
    this.endorserLiferayUserId =  ${endorserLiferayUserId};
    this.entries = new Array();
}
				
function PushEntry(key, value){
	this.key = key;
	this.value = value;
}
				
function postdata(method, url, data, callback){
	jQuery.ajax(url, {
				    'data': JSON.stringify(data),
				    'type': method,
				    'processData': false,
				    'contentType': 'application/json'
					})
				.done(callback);
}		
</script>
			</div>
		</section>
	</div>
</div>


