<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%> 
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<portlet:defineObjects />

<liferay-portlet:actionURL portletConfiguration="true" name="init" var="initAction"/>
<liferay-ui:error key="no-role" message="No citizen role found" />

<% String currentUrl = PortalUtil.getCurrentURL(renderRequest); %>

<form action="<%=initAction%>" method="POST">
	<p>Initialize the CDV after the deploy</p>
	<button><liferay-ui:message key="start"/></button>
</form>