# README #

This project is a Liferay 6.2 portlet

### What is this repository for? ###

Citizen Data Vault (CDV)

## Requirements  
| Framework                                                                        |Version  |License             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|
|[Java SE Development Kit](https://www.java.com/it/download/help/index_installing.xml?j=7) |7.0 |[Oracle Binary Code License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)         |Also v.8|
|[Liferay Portal CE bundled with Tomcat](https://sourceforge.net/projects/lportal/files/Liferay%20Portal/6.2.3%20GA4/liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip/download) |6.2 GA4 | [GNU LGPL](https://www.liferay.com/it/downloads/ce-license)      ||
|[postgreSQL](https://www.postgresql.org/download/)| 9.3  |[PostgreSQL License](https://www.postgresql.org/about/licence/) ||
|[Social Office CE](https://web.liferay.com/it/community/liferay-projects/liferay-social-office/overview) | 3.0 | GNU LGPL |

## Libraries

CDV is based on the following software libraries and frameworks.

|Framework                           |Version       |Licence                                      |
|------------------------------------|--------------|---------------------------------------------|
|[gson](https://github.com/google/gson)| 2.2.4 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Jersey](https://jersey.java.net)| 1.8|[GNU LGPL](https://jersey.java.net/license.html) |
|[jQuery](https://jquery.org/) | 2.1.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[JSTL API](http://jcp.org/en/jsr/detail?id=52) |1.2 |[CDDL+GPL](https://glassfish.dev.java.net/public/CDDL+GPL.html) |
|[Materialize](http://materializecss.com)| 0.97.6 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Material Design Lite](https://github.com/google/material-design-lite)| 1.0.4|[MIT License](https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE) |

## Dependencies
|Library                             |Project       |License                                      |
|------------------------------------|--------------|---------------------------------------------|
|Challenge62-portlet-service.jar| welive-oia | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|LiferayUserManager-portlet-service.jar| welive-lum | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|



### How do I get set up? ###

For details about configuration/installation you can see "D2.16 – TOOLS AND COMPONENTS FOR PERSONALIZATION AND TRANSPARENCY v2"

Remember to set the username/password for the Basic Authentication on the file \CitizenDataVault-portlet\docroot\WEB-INF\src\portlet.properties

### Who do I talk to? ###

ENG team, Antonino Sirchia, Filippo Giuffrida

### Copying and License

This code is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)